def billetpreis(alter, km):
    if alter < 6:
        preis = 0
    elif alter < 16:
        preis = (2 + int(km)*0.25)/2
    else:
        preis = 2 + int(km)*0.25
    # print("Der Preis für Ihr Billet beträgt", preis, "CHF")
    return preis
    

language = "de"

alter = int(input("Alter? "))
distanz = int(input("Distanz? "))

if language == "de":
    print("Sie zahlen: ", billetpreis(alter, distanz))
elif language == "fr":
    pass
elif language == "it":
    pass
else:
    print("Amount payable", billetpreis(alter, distanz))
