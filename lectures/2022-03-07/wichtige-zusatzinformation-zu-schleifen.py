minimum = None
maximum = None
summe = 0
anzahl_zahlen = 0

# Neue Schlüsselwörter funktionieren bei jeder Schleifentype (while als auch für for)
# - break ... bricht eine Schleife ab
# - continue ... starte nächsten Schleifendurchlauf

while True:
    eingabe = input("Zahl? ")
    if eingabe == 'q':
        break
    
    # wenn keine zahl eingegeben wurde, starte den nächsten schleifendurchlauf
    if not eingabe.isdigit():
        continue
    
    zahl = int(eingabe)
    anzahl_zahlen = anzahl_zahlen + 1
    summe = summe + zahl
    if minimum == None or zahl < minimum:
        minimum = zahl
    if maximum == None or zahl > maximum:
        maximum = zahl
            

print("Maximum", maximum)
print("Minimum", minimum)

