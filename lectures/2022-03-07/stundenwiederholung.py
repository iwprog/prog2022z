# Stundenwiederholung vom 7. März 2022
# Guten Morgen :-)


x = 2
if x == 1:
  print("Eins")
elif x == 2:
  print("Zwei")
else:
  print("Anderer Wert")


# Schleife? (loop)
for count in range(10):
  print("Wiederholt", count)

print("Nicht mehr wiederholt.")

# gleiche mit while
i = 0
while i<10:
    print("Wiederholt", i)
    i = i + 1
