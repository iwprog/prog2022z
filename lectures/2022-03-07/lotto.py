from random import randint

# Variante 1
for count in range(1, 7):
    zufallszahl = randint(1,42)
    print(str(count)+"." , "Zufallszahl:" , zufallszahl)
    
# Variante 1a
for count in range(6):
    zufallszahl = randint(1,42)
    print(str(count + 1) + "." , "Zufallszahl:" , zufallszahl)

    
# Variante 2
count = 1
while count <=6:
    zufallszahl = randint(1, 42)
    print(str(count) + ". Zufallszahl: " + str(zufallszahl)) # texte zusammenfügen mit + oder komma
    count = count + 1
    
# scenario: for "besser" - Abarbeiten von Listen oder Dictionaries
# scenario: while "besser" - Arbeiten mit "Menüs" oder Schleifen deren Länge vom Benutzer abhängig ist.
