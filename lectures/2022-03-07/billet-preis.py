# Aufgabenstellung mit Funktion
#
# (1) der entsprechende Aufruf sollte funktionierten
# (2) die Funktion sollte das entsprechende Ergebnis auch zurückgeben.


def get_billet_preis(alter, distanz):
    if alter < 6:
        preis = 0
    elif alter < 16:
        preis = (2 + int(distanz)*0.25)/2
    else:
        preis = 2 + int(distanz)*0.25   
    return preis

# get_billet_preis(30, 200)

alter = int(input("Alter? "))
distanz = int(input("Distanz? "))

print("Sie zahlen: ", get_billet_preis(alter, distanz))
