from turtle import *

def dreieck():
    forward(50)
    left(120)
    forward(50)
    left(120)
    forward(50)
    penup()
    left(120)
    forward(120)
    pendown()

def kreis():
    circle(50)
    penup()
    forward(120)
    pendown()

def quadrat():
    forward(50)
    left(90)
    forward(50)
    left(90)
    forward(50)
    left(90)
    forward(50)
    penup()
    left(90)
    forward(120)
    pendown()

muster = input("Geben Sie ein Muster bestehend aus o, d und # ein: ")

for zeichen in muster:
    if zeichen == "o":
        kreis()
    elif zeichen == "#":
        quadrat()
    elif zeichen == "d":
        dreieck()
