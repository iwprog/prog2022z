from turtle import *
speed(8)
laenge=50
abstand=50
radius=50
winkel=360

def dreieck(laenge, abstand):
    for dseiten in range(3):
        forward(laenge)
        left(120)
    forward(laenge) 
    penup()
    forward(abstand)
    pendown()
    
def quadrat(laenge):
    for qseiten in range(4):
        forward(laenge)
        left(90)
    forward(laenge) 
    penup()
    forward(abstand)
    pendown()
    
def kreis(radius, winkel):
    circle(radius, winkel)
    penup()
    forward(abstand)
    pendown()

muster = textinput("Eingabefenster", "Geben Sie ein Muster mit den Zeichen #, d und o ein")
anzahl= int(len(muster))
print("dein Muster: ", muster)

count = 0
for i in muster:
    if count <= anzahl:
        if "d" == i:                      # == würde ebenfalls gehen und ist die bevorzugte Lösung
            dreieck(laenge, abstand)
            count = count + 1
        if "o" in i:
            kreis(radius, winkel)
            count = count + 1
        if "#" in i:
            quadrat(laenge)
            count = count + 1