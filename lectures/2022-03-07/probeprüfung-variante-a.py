from turtle import *
speed(8)
pensize(5)

for farbe in "red", "blue", "green", "yellow", "orange":
    pencolor(farbe)
    fd(100)
    lt(72)
    