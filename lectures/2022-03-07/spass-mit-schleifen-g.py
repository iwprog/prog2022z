n=0
m=12
print("# # # # # # #")
for i in range (5):
    print("          #"[n:m])
    n=n+2
print("# # # # # # #")

print("# " * 7)
for i in range(5, 0, -1):
    print(i * "  " + "#")
print("# " * 7)
