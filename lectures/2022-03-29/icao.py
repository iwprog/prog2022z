icao_alphabet = {"A": "Alfa", "B": "Bravo", "C": "Charlie", "D": "Delta", "E": "Echo", "F": "Foxtrot", "G": "Golf", "H": "Hotel", "I": "India", "J": "Juliett", "K": "Kilo", "L": "Lima", "M": "Mike", "N": "November", "O": "Oscar", "P": "Papa", "Q": "Quebex", "R": "Romeo", "S": "Sierra", "T": "Tango", "U": "Uniform", "V": "Victor", "W": "Whiskey", "X": "X-ray", "Y": "Yankee", "Z": "Zulu", }

wort = input("Bitte geben Sie ein Wort ein. ")

def icao(inputwort):
    liste = []
    inputwort = inputwort.replace("ä", "ae").replace("ö", "oe").replace("ü", "ue")
    for buchstabe in inputwort:
        buchstabe = buchstabe.upper()
        liste.append(icao_alphabet[buchstabe])
     
    print("-".join(liste))
    text = ""
    for element in liste:
        text = text + element + "-"
    print(text[:-1])

icao(wort)