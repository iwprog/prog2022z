datei = input("Welche Datei soll ich kopieren? ")

with open(datei, encoding='utf8') as fileHandle: # Datei lesen
    text = fileHandle.read()

with open('backup.txt', 'w', encoding='utf8') as backup: # Datei speichern
    backup.write(text)
    
print()