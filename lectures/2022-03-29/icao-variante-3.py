wort = input("Welches Wort soll ich buchstabieren: ")
# wegen print in buchstabieren: ich will upper/lower behalten:
wort_l= wort.lower()


umlaut = {"ü": "ue", "ä": "ae", "ö": "oe"}

icao = {"a": "Alpha", "b": "Bravo", "c": "Charlie","d": "Delta", "e": "Echo", "f": "Foxtrot", "g": "Golph","h": "Hotel",
           "i": "India", "j": "Juliett","k": "Kilo", "l": "Lima", "m": "Mike", "n": "November","o": "Oscar", "p": "Papa",
           "q": "Quebec", "r": "Romeo","s": "Sierra", "t": "Tango", "u": "Uniform", "v": "Victor","w": "Whiskey",
           "x": "X-Ray", "y": "Yankee","z": "Zulu"}    


for key, value in umlaut.items():
    wort_l = wort_l.replace(key, value)

# Liste von Buchstaben in wort = input


def buchstabieren():
    # Kann es buchstabiert werden?
    moglich = True
    for buchstabe in wort_l:
        if buchstabe not in icao.keys():
            moglich = False
            print("Kann " + wort + " nicht buchstabieren, da " + "'" + buchstabe + "'" + " nicht definiert wurde.")
            break
            
    # Es kann buchstabiert werden
    icao_liste = []
    text = ""
    if moglich == True:
        for buchstabe in wort_l:
            text = text + "-" + icao[buchstabe]
        return text[1::]

print(buchstabieren())
