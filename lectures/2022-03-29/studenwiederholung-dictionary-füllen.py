#
# Eingabe: Name, Alter
# - befüllt das Dictionary bis 'x' als Name eingegeben wird
#

alterseingabe = ''
namenseingabe = ''
dictionary = {}

while namenseingabe != 'x':
    namenseingabe = input('Geben Sie einen Namen ein: ')
    if namenseingabe == 'x':
        print('Dictionary:', dictionary)
        break
    alterseingabe = input('Geben Sie das Alter ein: ')
    dictionary[namenseingabe] = alterseingabe