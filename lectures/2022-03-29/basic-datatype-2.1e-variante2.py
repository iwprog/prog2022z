Lebensmittel = {}
print("Geben Sie bitte Lebensmittel und Preis oder 'x' für exit.")
while True:
    essen = input("Lebensmittel: ")
    if essen == "x":
        break
    else:
        preis = input("Preis: ")
        if preis == "x":
            Lebensmittel[essen] = None
            break

        try:
            val = float(preis)
            Lebensmittel[essen] = preis
        except ValueError:
            print("Nicht eine Nummer! Geben Sie bitte das Lebensmittel und Preis noch einmal ein.")

            # So kann man auch negative Nummer geben. Möglichkeit um ein Rabatt einzugeben oder so etwas?

print(Lebensmittel)

"""
12.374 -> 1.2374 E10"
"""
