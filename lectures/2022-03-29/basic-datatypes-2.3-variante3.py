# Dictionaries filtern
# 2.3 c

# Lösung ohne .item
preisliste1 = {'Brot': 3.2, 'Milch': 2.05, 'Orangen': 3.75, 'Tomaten': 2.2, 'Tee': 1.5, 'Peanuts': 3.9, 'Ketchup': 2.1}  
preisliste2 = {}   
 
 
# Lösung mit .item
preisliste3 = {}
for lebensmittel, preis in preisliste1.items():
# wenn in den Lebensmitteln 'en' nicht gefunden wird (-1)
    if lebensmittel.find('en') < 0:
# befülle die Lebensmittel in P3 mit preis von Schlaufe (p1)
        preisliste3[lebensmittel] = preis  
print(preisliste3)
