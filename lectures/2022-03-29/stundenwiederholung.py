#
# Stundenwiederholung vom 29. März 2022
#
name = {'Jonas': 32, 'Marco': 30, 'Alexandra': 28, 'Ana': 22}
name['Jonas'] = 33

name["Jonas"] = name["Jonas"] + 1
print(name['Jonas'])

print(name)

print(sorted(name.keys()))
print(sorted(name))
print(sorted(name.items()))
print(sorted(name.values()))

del name["Ana"]
print(name)

