from urllib.request import urlopen

city = input("Für welche Stadt wollen Sie die Temperatur erfragen? ")
with urlopen("https://meteo.search.ch/" + city) as f:
    content = f.read().decode("utf8")
    
    before, after = content.split("""meteo-c-temp-15">""")
    print(after[:10])
    temperatur, rest = after.split("°", 1)
    
    print("Die Temperatur in", city, "beträgt", temperatur, "Grad Celsius")
