preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75,"Tomaten":2.2,
              "Tee":4.2, "Peanuts":3.9, "Ketchup":2.1}

lebensmittel_in_preisliste = list(preisliste)
for item in lebensmittel_in_preisliste:
    if "en" in item:
        del preisliste[item]

print(preisliste)