#2.1 a
preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

#2.1 b
preisliste["Milch"]= 2.05

#2.1 c
del preisliste["Brot"]

#2.1 d
preisliste["Tee"] = 4.2
preisliste["Peanuts"]= 3.9
preisliste["Ketchup"] = 2.1

lebensmittel = ""
preisliste = {}
while lebensmittel != "x":
    lebensmittel = input("Lebensmittel: ")
    if lebensmittel != 'x':
        preis = input("Preis: ")
        preisliste[lebensmittel] = float(preis)
    
print(preisliste)
