preisliste = {'Milch': 2.05, 'Orangen': 3.75, 'Tomaten': 2.2, 'Tee': 4.2, 'Peanuts': 3.9, 'Ketchup': 2.1}

print("(a) - einfache Ausgabe")
for item in preisliste:
    print(item, "kostet", preisliste[item], "CHF")
    

print("(e) - Ausgabe mit Filtering")
preisliste = {'Milch': 1.05, 'Orangen': 3.75, 'Tomaten': 2.2, 'Tee': 4.2, 'Peanuts': 3.9, 'Ketchup': 2.1}
for item in preisliste:
    if preisliste[item] < 2.0:
        print(item, "kostet", preisliste[item], "CHF")
        
print("(e) - Ausgabe mit Filtering - Variante II")

for lebensmittel, preis in preisliste.items():
     if preis < 2:
        print(lebensmittel, "kostet", preis, "CHF.")
