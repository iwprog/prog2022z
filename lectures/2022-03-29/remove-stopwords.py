text = input("Geben Sie einen Text ein ")
text = text.replace(",", "").replace(".", "").replace("!", "").replace("?", "")
l_input= text.split()
stopwords = ["der", "die", "das", "in", "auf", "unter",
"ein", "eine", "ist", "war", "es", "den", "im", "und"]

def stopword_filter(textliste,stoppwortliste):
    l_output = []
    for wort in textliste:
        if not wort.lower() in stoppwortliste:
            l_output.append(wort)
    # l_output.sort()
    return l_output

print(l_input)
print(stopword_filter(l_input, stopwords))