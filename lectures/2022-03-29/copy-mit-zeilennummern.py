datei = "stundenwiederholung.py"
try:
    with open(datei, "r", encoding='utf8') as f:
        text = f.read()
        with open('testdokument_sicherheitskopie.txt', "w", encoding='utf8') as f2:
            for count, zeile in enumerate(text.split("\n"), start=1):
                f2.write(str(count) + " " + zeile + "\n")
except FileNotFoundError:
    print("Konnte die Datei", datei, "nicht öffnen...")
    