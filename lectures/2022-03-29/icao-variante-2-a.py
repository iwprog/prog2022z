# dicts 3 Icao - (a)
# Morena Sager


icao = {"A":"Alfa", "B":"Bravo", "C":"Charlie", "D":"Delta", "E":"Echo",
        "F":"Foxtrott", "G":"Golf", "H":"Hotel", "I":"India",
        "J":"Juliett", "K":"Kilo", "L":"Lima", "M":"Mike",
        "N":"November", "O":"Oscar", "P":"Papa", "Q":"Quebec",
        "R":"Romeo", "S":"Sierra", "T":"Tango", "U":"Uniform",
        "V":"Victor", "W":"Whiskey", "X":"X-Ray", "Y":"Yankee", "Z":"Zulu"}


# ausgabe = []
# wort=input("Welches Wort soll ich buchstabieren? ")

ausgabe = []
failed = False
wort=input("Welches Wort soll ich buchstabieren? ")
for buchstabe in wort.upper():
    if buchstabe in icao.keys():
        ausgabe.append(icao[buchstabe])
    else:
        print("Kann", wort, "nicht buchstabieren, da", buchstabe, "nicht definiert wurde")
        failed = True

if not failed:
    print("Ausgabe:", "-".join(ausgabe))