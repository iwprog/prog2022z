"""
Erweiterung: Sortieren Sie die Wort/Buchstabenliste nach der Häufigkeit (absteigend)
             bevor Sie diese ins CSV File schreiben.
"""

from urllib.request import urlopen
from csv import writer

ressource = input("Ressourcenbezeichnung: ")

if "https://" in ressource or "http" in ressource:
    with urlopen(ressource) as source:
        web_content = source.read().decode("latin1")
        worthaeufigkeit = {}
        buchstabenhaeufigkeit = {}
        for wort in web_content.split():
            if wort in worthaeufigkeit:
                worthaeufigkeit[wort] = worthaeufigkeit[wort] + 1
            else:
                worthaeufigkeit[wort] = 1
        for buchstabe in list(web_content):
            if buchstabe in buchstabenhaeufigkeit:
                buchstabenhaeufigkeit[buchstabe] = buchstabenhaeufigkeit[buchstabe] + 1
            else:
                buchstabenhaeufigkeit[buchstabe] = 1
else:
    try:
        with open(ressource, encoding="utf8") as datei:
            datei_inhalt = datei.read()
            worthaeufigkeit = {}
            buchstabenhaeufigkeit = {}
            for wort in datei_inhalt.split():
                if wort in worthaeufigkeit:
                    worthaeufigkeit[wort] = worthaeufigkeit[wort] + 1
                else:
                    worthaeufigkeit[wort] = 1
            for buchstabe in list(datei_inhalt):
                if buchstabe in buchstabenhaeufigkeit:
                    buchstabenhaeufigkeit[buchstabe] = buchstabenhaeufigkeit[buchstabe] + 1
                else:
                    buchstabenhaeufigkeit[buchstabe] = 1
    except FileNotFoundError:
        print(f"Keine Datei mit Name {ressource} gefunden.")
        worthaeufigkeit = {}
        buchstabenhaeufigkeit = {}

"""
Erweiterung: Sortieren Sie die Wort/Buchstabenliste nach der Häufigkeit (absteigend)
             bevor Sie diese ins CSV File schreiben.
"""
# Variante 1 - Konventionelle Methode - Sortieren mittels Hilfsliste
with open("wort.csv", "w", newline="\n", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    sortierliste_w = []
    for wort, haeufigkeit in worthaeufigkeit.items():
        sortierliste_w.append([haeufigkeit, wort])
    sortierliste_w.sort()
    sortierliste_w.reverse()
    for häufigkeit, wort in sortierliste_w:
        csv_writer.writerow([wort, häufigkeit])

def sortierfunktion(wert):
    print(wert)
    return wert[1], wert[0]

with open("buchstabe.csv", "w", newline="\n", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    for buchstabe, haeufigkeit in sorted(buchstabenhaeufigkeit.items(), key=sortierfunktion,
                                         reverse=True):
        csv_writer.writerow([buchstabe, haeufigkeit])
