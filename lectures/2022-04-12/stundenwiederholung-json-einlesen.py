"""
Lesen Sie den JSON String vom vorhergenden Beispiel ein und geben Sie für alle
Städte die Temperatur + Luftfeuchtigkeit aus.

 Chur: 23.4 °C, Luftfeuchtigkeit: 34%
 ...
"""

from json import loads

with open('wetter.json', encoding = 'utf8') as f:
  json_string = f.read()
  wetter = loads(json_string)
  for stadt, details in wetter.items():
      # Ausgabe: Variante 1
      temperatur = details["temp"]
      luftfeuchtigkeit = details["hum"]
      print(stadt +':', str(temperatur) + '°C', str(luftfeuchtigkeit) + '%')
      print(stadt, details["wind"]["speed"], details["wind"]["direction"])
      
      # Ausgabe: Variante 1a
      print(f"{stadt}: {temperatur} °C, Luftfeuchtigkeit: {luftfeuchtigkeit}%")
      
  for stadt in wetter:
       print(stadt, wetter[stadt]["temp"], wetter[stadt]["hum"])
       print(stadt, wetter[stadt]["wind"]["speed"], wetter[stadt]["wind"]["direction"])

  for stadt, wetterinfo in wetter.items():
      print("Wetterinformation für Stadt:", stadt)
      for key, value in wetterinfo.items():
          print(key, ":", value)
      