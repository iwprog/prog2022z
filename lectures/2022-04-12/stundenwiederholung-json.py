from json import loads, dumps

wetter = {'chur': {'temp': 23.4,
                   'hum': 34,
                   'wind': {'direction': 'S',
                            'speed': 34}
                   },
          'zürich': {'temp': 19.4,
                     'hum': 45,
                     'wind': {'direction': 'NO',
                              'speed': 45}
                     }
          }




with open("wetter.json", "w", encoding="utf8") as f:
  json_string = dumps(wetter)
  f.write(json_string)
