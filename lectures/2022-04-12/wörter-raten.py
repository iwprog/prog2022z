"""
Schreiben Sie ein Programm, welches aus einer Liste von Worten, zufällig eines
auswählt und bei diesem die Vokale nach folgendem Muster vertauscht:

 a > e
 e > i
 i > o
 o > u
 u > a
 
Beispiel:
  Birne > Borni
  Apfel > Epfil
  
Der/die Benutzer(in) erhält im Anschluss das veränderte Wort und muss das
ursprüngliche Wort erraten.
"""
from random import randint, choice

austauschen = {'a':'e','e':'i','i':'o','o':'u','u':'a'}
zufallsliste = ['apfel','birne','ei','kirsche','banane','kiwi','aprikose','spinat']

# Zufälliges Wort auswählen
anzahl_worte_in_liste = len(zufallsliste)
kleinster_wert = 0
höchster_wert = anzahl_worte_in_liste - 1
zufalls_zahl = randint(kleinster_wert, höchster_wert)
zufalls_wort = zufallsliste[zufalls_zahl]

# alternative:
zufalls_wort = choice(zufallsliste)

# Umlaute ersetzen
neues_wort = ''
for buchstabe in zufalls_wort:
    if buchstabe in austauschen.keys():
        neues_wort = neues_wort + austauschen[buchstabe]
    else:
        neues_wort = neues_wort + buchstabe
        
eingabe = input('Welches Wort verbirgt sich hinter: ' + neues_wort + "? ")
if eingabe == zufalls_wort:
    print("Richtig!!!")
else:
    print("Leider nicht richtig - die Lösung lautet:", zufalls_wort)
    
