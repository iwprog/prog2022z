from urllib.request import urlopen
from csv import writer
from csv import reader

buchstabenliste=[]
woerterliste=[]

eingabe_url=input("Geben Sie eine URL ein: ")
eingabe_wort=input("Geben Sie ein Wort ein; Anzahl des in der angegebener URL wird gefundene Wort, wird angegeben.")
eingabe_buchstabe=input("Gib einen Buchstaben ein: ")
with urlopen(eingabe_url) as f:
    content = f.read().decode("latin1") #da stimmt etwas nicht...           
    with open("buchstabe.csv","a", newline ="\n", encoding="utf8") as file:
        csv_writer= writer(file, delimiter=";")
        buchstabenliste.append([eingabe_buchstabe,(content.count(eingabe_buchstabe.upper())+content.count(eingabe_buchstabe.lower()))])
        for buchstaben in buchstabenliste:
            if buchstaben == []:
                continue
            #problem es gibt immer eine leere zeile, auch noch beim angeben, dass ignorieren soll. 
            csv_writer.writerow(buchstaben)

    with open("wort.csv", "a", newline ="\n", encoding="utf8") as f:
        csv_writer_2= writer(f, delimiter=";")
        csv_writer_2.writerow([eingabe_wort, (content.count(eingabe_wort.upper())+content.count(eingabe_wort.lower()))])
                
print(eingabe_wort, ":", content.count(eingabe_wort))
print(eingabe_buchstabe, ":", content.count(eingabe_buchstabe))
