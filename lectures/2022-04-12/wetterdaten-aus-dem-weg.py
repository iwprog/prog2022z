from urllib.request import urlopen
from inscriptis import get_text

with urlopen("https://meteo.search.ch/chur") as f:
    content = f.read().decode("utf8")
    text = get_text(content)
    
    # extrahieren der temperatur
    text_vorher, text_danach = text.split("Temperatur   ")
    temperatur, text_danach = text_danach.split("°C")
    print("Temperatur:", temperatur.strip())
    
    # extrahieren ist die luftfeuchtigkeit
    text_vorher, text_danach = text.split("Relative Luftfeuchtigkeit  ")
    humidity, text_danach = text_danach.split("%", 1)
    print("Luftfeuchtigkeit:", humidity.strip())