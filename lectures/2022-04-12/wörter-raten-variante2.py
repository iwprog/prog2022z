from random import *

wortliste = ["Apfel", "Birne", "Ananas", "Baum"]
tausch = {"a": "e",
          "e":"i",
          "i":"o",
          "o":"u",
          "u":"a",
          }

zufallswort = choice(wortliste)

neues_wort = []
for buchstabe in zufallswort:
        if buchstabe.lower() in tausch.keys():
            neues_wort.append(tausch[buchstabe.lower()])
        else:
            neues_wort.append(buchstabe)

neues_wort = "".join(neues_wort)

eingabe = input("Erraten Sie das ursprüngliche Wort:" + neues_wort)

