"""
Stundenwiederholung vom 12. April 2022
"""

telefonbuch = [
  ["Ana", "+41 81 299 12 12"],
  ["Jim", "+41 79 212 23 12"],
  ["Zoe", "+41 76 234 12 98"]
]

from csv import writer

with open("daten.csv", "w", newline="\n", encoding="utf8") as f:
  csv_writer = writer(f, delimiter=";")
  """ Variante 1 """
  for name, telefon in telefonbuch:
      csv_writer.writerow([name, telefon])
      
  """ Variante 2 """
  for row in telefonbuch:
      csv_writer.writerow(row)
