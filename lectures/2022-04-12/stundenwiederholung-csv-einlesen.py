"""
Lesen Sie "daten.csv" ein und geben Sie alle Einträge mit Telefonnummer aus.

Bsp:
   Ana: +41 ....
"""
from csv import reader

with open("daten.csv", encoding="utf8") as f: 
  telefonbuch = reader(f, delimiter=";")
  for name, nr in telefonbuch: 
    print(name, ":", nr)
    print(f"{name}: {nr}")
  

with open("daten.csv", encoding="utf8") as f: 
  telefonbuch = reader(f, delimiter=";")
  for row in telefonbuch:
      print(row)
      print(row[0], ":", row[1])
