text = input("Bitte geben Sie einen beliebigen Text ein: ")
zeichen = input("Nach welchem Zeichen soll gesucht werden? ")
positionen=[]

for position, element in enumerate(text.lower()):
    if element == zeichen.lower():
        positionen.append(str(position))
print("Ausgabe:", str(positionen)[1:-1].replace(",", ""))


# alternative möglichkeit
print("Ausgabe:", " ".join(positionen))


