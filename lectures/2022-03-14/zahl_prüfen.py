def zahl_prüfen(eingabe):
    for ziffer in eingabe:
        # prüfung ob die ziffer in ordnung ist
        if not ziffer.isdigit() and ziffer != '.':
            return False
    
    if eingabe.count(".") > 1:
        return False
    return True
        
    
    
print(zahl_prüfen("12"))
print(zahl_prüfen("12.22"))
print(zahl_prüfen("12.22abc"))
print(zahl_prüfen("12.22.22"))