text="Ananas ist eine exotische Frucht"
#     0123456789....
position=6                #es sind Positionen von 0 bis 32 möglich
einfügetext="saft"

def replace(text, position, einfügetext):
    print("text: ", text)
    print("position: ", position)
    print("einfügetext: ", einfügetext)
    print("Ergebnis: ", text[:position] + einfügetext + text[position:])

replace(text, position, einfügetext)

