jahreszeiten = ['frühling', 'sommer', 'herbst', 'winter']
print(jahreszeiten)
#b). Löschen Sie die Jahreszeit Frühling aus der Liste.
del jahreszeiten[0]
# alternative: jahreszeiten.remove("frühling")
print(jahreszeiten)

#c. Fügen Sie die Jahreszeit Langas der Liste hinzu.
jahreszeiten.append('Langas')
print(jahreszeiten)

#d. Schreiben Sie ein Programm, welches Sie nach Namen fragt und diese solange einer Liste hinzufügt, bis der Benutzer x eingibt. Im Anschluss soll die Liste ausgegeben werden.
#Name: Martin
#Name: Julia
#Name: x
#Liste: ["Martin", "Julia]
namen=[]
while True:
    eingabe = input('Name? ')
    if eingabe == 'x':
        break
    else:
        namen.append(eingabe)

print('Liste: '+ str(namen))