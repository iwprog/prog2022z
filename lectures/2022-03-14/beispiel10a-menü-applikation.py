def menue():
  print("====================")
  print("Programmübersicht:")
  print(1, "...", "Preis für eine Fahrkarte berechnen")
  print(2, "...", "Herzfrequenz berechnen")
  print("")
  print(0, "...", "Programm beenden")
  print("====================")
  
def get_billet_preis():
  alter = int(input("Alter:"))
  km = int(input("Km angeben:"))
  if alter < 6:
    preis = 0
  elif alter < 16:
    preis = ((2 + (0.25*km)) / 2)
  else:
    preis =  (2 + (0.25*km))
  return preis
  
def herzfrequenz():
  alter = input("Was ist ihr Alter?")
  herzfrequenz = 220 - int(alter)
  return herzfrequenz
 
while True:
  menue()
  eingabe = input("Gewählte Option")
  if eingabe == "2":
    print()
    print("Maximale Herfrequenz berechenen")
    print("--------------------------------------")
    print("Ihre Maximale Herzfrequenz beträgt:",herzfrequenz())
    print()
  elif eingabe == "1":
    print()
    print("Preis für Fahrkarte berechnen")
    print("--------------------------------------")
    print("Preis:", get_billet_preis())
  elif eingabe == "0":
    break
  else:
    print("Ungültige Funktion")