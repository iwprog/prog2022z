eingabe = ""
count = 1
betrag = 0
while eingabe != "x":
    eingabe = input("Geben Sie den "+ str(count)+". Preis ein: ") #Bei input darf ich keine Kommas verwenden, um die Variable mit den STrings zu verknüpfen
    count = count + 1
    if eingabe.isdigit():#damit "x" keine Fehlermeldung bei nachfolgender REchnung verursacht
        betrag = float(eingabe) + betrag#Summenberechnung

if 1000 > betrag >= 100:
    rabatt = betrag * 0.95
    print("5% Rabattt, Gesamtpreis =", round(rabatt, 2), "CHF")
elif betrag >= 1000:
    rabatt10 = betrag *0.9
    print("10% Rabatt, Gesamtpreis", round(rabatt10, 2), "CHF")
else:
    print("Kein Rabatt, Gesamtpreis =", round(betrag, 2), "CHF")