from turtle import *

def dreieck(länge, color, bgcolor):
    pencolor(color)
    fillcolor(bgcolor)
    begin_fill()
    forward(länge)
    left(120)
    forward(länge)
    left(120)
    forward(länge)
    end_fill()
    
pensize("5")
right(90)
dreieck(100, "red", "cyan")
dreieck(100, "red", "yellow")
dreieck(100, "red", "lime")