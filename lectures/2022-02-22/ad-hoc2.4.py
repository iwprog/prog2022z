#def notendurchschnitt(name, n1, n2, n3, n4, n5, n6):
#    summe = n1 + ...


# Variablen und Funktionen:
# - kleingeschrieben, mehrere Worte verbunden mit "_"
# - Bsp: task_liste, note, mwst_durchschnitt, ...
#
# CONSTANTEN
# - immer in uppercase
# - Bsp: PI = 3.14
#
# Klassen
# - gross/klein geschrieben
# - Bsp: ObstTeller, Reader, Writer

def notendurchschnitt(Name, Note1, Note2, Note3, Note4, Note5, Note6):
    summe = Note1 + Note2 + Note3 + Note4 + Note5 + Note6
    ergebnis = round((summe/6), 2)
    return Name + " " + str(ergebnis)
    

print(notendurchschnitt("Max Muster", 4, 5.5, 6, 4, 5, 4.5))
