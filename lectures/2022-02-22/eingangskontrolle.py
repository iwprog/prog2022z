# Variante 1
# -----------
# 1. Fragen Sie nach dem Alter
# 2. Wenn das Alter >= 18 ist => "Eingang erlaubt"; ansonsten "Eingang verweigert..."
#
# Variante 2
# ----------
# Frage: Geburtsjahr --> der Rest bleibt gleich
#
# Zeit: bis 19:49

alter = input("Wie alt sind sie? ")
#alter = int(alter)

if int(alter) >= 18:
    print("Eintritt gewährt")
    
else:
    print("Eintritt verweigert")


