from turtle import *

def dreieck(länge):
    forward(länge)
    left(120)
    forward(länge)
    left(120)
    forward(länge)
    left(120)

i = 0
while i < 40:
    dreieck(5*i)
    left(3*i)
    i = i + 1