# allgemeiner Aufbau einer Funktion

def summe(variable1, variable2, variable3):
    summe = variable1 + variable2 + variable3
    return summe


ergebnis = summe(1, 3, 7)
print(ergebnis)

# Funktionen ohne Rückgabewert (return) liefern None
from turtle import forward
print(forward(100))