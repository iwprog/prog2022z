from turtle import *

def dreieck():
    begin_fill()
    forward(100)
    left(120)
    forward(100)
    left(120)
    forward(100)
    end_fill()
    
pencolor("red")
pensize(5)
speed(8)

right(90)
fillcolor("cyan")
dreieck()

fillcolor("yellow")
dreieck()

fillcolor("lime")
dreieck()