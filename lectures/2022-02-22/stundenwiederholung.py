# Stundenwiederholung vom 22. Feburar 2022

# - Syntax - welche Zeichen
# - Semantik - welche Befehle was tun
# - Pragmatik - was das Programm tut bzw. tun soll
# - Paradigma - der von der Programmiersprache ermöglichte Lösungsansatz
#   - imperativ
#   - logisch/descriptiv
#   - funktionale
#   - objektorientiert
#
#name = input("Bitte geben Sie Ihren Namen ein? ")
#print(("Guten Abend "+ name + "! ") * 10 )

betrag = 100
print("MWSt = ", betrag * 0.077)

text = "Donaudampfschiffsfahrtsgesellschaftskapitän"
print(text[23:35])

# was stimmt (nicht)
betrag = 100
#print("Betrag:" + betrag)
print("Betrag:", betrag)

text = "text"
zahl = 50
kommazahl = 6.022E22

