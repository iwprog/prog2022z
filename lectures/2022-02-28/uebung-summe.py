#
# Aufgabenstellung:
# - schreiben Sie ein Programm, dass so lange Zahlen einliest und summiert, bis Sie
# - den Buchstaben 'q' eingeben.

# - 9:17

# Zahl? 12
# Zahl? 3
# Zahl? 22
# Zahl? q
#
# Summe = 37

summe = 0
eingabe = "0"
while eingabe != 'q':
    zahl = int(eingabe)
    summe = summe + zahl
    eingabe = input("Zahl oder 'q'? ")
    
print("Summe:", summe)

# Pause bis 9:43