from math import pi

def volumen(hoehe, radius):
    zylinder = (radius^2)* pi * hoehe
    return zylinder

print("Das Volumen beträgt:", volumen(22, 11))
# habe 2 eigene Zahlen für hoehe und radius gewählt, stimmt nicht mit Aufgabe überein. Warum ist dort 2,2 und  25.1327?!