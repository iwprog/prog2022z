# Was sollen wir zeichnen?
# Wie viele sollen wir zeichnen?
# Advanced: Zeichnmuster
#    #do#  -> Viereck, Dreieck, Kreis, Viereck
#    ####  -> Viereck, Viereck, Viereck, Viereck
#    oodd#  -> Kreis, Kreis, Dreieck, Dreieck, Viereck

from turtle import*

def dreieck():
    for i in range(3):
        forward(100)
        left(120)
    
for count in range(4):
    dreieck()
    penup()
    forward(110) 
    pendown()
