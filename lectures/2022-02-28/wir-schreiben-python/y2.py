# Gruppenarbeit "Wir schreiben Python"

# Autor: Ursula Bütschli
# Datum: 27.02.2022
# Buchstabe Y

from turtle import *


def y():
    # 300x400px Rahmen

    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)


    pensize(15)

    # Platzierung des Cursors in der Mitte
    penup()
    lt(90)
    fd(200)
    rt(90)
    fd(150)
    pendown()

    pencolor("cyan")
    rt(90)
    fd(100)

    # Platzierung des Cursors in der Mitte
    penup()
    rt(180)
    fd(100)
    pendown()

    lt(45)
    fd(100)

    # Platzierung des Cursors in der Mitte
    penup()
    rt(180)
    fd(100)
    pendown()

    lt(90)
    fd(100)

    # Platzierung des Cursors unten rechts mit Zeicherichting rechts
    penup()
    rt (45)
    fd(77)
    rt(90)
    fd(275)
    pendown()
    lt(90)
