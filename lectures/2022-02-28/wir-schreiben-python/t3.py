# wir schreiben python übung 1)
# buchstaben T

from turtle import *
speed(10)
hideturtle()


def t():
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    penup()
    forward(150)
    left(90)
    forward(50)
    pendown()

    begin_fill()
    fillcolor("black")
    pencolor("black")
    left(90)
    forward(25)
    right(90)
    forward(200)
    left(90)
    forward(75)
    right(90)
    forward(50)
    right(90)
    forward(200)
    right(90)
    forward(50)
    right(90)
    forward(75)
    left(90)
    forward(200)
    end_fill()

    penup()
    forward(50)
    left(90)
    forward(150)
