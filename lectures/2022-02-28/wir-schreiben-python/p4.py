#wir schreiben Python
#Buchstabe P

from turtle import *

def p():
    fillcolor("forestgreen")
    begin_fill()
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)
    end_fill()

    penup()
    pencolor("maroon")
    pensize(8)
    forward(100)
    pendown()
    left(90)
    forward(350)
    left(90)

    circle(100, -180)

    penup()
    forward(200)
    right(90)
    forward(150)
    left(90)
    pendown()
