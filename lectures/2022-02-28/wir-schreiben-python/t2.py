#23.02.22
#Autor: Janosch Kohler
#Python T

from turtle import *

def t():
    speed(10) #beschleunigt die Turtle
    fd(300)
    lt(90)
    fd(400)
    lt(90)
    fd(300)
    lt(90)
    fd(400)
    lt(90) #Rahmen
    fd(115) #Positionierung für den Beginn des T
    pensize(5)
    pencolor("forest green")
    colormode(255) #Colormode 255, weil fillcolor in rgb angegeben ist
    fillcolor((145, 216, 178))
    begin_fill()
    lt(45) #Beginn T. An den Enden jeweils 45 Grad Winkel
    fd(20)
    lt(45)
    fd(300)
    lt(90)
    fd(100)
    lt(45)
    fd(20)
    rt(135)
    fd(50)
    rt(135)
    fd(20)
    lt(45)
    fd(240)
    lt(45)
    fd(20)
    rt(135)
    fd(50)
    rt(135)
    fd(20)
    lt(45)
    fd(100)
    lt(90)
    fd(300)
    lt(45)
    fd(20)
    rt(135)
    fd(70)
    end_fill()
    pensize(1) #zurück zur Ausgangslage der Turtle
    pencolor("black")
    fillcolor("white") #Hier wusste ich nicht, wie man die Fillcolor ganz zurücksetzt.
    penup()
    lt(180)
    fd(187)
    pendown()

