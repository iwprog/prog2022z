from turtle import *
def o():
    laenge = 300
    hohe = 400
    radius = 100
    forward(laenge)
    right(-90)
    forward(hohe)
    right(-90)
    forward(laenge)
    right(-90)
    forward(hohe)
    right(-90)
    penup()
    forward(int(laenge)-int(radius)+50)
    right(-90)
    forward(200)
    pendown()
    circle(radius)
    penup()
    right(180)
    forward(200)
    right(-90)
    forward(50)
    pendown()
