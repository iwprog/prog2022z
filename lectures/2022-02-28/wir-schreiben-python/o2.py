#Hausübung Wir schreiben Python, Buchstabe O, Martina Hediger 27.2.22
# circle(100) #Radius
# circle(100, 90) #Kreis mit Radius 100 und Sektor 90Grad

from turtle import *
speed(8)
fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90) #vorgegebener Rahmen

def o():
    def buchstabenfarbe():
        pensize(5)
        pencolor("cyan")

    def grosses_O():
        circle(120, 90)
        forward(40)
        circle(120, 90)
        circle(120, 90)
        forward(40)
        circle(120, 90)

    def kleines_0():
        circle(100, 90)
        forward(40)
        circle(100, 90)
        circle(100, 90)
        forward(40)
        circle(100, 90)

    #Buchstabe O
    #äusseres O
    speed(8)
    forward(145)
    penup()
    left(90)
    forward(5)
    right(90)
    pendown()
    buchstabenfarbe()
    grosses_O()

    penup()
    left(90)
    forward(20)
    right(90)
    pendown()

    #inneres O
    buchstabenfarbe()
    kleines_0()

    penup()
    right(90)
    forward(24)
    left(90)
    pendown()

    pencolor("black")
    pensize(1)

    #zur rechten Ecke gehen
    penup()
    forward(155)
    pendown()
