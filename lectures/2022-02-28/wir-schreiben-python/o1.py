from turtle import *

speed(10)

def o(farbe, stiftfarbe):
    pensize(4)
    fillcolor(farbe)
    pencolor(stiftfarbe)

    penup()
    fd(300)
    lt(90)
    fd(400)
    lt(90)
    fd(300)
    lt(90)
    fd(400)
    lt(90)

    lt(90)
    fd(125)
    rt(90)
    fd(50)
    lt(90)
    pendown()

    begin_fill()
    rt(180)
    circle(100, 180)
    fd(150)
    circle(100, 180)
    fd(150)
    end_fill()

    penup()
    lt(90)
    fd(50)
    pendown()

    fillcolor('white')
    begin_fill()
    rt(90)
    circle(50, 180)
    fd(150)
    circle(50, 180)
    fd(150)
    end_fill()

    penup()
    fd(125)
    lt(90)
    fd(200)



