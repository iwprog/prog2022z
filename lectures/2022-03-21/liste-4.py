l_input = ['Gesundheit', 'Wanderung', 'Heiterkeit',
'Gewandtheit', 'Lustig']

def get_suffix_words(liste, suffix):
    ergebnis = []
    suffix_länge = len(suffix)
    for element in liste:
        if element[-suffix_länge:] == suffix:
            ergebnis.append(element)
    return ergebnis

def get_suffix_words_case_insensitive(liste, suffix):
    ergebnis = []
    suffix_länge = len(suffix)
    for element in liste:
        if element[-suffix_länge:].lower() == suffix.lower():
            ergebnis.append(element)
    return ergebnis



print(get_suffix_words(l_input, 'heit'))
print(get_suffix_words(l_input, 'g'))
