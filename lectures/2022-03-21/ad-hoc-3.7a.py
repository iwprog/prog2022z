staedte = {}
staedte ['Zürich'] = 370000
staedte ['Genf'] = 190000

staedte = {'Zürich': 370000, 'Genf': 190000, 'Chur': 39000}

# ausgabe: variante 1
for stadt in staedte:
    print(stadt, "hat", staedte[stadt], "Einwohner")

# ausgabe: variante 2
for stadt, einwohnerzahl in staedte.items():
    print(stadt, "hat", einwohnerzahl, "Einwohner")
    
print("Sortiert:")
for stadt in sorted(staedte):
    print(stadt, "hat", staedte[stadt], "Einwohner")

for stadt, einwohnerzahl in sorted(staedte.items()):
    print(stadt, "hat", einwohnerzahl, "Einwohner")
    
### Sortierung nach den Einwohner
sortierliste = []
for stadt, einwohnerzahl in staedte.items():
    sortierliste.append([einwohnerzahl, stadt])
print(sortierliste)


