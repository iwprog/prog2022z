jahreszeiten=["Frühling", "Sommer", "Herbst", "Winter"]

for element in jahreszeiten:
    if element[-2:]=="er":
        print(element, "-", element[-2:])
        
# Variante 1: mit er mit Wort
for element in jahreszeiten:
    if element.find("er") != -1:
        print(element)
        
# Variante 2: mit er mit Wort
for element in jahreszeiten:
    if "er" in element:
        print(element)
