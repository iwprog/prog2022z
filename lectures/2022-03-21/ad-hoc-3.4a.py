
def allIndex(sequenz, gesuchtes_element):
    liste=[]
    for no, aktuelles_element in enumerate(sequenz):
        # Erweiterung: ignoriere Gross-/Kleinschreibung für Text
        if type(aktuelles_element) == str:
            aktuelles_element = aktuelles_element.lower()
            gesuchtes_element = gesuchtes_element.lower()
        if aktuelles_element == gesuchtes_element:
            liste.append(no)
    return liste

print(allIndex(["H", "a", "l", "l", "o", 0, 0, 7], "l"))
print(allIndex("Chur", "c"))
print(allIndex("Chur", "r"))