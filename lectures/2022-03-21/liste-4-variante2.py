"""
Begründung, warum es besser ist der Funktion Parameter
zu übergeben.
"""

liste = ["Luzern", "Stern", "Eins", "Keins", "Peter", "Meter", "Zwei", "Ei"]
liste2 = ["Ana", "Tom", "Kreis", "Mais"]
suffix = "er"
suffix2 = "s"
 
def suffix_filter(): 
    for element in liste: 
        if element[-len(suffix):].lower() == suffix:
        #if Peter [-len(er):] -> [-2:] == er
        # Wenn die letzten beiden Buchstaben er sind
            print(element)
            
    
suffix_filter()
liste = liste2
suffix = suffix2
suffix_filter()