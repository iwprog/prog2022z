def hauptmenü():
    print("Willkommen zum Textadventure")
    print("============================")
    print("  1 ... Spielregeln")
    print("  2 ... Anfangen")
    print("  3 ... Beenden")

def menü_1():
    print("Spielregeln")
    print("============================")
    print("  A ... gehe nach links")
    print("  D ... gehe nach rechts")
    print("  X ... nehme Objekt auf")
    print("  C ... benutze Objekt")
    print("  Y ... öffne Inventar")
    print()
    print("  2 ... Anfangen")
    print("  3 ... Beenden")
    


def raum_1_mit_item():
        print("_____________________________________")
        print("| Raum 1 | Raum 2 | Raum 3 | Raum 4 |")
        print("|    x   |        |        |        |")
        print("|Kaugummi|________|________|________|")
        print()

def raum_1_ohne_item():
        print("_____________________________________")
        print("| Raum 1 | Raum 2 | Raum 3 | Raum 4 |")
        print("|    x   |        |        |        |")
        print("|________|________|________|________|")
        print()

def raum_2():
        print("_____________________________________")
        print("| Raum 1 | Raum 2 | Raum 3 | Raum 4 |")
        print("|        |    x   |        |        |")
        print("|________|________|________|________|")
        print()

def raum_3_mit_item():
        print("_____________________________________")
        print("| Raum 1 | Raum 2 | Raum 3 | Raum 4 |")
        print("|        |        |    x   |        |")
        print("|________|________|Motorrad|________|")
        print()

def raum_4_mit_item():
        print("_____________________________________")
        print("| Raum 1 | Raum 2 | Raum 3 | Raum 4 |")
        print("|        |        |        |    x   |")
        print("|________|________|________| Benzin |")
        print()

#Bestätigung um Spielmenü zu öffnen
bestätigung = input("Wollen Sie Textadventure spielen? (ja/nein) ")
print()

if bestätigung.lower().strip() == "ja":
    #Hauptmenü
    hauptmenü()
    print()
    eingabe_menu = input("Option: ")
    print()
    
    #Spielregeln
    if eingabe_menu == "1":
        menü_1()
        
        print()
        eingabe_menu = input("Option: ")
        print()
    
    #Spiel starten
    if eingabe_menu == "2":
        raum_2()
        #Einführung in die Spielgeschichte > nur 1x
        print("Sie befinden sich im Raum 2 einer Station im Planet C-86187.")
        print("Sie müssen so schnell wie möglich zum Commander, weil Sie ansonsten kein Stück Kuchen kriegen. Zu Fuss werden Sie es niemals rechtzeitig schaffen. Bis dahin wird Kevin alles gegessen haben.")
        print("In dieser Station muss es irgendetwas geben, was Ihnen dabei hilft, schneller zum Commander zu kommen.")
        print()
        
        i = 1 
        position = 2 #Raum wo sich Spielfigur befindet
        inventar = [] #Inventar anfangs leer
        durchlauf_r_1 = 1 #Durchlauf Raum 1
        durchlauf_r_3 = 1 #Durchlauf Raum 3
        durchlauf_r_3_2 = 1  #Durchlauf Raum 3 Teil 2
        
        while i == 1: #Schleife bis Spiel fertig ist
            aktion = input("Aktion: ")
            
            #Bewegung Links und Rechts
            if aktion.lower() == "a":
                position = position - 1
                if position < 1: 
                    position = 4 #von Raum 1 nach Raum 4
            if aktion.lower() == "d":
                position = position + 1
                if position > 4:
                    position = 1 #von Raum 4 nach Raum 1
            
            #Inventar öffnen
            if aktion.lower() == "y":
                print()
                print("Inventar:")
                print(inventar)
            
            #Raum 1
            elif position == 1:
                if durchlauf_r_1 == 1: #Wenn Kaugummi aufgenommen und gebraucht wird, soll sie im Raum nicht mehr erscheinen
                    if "Kaugummi" in inventar:
                        raum_1_ohne_item()
                        print("Sie betreten Raum 1")
                        print("Ganz schön leer hier")
                    else:
                        raum_1_mit_item()
                        print("Sie betreten Raum 1")
                        print("Ganz hinten in der Ecke befindet sich ein Kaugummi... Minze.")
                        
                        if aktion.lower() == "x":
                            if "Kaugummi" in inventar:
                                print("Hier gibt es nichts mehr zum aufnehmen")
                                inventar.remove("Kaugummmi") #Kaugummi wird entfert und anschliessend wieder hinzugefügt
                            inventar.append("Kaugummi")
                            print("Sie habe 'Kaugummi' aufgenommen")
                else: #Kaugummi soll nach gebrauch nicht mehr erscheinen
                    raum_1_ohne_item()
                    print("Sie betreten Raum 1")
                    print("Ganz schön leer hier")
            
            #Raum 2            
            elif position == 2:
                raum_2()
                print("Sie betreten Raum 2")
                print("Sie sollten sich langsam auf dem Weg machen. Kevin zeigt der Kuchen keine Gnade")
                    
            
            #Raum 3
            elif position == 3:
                raum_3_mit_item()
                print("Sie betreten Raum 3")
                
                if durchlauf_r_3 == 1: #soll nur erscheinen, wenn man Raum zum ersten Mal besucht
                    print("Mitten im Raum steht ein Motorrad mit einem Loch im Reifen. Was ein Motorrad hier macht, bleibt ein Rätsel. Sie können jedoch der Motorrad brauchen um zum Commander zu gelangen")
                    durchlauf_r_3 = durchlauf_r_3 + 1
                elif durchlauf_r_3 == 2:
                    print("Der Motorrad kann hilfreich sein. Sie müssen es nur irgendwie reparieren")
                    
                if aktion.lower() == "x":
                    print("Sie können kein Motorrad in eine Tasche packen")
                if "Kaugummi" in inventar:
                    print()
                    print("Interaktion mit Motorrad möglich")
                    if aktion.lower() == "c":
                        print()
                        print("Sie benutzen der Kaugummi um das Loch im Reifen des Motorrads zu reparieren.")
                        inventar.remove("Kaugummi")
                        durchlauf_r_1 = durchlauf_r_1 + 1 #Kaugummi in Raum 1 soll nach gebrauch nicht mehr erscheinen
                        durchlauf_r_3_2 = durchlauf_r_3_2 + 1 #zuerst soll der Spieler das Rad reparieren
                if durchlauf_r_3_2 == 2: #nach Reparatur des Rads ist die Interaktion mit Benzin möglich
                    if "Benzin" in inventar:
                        print()
                        print("Interaktion mit Motorrad möglich")
                        if aktion.lower() == "c":
                            print()
                            print("Sie benutzen den Benzin um der Motorrad zu starten. Endlich können sie sich auf dem Weg zum Commander machen")
                            print("...")
                            print("Nach eine Stude Motorrad Fahren sind sie endlich angekommen")
                            print("Der Kuchen war eine Lüge")
                            print()
                            print("Vielen Dank fürs spielen")
                            break
            
            #Raum 4
            elif position == 4:
                raum_4_mit_item()
                print("Sie betreten Raum 4")
                print("Ein Benzinkanister liegt auf einen Tisch. Damit können Sie sicherlich das Motorrad brauchen.")
                if aktion.lower() == "x":
                    if "Benzin" in inventar:
                        print("Sie haben Benzin bereits im Inventar")
                        inventar.remove("Benzin")
                            
                    inventar.append("Benzin")
                    print("Sie habe 'Benzin' aufgenommen")
            
            elif aktion.lower() == "3":
                break
    elif eingabe_menu =="3":
        print("Schade")
