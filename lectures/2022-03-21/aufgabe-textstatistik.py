# Ermitteln Sie für einen Eingabetext wie oft jedes Wort in diesem
# vorkommt.

text = "Ana mag Ana gerne und Tom mag Tom."

lookup = {}
for wort in text.split():
    print(wort)
    if wort in lookup:
        lookup[wort] = lookup[wort] + 1
    else:
        lookup[wort] = 1
        
print(lookup)