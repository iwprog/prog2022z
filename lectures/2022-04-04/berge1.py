berge = {}

for i in range(3):
    berg = input('Berg: ')
    hoehe = float(input("Höhe von " + berg + " (in m): "))
    berge[berg] = str(hoehe) + " m (" + str(round(hoehe * 3.28)) + 'ft)'
    #                 1000 m        (1640 ft)
    # berge[berg] = hoehe -> in Dict Berge berg = Wert hoehe setzen
print()# macht einen Absatz
for berg, hoehe in sorted(berge.items()):
    print(berg, 'ist', hoehe, 'hoch.')
