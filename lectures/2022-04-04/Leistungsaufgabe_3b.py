# Dictionary erstellt:
woerterbuch = {"Haus":"house", "Zahn":"tooth",
               "Affe":"monkey", "Delfin":"dolphin",
               "Tintenfisch":"octopus", "Schlafen":"sleep",
               "Traum":"dream", "Kuss":"kiss",
               "Leben":"life", "Tod":"death"} 
# richtige Antwort zähler auf 0
i = 0

for d_wort, e_word in woerterbuch.items():
    eingabe = input('Was heisst ' + d_wort + ': ') #Frage stellen mit erstem Teil vom Tupel
    #Test against dict: if same or if not same
    if eingabe == e_word: 
        print('RICHTIG!') 
        i = i + 1
    else:
        print('FALSCH!')
        
# Abfragestatistik:
print('Sie haben', i, 'von 10 Vokabeln richtig beantwortet!')


# b) Erstellen Sie mittels einem Dictionary (dict) ein Mini-Deutsch-Englisch Wörterbuch (10 Vokabeln).
# Ein zugehörige Vokabeltrainer soll alle Vokabeln nach folgendem Muster abfragen:
# Was heisst Haus: house
# RICHTIG!
# Was heisst Zahnarzt: dentist
# RICHTIG!
# Was heisst Zahn: dent
# FALSCH!
# …
# Nach Abfrage aller Vokabeln wird noch die Abfragestatistik ausgegeben:
# Sie haben 7 von 10 Vokabeln richtig beantwortet!