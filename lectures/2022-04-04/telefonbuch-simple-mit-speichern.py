from json import loads, dumps



try:
    with open("telefonbuch.json") as f:
        telefonbuch_str = f.read()
        telefonbuch = loads(telefonbuch_str)
except FileNotFoundError:
    print("Leider kein gespeichertes Telefonbuch vorhanden.")
    telefonbuch = {}
    
    
while True:
    name = input("Name: ")
    if name == "x":
        break
    phone = input("Phone: ")
    telefonbuch[name] = phone
    
print(telefonbuch)

with open("telefonbuch.json", "w") as f:
    telefonbuch_str = dumps(telefonbuch)
    f.write(telefonbuch_str)
    
