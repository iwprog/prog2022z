cities = {'Chur': 39000, 'Buchs SG': 39000, 'Basel': 170000, 'Zürich': 390000}


def sortiere_einwohner(wert):
    return (wert[1], wert[0])

for city, population in sorted(cities.items(), key=sortiere_einwohner):
    print(f"{city} hat {population} einwohner.")