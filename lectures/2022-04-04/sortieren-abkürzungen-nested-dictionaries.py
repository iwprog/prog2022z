berge = [{"Gebirge": "G1",
          "Höhe": 1200,
          "Berg": "Berg1"},
         {"Gebirge": "G2",
          "Höhe": 1200,
          "Berg": "Berg2"},
         {"Gebirge": "G1",
          "Höhe": 3600,
          "Berg": "Berg3"},
         {"Gebirge": "G1",
          "Höhe": 1900,
          "Berg": "Berg4"}
         ]

def sortierfunktion(wert):
    print(wert)
    return (wert["Gebirge"], wert["Höhe"])

for berg in sorted(berge, key=sortierfunktion):
    print(f"{berg['Berg']} ist {berg['Höhe']} hoch und im Gebirge {berg['Gebirge']}")