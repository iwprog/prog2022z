# Erstellen Sie ein CSV file, welches alle
# Wörter und deren Häufigkeit in der untenstehenden
# URL ermittelt.
# bis 11:20

from urllib.request import urlopen
from csv import writer

wörter = {}

# schritt 1 - wörter zählen
with urlopen('https://www.gutenberg.org/cache/epub/6079/pg6079.txt') as file:
    text = file.read().decode("utf8")
    text = text.replace("(", "").replace(")", "").replace("'", "").replace('"', "").replace(",", "").replace(".", "").replace("!", "")
    for wort in text.split():
        if wort in wörter:
            wörter[wort] = wörter[wort] + 1
        else:
            wörter[wort] = 1

with open("wortliste.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for element in sorted(wörter.items()):
        csv_writer.writerow(element)

