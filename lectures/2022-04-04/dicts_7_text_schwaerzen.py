# dicts 7 - text schwärzen
# Morena Sager

eingabetext= input("Geben Sie einen Text ein ")
textsplit = eingabetext.split()
blacklist = {}
while True:
    black_word=input("Wort zum schwärzen eingeben, 'x' zum beenden ")
    if black_word=="x":
        break
    blacklist[black_word.lower()] = "*"*len(black_word)

for wort in textsplit:
    if wort.lower() in blacklist:
        eingabetext = eingabetext.replace(wort, blacklist[wort.lower()])
      
print(eingabetext)