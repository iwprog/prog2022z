""" Lesen Sie eine Datei aus dem Web ein.
     https://www.rfc-editor.org/rfc/rfc5321.txt
    und am Bildschirm ausgibt.
"""
from urllib.request import urlopen
from inscriptis import get_text

with urlopen("https://www.fhgr.ch/") as f:
  web_content = f.read().decode('utf8')
  
  text = get_text(web_content)
  print(text)
  
  # no_zeilen = len(web_content.split("\n"))
  # print("Die RFC hat", no_zeilen, "Zeilen.")
