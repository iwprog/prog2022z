bergliste = []
datensatz = []

feet=3.28
#Daten erheben
def daten_erheben():
    return [["Berg1", 2400, "G1"],
            ["Berg2", 1200, "G1"],
            ["Berg3", 3600, "G2"]]
    # Memo: replace with input later
    bergliste = []
    while True:
        berg = input("Berg (oder 'x' für Exit): ")
        if berg == 'x':
            break
        höhe = float(input("Höhe: "))
        gebirge = input("Gebirge: ")
        datensatz = [berg, höhe, gebirge]
        bergliste.append(datensatz)
    return bergliste

#Daten auspacken
def ausgabe_bergliste(bergliste):
    count = 1
    for berg, höhe, gebirge in bergliste:
        print(f"{berg} ist {höhe} m ({höhe*feet}) hoch und gehört zum {gebirge} Gebirge.")
        #print(berg, "ist", höhe, "m", "(" + str(i["Höhe"]*feet), "ft) hoch und gehört zum", i["Gebirge"], "Gebirge.")
        count = count + 1

#sortieren
def sortieren_berg():
    return sorted(bergliste)

# schlüsselfunktion für's sortieren
def sortieren_höhe():
    # eine temporäre liste, welche nur der sortierung dient
    # ziel: dass jenes feld nachdem sortiert werden soll, an der ersten position
    #       steht.
    sortierliste = []
    for berg, höhe, gebirge in bergliste:
        # höhe an position 0
        sortierliste.append([höhe, berg, gebirge])
    
    ergebnis = []
    for höhe, berg, gebirge in sorted(sortierliste):
        ergebnis.append([berg, höhe, gebirge])
    return ergebnis
    
        
#def sortieren_gebirge():
#Menu
def menu():
    sortieren_nach = input("Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge: ")
    
    # sortieren nach Berg
    if sortieren_nach == "1":
        auf_absteigend = input("Nach xy (1) aufsteigend, oder (2) absteigend sortieren?")#xy noch variable rein packen
        if auf_absteigend == "1":
            #aufsteigend
            sortieren_berg()
        elif auf_absteigend == "2":
            #absteigend Absteigend (Z bis A)
            sortieren_berg()
            bergliste.reverse()
        ausgabe_bergliste()
        
    #sortieren nach Höhe
    elif sortieren_nach == "2":  
         auf_absteigend = input("Nach xy (1) aufsteigend, oder (2) absteigend sortieren?")#xy noch variable rein packen
         if auf_absteigend == "1":
             bergliste = sortieren_höhe()
             print(bergliste)
             ausgabe_bergliste(bergliste)
#             #aufsteigend
#         elif auf_absteigend == "2":
#             #absteigend
#     elif sortieren_nach == "3":
#         #sortieren nach Gebirge
#         auf_absteigend = input("Nach xy (1) aufsteigend, oder (2) absteigend sortieren?")#xy noch variable rein packen
#         if auf_absteigend == "1":
#             #aufsteigend
#         elif auf_absteigend == "2":
#             #absteigend
bergliste = daten_erheben()

# auspacken()
menu()