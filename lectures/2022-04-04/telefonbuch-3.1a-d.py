# 3.1a
person = {"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"}

# 3.1b
telefonbuch = []
telefonbuch.append(person)

# 3.1c
telefonbuch.append({"Vorname": "Tim", "Nachname": "Kurz", "Phone": "732"})
telefonbuch.append({"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"})

# 3.1d
while True:
    vorname = input("Vorname (beenden mit 'x'): ")
    if vorname == "x":
        break
    nachname = input("Nachname: ")
    phone = input("Telefonnummer: ")
    telefonbuch.append({"Vorname": vorname, "Nachname": nachname, "Phone": phone})

print("Datenstruktur:", telefonbuch)


def datensatz_ausgeben(telefonbuch):
    for eintrag in telefonbuch:
        print("Vorname : ",eintrag["Vorname"])
        print("Nachname: ", eintrag["Nachname"])
        print("Phone: ", eintrag["Phone"])

print("Ursprünglicher Datensatz")
datensatz_ausgeben(telefonbuch)

print("Gefilterter Datensatz (alle Telefonnummmern mit '1' entfernt)")

neues_telefonbuch = []
for eintrag in telefonbuch:
    if "1" not in eintrag["Phone"]:
        neues_telefonbuch.append(eintrag)

telefonbuch = neues_telefonbuch
print(telefonbuch)