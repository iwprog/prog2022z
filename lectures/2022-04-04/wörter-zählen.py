from urllib.request import urlopen

with urlopen('https://www.gutenberg.org/cache/epub/6079/pg6079.txt') as file:
    text = file.read().decode("utf8")
    
print(text)


print("Himmel:", text.count("Himmel ") + text.count("Himmel,") + text.count("Himmel."))
print("Freiheit:", text.count("Freiheit"))
print("Spiel:", text.count("Spiel"))
print("Tanz:", text.count("Tanz"))