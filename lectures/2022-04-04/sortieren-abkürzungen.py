bergliste = [["Berg1", 2400, "G2"],
             ["Berg2", 1200, "G1"],
             ["Berg2", 9999, "G1"],
             ["Berg3", 3600, "G1"]]


def schlüsselfunktion(wert):
    print(wert)
    return wert[1]

def schlüsselfunktion_gebirge(wert):
    return wert[2]

def schlüsselfunktion_gebirge_und_höhe(wert):
    """Sortiere nach Gebirge und im Anschluss nach Höhe"""
    return (wert[2], wert[1])

# sortieren und ausgabe nach der höhe
reverse = input("Sortierung umdrehen (ja/nein)? ")
if reverse == "ja":
    reverse = True
else:
    reverse = False
for berg, höhe, gebirge in sorted(bergliste, key=schlüsselfunktion, reverse=reverse):
    print(f"{berg} ist im {gebirge} und {höhe} hoch.")

# sortieren und ausgabe nach der höhe und dem gebirge
for berg, höhe, gebirge in sorted(bergliste, key=schlüsselfunktion_gebirge_und_höhe):
    print(f"{berg} ist im {gebirge} und {höhe} hoch.")
