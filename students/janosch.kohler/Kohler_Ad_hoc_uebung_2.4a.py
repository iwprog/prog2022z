#23.02.22
#Autor: Janosch Kohler
#Ad hoc Übung 2.4a

import math

def zylinder(radius, hoehe):
    volumen = (radius**2) * math.pi * hoehe
    return volumen

print("Das Volumen beträgt: ", zylinder(2.2, 10))

#ich musste noch eine Höhe angeben, sonst hätte
#ein Wert für hoehe gefehlt
