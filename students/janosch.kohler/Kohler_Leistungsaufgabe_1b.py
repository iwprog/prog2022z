#23.02.22
#Autor: Janosch Kohler
#Leistungsaufgabe 1b

from turtle import *

laenge = int(numinput("Flagge", "Wie lang soll die Flagge sein?"))
#Nicht sicher, ob das int hierhin gehört, es funktioniert auf jeden Fall damit.

hideturtle() #Befehl versteckt den Pfeil
pensize(5)
pencolor("red")
fillcolor("red") #Das Quadrat muss zuerst, da die Färbung sonst das Kreuz überdeckt
pendown()
begin_fill()
fd(laenge) #jeweils laenge als Variable für die Länge der Flaggenseiten
lt(90)
fd(laenge) 
lt(90)
fd(laenge)
lt(90)
fd(laenge)
end_fill()
penup() #Positionierung für den Beginn des Kreuzes, ohne etwas zu zeichnen
lt(90)
fd(int(laenge/8)) #Jeweils das Verhältnis mathematisch nachvollzogen zu Aufgabe 1a. Die Bewegung hier war 50 --> 400/50=8
lt(90)
fd(int(laenge/2.7)) #int wegen Komma, hat beim Ausprobieren aber auch ohne geklappt.
rt(90)
fillcolor("white")
begin_fill()
fd(int(laenge/4))
rt(90)
fd(int(laenge/4))
lt(90)
fd(int(laenge/4))
lt(90)
fd(int(laenge/4))
rt(90)
fd(int(laenge/4))
lt(90)
fd(int(laenge/4))
lt(90)
fd(int(laenge/4))
rt(90)
fd(int(laenge/4))
lt(90)
fd(int(laenge/4))
lt(90)
fd(int(laenge/4))
rt(90)
fd(int(laenge/4))
lt(90)
fd(int(laenge/4))
end_fill()



