#23.02.22
#Autor: Janosch Kohler
#Leistungsaufgabe 1a

from turtle import *

hideturtle() #Befehl versteckt den Pfeil
pensize(5)
pencolor("red")
fillcolor("red") #Das Quadrat muss zuerst, da die Färbung sonst das Kreuz überdeckt
pendown()
begin_fill()
fd(400)
lt(90)
fd(400)
lt(90)
fd(400)
lt(90)
fd(400)
end_fill()
penup() #Positionierung für den Beginn des Kreuzes, ohne etwas zu zeichnen
lt(90)
fd(50)
lt(90)
fd(150)
rt(90)
fillcolor("white")
begin_fill()
fd(100)
rt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
rt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
rt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
rt(90)
fd(100)
lt(90)
fd(100)
end_fill()


