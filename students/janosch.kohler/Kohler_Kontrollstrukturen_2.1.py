#23.02.22
#Autor: Janosch Kohler
#Funktionen und Kontrollstrukturen 2.1

def guten_morgen():
    print("Guten Morgen!")

guten_morgen()

#2.2
def guten_morgen_2(name):
    print("Guten Morgen "+ name+ "!")

name = input("Wie heisst du?")
guten_morgen_2(name)