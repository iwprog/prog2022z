#23.02.22
#Autor: Janosch Kohler
#Schleifen mit while 2.8c

i = 10
while i >= 0:
    print(i)
    i = i-3 #zählt aber nur bis 1
if i < 3:
    print(0) #wenn i kleiner als 3 ist, wird null ausgegeben.

