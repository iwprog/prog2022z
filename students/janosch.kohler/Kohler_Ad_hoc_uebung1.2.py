#Ad hoc Übung 1.2


#1.2 a)
from turtle import *

fd(50)
lt(90)
fd(50)
lt(90)
fd(50)
lt(90)
fd(50)

penup() #um Abstand zur zweiten Aufgabe zu generieren
fd(200)
lt(90)
pendown()


#1.2 e)

pencolor("blue")
pensize(5)
fd(100)
lt(120)
fd(100)
lt(120)
fd(100)

penup() #um wieder zum Mittelpunkt zu kommen
lt(120)
fd(100)
pendown()

pencolor("red")
fd(100)
lt(120)
fd(100)
lt(120)
fd(100)

pencolor("lime")
fd(100)
lt(120)
fd(100)
lt(120)
fd(100)
