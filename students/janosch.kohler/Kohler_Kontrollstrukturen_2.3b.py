#23.02.22
#Autor: Janosch Kohler
#Funktionen und Kontrollstrukturen 2.3b


def flaeche_rechteck(laenge, breite):
    flaeche = laenge * breite
    return flaeche #ohne Return gibt die Funktion 'none' aus.

flaeche = flaeche_rechteck(10, 20)
print("Die Fläche beträgt", flaeche, "m2.")
    