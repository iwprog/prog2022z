#23.02.22
#Autor: Janosch Kohler
#Funktionen und Kontrollstrukturen 2.2

def guten_morgen(name):
    print("Guten Morgen "+ name+ "!")

guten_morgen("Ana")

#Alternativ mit Input:

def guten_morgen(name):
    print("Guten Morgen "+ name+ "!")
    
name = input("Wie heisst du?")
guten_morgen(name)