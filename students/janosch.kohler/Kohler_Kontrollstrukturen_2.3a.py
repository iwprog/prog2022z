#23.02.22
#Autor: Janosch Kohler
#Funktionen und Kontrollstrukturen 2.3a

def guten_morgen(name):
    print("Guten Morgen "+ name+ "!")

gruss = guten_morgen("Ana")
print(gruss) #Das print(gruss) wäre aber nicht notwendig und gibt 'none' zurück.

#name = input("Wie heisst du?")
#guten_morgen(name)
