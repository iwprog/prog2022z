from turtle import*

hideturtle()      #turtle nicht sichtbar machen
speed(100)        #turtle verschnellern
pensize(5)

def balken(länge, breite, pcolor, fcolor):
    pencolor(pcolor)
    fillcolor(fcolor)
    begin_fill()
    forward(länge)
    left(90)
    forward(breite)
    left(90)
    forward(länge)
    left(90)
    forward(breite)
    end_fill()
    
# stift platzieren
penup()          
left(90)
forward(90)
left(90)
forward (150)
left(180)
pendown()        

balken(300, 30, "blue", "blue")

forward(30)
left(90)

balken(300, 30, "cyan", "cyan")

forward(30)
left(90)


balken(300, 30, "green", "green")

forward(30)
left(90)

balken(300, 30, "yellow", "yellow")

forward(30)
left(90)

balken(300, 30, "orange", "orange")

forward(30)
left(90)

balken(300, 30, "red", "red")

penup()
left(180)
forward(60)
right(90)
forward(150)
pendown()
    
    
# Schriftzug
color('white')
style = ('helvetica', 50, 'bold')
write('P A C E', font=style, align='center')