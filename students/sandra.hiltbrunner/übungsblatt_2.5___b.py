alter = input("Geben Sie ein Alter ein")
alter = int(alter)

def legal_status(alter):
    if alter <0:
        return("ungeboren")
    elif alter >=0 and alter <=6:
        return("geschäftsunfähig")
    elif alter >6 and alter <=14:
        return ("unmündig")
    elif alter <14 and alter <=17:
        return ("mündig minderjährig")
    elif alter <=18:
        return("volljährig")


print("Mit", alter, "ist man", legal_status(alter) + ".")

