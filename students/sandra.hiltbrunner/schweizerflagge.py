from turtle import*

hideturtle()   #turtle nicht sichtbar machen
speed(10)      #turtle verschnellern
pensize(5)

#mit def mache ich eine Viereck Funtkion die ich anschliessend pro Ausgabe definieren kann
def viereck(länge, pcolor, fcolor):
    pencolor(pcolor)
    fillcolor(fcolor)
    begin_fill()
    forward(länge)
    left(90)
    forward(länge)
    left(90)
    forward(länge)
    left(90)
    forward(länge)
    left(90)
    end_fill()
    
#grosses rotes viereck als Hintergrund
viereck(300, "red", "red")
forward(180)
left(90)
forward(60)

#ich mache fünf kleine weisse vierecke die ich so platziere damit es ein Kreuz ergibt
viereck(60, "white", "white")
forward(60)

viereck(60, "white", "white")
forward(60)

viereck(60, "white", "white")
left(90)
forward(60)

viereck(60, "white", "white")
left(180)
forward(60)
right(90)

viereck(60, "white", "white")


