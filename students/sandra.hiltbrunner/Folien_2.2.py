from  turtle import*

def dreieck(länge, color, bgcolor):
    pencolor(color)
    fillcolor(bgcolor)
    begin_fill()
    forward(länge)
    right(120)
    forward(länge)
    right(120)
    forward(länge)
    end_fill()
    
speed(10)  
pensize(5)
left(90)
dreieck(50, "red", "cyan")

left(120)
dreieck(100, "red", "yellow")

left(120)
dreieck(200, "red", "lime")