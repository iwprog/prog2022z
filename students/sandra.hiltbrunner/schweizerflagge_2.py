from turtle import*

speed(100)
hideturtle()

def kreuz(länge, grad, pcolor, fcolor):
    pencolor(pcolor)
    fillcolor(fcolor)
    forward(länge)  
    right(grad)      
    forward(länge)   
    left(grad)      
    forward(länge)   
    right(grad)      
    
def quadrat(länge, pcolor, fcolor):
    pencolor(pcolor)
    fillcolor(fcolor)
    begin_fill()
    forward(länge)
    right(90)
    forward(länge)
    right(90)
    forward(länge)
    right(90)
    forward(länge)
    right(90)
    end_fill()
    
penup()
left(90)
forward(200)
left(90)
forward(200)
pendown()

right(180)

quadrat(400, 'red', 'red')

penup()
forward(160)
right(90)
forward(80)
pendown()

begin_fill()
kreuz(80, 90, 'white', 'white')
left(180)
kreuz(80, 90, 'white', 'white')
left(180)
kreuz(80, 90, 'white', 'white')
left(180)
kreuz(80, 90, 'white', 'white')
end_fill()