# 3.1 a
datensatz = {'Vorname': 'Ana', 'Nachname': 'Skupch', 'Phone': '123'}
print(datensatz)

# 3.1 b
telefonbuch = []
telefonbuch.append(datensatz)
print(telefonbuch)
# Alternative: telefonbuch = [{'Vorname': 'Ana', 'Nachname': 'Skupch', 'Phone': '123'}]
# Alternative: telefonbuch = [datensatz]

# 3.1 c
telefonbuch.append({"Vorname": "Tim", "Nachname": "Kurz", "Phone": "732"})
telefonbuch.append({"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"})
print(telefonbuch)

# 3.1 d
while True:
    vorname = input("Vorname (beenden mit 'x'): ")
    if vorname == 'x':
        break
    nachname = input('Nachname: ')
    phone = input('Phone: ')
    telefonbuch.append({'Vorname': vorname, 'Nachname': nachname, 'Phone': phone})
print("Datenstruktur:", telefonbuch)
    