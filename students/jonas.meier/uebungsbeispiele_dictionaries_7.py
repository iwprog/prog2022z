# 7
eingabetext = input('Eingabetext: ')
blacklist = input('Blacklist (kleingeschrieben): ')

for eingabe in eingabetext.split():
    wort_klein = eingabe.lower()
    if wort_klein in blacklist:
        eingabetext = eingabetext.replace(eingabe,'*' * len(eingabe))
print('Ausgabe:', eingabetext)

# Voraussetzung: alle Wörter in der Blacklist sind kleingeschrieben