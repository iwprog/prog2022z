# Text Statistiken
# 1. Schreiben sie ein Programm, dass eine URL entgegennimmt: https://www.ietf.org/rfc/rfc2616.txt
# (a) jedes Wort und die Anzahl der Vorkommnisse des Wortes berechnet.
# (b) berechnet wie oft jeder Buchstaben vorkommt.
from urllib.request import urlopen
from csv import writer

wörter = {}
buchstaben = {}

with urlopen("https://www.ietf.org/rfc/rfc2616.txt") as source:
    web_content = source.read().decode('utf8')
    for wort in web_content.split():
        if wort in wörter:
            wörter[wort] = wörter[wort] + 1
        else:
            wörter[wort] = 1
            
    for buchstabe in list(web_content): # macht eine Liste aus web_content mit allen Buchstaben einzeln
        if buchstabe in buchstaben:
            buchstaben[buchstabe] = buchstaben[buchstabe] + 1
        else:
            buchstaben[buchstabe] = 1
            
# 2. Schreiben sie die Ergebnisse in zwei CSV Files
with open('wörter.csv', 'w', encoding = 'utf8') as f:
    csv_writer = writer(f, delimiter=';')
    for wort, count in sorted(wörter.items()):
        csv_writer.writerow([wort, count])
        
with open('buchstaben.csv', 'w', encoding = 'utf8') as f:
    csv_writer = writer(f, delimiter = ';')
    for buchstabe, count in sorted(buchstaben.items()):
        csv_writer.writerow([buchstabe, count])
    