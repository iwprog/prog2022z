# über Dictionaries iterieren
# 2.2 e
preisliste = {'Brot':3.2, 'Milch':1.9, 'Orangen':3.75, 'Tomaten':2.2}

for lebensmittel, preis in preisliste.items():
    if preis < 2.0:
        print(lebensmittel)