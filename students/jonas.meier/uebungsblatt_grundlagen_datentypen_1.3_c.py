liste = ["Ich heisse Jonas", "Luzern", "LUZ", "LU", "12"]
liste2 = []
 
#Variante 1:
# def entfernen():
#     for i in liste:
#         if len(i) < 3:
#             liste.remove(i)
#     print(liste)
# entfernen()
 
#Problem: Wenn ein Element aus der Liste entfernt wird,
#rutschen die anderen Elemente einen Index nach vorne,
#wodurch das nächste Element nicht geprüft wird.
 
#Variante 2:
liste = ["Ich heisse Jonas", "Luzern", "LUZ", "LU", "12"]
liste1 = []
 
def hinzufuegen():
  for element in liste:
    if len(element) >= 3:
      liste1.append(element)    
  print(liste1)
hinzufuegen()