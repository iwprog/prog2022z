# Leistungsaufgabe 3b
woerterbuch = {"Haus": "house", "Schule": "school", "Katze": "cat",
               "Hund": "dog", "Tisch": "table", "Eis": "ice",
               "Eins": "one", "Auto": "car", "Welt": "world", "Baum": "tree"}
counter = 0

for wort, word in woerterbuch.items():
    eingabe = input('Was heisst ' + wort + ': ')
    if eingabe == word:
        print('RICHTIG!')
        counter = counter + 1
    else:
        print('FALSCH!')
print('Sie haben', counter, 'von 10 Vokabeln richtig beantwortet!')