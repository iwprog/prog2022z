# dictionaries erstellen und manipulieren
# 2.1 a
preisliste = {'Brot':3.2, 'Milch':2.1, 'Orangen':3.75, 'Tomaten':2.2}
print(preisliste)

# b
preisliste['Milch']= 2.05
print(preisliste)

# c
del preisliste['Brot']
print(preisliste)

# d
preisliste.update({'Tee':4.2, 'Peanuts': 3.9, 'Ketchup':2.1})
# d alternative
preisliste['Tee'] = 4.2
preisliste['Peanuts'] = 3.9
preisliste['Ketchup'] = 2.1

print(preisliste)

# e
liste = {}
 
while True:    
    lebensmittel = input("Bitte geben Sie ein Lebensmittel ein: ")      
    if lebensmittel == "x":
        break
    preis = input('Bitte geben Sie einen Preis ein: ')
    # auf dieser Linie, damit es abbricht nach Eingabe von x
    # und nicht noch nach dem preis fragt
    liste[lebensmittel] = preis # oder liste.update({Lebensmittel : Preis})
print(liste)
    

