from turtle import *
pencolor("black")
pensize(5)
speed(8)

def rahmen(laenge, breite):
    for count in range(2):
        fillcolor("white")
        begin_fill()
        fd(laenge)
        lt(90)
        fd(breite)
        lt(90)
        end_fill()

    penup()
    fd(laenge/4)
    lt(90)
    fd(breite/4)
    rt(90)
    pendown()

    for count in range(2):
        fillcolor("white")
        begin_fill()
        fd(laenge/2)
        lt(90)
        fd(breite/2)
        lt(90)
        end_fill()
rahmen(100,100)