# Übungsbeispiele Dictionaries 3a
# Übungsbeispiele Dictionaries 3a
alphabet = {'A':'Alfa','B':'Bravo','C':'Charlie','D':'Delta','E':'Echo','F':'Foxtrot','G':'Golf','H':'Hotel','I':'India','J':'Juliett','K':'Kilo','L':'Lima','M':'Mike','N':'November','O':'Oscar','P':'Papa','Q':'Quebec','R':'Romeo','S':'Sierra','T':'Tango','U':'Uniform','V':'Victor','W':'Whiskey','X':'X-ray','Y':'Yankee','Z':'Zulu'}
eingabe = input('Welches Wort soll ich buchstabieren: ')
liste2 = []

def icao(inputwort): # wird gefüllt mit eingabe
    inputwort_neu = inputwort.replace("ä", "ae")
# Beim eingegebenen Wort (inputwort) wird 'ä' mit 'ae' ersetzt und wird in neuer Variabel gespeichert (inputwort_neu)
    inputwort_neu = inputwort_neu.replace("ö", "oe") # bestehende Variabel (inputwort_neu) wird überschrieben
    inputwort_neu = inputwort_neu.replace("ü", "ue")
    
    for buchstabe in inputwort_neu: # man kann jetzt die Abfrage in inputwort_neu machen...
# ... in dieser Variable ist das Wort gespeichert, in dem die Umlaute ersetzt worden sind, weil es mit der ursprünglichen Eingabe verknüpft ist
# wenn die Eingabe (ü) in inputwort_neu ist, ersetze mit 'ue', mach es gross und füge es zur neuen liste hinzu
        buchstabe = buchstabe.upper()
        if buchstabe in alphabet.keys(): # or direkt liste.append(icao_alphabet[buchstabe]) ohne diese Zeile
            liste2.append(alphabet[buchstabe])
    for i in liste2:
        print(i, end='-') 
icao(eingabe)
