listeAllIndex = [] # leere neue Liste
def allIndex(sequenz, element): # ('Chur', 'l')
    for position, buchstabe in enumerate(sequenz): # 'Chur' nummerieren
        if buchstabe == element: # wenn ein Buchstabe von 'Chur' == 'l' ist...
            listeAllIndex.append(position) # ... füge die Position in die Liste hinzu
    print(listeAllIndex) # ... und gib es aus
 
allIndex('Chur','l')
allIndex(['H','a','l','l','o',0,0,7],'l')
allIndex('Chur','r') # ergibt [2,3,3], weil Zeile 9 übernommen wird und angehänkt wird