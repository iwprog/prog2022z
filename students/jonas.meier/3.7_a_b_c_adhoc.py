# a
staedte = {'Zürich': 370000, 'Genf': 190000, 'Chur': 39000}

for stadt, einwohnerzahl in staedte.items():
    print(stadt, 'hat', einwohnerzahl, 'Einwohner')

# alternative
# for stadt in staedte:
#     print(stadt, "hat", staedte[stadt], "Einwohner")
# print("Sortiert:")
# for stadt in sorted(staedte):
#     print(stadt, "hat", staedte[stadt], "Einwohner")

# b
for stadt, einwohnerzahl in sorted(staedte.items()):
    print(stadt, "hat", einwohnerzahl, "Einwohner")

# c -> Sortierung nach den Einwohnern
sortierliste = []
for stadt, einwohnerzahl in staedte.items():
    sortierliste.append([einwohnerzahl, stadt])

for einwohnerzahl, stadt in sorted(sortierliste):
    print(einwohnerzahl, 'Einwohner hat', stadt)
