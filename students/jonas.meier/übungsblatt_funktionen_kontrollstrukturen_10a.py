def menue():
    print("======================================")
    print("Programmübersicht:")
    print(1, "...", "Preis für Fahrkarte berechnen")
    print(2, "...", "Herzfrequenz berechnen")
    print("")
    print(0, ". Programm beenden")
    print("======================================")
    

# Herz
def herzfrequenz():
    alter = input("Alter: ")
    return 220 - int(alter)

# SBB
def get_billet_preis():
    alter = input("Alter: ")
    km = input("Km: ")
    if int(alter) < 6:
        return 0
    elif int(alter) < 16:
        return (2 + (0.25 * int(km))) / 2
    else:
        return 2 + (0.25 * int(km))

    
while True:
    menue()
    eingabe = int(input("Gewählte Option"))
    if eingabe == 1:
        print("Preis für Fahrkarte berechnen")
        print("______________________________")
        print("Preis:", get_billet_preis())
    elif eingabe == 2:
        print("Herzfrequenz berechnen")
        print("______________________________")
        print("Ihre max. Herzfrequenz ist:", herzfrequenz())
    elif eingabe == 0:
        break
    
    
