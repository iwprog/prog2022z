# aufgabe 2.1
def guten_morgen():
  print("Guten Morgen!")
guten_morgen()

# aufgabe 2.2
def guten_morgen(name):
    print("Guten Morgen "+name+"!")    
guten_morgen("Jonas")

# aufgabe 2.3 a
def guten_morgen():  
  gruss = input("Wie heisst du? ")  
  print("Guten Morgen", gruss + "!") 
guten_morgen()

# aufgabe 2.3 b -> Lösung ohne return
def rechteck(laenge, breite):
  flaeche = (laenge*breite)
  ergebnis = flaeche
  print("Die Fläche beträgt", ergebnis, "m2")
rechteck(10, 20)

# aufgabe 2.3 b -> Lösung mit return
def rechteck(laenge, breite):
  return laenge*breite
ergebnis = rechteck(10, 20)
print("Die Fläche beträgt", ergebnis, "m2")

# aufgabe 2.8 a -> mit Anzahl
i = 1
anzahl = 10
while i <= anzahl:
  print(i)
  i = i + 1
  
# aufgabe 2.8 a -> Anzahl integriert
i=1
while i <= 10:
    print(i)
    i = i + 1

# aufgabe 2.8 c
i = 10
while i > 0:
    print(i)
    i = i - 3
if i<0:
    print(0)