# Übungsbeispiele 5 erweitert
# erweitern sie das programm aus der letzten hausübung, sodass es sich die bereits eingegebenen berge merkt.
from json import loads, dumps
berge = {}

try: # wenn "berge.json" existiert, sollen die berge aus der datei eingelesen werden.
# Folienteil: Daten wieder einlesen
    with open('berge.json', encoding='utf8') as f: # öffnen und einlesen
        berge_str = f.read()
        berge = loads(berge_str) # umwandeln (Inhalt von berge.json ist jetzt in berge)

# wenn ich jetzt das Programm ausführe gibt es das berge JSON nicht -> abfangen mit except, try
except FileNotFoundError:
    print('Leider keine gespeicherte Bergliste vorhanden')
    # wenn es keines gibt, setzen wir berge auf Leer und erweitern es danach
    berge = {}
    
while True:
    berg = input("Berg (Abbruch mit 'x'): ")
    if berg == 'x':
      break
    hoehe = float(input("Höhe von " + berg + " (in m): "))
    berge[berg] = str(hoehe) + " m (" + str(round(hoehe * 3.28)) + 'ft)'
 
print()
 
for berg, hoehe in sorted(berge.items()):
    print(berg, 'ist', hoehe, 'hoch.')
    
# zu programmende sollen die berge in der datei "berge.json" gespeichert werden.
# Foliensatz: Daten speichern
with open('berge.json', 'w', encoding='utf8') as f:
    berge_str = dumps(berge)
    f.write(berge_str) # String in die Datei schreiben
