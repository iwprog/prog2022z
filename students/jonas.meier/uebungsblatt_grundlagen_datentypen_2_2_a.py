# über Dictionaries iterieren
# 2.2 a
preisliste = {'Brot':3.2, 'Milch':2.1, 'Orangen':3.75, 'Tomaten':2.2}

for lebensmittel, preis in preisliste.items():
    print(lebensmittel, 'kostet', preis, 'CHF')

# alternative
for item in preisliste:
    print(item, 'kostet', preisliste[item], 'CHF')