from turtle import *
speed(10)

# Turtle verstecken
hideturtle()
 
fillcolor("red")
pencolor("red")
 
# Quadrat
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
end_fill()
 
# versteckt an Ausgangsposition für Kreuz gehen
penup()
left(90)
forward(50)
left(90)
forward(150)
right(90)
pendown()

# Kreuz malen
fillcolor("white")
 
begin_fill()
forward(100)
right(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
forward(100)
left(90)
forward(100)
end_fill()
