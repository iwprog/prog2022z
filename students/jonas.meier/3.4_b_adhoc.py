liste = [['Zürich', '370000'], ['Genf', '190000'], ['Basel', '170000'], ['Bern', '130000']] 
for ort, einwohner in liste:
  print(ort, "hat", einwohner, "Einwohner") 
 
# Alternative Variante
 
liste = [["Zürich", 370000], ["Genf", 190000], ["Basel", 170000], ["Bern", 130000]]
for ort, einwohner in enumerate(liste):
    print(liste[ort][0], "hat", liste[ort][1], "Einwohner")
 
# Alternative nummerierte Variante
 
liste = [["Zürich", 370000], ["Genf", 190000], ["Basel", 170000], ["Bern", 130000]]
for nr, (ort, anzahl) in enumerate(liste, start = 1):
    print(str(nr) +'.', ort, 'hat', anzahl, 'Einwohner')
