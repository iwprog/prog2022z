# listen 5 (remove stopwords)

l_input = 'Heinz war heute in den Bergen. Es war eine lange Wanderung'
stopwords = ['der', 'die', 'das', 'in', 'auf', 'unter', 'ein', 'eine', 'ist', 'war', 'es', 'den']
liste2 = []

def stopword_filter(l_input, stopwords):
    for element in l_input.split():
        if element.lower() not in stopwords:
# != geht nicht bei Listen. Man kann nicht ein Element mit einer Liste vergleichen.
            liste2.append(element)
    return liste2
print(stopword_filter(l_input, stopwords))
            