# Zeile 10 ergibt None???
 
liste_1 = ['der', 'junge', 'ist', 'sehr', 'freundlich']
liste_2 = []
 
def filter_list():
  for i in liste_1:
    if len(i) >= 4:
      liste_2.append(i)
print(liste_2, filter_list())
# ['junge', 'sehr', 'freundlich'] ist Ergebnis von liste_2
# None von filter_list weil nichts ausgegeben wird
# weil die Funktion in einem Print innen aufgerufen wird, gibt es den return aus.
# in der Funktion ist aber kein return definiert. deshalb gibt es zusätzlich none aus
# Funktion darf nur ins print genommen werden, wenn man mit return schafft, sonst hat man das None
 
# ohne None (Zeile 21 und 22)
 
liste1 = ["der", "Junge", "ist", "sehr", "freundlich"]
liste2 = []
 
def funktion():
  for element in liste1:
    if len(element) >= 4:
      liste2.append(element)
  print(liste2)  
funktion()