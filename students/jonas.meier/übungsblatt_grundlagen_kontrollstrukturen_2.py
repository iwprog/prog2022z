# 2.5 b

def alterspruefung():
    alter = input("Alter eingeben: ")
    if int(alter) <= 0:
        print("ungeboren")
    elif int(alter) <= 6:
        print("geschäftsunfähig")
    elif int(alter) <= 14:
        print("unmündig")
    elif int(alter) < 18:
        print("unmündig minderjährig")
    elif int(alter) >= 18:
        print("volljährig")
alterspruefung()

# 2.7 c

i = 1
for zeichen in "Dampfschiff":
    print(str(i)+ ".", zeichen)
    i = i + 1

# 2.8 d

for i in range(10, -1, -1):
    print(i)
    if i == 0:
        print("start")    

# 2.8 f

lauf = 1 # die durchnummerierten Zahlen
i = 0 # startet bei Null
while i < 10:
    zahl = input("Bitte geben Sie Zahl "+str(lauf)+" ein: ")
    i = i + int(zahl)
    lauf = lauf + 1
print(i)

# 2.8 g

lauf = 1 # die durchnummerierten Zahlen
i = 0 # startet bei Null
zahl = input("Bitte geben Sie Zahl "+str(lauf)+" ein: ")

while zahl != "-1":
    i = i + int(zahl)
    lauf = lauf + 1
    zahl = input("Bitte geben Sie Zahl "+str(lauf)+" ein: ") 
print(i)       
    