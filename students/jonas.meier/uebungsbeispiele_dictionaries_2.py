# Übungsbeispiele Dictionaries 2

l_input = "Der Tag begann sehr gut! Der Morgen war schön."
lookup = {} # jedes Wort soll in diesem lookup vorkommen mit dem Count 1

 
def word_stat(l_input):
  for wort in l_input.split(): # Einzelwörter ausgeben (Text in Wörter teilen)
    if wort in lookup:
# wenn Wort schon da -> Count um 1 erhöhen 
      lookup[wort] = lookup[wort] + 1  
    else:
# wenn Wort noch nicht da ist -> hinzufügen,  
      lookup[wort] = 1
  print(lookup)
word_stat(l_input)
