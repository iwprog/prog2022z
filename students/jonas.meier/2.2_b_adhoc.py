# Lösung mit Rückgabewert
from turtle import *
 
def dreieck():
  seitenlaenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
  begin_fill()
  forward(seitenlaenge)
  left(120)
  forward(seitenlaenge)
  left(120)
  forward(seitenlaenge)
  end_fill()
 
pencolor("red")
pensize(5)
speed(8)
right(90)
 
fillcolor("cyan")
dreieck()
 
fillcolor("yellow")
dreieck()
 
fillcolor("lime")
dreieck()

# Lösung mit Parameter
# from turtle import *
 
# def dreieck(seitenlaenge):
  # begin_fill()
  # forward(seitenlaenge)
  # left(120)
  # forward(seitenlaenge)
  # left(120)
  # forward(seitenlaenge)
  # end_fill()
 
# pencolor("red")
# pensize(5)
# speed(8)
# right(90)
 
# fillcolor("cyan")
# dreieck(100)
 
# fillcolor("yellow")
# dreieck(200)
 
# fillcolor("lime")
# dreieck(50)