# 6
# ohne Optionen
berge = {}

for i in range(3):
    berg = input('Berg: ')
    hoehe = float(input("Höhe von " + berg + " (in m): "))
    gebirge = input('Gebirge:')
    berge[berg] = {'Höhe': str(hoehe) + " m (" + str(round(hoehe * 3.28)) + 'ft)', 'Gebirge': gebirge}
    
print() # macht einen Absatz

for berg, details in sorted(berge.items()):
    print(berg, 'ist', details['Höhe'], 'hoch und gehört zum', details['Gebirge'],'Gebirge.')