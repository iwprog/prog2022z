from turtle import *
speed(10)
hideturtle()
pensize(5)
 
# Eingabefeld für Rückgabe
laenge = numinput("Abfragedialog", "Bitte geben Sie die gewünschte Seitenlänge ein")
 
fillcolor("red")
pencolor("red")
 
# Quadrat
begin_fill() 
forward(laenge)
left(90) 
forward(laenge)
left(90) 
forward(laenge)
left(90) 
forward(laenge)
end_fill()
 
# Unsichtbar zum Ausgangspunkt für das Kreuz
penup() 
left(90) 
forward(laenge/8)
left(90) 
forward(laenge/2.7)
right(90)
 
# Kreuz
fillcolor("white")
 
begin_fill()
forward(laenge/4)
right(90) 
forward(laenge/4)
left(90) 
forward(laenge/4)
left(90) 
forward(laenge/4)
right(90) 
forward(laenge/4)
left(90) 
forward(laenge/4)
left(90) 
forward(laenge/4)
right(90) 
forward(laenge/4)
left(90) 
forward(laenge/4)
left(90) 
forward(laenge/4)
right(90) 
forward(laenge/4)
left(90) 
forward(laenge/4)
end_fill() 
