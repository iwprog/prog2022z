# Übungsbeispiele Dictionaries 3a
alphabet = {'A':'Alfa','B':'Bravo','C':'Charlie','D':'Delta','E':'Echo','F':'Foxtrot','G':'Golf','H':'Hotel','I':'India','J':'Juliett','K':'Kilo','L':'Lima','M':'Mike','N':'November','O':'Oscar','P':'Papa','Q':'Quebec','R':'Romeo','S':'Sierra','T':'Tango','U':'Uniform','V':'Victor','W':'Whiskey','X':'X-ray','Y':'Yankee','Z':'Zulu'}
eingabe = input('Welches Wort soll ich buchstabieren: ')
liste2 = []

for buchstabe in eingabe:    
    buchstabe = buchstabe.upper()
# wandelt buchstabe in Grosschreibung um, weil Alphabet grossgeschrieben ist
    if buchstabe in alphabet.keys():
        liste2.append(alphabet[buchstabe]) # füge von Liste alphabet den value hinzu
    else:
        print('Kann', eingabe, 'nicht buchstabieren, da,', buchstabe, 'nicht definiert wurde')
        break
print(liste2)