# 5
berge = {}

for i in range(3):
    berg = input('Berg: ')
    hoehe = float(input("Höhe von " + berg + " (in m): "))
    berge[berg] = str(hoehe) + " m (" + str(round(hoehe * 3.28)) + 'ft)'
    #                 1000 m        (1640 ft) -> + ist wegen str (keine Summe gemeint)
    # berge[berg] = hoehe -> in Dict Berge berg = Wert hoehe setzen
print()# macht einen Absatz
for berg, hoehe in sorted(berge.items()):
    print(berg, 'ist', hoehe, 'hoch.')

# Variante mit eingegebenen Werten
# berge = {'Mt. Everest': 8848, 'Matterhorn': 4478, 'Dufourspitze': 4634}
#  
# def funktion(hoehe):
#   count = 1  
#   for berg, hoehe in sorted(berge.items()):
#     feet = round(hoehe * 3.28)
#     print(str(count) +'.', berg, 'ist', hoehe,'m','(' + str(feet) + ' ft)','hoch.')
#     count = count + 1    
# funktion(berge)