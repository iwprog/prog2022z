liste = ["Luzern", "Stern", "Eins", "Keins", "Peter", "Meter", "Zwei", "Ei"] 
suffix = input("Bitte geben Sie einen Suffix ein: ")   
 
def suffix_filter(): 
    for element in liste: 
        if element[-len(suffix):].lower() == suffix:
        #if Peter [-len(er):] -> [-2:] == er
        # Wenn die letzten beiden Buchstaben er sind
            print(element) 
suffix_filter() 
 
# if element[:len(suffix)].lower() == suffix:
# if Luzern [:len(lu)] -> [:(2)] == lu
# nun könnte man lu eingeben, für Luzern Ausgabe
# wenn der Anfang vom Text == lu ist 
