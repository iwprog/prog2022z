# Leistungsaufgabe 3a
liste = []
 
while True:
  stadt = input("Geben Sie die Stadt ein: ")
  if stadt == 'x':
    break
  einwohner = input('Geben sie die zugehörige Einwohnerzahl ein: ')
  liste.append([stadt, einwohner])
print(sorted(liste))


# Alternative, wenn man ein Dict. befüllen müsste
liste = {}
 
while True:
  stadt = input("Geben Sie die Stadt ein: ")
  if stadt == 'x':
    break
  einwohner = input('Geben sie die zugehörige Einwohnerzahl ein: ')
  liste[stadt] = einwohner
print(sorted(liste.items()))