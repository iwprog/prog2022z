# Übung Ein- und Ausgabe, erweitert

from urllib.request import urlopen # Webressource öffnen wie eine normale Datei 
from inscriptis import get_text # Inscriptis macht aus Quelltext eine Textrepräsentation

with urlopen('https://www.gutenberg.org/cache/epub/6079/pg6079.txt') as source:
    text = source.read().decode('utf8') # decode: Daten in Strings konvertieren
print(text)
print("Himmel:", text.count("Himmel"))
print("Freiheit:", text.count("Freiheit"))
print("Spiel:", text.count("Spiel"))
print("Tanz:", text.count("Tanz"))
