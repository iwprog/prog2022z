# Text Statistiken
# Programm erweitern, dass es die Ressourcenbezeichnung mittels input holt und je nach Bezeichnung
# die Daten aus dem Web oder einer Datei holt

from urllib.request import urlopen
from csv import writer
 
# URL oder gespeicherte Datei eingeben, welche dann in den beiden CSV landet
ressource = input("Ressourcenbezeichnung: ")
 
if "https://" in ressource:
    with urlopen(ressource) as source:
        web_content = source.read().decode("utf8")
        worthaeufigkeit = {}
        buchstabenhaeufigkeit = {}
        for wort in web_content.split():
            if wort in worthaeufigkeit:
                worthaeufigkeit[wort] = worthaeufigkeit[wort] + 1
            else:
                worthaeufigkeit[wort] = 1
        for buchstabe in list(web_content):
            if buchstabe in buchstabenhaeufigkeit:
                buchstabenhaeufigkeit[buchstabe] = buchstabenhaeufigkeit[buchstabe] + 1
            else:
                buchstabenhaeufigkeit[buchstabe] = 1
else:
#     try:
        with open(ressource, encoding="utf8") as datei:
            datei_inhalt = datei.read()
            worthaeufigkeit = {}
            buchstabenhaeufigkeit = {}
            for wort in datei_inhalt.split():
                if wort in worthaeufigkeit:
                    worthaeufigkeit[wort] = worthaeufigkeit[wort] + 1
                else:
                    worthaeufigkeit[wort] = 1
            for buchstabe in list(datei_inhalt):
                if buchstabe in buchstabenhaeufigkeit:
                    buchstabenhaeufigkeit[buchstabe] = buchstabenhaeufigkeit[buchstabe] + 1
                else:
                    buchstabenhaeufigkeit[buchstabe] = 1
#     except FileNotFoundError:
#         print(f"Keine Datei mit Name {ressource} gefunden.")
#         worthaeufigkeit = {}
#         buchstabenhaeufigkeit = {}

with open("wort.csv", "w", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    for wort, haeufigkeit in worthaeufigkeit.items():
        csv_writer.writerow([wort, haeufigkeit])
 
with open("buchstabe.csv", "w", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    for buchstabe, haeufigkeit in buchstabenhaeufigkeit.items():
        csv_writer.writerow([buchstabe, haeufigkeit])

