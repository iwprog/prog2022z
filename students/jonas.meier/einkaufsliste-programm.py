from csv import writer
from json import loads, dumps 
 
einkaufsliste = []
 
# (1) Artikel hinzufügen
def artikel_hinzufügen():
  while True:
    artikel = input("Neuer Artikel eingeben (Abbrechen mit 'x'): ")
    if artikel == 'x':
      break
    preis = float(input('Preis eingeben: '))
    menge = float(input('Menge eingeben: '))
    einkaufsliste.append({'Artikel': artikel, 'Preis': preis, 'Menge': menge})
 
# (2) Artikel löschen
def artikel_loeschen():
  while True:
    loeschen = input("zu löschender Artikel eingeben (Abbrechen mit 'x'): ")
    if loeschen == 'x':
      break
    for zahl, eintrag in enumerate(einkaufsliste):
# [{"Artikel": "Orange", "Preis": 1.2, "Menge": 2.0}, {"Artikel": "Birne", "Preis": 1.2, "Menge": 2.0}]
      if eintrag['Artikel'] == loeschen:
        del einkaufsliste[zahl]  
 
# (3) Artikel suchen
def artikel_suchen():
  suche = input("Nach welchem Artikel wollen Sie suchen? ")
  for eintrag in einkaufsliste:
    if suche in eintrag['Artikel']:
      print(eintrag['Artikel'])
 
# (4) Einkaufsliste leeren -> unten im Menü
    
# (5) Einkaufsliste im csv format exportieren
# Foliensatz: Ausgabe von Listen und Tupeln als CSV Datei
def einkaufsliste_csv():
  with open('einkaufsliste.csv', 'w', encoding='utf8') as f:
    csv_writer = writer(f, delimiter=';')
    for element in einkaufsliste:
      csv_writer.writerow(element.values()) # von Artikel;Preis;Menge die Values 
  
# (6) Gesamtbetrag einkauf berechnen
def gesamtbetrag_einkauf_berechnen():
  summe = 0
  for element in einkaufsliste:
    preis = element['Preis'] * element['Menge']
    summe = summe + preis
  print('Summe: ', summe) # alternativ print(f'Summe: {summe} CHF')  
    
# (0) Exit -> unten im Menü
  
# einkaufsliste soll mit programmende automatisch in der datei "einkaufsliste.json" gespeichert
# und beim öffnen des programms wiederhergestellt werden -> try, except mit Fortsetzung.
def exit():
  try:
    with open('einkaufsliste.json', encoding='utf8') as f:
      einkaufsliste_str = f.read()
      einkaufliste = loads(einkaufsliste_str) # umwandeln (Inhalt von einkaufsliste.json ist jetzt in einkaufliste)
# wenn ich jetzt das Programm ausführe gibt es einkaufsliste JSON nicht -> abfangen mit except, try
  except FileNotFoundError:
    print("Leider keine gespeicherte Einkaufsliste vorhanden.")
    einkaufsliste = [] # wenn es noch kein file mit dieser Liste gibt, erstelle eine neue Leere
# der Rest muss am Schluss folgen
 
 
# Menü 
while True:
  print()
  print("Menü:")
  print("(1) Artikel hinzufügen")
  print("(2) Artikel löschen")
  print("(3) Artikel suchen")
  print("(4) Einkaufsliste leeren")
  print("(5) Einkaufsliste in CSV Format exportieren")
  print("(6) Gesamtbetrag des Einkaufs berechnen")
  print("(0) Exit")
  print()
  aktion = int(input("Was wollen sie tun? "))
  if aktion == 1:
    artikel_hinzufügen()
  elif aktion == 2:
    artikel_loeschen()
  elif aktion == 3:
    artikel_suchen()
  elif aktion == 4:
    einkaufsliste = []
  elif aktion == 5:
    einkaufsliste_csv()
  elif aktion == 6:
    gesamtbetrag_einkauf_berechnen()
  elif aktion == 0:
    break
  else:
    continue
 
# Fortsetzung -> Daten speichern
  with open('einkaufsliste.json', 'w', encoding="utf8") as f:
    einkaufsliste_str = dumps(einkaufsliste)
    f.write(einkaufsliste_str)
