numbers = [12, 23, 5, 56, 77, 18, 9]

max_value = max(numbers)
print("Maximalwert", max_value)
 
min_value = min(numbers)
print("Minimalwert", min_value)
 
print("Durchschnitt:", sum(numbers)/len(numbers))

# oder mit den Mitteln die wir gelernt haben

def zahlenreihe(z1, z2, z3, z4, z5, z6, z7):
    maximum = (max(z1, z2, z3, z4, z5, z6, z7))
    print("Max: " + str(maximum))
    minimum = (min(z1, z2, z3, z4, z5, z6, z7))
    print("Min: " + str(minimum))
    summe = (z1 + z2 + z3 + z4 + z5 + z6 + z7)
    durchschnitt = round((summe/7), 2)
    return "Durchschnitt: " + str(durchschnitt)

print(zahlenreihe(12, 23, 5, 56, 77, 18, 9))




