# 3.2a
telefonbuch = [{"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"},
               {"Vorname": "Tim", "Nachname": "Kurz", "Phone": "732"},
               {"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"}]

def datensatz_ausgeben(telefonbuch):
    print('Vorname:', telefonbuch[0]['Vorname'])
    print('Nachname:', telefonbuch[0]['Nachname'])
    print('Phone:', telefonbuch[0]['Phone'])
datensatz_ausgeben(telefonbuch)

# alternative 
def datensatz_ausgeben():
    for count, eintrag in enumerate(telefonbuch):
# count ist 0,1,2 (für die Dictionaries)
# eintrag sind die Inhalte der Dicts ('Vorname': 'Ann', ...)
        if count == 0:
            print("Vorname:", eintrag["Vorname"])
            print("Nachname:", eintrag["Nachname"])
            print("Phone:", eintrag["Phone"])
datensatz_ausgeben()