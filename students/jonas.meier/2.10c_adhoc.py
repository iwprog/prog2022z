from turtle import *
speed(8)

laenge = 100
abstand = 40

def dreieck(laenge, abstand):
    for i in range(3):
        fd(laenge)
        lt(120)
    penup()
    fd(laenge)
    fd(abstand)
    pendown()
        
anzahl_dreiecke = int(input("Anzahl Dreiecke:"))
anzahl_reihen = int(input("Anzahl Reihen:"))

for i in range(anzahl_reihen):
    for i in range(anzahl_dreiecke):
        dreieck(laenge, abstand)
    #das eröffnet eine neue Reihe:
    penup()
    hideturtle()
    back((laenge+abstand)*anzahl_dreiecke)
    right(90)
    forward(laenge)
    left(90)
    showturtle()
    pendown()

                    