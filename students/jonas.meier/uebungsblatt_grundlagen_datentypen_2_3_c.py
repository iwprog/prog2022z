# Dictionaries filtern
# 2.3 c

# Lösung ohne .item
preisliste1 = {'Brot': 3.2, 'Milch': 2.05, 'Orangen': 3.75, 'Tomaten': 2.2, 'Tee': 1.5, 'Peanuts': 3.9, 'Ketchup': 2.1}  
preisliste2 = {}   
 
for item in preisliste1: 
  if item.find('en') < 0: # Pärchen in p1 abfragen wo -1 ist
# > (umgekehrt) würde Orangen, Tomaten ausgeben, weil bei Orangen 'en' an 5ter Stelle ist
# > 5 wäre entsprechend {}
# befülle neue Liste p2 mit Pärchen aus p1 
    preisliste2[item] = preisliste1[item]      
print(preisliste2) 
 
# Lösung mit .item
preisliste3 = {}
for lebensmittel, preis in preisliste1.items():
# wenn in den Lebensmitteln 'en' nicht gefunden wird (-1)
    if lebensmittel.find('en') < 0:
# befülle die Lebensmittel in P3 mit preis von Schlaufe (p1)
        preisliste3[lebensmittel] = preis  
print( preisliste3)

# Lösung mit -1 
preisliste3 = {} 
 
for lebensmittel, preis in preisliste1.items(): 
    if lebensmittel.find('en') == -1: 
        preisliste3[lebensmittel] = preis    
print( preisliste3) 
 
# Lösung mit Count
preisliste4 = {}
 
for item in preisliste1:
# wenn Häufigkeit von 'en' in item == 0 ist (nicht vorkommt)
    if item.count("en") == 0:
# dann befülle die neue Liste p4 mit den Pärchen aus alter Liste p1 
        preisliste4[item] = preisliste1[item]
print(preisliste4)

