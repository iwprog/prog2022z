l_input = "Heinz war heute in den Bergen. Es war eine lange Wanderung"
stopwords = ("der", "die", "das", "in", "auf", "unter", "ein", "eine", "ist", "war", "es", "den")

text = (l_input.lower()).split()
#print(text)

def stopword_filter():
    text_ohnestopwords = []
    for word in text:
        if word in stopwords:
            continue
        else:
            text_ohnestopwords.append(word)
            #print(word)
    return(text_ohnestopwords)

print(stopword_filter())

