position = 1
raum_liste = ["Raum 1: Schmutzige Strasse. Viel KAUGUMMI liegt auf dem Boden. Kann man sie benutzen?" ,
              "Raum 2: START" ,
              "Raum 3: Sie befinden sich auf einem Waldweg. Am Wegrand steht ein altes MOTORRAD. Es funktioniert nicht.",
              "Raum 4: Sie sind auf einer Tankstelle. Da liegt es eine Kanne mit dem Schild 'BENZIN zu mitnehmen'."]
bewegung = " "
gegenstand = None
aufgabe = None

print(raum_liste[position])

while bewegung != "q":
    bewegung = input("Schreiben Sie links, rechts, nimm *, benutze * mit *.").lower()

    if bewegung == "links":
        if position != 0:
            position = position - 1
            print(raum_liste[position])
        else:
            print("Das geht nicht. Wähle rechts, nimm *, benutze * mit *.")
        
    elif bewegung == "rechts":
        if position != 3:
            position = position + 1
            print(raum_liste[position])
        else:
            print("Das geht nicht. Wähle links, nimm *, benutze * mit *.")
 #NIMM   
    elif bewegung == "nimm kaugummi":
        if position != 0:
            print("Kein Kaugummi hier.")
        else:
            gegenstand = "kaugummi"
            print("Kaugummi genommen.")
            
    elif bewegung == "nimm motorrad":
        if position != 2:
            print("Kein Motorrad hier.")
        else:
            print("Motorrad kannst du nicht mitnehmen.")
    elif bewegung == "nimm benzin":
        if position != 3:
            print("Kein Benzin hier.")
        else:
            gegenstand = "benzin"
            print("Benzin genommen.")
            
# BENUTZE MIT
    elif bewegung == "benutze kaugummi mit motorrad":
        if gegenstand == "kaugummi" and position == 2 and aufgabe == "motor aufgetankt":
            print("Du hast den Reifen mit Kaugummi repariert und Motor aufgetankt. Gute Fahrt. Ende")
            break
        elif gegenstand == "kaugummi" and position == 2:
            aufgabe = "motor repariert"
            print("Du hast den Reifen mit Kaugummi repariert.")
        else:
            print("Das geht leider nicht. Probier noch einmal.")
            
    elif bewegung == "benutze benzin mit motorrad":
        if gegenstand == "benzin" and position == 2 and aufgabe == "motor repariert":
            print("Du hast den Reifen mit Kaugummi repariert und Motor aufgetankt. Gute Fahrt. Ende")
            break
        elif gegenstand == "benzin" and position == 2:
            aufgabe = "motor aufgetankt"
            print("Du hast den Motorrad mit Benzin gefullt.")
      
        else:
            print("Das geht leider nicht. Probier noch einmal.")

    else:
        print("error")