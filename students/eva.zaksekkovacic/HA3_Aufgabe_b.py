#Beispiel 2 (herzfrequenz)
def berechne_herzfrequenz(alter):
    return (220 - alter)

print(berechne_herzfrequenz(25))

#Beispiel 3 (billet preis)
def get_billet_preis(alter, km):
    preis = 2 + (0.25 * km)
    if alter < 6:
        preis = 0
    if alter >= 6 and alter < 16:
        preis = (preis * 0.5)
    return preis

print(get_billet_preis(3,200))
print(get_billet_preis(8,200))
print(get_billet_preis(30,200))

#Beispiel 6

from turtle import*

def zeichne_rahmen(laenge, breite, farbe1, farbe2):
    pencolor(farbe1)
    fillcolor(farbe2)
    begin_fill()
    
    for i in range(4):
        forward(laenge)
        left(90)
        
    penup()
    forward(breite)
    left(90)
    forward(breite)
    right(90)
    pendown()
    
    for i in range(4):
        forward(laenge - (2 * breite))
        left(90)
        
    penup()
    right(90)
    forward(breite)
    right(90)
    forward(breite)
    pendown()
        
    end_fill()
    
zeichne_rahmen(100, 25, "black", "white")

# Aufgabe 11 e
