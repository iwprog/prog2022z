#a Erstellen Sie ein Dictionary für den folgenden Datensatz:
#Vorname → Ana, Nachname → Skupch, Phone → 123.

person = {
    "Vorname": "Ana",
    "Nachname": "Skupch",
    "Phone": 123
}

#b Fügen Sie den Datensatz der Liste Telefonbuch hinzu.
telefonbuch = [person]

#c Fügen Sie die folgenden beiden Datensätze der Liste hinzu: Vorname → Tim,
#Nachname → Kurz, Phone → 732; Vorname → Julia, Nachname → Lang, Phone → 912

person = {
    "Vorname": "Tim",
    "Nachname": "Kurz",
    "Phone": 732
}

telefonbuch.append(person)

person = {
    "Vorname": "Julia",
    "Nachname": "Lang",
    "Phone": 912
}
telefonbuch.append(person)

print(telefonbuch)