# Text schwärzen: Das Programm soll einen Eingabetext und eine Liste von Begriffen entgegennehmen
# ; in der Ausgabe sollen alle Begriff durch Sterne überschrieben werden. Dabei sollen erneut Gross-
# und Klein- schreibung ignoriert werden.
#   Eingabetext: David Mayer von der HTW hat um 9:00 seine
#                Wohnung in Chur verlassen.
#   Blacklist: david, mayer, htw, chur
#   Ausgabe: ***** ***** von der *** hat um 9:00 seine
#            Wohnung in **** verlassen.

eingabetext = ((input("Eingabetext: ")).split(" "))
# text = eingabetext.split(" ")
blacklist = input("Blacklist: ")
ausgabe = []

for wort in eingabetext:
    if wort.lower() in blacklist.lower():
        ausgabe.append(len(wort) * "*")
        
    else:
        ausgabe.append(wort)

satz = " ".join(ausgabe)
print("Ausgabe: ", satz)

    