#a Preisliste so ausgeben: Brot kostet .. chf

Preisliste = {'Milch': 2.05, 'Orangen': 3.75, 'Tomaten': 2.2, 'Tee': 4.2, 'Peanuts': 3.9, 'Ketchup': 2.1}
for key, value in Preisliste.items():
    print(key + " kostet " + str(value) + "CHF")


#e e. Geben Sie nur Lebensmittel mit einem Preis < 2.0 CHF aus.
print("-------------------")

for key, value in Preisliste.items():
    if value < 2.0:
        print(key + " kostet " + str(value) + "CHF")