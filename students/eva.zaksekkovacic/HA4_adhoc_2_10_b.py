from turtle import *

def dreieck():
    fillcolor("blue")
    begin_fill()
    for i in range(3):
        fd(50)
        lt(120)     
    end_fill()
    
def abstand():
    penup()
    fd(80)
    pendown() 

for i in range(int(numinput("Bitte wählen", "Wie viele Dreiecke möchten Sie?"))):
    dreieck()
    abstand()
   