from turtle import *

def dreieck():
    fillcolor("blue")
    begin_fill()
    for i in range(3):
        fd(50)
        lt(120)     
    end_fill()
    
def abstand():
    penup()
    fd(80)
    pendown()

for reihe_zahl in range(1, int(numinput("Bitte wählen", "Wie viele Reihen von Dreiecken möchten Sie?")) + 1):

    for dreieck_zahl in range(int(numinput("Bitte wählen", "Wie viele Dreiecken in Reihe " + str(reihe_zahl) + " möchten Sie?"))):
        dreieck()
        abstand()
        
    penup()
    goto(0, - 80 * (reihe_zahl))
    pendown()
    