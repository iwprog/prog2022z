# b) Erstellen Sie mittels einem Dictionary (dict) ein Mini-Deutsch-Englisch Wörterbuch
# (10 Vokabeln). Ein zugehörige Vokabeltrainer soll alle Vokabeln nach folgendem Muster abfragen:
# Was heisst Haus: house
# RICHTIG!
# Was heisst Zahnarzt: dentist
# RICHTIG!
# Was heisst Zahn: dent
# FALSCH!
# …
# Nach Abfrage aller Vokabeln wird noch die Abfragestatistik ausgegeben:
# Sie haben 7 von 10 Vokabeln richtig beantwortet!

worterbuch = {"Hund": "dog", "Katze": "cat", "Haus": "house", "Kind": "child",
              "Buch": "book", "Arbeit": "work", "Liebe": "love", "Schlaf": "sleep",
              "Eis": "ice cream", "Freude": "joy"}
richtige = []

for keys, values in worterbuch.items():
    
    print("Was heisst", keys + ":")
    wort = input()
    if wort == worterbuch[keys]:
        print("RICHTIG!")
        richtige.append(wort)
    else:
        print("FALSCH!")

print("Sie haben", len(richtige), "von 10 Vokabeln richtig beantwortet!")




