#Übungsblatt Grundlagen KOntrollstrukturen

#Aufgabe 2.1

def guten_morgen():
    print("Guten Morgen!")
guten_morgen()

#Aufgabe 2.2

def guten_morgen(name):
    print("Guten Morgen " + name + "!")
guten_morgen("Ana")

#Aufgabe 2.3 a

def guten_morgen(name):
    return "Guten Morgen " + name + "!"
gruss = guten_morgen("Ana")
print(gruss)

#Aufgabe 2.3 b

def flaeche_rechteck(laenge, breite):
    return laenge * breite
flaeche = flaeche_rechteck(10,20)
print("Die Fläche beträgt", flaeche, "m2")

#Aufgabe 2.8 a

i = 0
anzahl = 10
while i <= anzahl:
    print(i)
    i=i+1

#Aufgabe 2.8 c

i = 10
anzahl = 0
while i >= anzahl:
    print(i)
    i=i-3
    
#Aufgabe 2.8 c

i = 10
schritt = i/3
anzahl = 0
while i > 0:
    print(i)
    i=i-schritt

#ad hoc Übungen
    
#2.2
from turtle import *
laenge = numinput("Eingabefenster", "Bitte geben Sie die Seitenlänge an:") 
def dreieck(laenge):
    begin_fill()
    forward(laenge)
    left(120)
    forward(laenge)
    left(120)
    forward(laenge)
    end_fill()
      
pencolor("red")
pensize(5)
fillcolor("cyan")
right(90)
dreieck(laenge)

laenge = numinput("Eingabefenster", "Bitte geben Sie die Seitenlänge an:") 

pencolor("red")
pensize(5)
fillcolor("yellow")
dreieck(laenge)

laenge = numinput("Eingabefenster", "Bitte geben Sie die Seitenlänge an:")
pencolor("red")
pensize(5)
fillcolor("green")
dreieck(laenge)
reset()

#2.4 a
from math import pi

def volumen(radius, hoehe):
    return (radius * radius) * pi * hoehe

print("Das Volumen beträgt:" , volumen(2,2))

 