wort = input("Welches Wort soll ich buchstabieren: ").lower()
# print(wort)
icao = {"a": "Alpha", "b": "Bravo", "c": "Charlie","d": "Delta", "e": "Echo", "f": "Foxtrot", "g": "Golph","h": "Hotel",
           "i": "India", "j": "Juliett","k": "Kilo", "l": "Lima", "m": "Mike", "n": "November","o": "Oscar", "p": "Papa",
           "q": "Quebec", "r": "Romeo","s": "Sierra", "t": "Tango", "u": "Uniform", "v": "Victor","w": "Whiskey",
           "x": "X-Ray", "y": "Yankee","z": "Zulu"}    
# print(buchstabe)
# print(aussage.values())
wort_liste = []
icao_liste = []

# Liste von Buchstaben in wort = input

def verteilen():
    for position, letter in enumerate(wort):
        wort_liste.append(letter)
    return wort_liste

# Values von icao nehmen, die auch in wort_liste sind

def buchstabieren():
    verteilen()
#     print(wort_liste)
    for buchstabe in wort_liste:
        if buchstabe in icao.keys():
            icao_liste.append(icao[buchstabe])  
  
    return icao_liste

# Print von aussage_liste im Form eines Str

def zusammen():
    buchstabieren()
    text = ""
    for element in icao_liste:
        text = text + "-" + element
        
    print(text[1::])  # Weil ich das "-" am Anfang nicht haben will.  
          
zusammen()

