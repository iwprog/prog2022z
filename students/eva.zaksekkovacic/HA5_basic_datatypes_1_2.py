#1.2 c

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
for zeit in jahreszeiten:
    print(zeit, "-", zeit[-2:])
  
#1.2 d
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
for zeit in jahreszeiten:
    if zeit[-2:] == "er":
        print(zeit)