#1.1
#a
print("Name", "Vorname", "Strasse", "Hausnr.", "PLZ", "Wohnort")
#Unklar: Sollte ich das in mehrere Reihen schreiben?

#d
print ("Hello" [4])
print ("Hello" [3])
print ("Hello" [2])
print ("Hello" [1])
print ("Hello" [0])

#oder ??
text = "Hello"[::-1]
print(text)

#1.2
from turtle import *
#a
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)

reset()
#e
pensize(5)
pencolor("red")
forward(50)
left(120)
forward(50)
left(120)
forward(50)

pencolor("blue")

right(60)
forward(50)
right(120)
forward(50)
right(120)
forward(50)

pencolor("limegreen")
forward(50)
right(120)
forward(50)
right(120)
forward(50)
right(270)

reset ()

#1.3
from turtle import *
#b
pencolor("red")
pensize(5)

fillcolor("aqua")
begin_fill()
right(48)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

fillcolor("yellow")
begin_fill()
left(15)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

fillcolor("fuchsia")
begin_fill()
left(15)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

fillcolor("blue")
begin_fill()
left(15)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

fillcolor("lime")
begin_fill()
left(15)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()
left(65)

reset()

#1.5
#a
from turtle import *

laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")


pencolor("red")
pensize(5)

fillcolor("yellow")
begin_fill()
left(150)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

#länge anpassen
laenge = laenge / 2

fillcolor("teal")
begin_fill()
right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

#länge anpassen
laenge = laenge / 2

fillcolor("green")
begin_fill()
right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

reset()

#b
from turtle import *

laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")


pencolor("red")
pensize(5)

fillcolor("teal")
begin_fill()
left(45)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
end_fill()

#länge anpassen
laenge = laenge * 0.8

fillcolor("yellow")
begin_fill()
right(165)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
end_fill()

#länge anpassen
laenge = laenge * 0.8

fillcolor("fuchsia")
begin_fill()
right(165)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
end_fill()

#länge anpassen
laenge = laenge * 0.8

fillcolor("blue")
begin_fill()
right(165)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
end_fill()

#länge anpassen
laenge = laenge * 0.8

fillcolor("green")
begin_fill()
right(165)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
end_fill()
right(90)