# a. Erstellen Sie eine Liste jahreszeiten mit den vier Jahreszeiten.
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]



#b Löschen Sie die Jahreszeit Frühling aus der Liste
jahreszeiten.remove("Frühling")


#c Fügen Sie die Jahreszeit "Langas" der Liste hinzu.
jahreszeiten.append("Langas")


# d Schreiben Sie ein Programm, welches Sie nach Namen fragt und
# diese so lange einer Liste hinzufügt, bis der Benutzer "x" eingibt.
#Im Anschluss soll die Liste ausgegeben werden

from turtle import *

namen_liste = []

name = None

while name != "x" and name != "X":
    name = textinput("Frage", "Schreiben Sie Ihre Name oder 'x' für exit.")
    
    if name == "x" or name == "X":
        print(namen_liste)
    
    else:
        namen_liste.append(name)
        
#   Ermöglicht auch die Namen wie 'X Æ A-12'.