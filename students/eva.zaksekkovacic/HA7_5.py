# Berge: Schreiben Sie ein Programm, welches für drei Berge deren Höhe in m entgegennimmt und
# diese anschliessend alphabetisch sortiert mit der Höhe in m und in Feet (ft; 1m =3.28 ft) ausgibt.
# 1. Berg: Mt. Everest 1. Höhe: 8848
# 2. Berg: ... 2. Ho ̈he: ...
# ....
# Mt. Everest ist 8848 m (29029 ft) hoch.

berge = []

print("Bitte, geben Sie den Namen und Höhe des Berges ein.")

x = 1
while x < 4:

    berg_name = input(str(x)+ ". Berg:")
    h = input(str(x)+ ". Höhe:")
    f = float(h) * 3.28
    
    berg = [berg_name, h, f]
    berge.append(berg)
    
    x = x + 1

#print(berge)
sortiert = sorted(berge)
# print(sortiert)


for berge in sortiert:
    print(berge[0], "ist", berge[1], "m ("+ str(berge[2])+")ft", "hoch.\n")

    
