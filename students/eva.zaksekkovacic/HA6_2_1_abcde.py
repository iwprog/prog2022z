# a. Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit
#denzugehörigenPreisenspeichert:Brot → 3.2, Milch → 2.1, Orangen → 3.75, Tomaten → 2.2.

#FLOAT OK???

Preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}
# print(Preisliste)

# b. Ändern Sie den Preis für Milch auf 2.05.
Preisliste["Milch"] = 2.05
#print(Preisliste)

#c Entfernen Sie den Eintrag für Brot.
del Preisliste["Brot"]
#print(Preisliste)

#d. Fügen Sie neue Einträge für Tee → 4.2, Peanuts → 3.9 und Ketchup → 2.1 hinzu.
#variante 1

# Preisliste.update({"Tee": 4.2})
# Preisliste.update({"Peanuts": 3.9})
# Preisliste.update({"Ketchup": 2.1})
# print(Preisliste)
#
# Variante 2
# Preisliste["Tee"] = 4.2
#usw
# print(Preisliste)
# 
# 
# #variante 3
Preise2 = {"Tee": 4.2, "Peanuts": 3.9, "Ketchup": 2.1}
#Preisliste.update(Preise2)
print(Preisliste)
# print(Preise2)

#e Schreiben Sie ein Programm, welches Sie nach Lebensmittel und den zu- gehörigen Preisen fragt
# und diese so lange einem Dictionary hinzufügt, bis der Benutzer "x" eingibt.
# Im Anschluss soll das Dictionary ausgegeben werden.

Lebensmittel = {}
print("Geben Sie bitte Lebensmittel und Preis oder 'x' für exit.")
while True:
    essen = input("Lebensmittel: ")
    if essen == "x":
        break
    else:
        preis = input("Preis: ")
        if preis == "x":
            Lebensmittel[essen] = None
            break
        else:
#             Lebensmittel[essen] = float(preis)
#------------------------
#             if preis.isdecimal():
#                 Lebensmittel[essen] = float(preis)
#             else:
#                 print("Nicht eine Nummer! Geben Sie bitte das Lebensmittel und Preis noch einmal ein.")
#---------------------------              
            try:
                val = float(preis)
                Lebensmittel[essen] = preis
            except ValueError:
                print("Nicht eine Nummer! Geben Sie bitte das Lebensmittel und Preis noch einmal ein.")

            # So kann man auch negative Nummer geben. Möglichkeit um ein Rabatt einzugeben oder so etwas?

print(Lebensmittel)

