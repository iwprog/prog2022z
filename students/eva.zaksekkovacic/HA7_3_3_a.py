# Entfernen Sie alle Datensätze aus dem Telefonbuch, bei denen
# eine "1" in der Telefonnummer vorkommt.

person = {
    "Vorname": "Ana",
    "Nachname": "Skupch",
    "Phone": "123"
}

telefonbuch = [person]

person = {
    "Vorname": "Tim",
    "Nachname": "Kurz",
    "Phone": "732"
}

telefonbuch.append(person)

person = {
    "Vorname": "Julia",
    "Nachname": "Lang",
    "Phone": "912"
}

telefonbuch.append(person)

telefonbuch_ohne1 = []

for person in telefonbuch:
    if "1" not in person["Phone"]:
        telefonbuch_ohne1.append(person)      

telefonbuch = telefonbuch_ohne1

print(telefonbuch)