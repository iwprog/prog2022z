from turtle import *

# Möglichkeit 1
alter = None
km = None
preis = 0
preis2 = None
wahl = None

def get_billet_preis(alter,km):
    preis = 2 + (0.25 * km)
    if alter < 6:
        preis = 0
    if alter >= 6 and alter < 16:
        preis = (preis * 0.5)
    return preis
    print(preis)

# Möglichkeit 2

def berechnet_eintrittspreis(karte, ermassigung, alter):
    if karte == "kurzzeitkarte":
        preis = 5
    elif karte == "nachmittagskarte":
        preis = 6
    elif karte == "tageskarte":
        preis = 10
    
    if ermassigung == True:
        preis = preis * 0.9

    if alter < 6:
         preis = 0
    elif alter <= 12:
        preis = preis * 0.5
        
    return preis

#Möglichkeit 3

def berechne_mahngebuehr(nr_mahnung,medium,brief):
  if nr_mahnung == 1:
    preis2 = 2 * medium
  elif nr_mahnung == 2:
    preis2 = 4 * medium
  elif nr_mahnung == 3:
    preis2 = 6 * medium
  if brief == True:
    preis2 = preis2 + 2
  return preis2


###################

while wahl != 0:
    print("======================================== \nProgramübersicht:\n 1 ... Preis für eine Fahrkarte berechnen \n 2 ... Eintritt für das Schwimmbad berechnen \n 3 ... Mahngebühr für die Bibliothek berechnen \n\n 0 ... Programm beenden \n ========================================")
    wahl = int(input("Gewählte Option:"))
    if wahl == 0:
        break
    elif wahl > 3:
        print("\nUngultige Option!")
        
    elif wahl == 1:
        print("\n\nPreis für Fahrkarte berechnen\n --------------------------------- \n")
        alter = int(input("Bitte geben Sie Ihr Alter ein: "))
        km = int(input("Wie viele km wollen Sie reisen? "))
        print("Die Fahrkarte kostet: " + str(get_billet_preis(alter,km)) + " CHF")
       
    elif wahl == 2:
        print("\n\nEintritt fürs Schwimmbad berechnen\n --------------------------------- \n")
        karte = input("Wählen Sie: kurzzeitkarte ODER nachmittagskarte ODER tageskarte bitte.")
        ermassigung = input("Haben Sie Ermässigungskarte? Schreiben Sie ja oder nein.")
        ermassigungBool = ermassigung == "ja"
        alter = int(input("Wie alt sind Sie?"))
        print("Die Eintritt kostet: " + str(berechnet_eintrittspreis(karte, ermassigungBool, alter)) + " CHF")
        
    elif wahl == 3:
        print("\n\nMahngebühr für die Bibliothek berechnen\n --------------------------------- \n")
        nr_mahnung = int(input("Mahnung 1, 2 oder 3?"))
        medium = int(input("Wie viele Medien?"))
        brief = input("Brief bekommen: ja/nein?")
        briefBool = brief == "ja"
        print("Ihre Gebühren: " + str(berechne_mahngebuehr(nr_mahnung,medium,briefBool)) + "CHF")
    
