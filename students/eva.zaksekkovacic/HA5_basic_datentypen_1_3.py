#1_3_c
# Schreiben Sie eine Funktion, welche aus einer Liste alle Texte
# entfernt, welche weniger als drei Buchstaben haben.


def entfern(liste1):
    liste2 = list(liste1)
    
    for element in liste1:
#       print(element)
        if len(element) < 3:
            liste2.remove(element)
    
#   print(liste1)
    return liste2
    
liste = ["Brot", "", "Zucker", "Salz", "Mehl", "Kerzen", "a", "hk", "h"]
print(entfern(liste))


# l1 = ["Brot", "Zucker", "Salz", "Mehl", "Kerzen", "a", "hk", "h"]
# l2 = list(l1)
# print(l1)
# print(l2)
# 
# l1.remove("Brot")
# 
# print(l1)
# print(l2)

# l = [1,2,3,4,5]
# 
# i = 0
# length = len(l)
# while i < length:
#     element = l[i]
#     i = i + 1
#     print(element)
#     if element == 3:
#         l.remove(element)
# print(l)
# 
# 
# for element in l:
#     print(element)
#     if element == 3:
#         l.remove(element)
# print(l)