# a. Schreiben Sie eine Funktion datensatz_ausgeben, welche den ersten Datensatz aus
#Beispiel 3.1 wie folgt ausgibt:
# Vorname : Ana
# Nachname: Skupch
# Phone   : 123

person = {
    "Vorname": "Ana",
    "Nachname": "Skupch",
    "Phone": 123
}

telefonbuch = [person]

person = {
    "Vorname": "Tim",
    "Nachname": "Kurz",
    "Phone": 732
}
telefonbuch.append(person)

person = {
    "Vorname": "Julia",
    "Nachname": "Lang",
    "Phone": 912
}

telefonbuch.append(person)
print(telefonbuch)


def datensatz_ausgeben():
    erste_person = telefonbuch[0]
    for keys, values in erste_person.items():
        print (keys, ":", values)
    
datensatz_ausgeben()