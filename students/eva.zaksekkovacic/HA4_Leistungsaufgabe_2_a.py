from turtle import *

num = 1
preis = None
preis_liste = []
summe = 0

while preis != "x":
    preis = textinput("Eingabe", "Geben Sie den " + str(num) + " Preis ein: ")
    print("Geben sie den Preis " + str(num) + ": " + str(preis))
    if preis != "x":
        preis = float(preis)
        preis_liste.append(preis)
        num = num +1

for preis in preis_liste:
    summe = summe + preis
#     print(summe)

if summe < 100:
    print("Kein Rabatt, Gesamtpreis = ", summe, "CHF")
elif summe >= 100 and summe < 1000:
    print("5% Rabatt, Gesamtpreis = ", summe * 0.95, "CHF")
else:
    print("10% Rabatt, Gesamtpreis = ", summe * 0.9, "CHF")
