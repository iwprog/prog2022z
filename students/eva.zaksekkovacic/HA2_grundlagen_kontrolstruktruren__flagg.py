from turtle import *

#Zuerst möchte ich das rote Quadrat machen.
speed(8) #Wie schnell ist zu schnell?

def viereckrot(laenge):
    fillcolor("red")
    begin_fill()
    fd(laenge); lt(90)
    fd(laenge); lt(90)
    fd(laenge); lt(90)
    fd(laenge)
    end_fill()
    
# Dann aufteile ich das Quadrat auf 5x5 Teile. Der weisse Kreuz steht auf Mittel.
# Kreuz besteht aus einem stehendem und aus einem ligendem Dreieck. Liegendes (grösseres) Seitenteil
# ist cca 3/5 der Länge vom roten Quadrant, stehendes 1/5.
# Das grosse Teil nenne ich "laenge_g", kleine "laenge_k".
# Was vielleicht besser wäre statt laenge_g "laengeweisslang"
# und "laengeweisskurz" - aber das ist zu viel zu schreiben :) .

def viereckweiss1(laenge_g, laenge_k):
    begin_fill()
    fd(laenge_g); lt(90)
    fd(laenge_k); lt(90)
    fd(laenge_g); lt(90)
    fd(laenge_k)
    end_fill()
    
#Noch das liegende Teil.

def viereckweiss2(laenge_k, laenge_g):
    begin_fill()
    fd(laenge_k); lt(90)
    fd(laenge_g); lt(90)
    fd(laenge_k); lt(90)
    fd(laenge_g);
    end_fill()
    
#Jetzt kann ich noch die Funktion definieren mit den richtigen Dimensien.

def schweizer_flagge(laenge):
    laenge_g = laenge * 0.6
    laenge_k = laenge * 0.2

    pencolor("red")
    viereckrot(laenge)

# Jetzt müssen wir uns vom roten Kreuz zum start des weisses Viereck bewegen.

    lt(90)
    fd(laenge_g)
    lt(90)
    fd(laenge_k)
#Zuerst liegendes Viereck.
    
    pencolor("white")
    fillcolor("white")
    viereckweiss1(laenge_g, laenge_k)
    
#Jetzt zur Position des stehendes Viereck.
    
    lt(90)
    fd(laenge_k)
    rt(90)
    fd(laenge_k)
    lt(90)
    
#Und noch Viereck selbst.
    
    viereckweiss2(laenge_k, laenge_g)

#a)
hideturtle() #Weil wir die Pfeilen nicht sehen wollen.   
schweizer_flagge(400)

#b) Fast alles schon bereit. Nurt input Möglcihkeit fählt.
reset() #Damit alles im gleichen Dokumnent bleiben kann.
hideturtle()
laenge = numinput ("Schweizer Flagge", "Seitenlänge?")
schweizer_flagge(laenge)