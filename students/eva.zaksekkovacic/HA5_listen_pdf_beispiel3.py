#Beispiel 3 Adventskalender

def generate_kalendar():
    from random import choice
    kalendar = []
    auswahlliste = ["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]
    for i in range(24):
        kalendar.append(choice(auswahlliste))
    return kalendar

kal = generate_kalendar()
print(kal)


eingabe = None
while eingabe != "x":
    eingabe = input("Welche Kalendertür wollen Sie  öffnen (oder x für Exit): ")
    
    if int(eingabe) >=1 and int(eingabe) <= 24:
        print(kal[int(eingabe)-1])
    elif eingabe == "x":
        break
    else:
        print("error")
    
    