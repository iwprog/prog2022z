#übungsblatt zu funktionen und kontrollstr 10a, 11j


def menu ():
    print('-'*20)
    print('Programmübersicht, wählen Sie:')
    print('1 ... Preis für eine Fahrkarte berechnen')
    print('2 ... Herzfrequenz berechnen')
    print('0 ... Programm beenden')
    print('-'*20)

def herzfrequenz():
    print('Herzfrequenz berechnen')
    print('-'*20)
    alter=int(input('Bitte geben Sie ihr Alter an: '))
    max_frequenz= 220 - alter
    print('Ihre maximale Herzfrequens ist theoretisch: ' + str(max_frequenz))

def billet():
    print('Preis für eine Fahrkarte berechnen')
    print('-'*20)
    alter=int(input('Bitte geben Sie ihr Alter an: '))
    kilometer = int(input('Wie viele Kilometer möchten Sie reisen? '))
    preis = 2 + (kilometer*0.25)

    if alter > 16:
        print( 'Ihr Billet kostet: ' + str(preis))
    elif (alter < 16 and alter > 6):
        print( 'Ihr Billet kostet: ' + str(preis/2))
    else:
        print( 'Gratis (Kinder unter 6 reisen kostenlos)')

menu()
auswahl=int(input())
while auswahl !=0:
    if auswahl == 1:
        billet()
       
    elif auswahl == 2:
        herzfrequenz()
        
    menu()
    auswahl=int(input())

#11j 
zeichen = '# '
leer = '  '
i=11
for zeile in range (0,6,1):
     print(leer*zeile + i*zeichen)
     i = i-2
    
    