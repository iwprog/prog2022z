from turtle import *

#ad hoc übung 1.2
#) a)

forward(50)
left(90)
forward(50)
left(90)
forward(50)
left(90)
forward(50)

#stift bewegen ohne linie ziehen
penup()
left(90)
forward(100)
pendown()

#) b)

forward(100)
left(90)
forward(50)
left(90)
forward(100)
left(90)
forward(50)

#stift bewegen ohne linie ziehen
penup()
left(90)
forward(200)
pendown()

# c)
pensize(5)
pencolor('blue')
left(45)
forward(50)
pencolor('red')
right(90)
forward(50)
pencolor('cyan')
left(90)
forward(50)
pencolor('black')
right(90)
forward(50)


#stift bewegen ohne linie ziehen
penup()
right(45)
forward(200)
right(90)
forward(400)
pendown()

#d)
pencolor('red')
left(180)
forward(50)
left(120)
forward(50)
left(120)
forward(50)

#stift bewegen ohne linie ziehen
reset()


#e)
pencolor('blue')
pensize(5)

left(120)
forward(50)
left(120)
forward(50)
left(120)
forward(50)
pencolor('red')
forward(50)
left(120)
forward(50)
left(120)
forward(50)
pencolor('lightgreen')
forward(50)
left(120)
forward(50)
left(120)
forward(50)







exitonclick()