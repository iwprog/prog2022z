#Adhoc 2.6
for i in range (6):
    print(i)
for i in range(1, 4):
    print("Guten Tag!", i)
for i in range(5, 2, -1):
    print("Die aktuelle Zahl lautet:", i)
for i in "Äpfel":
    print("Die Variable hat den Wert:", i)
# verschachtelte Schleifen
for anzahl in range(1, 5):
    for obst in "Äpfel", "Birnen", "Kirchen", "Zwetschge":
        print("Kaufen wir", anzahl, obst)
#____________________________________________________________        
#adhoc 2.6a        
for i in range (100, 79,-2):
    i = str(i)
    print('Wert: ' +i)
#____________________________________________________________    
#adhoc 2.6b
from random import randint
kleinster_wert = 1
höchster_wert = 42

anzahl=0
while anzahl <6:
    zufallszahl = randint(kleinster_wert, höchster_wert)
    print(zufallszahl)
    anzahl = anzahl + 1
    
   