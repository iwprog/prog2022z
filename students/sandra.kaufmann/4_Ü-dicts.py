# 2. Wortstatistik: Schreiben Sie eine Funktion, welche einen Text entgegennimmt
# und alle Worter mit deren Haugkeit ausgibt. Die Gross-
# /Kleinschreibung soll dabei nicht berucksichtigt werden.
# l_input = "Der Tag begann sehr gut! Der Morgen war schön."
# >>> word_stat(l_input)
# {'der': 2, 'tag': 1, ....}

text = input('Geben Sie einen Text ein: ')
ignorieren = ('.',',','?','!')
text = text.replace(',','')
text = text.replace('.','')
text = text.replace('?','')
text = text.replace('!','')
text = text.lower()
text = text.split(' ')

def zählen (textinput):
    wortstatistik={}
    for anzahl in text:
        vorkommen = textinput.count(anzahl)
        wortstatistik[anzahl]=vorkommen
    return wortstatistik

print(zählen(text))

#Aufgabe 3
ICAO={'a':'Alpha','b':'Bravo','c':'Charly','d':'Delta','e':'Echo','f':'Foxtrot','g':'Golf','h':'Hotel','i':'Indiana','j':'Juliett','k':'Kilo','l':'Lima','m':'Mike','n':'November','o':'Oscar','p':'Papa','q':'Quebec','r':'Romeo','s':'Sierra','t':'Tango','u':'Uniform','v':'Victor','w':'Whiskey','x':'X-Ray','y':'Yankee','z':'Zulu'}
buchstabieren = input('Welches Wort soll buchstabiert werden? ')
buchstabieren.lower()
ausgabe = ''

for element in buchstabieren:
    ausgabe = ausgabe + ICAO[element] + ' - '
print(ausgabe)

#Aufgabe 3b)
ICAO={'a':'Alpha','b':'Bravo','c':'Charly','d':'Delta','e':'Echo','f':'Foxtrot','g':'Golf','h':'Hotel','i':'Indiana','j':'Juliett','k':'Kilo','l':'Lima','m':'Mike','n':'November','o':'Oscar','p':'Papa','q':'Quebec','r':'Romeo','s':'Sierra','t':'Tango','u':'Uniform','v':'Victor','w':'Whiskey','x':'X-Ray','y':'Yankee','z':'Zulu'}
buchstabieren = input('Welches Wort soll buchstabiert werden? ')
buchstabieren.lower()
ausgabe = ''

for element in buchstabieren:
    if element not in ICAO.keys():
        ausgabe = ''
        print('Kann Übung nicht buchstabieren, da '+element+' nicht definiert wurde.')
        exit
        
    else:
        ausgabe = ausgabe + ICAO[element] + ' - '
        
    
print(ausgabe)
