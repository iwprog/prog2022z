#übungsblatt Grundlagen Kontrollstrukturen
#Aufgabe 2.1

#Schreiben Sie eine Funktion guten morgen, welche den Text:
#'Guten Morgen!' auf dem Bildschirm ausgibt.

def guten_morgen():
    print('Guten Morgen!')
    
guten_morgen()

#Aufgabe 2.2
#Schreiben Sie eine Funktion, welche als Parameter einen Namen
#entgegennimmt und anschliessend den Text Guten Morgen Name! ausgibt.
def guten_morgen_2(name):
    print('Guten Morgen ' + name + '!')

guten_morgen_2('Ana')

#Aufgabe 2.3a)
#Schreiben Sie eine Funktion, welche einen Namen entgegennimmt und
#anschliessend den Text Guten Morgen Name! zuruckgibt
   
name = "was ist ihr name? "
name = input(name)

def guten_morgen_3():
    print('Guten Morgen ' + name + '!')
    
guten_morgen_3()
    
#Aufgabe 2.3 b)
#Schreiben Sie eine Funktion, welche die Lange und Breite eines
#Rechtecks entgegennimmt und die Flache des Rechtecks an das
#Programm zuruckgibt.
    
laenge = 'bitte länge des rechtecks angeben: '
laenge = input(laenge)
laenge = int(laenge)
breite = 'bitte breite des rechtecks angeben: '
breite = input(breite)
breite = int(breite)

def flaeche ():
    flaeche = laenge * breite
    flaeche = str(flaeche)
    print('Die Fläche beträgt: ' + flaeche + 'm2.')
    
flaeche()

#Aufgabe 2.8a)
#Schreiben Sie eine Schleife, welche von 0 bis 10 zahlt.
i = 0
anzahl = 10
while i <= anzahl:
    print (i)
    i = i + 1
    
#Aufgabe 2.8c)
#Schreiben Sie eine Schleife, welche von 10 bis 0 in Dreierschritten zahlt.
i2 = 10

while i2 > 0:
    print (i2)
    i2 = i2 -3


    