#übungsbeispiele zu funktionen
#2
# Schreiben Sie eine Funktion, welche das Alter einer Person entgegennimmt
# und daraus die theoretische maximale Herzfrequenz berechnet.
# Die Formel fur die Berechnung der Herzfrequenz lautet:
# max Herzfrequenz = 220 􀀀 Alter in Jahren

alter = 'Bitte geben Sie ihr Alter an: '
alter = input(alter)
alter = int(alter)

max_frequenz= 220 - alter

print('Ihre maximale Herzfrequens ist theoretisch: ' + str(max_frequenz))

#3
# Schreiben Sie eine Funktion welche basierend auf dem Alter und der
# Entfernung den Preis fur ein SBB Billet berechnet:
#  jedes Ticket verfugt uber einen Grundpreis von 2 CHF, der unabh
# angig von der Entfernung berechnet wird.
#  zusatzliche werden 0.25 CHF / km berechnet.
#  Kinder unter 16 Jahren zahlen den halben Preis.
#  Kinder unter 6 Jahren reisen kostenlos.

alter = 'Bitte geben Sie ihr Alter an: '
alter = input(alter)
alter = int(alter)

kilometer = 'Kilometer: '
kilometer = input(kilometer)
kilometer = int(kilometer)

preis = 2 + (kilometer*0.25)

if alter > 16:
    print( 'Ihr Billet kostet: ' + str(preis))
elif (alter < 16 and alter > 6):
    print( 'Ihr Billet kostet: ' + str(preis/2))
else:
    print( 'Gratis (Kinder unter 6 reisen kostenlos)')

#6
# Schreiben Sie eine Funktion welche einen Bilderrahmen mit einer bestimmten
# Lange und Breite unter Zuhilfenahme von Schleifen zeichnet.
# Weiters soll die Funktion noch die Rahmen- und Fullfarbe entgegennehmen.

from turtle import *
laenge = numinput('länge', 'Bitte geben Sie die länge an: ')
breite = numinput('breite', 'Bitte geben Sie die breite an: ')
rahmenfarbe = textinput('rahmen', 'Bitte geben Sie die Farbe des Rahmens an: ')
innenfarbe = textinput('rahmen', 'Bitte geben Sie die Farbe des hintergrundes an: ')

def rahmen():
    begin_fill()
    for quadrat in range(2):
        fd(laenge)
        left(90)
        fd(breite)
        left(90)
    end_fill()
def rahmen_innen():

    begin_fill()
    for quadrat in range(2):
        fd(laenge-50)
        left(90)
        fd(breite-50)
        left(90)
    end_fill()
    


pencolor(rahmenfarbe)
fillcolor(rahmenfarbe)
rahmen()
penup()
fd(25)
left(90)
fd(25)
right(90)
pendown()
pencolor(innenfarbe)
fillcolor(innenfarbe)
rahmen_innen()

#alternative

from turtle import *

laenge = numinput('länge', 'Bitte geben Sie die länge an: ')
breite = numinput('breite', 'Bitte geben Sie die breite an: ')
farbe_stift = textinput('rahmen', 'Bitte geben Sie die Farbe des Rahmens an: ')
farbe_inhalt = textinput('rahmen', 'Bitte geben Sie die Farbe des hintergrundes an: ')

def rahmen(laenge,breite,farbe_stift,farbe_inhalt):
    pencolor(farbe_stift)
    fillcolor(farbe_inhalt)
    begin_fill()
    for quadrat in range(2):
        fd(laenge)
        left(90)
        fd(breite)
        left(90)
    end_fill()
    
for quadrat in range(2):
    rahmen(laenge,breite,farbe_stift,farbe_inhalt)
    penup()
    fd(25)
    left(90)
    fd(25)
    right(90)
    pendown()
    laenge= laenge-50
    breite= breite-50


#11a
textstein = '# '
zeile = ''
i=1
while i<8:
    zeile = zeile + textstein
    print(zeile)
    i=i+1
        
print('')
#11e

for zeile in range(1,8,1):
    if (zeile == 1 or zeile == 7):
        print ( '# # # # # # #')
    else:
        print ( '#           #')
    
print('')    
#11g
textstein = '# '
leer = '  '
print_zeile = ''
for zeile in range(1,8,1):
    if (zeile == 1 or zeile == 7):
        print ( '# # # # # # #')
    else:
        print_zeile = leer*(zeile-1) + textstein
        print(print_zeile)
   