#Bibliotheken importieren
from urllib.request import urlopen
from inscriptis import get_text
from csv import writer

#URL entgegen nehmen
url = input('Geben Sie eine URL ein: ')

#eingegebene url öffnen und in variable speichern
with urlopen(url) as f:
      web_content = f.read().decode("utf8")
#nur text ohne html in variable speichern
text_content = get_text(web_content)
text_content = text_content.replace("("," ").replace(")"," ").replace("."," ").replace("!", " ").replace(","," ").replace("?"," ").replace("\n"," ")

# AUFGABE 1.B)
# da ich alle buchstaben sehen will, auch die die allenfalls nicht vorkommen:
alphabet = ('abcdefghijklmnopqrstuvwxyzäöü')
with open("count_letters.csv", "w", encoding="utf8") as f:
    #Trennsymbol definieren, das im CSV verwendet werden soll
    csv_writer = writer(f, delimiter=";")
    # zählt buchstaben
    for buchstabe in alphabet:
        anzahl_buchstabe = text_content.count(buchstabe)
        csv_writer.writerow([buchstabe, anzahl_buchstabe])

#AUFGABE 1.A)
text_content = text_content.split(' ')
geprüfte_wörter = []
with open("count_words.csv", "w", encoding="utf8") as f:
    #Trennsymbol definieren, das im CSV verwendet werden soll
    csv_writer = writer(f, delimiter=";")
    # zählt buchstaben
    for wort in text_content:
        anzahl_wort = text_content.count(wort)
        
        if wort not in geprüfte_wörter:
        #if wort in geprüfte_wörter == False:
            csv_writer.writerow([wort, anzahl_wort])    
        
        geprüfte_wörter.append(wort)
       
