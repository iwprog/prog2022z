#Ad Hoc 3.7 a)

#Dictionary befüllen
städte = {}
# auch möglich: stadt = {'Zürich':370000, 'Genf':54888, ...}

städte ['Zürich'] = 370000
städte ['Bern'] = 130000
städte ['Genf'] = 190000
städte ['Chur'] = 30000
städte ['Basel'] = 170000

#Ausgabe als Liste
print ('unsortierte Liste:',städte)
#Ausgabe als einzelne Zeilen
for stadt, einwohnerzahl in städte.items():
    print(stadt, 'hat', einwohnerzahl, 'Einwohner')

#b)
    #Ausgabe als Liste, sortiert
print('sortierte liste:',sorted(städte.keys()))
#Ausgabe in einzelen Zeilen
for element in sorted(städte.items()):
    print(element)
    
#c)
    
sortierliste = []
for stadt, einwohnerzahl in städte.items():
    sortierliste.append([einwohnerzahl,stadt])
sortierliste.sort()
print('Nach Einwohnerzahl: ',sortierliste)

