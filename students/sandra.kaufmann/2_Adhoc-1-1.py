#ad hoc übung 1.1

#Aufgabe a) Stellen Sie mit der Printanweisung einen DSatensatz ais folgenden einzelnen Bestandteilen zusammen
print('Name', 'Vorname', 'Strasse', 'Hausnummer', 'PLZ', 'Wohnort')


#Aufgabe b) schreiben sie einen Befehl, der alle Zahlen von 1-5 addiert, subthrahiert, multipliziert und dividiert
print(1+2+3+4+5)
print(5-4-3-2-1)
print(1*2*3*4*5)
print(5/4/3/2/1)

#Aufgabe c) schreiben sie einen Befehl, der den Satz ausgibt und so kurz wie möglich ist
#wenn fliegen hinter fliegen fliegen fliegen fliegen fliegen hinterher

print('wenn fliegen hinter '+'fliegen '*5+'hinterher')

#alternative
f = "fliegen "
print("wenn", f+ "hinter", 5*f+ "hinterher")

#Aufgabe d)schreiben sie einen befehl, der den String "Hello" rückwärts ausgibt
print('hello'[::-1])


