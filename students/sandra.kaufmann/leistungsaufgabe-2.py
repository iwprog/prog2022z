# a) Erstellen Sie ein Programm, bei dem mehrere Preisangaben (durchnummeriert) eingegeben werden können. Wenn der Benutzer ein „x“ eingibt, wird die Eingabe gestoppt.
# 
# Bsp.:
# Geben Sie den 1. Preis ein: 20
# Geben Sie den 2. Preis ein: 165
# Geben Sie den 3. Preis ein: 120.50
# Geben Sie den 4. Preis ein: x
# 
# Danach erfolgt die Summenberechnung nach folgenden Kriterien:
# Bei einem Gesamtbetrag grösser gleich 100 und kleiner 1000 Franken
#wird ein Rabatt von 5%, ab einem Betrag grösser gleich 1000 Franken ein
#Rabatt von 10% und unter 100 Franken keinen Rabatt gewährt.
#Das Programm soll eine Ausgabe erzeugen, aus der der Gesamtbetrag sowie der
#gewährte Rabatt hervorgehen.
# 
# Beispiele:
# Kein Rabatt, Gesamtpreis = 50 CHF
# 5% Rabattt, Gesamtpreis = 768.50 CHF
# 10% Rabatt, Gesamtpreis = 1922.30 CHF
#
preis = 0
summe = 0
stand=0
while preis != 'x':
    stand = stand+1
    summe = summe + float(preis)
    preis = input('Geben Sie den '+str(stand) +' .Preis ein oder x zum beenden: ')

print()
if (summe < 100):
    print('Kein Rabatt, Gesamtpreis = ' + str(round(summe,2)) + ' CHF')
elif(summe >= 100 and summe <= 1000):
     print('5% Rabatt, Gesamtpreis = ' + str(round(summe* 0.95,2)) + ' CHF')
else:
     print('10% Rabatt, Gesamtpreis = ' + str(round(summe* 0.9,2)) + ' CHF')


# b) Schreiben Sie ein Programm, welches das 1x1 nach folgendem Muster ausgibt:
# 
# 1er Reihe:
# 1 x 1 = 1
# 2 x 1 = 2 …
# 10 x 1 = 10
# --------------
# 2er Reihe:
# 1 x 2 = 2
# 2 x 2 = 4
# …
# 10 x 2 = 20
# --------------
# …
# --------------
# 10er Reihe:
# 1 x 10 = 10
# 2 x 10 = 20
# …
# 10 x 10 = 100
# 
# Tipp: Verwenden Sie dazu zwei ineinander geschachtelte for-Schleifen.
#
for text in range(1,11,1):
    print('-'*20)
    print(str(text)+'er Reihe:')
    
    for reihe in range (1,11,1):
        ergebnis = text*reihe
        print(str(reihe) + ' x ' + str(text) + ' = ' +str(ergebnis))
    
        
    