#übungsblatt grundlagen kontrollstrukuren
    # Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach
    # Alter den Wert volljahrig oder minderjahrig zuruckgibt.
    # Erweitern Sie die Funktion, sodass diese zwischen geschaftsunfahig (bis
    # inklusive 6 Jahre), unmundig (bis inklusive 14 Jahre), mundig minderjahrig
    # und volljahrig unterscheidet. Weiters soll fur ein negatives Alter ungeboren
    # zuruckgegeben werden.
#2.5b)

alter = "was ist ihr alter? "
alter = input(alter)
alter = int(alter)

if alter <0:
    print('ungeboren')
    
elif (alter <= 6 and alter > 0):
    print('geschäftsunfähig')
elif (alter > 6 and alter <= 14):
    print('unmündig')
elif (alter > 14 and alter < 18):
    print('mündig und minderjährig')
else:
    print('volljährig')
    
#2.7c)
    #Schreiben Sie eine Schleife, welche alle Buchstaben desWortes Dampfschi
    #nummeriert ausgibt.
wort = 'Dampfschiff'
anzahl = len(wort)


anzahl = int(anzahl)
i=1
while i <= anzahl:
    for name in wort:
        print(str(i) + ' ' + name)
        i = i+1
        
#2.8d)
#d. Schreiben Sie eine Schleife, welches von 10 bis 0 zahlt und statt "0" das
#Wort "Start" ausgibt.
for zahl in range(10,-1,-1):
    if zahl > 0:
        print(zahl)
    else:
        print('start')
    

#2.8f)
# Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis
# deren Summe grosser als 10 ist. Im Anschluss soll die Summe ausgegeben
# werden.
# 
summe = 0
while summe < 10:
    nummer = 'geben sie eine zahl unter 10 ein: '
    nummer = input(nummer)
    nummer = int(nummer)
    summe = summe + nummer
print(summe)
    
#2.8 g. Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der
#Benutzer -1 eingibt und anschliessend die Summe dieser Zahlen ausgibt.
summe = 0


zahl = 'geben sie eine zahl ein oder -1 zum beenden: '
zahl = input(zahl)
zahl = int(zahl)


while zahl != -1:
    summe = summe + zahl
    zahl = 'geben sie eine zahl ein oder -1 zum beenden: '
    zahl = input(zahl)
    zahl = int(zahl)

print(summe)
    