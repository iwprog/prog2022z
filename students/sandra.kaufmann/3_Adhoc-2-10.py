#adhoc 2.10

#adhoc2.10a
#Nutzen Sie verschachtelte Schleifen, um vier Dreiecke
#hintereinander zu zeichnen:
from turtle import *
speed(10)
fillcolor('blue')
pencolor('blue')

def dreieck():
    begin_fill()
    for dreieck in range(3):
        fd(50)
        left(120)
    end_fill()
    penup()
    fd(80)
    
for zeichnen in range(4):
    dreieck()
reset()
#b
from turtle import *
fillcolor('blue')
pencolor('blue')
speed(10)

def dreieck():
    begin_fill()
    for dreieck in range(3):
        fd(50)
        left(120)
    end_fill()
    penup()
    fd(80)
    
anzahldreieck = numinput('anzahl','Anzahl dreiecke :')
anzahldreieck = int(anzahldreieck)
for zeichnen in range(anzahldreieck):
    dreieck()   
reset()
#auswahl zwischen kreis, quadrat und dreieck
from turtle import *
fillcolor('blue')
pencolor('blue')
speed(10)

def Dreieck():
    begin_fill()
    for dreieck in range(3):
        fd(50)
        left(120)
    end_fill()
    penup()
    fd(80)
def Quadrat():
    begin_fill()
    for quadrat in range(4):
        fd(50)
        left(90)
    end_fill()
    penup()
    fd(80)
def Kreis():
    begin_fill()
    circle(25)
    end_fill()
    penup()
    fd(80)

form=textinput('form', 'Kreis, Quadrat oder Dreieck? ')
anzahlformen = numinput('anzahl','Anzahl formen :')
anzahlformen = int(anzahlformen)

for zeichnen in range(anzahlformen):
    if (form == 'Kreis'):
        Kreis()
    elif(form == 'Dreieck'):
        Dreieck()
    elif(form == 'Quadrat'):
        Quadrat()
        
#2.10 erweitert: basierend auf einem muster sollen quadrate,
#     dreiecke und kreise gezeichnet werden. dabei steht ein "d" für ein dreieck,
#     ein "o" für einen kreis und eine "#" für ein quadrat. das muster "##ddoo"
#     zeichenet als zum beispiel:
#     quadrat - quadrat - dreieck - dreieck - kreis - kreis.