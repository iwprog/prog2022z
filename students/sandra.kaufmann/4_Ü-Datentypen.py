#übungsblatt Datentypen

#2.1 a)
# Erstellen Sie ein Dictionary Preisliste, welches folgende
# Produkte mit den zugehörigen Preisen speichert:
# Brot ! 3.2, Milch ! 2.1, Orangen! 3.75, Tomaten ! 2.2.

preisliste = {'Brot':3.2, 'Milch':2.1, 'Orangen':3.75, 'Tomaten':2.2}
print(preisliste)
print('')

#b. Ändern Sie den Preis für Milch auf 2.05.
preisliste['Milch']=2.05
print(preisliste)
print('')

#c. Entfernen Sie den Eintrag für Brot.
del preisliste['Brot']
print(preisliste)
print('')

#d. Fügen Sie neue Einträge für
#Tee ! 4.2, Peanuts ! 3.9 und Ketchup! 2.1 hinzu.
preisliste['Tee']=4.2
preisliste['Peanuts']=3.9
preisliste['Ketchup']=2.1
print(preisliste)
print('')

# e. Schreiben Sie ein Programm, welches Sie nach Lebensmittel
# und den zugehörigen Preisen fragt und diese so lange einem
# Dictionary hinzufügt,bis der Benutzer x eingibt.
# Im Anschluss soll das Dictionary ausgegeben werden.
# Lebensmittel: Brot
# Preis: 3.2
# Lebensmittel: Milch
# Preis: 2.1
# Lebensmittel: x
# Dictionary: {"Milch": "2.1", "Brot": "3.2"}

lebensmittelliste={}
lebensmittel = ''
while lebensmittel != 'x':
    lebensmittel = input('Geben Sie ein Lebensmittel ein oder x zum beenden: ')
    if lebensmittel !='x':
        preis = input('Geben Sie den Preis des Lebensmittels ein: ')
        lebensmittelliste[lebensmittel]=preis
for lebensmittel,preis in lebensmittelliste.items():
    print('Lebensmittel: ',lebensmittel)
    print('Preis: ',preis)
print('Dictionary: ', lebensmittelliste)

#2.2a) Geben Sie die Preiliste aus dem obigen Beispiel aus.
# Brot kostet 3.2 CHF.
# Milch kostet 2.1 CHF.
# ...
# Ketchup kostet 2.1 CHF.
print('')
print('Aufgabe 2.2a)')
for lebensmittel,preis in lebensmittelliste.items():
    print(lebensmittel,'kostet',preis,'CHF')
#e. Geben Sie nur Lebensmittel mit einem Preis < 2.0 CHF aus.
print('')
print('Aufgabe 2.2e)')
for lebensmittel,preis in lebensmittelliste.items():
    if float(preis) > 2.0:
        print(lebensmittel,'kostet',preis,'CHF')
        
#2.3c. Entfernen Sie alle Lebensmittel aus dem Dictionary, welche die Zeichenkette
#en im Namen enthalten.
print('')
print('Aufgabe 3.1c)')

##fehler: RuntimeError: dictionary changed size during iteration
liste_ohne_en={}
for item in lebensmittelliste:
    if item.count('en') == 0:
        liste_ohne_en[item] = lebensmittelliste[item]
print(liste_ohne_en)

