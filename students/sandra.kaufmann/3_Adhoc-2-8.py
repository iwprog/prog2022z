#übung 2.8
zahlen = [12,23,5,56,77,18,9]
kleinste_zahl = None
grösste_zahl = None
durchschnitt = sum(zahlen) / 7

for zahl in zahlen:
    if(kleinste_zahl is None or zahl < kleinste_zahl):
        kleinste_zahl = zahl
        
    if(grösste_zahl is None or zahl > grösste_zahl):
        grösste_zahl = zahl 
       
    
print ('Kleinste Zahl: ' + str(kleinste_zahl))
print ('Grösste Zahl: ' + str(grösste_zahl))
print ('Durchschnitt: ' + str(durchschnitt))

