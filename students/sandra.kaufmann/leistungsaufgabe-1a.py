# leistungsaufgabe 1 Sandra Kaufmann
# a)

#Befehle importieren
from turtle import *
#geschwindigkeit beim zeichnen auf maximum
speed(10)
#turtle ausblenden
hideturtle()

#rotes Quadrat
pencolor('red')
fillcolor('red')

begin_fill()
forward(400) 
left(90)
forward(400) 
left(90)
forward(400)
left(90)
forward(400) 
left(90)
end_fill()



#um grösse des kreuz beliebig anzupassen habe ich variabeln gewählt
#schenkellänge (innenseiten des kreuzes)
laenge_schenkel = 90
#aussenseiten des kreuzes, etwas (10) kürzer
laenge_aussen = laenge_schenkel - 10
#startpunkt auf der y achse, unabhängig der grösse des kreuzes
startpunkty = (400-laenge_schenkel*2-laenge_aussen)/2
#startpunkt auf der x achse, unabhängig der grösse des kreuzes
startpunktx = (400-laenge_schenkel*2-laenge_aussen)/2 + laenge_schenkel
#stift heben und verschieben zu startpunkt
penup()
forward(startpunktx)
left(90)
forward(startpunkty)
pendown()

#weisses Kreuz farben
pencolor('white')
fillcolor('white')

#funktion um jeweils ein viertel des kreuzes zu zeichnen
def viertelkreuz():
    forward(laenge_schenkel)
    left(90)
    forward(laenge_schenkel)
    right(90)
    forward(laenge_aussen)
    right(90)

begin_fill()

#schleife um das kreuz zu zeichnen
index=1
while index <=4:
    viertelkreuz()
    index=index+1
end_fill()




