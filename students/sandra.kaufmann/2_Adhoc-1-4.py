#Adhoc übungen 1.4
from turtle import *
help(circle)

circle(120)
clear()
circle(120, 180)  # semicircle
clear()
circle(60, steps=6) #sechseck
clear()
circle(60,240,steps=8) #8-eck 240°




#radius -- a number
 #   extent (optional) -- a number
 #   steps (optional) -- an integer
 #call: circle(radius)                  # full circle
 #   --or: circle(radius, extent)          # arc
#    --or: circle(radius, extent, steps)
#    --or: circle(radius, steps=6)         # 6-sided polygon