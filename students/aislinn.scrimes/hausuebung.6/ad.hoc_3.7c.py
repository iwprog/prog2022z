cities = {"Zürich":370000, "Genf":190000, "Basel":170000, "Bern":130000}
    
sort = []
for stadt, anzahl in cities.items():
    sort.append([anzahl, stadt])
    
for anzahl, stadt in sorted(sort):
    print(stadt, "hat", anzahl, "Einwohner")
