# a) Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit den
# zugehörigen Preisen speichert:Brot → 3.2, Milch → 2.1, Orangen → 3.75, Tomaten → 2.2.

preisliste = {"Brot":3.20, "Milch":2.10, "Orangen":3.75, "Tomaten":2.20}

# b) Ändern Sie den Preis für Milch auf 2.05.
preisliste["Milch"] = 2.05

# c) Entfernen Sie den Eintrag für Brot.
del preisliste["Brot"]

# d) Fügen Sie neue Einträge für Tee → 4.2, Peanuts → 3.9 und Ketchup → 2.1 hinzu.
preisliste["Tee"] = 4.20
preisliste["Peanuts"] = 3.90
preisliste["Ketchup"] = 2.10

# e) Schreiben Sie ein Programm, welches Sie nach Lebensmittel und den zu- gehörigen Preisen fragt
# und diese so lange einem Dictionary hinzufügt, bis der Benutzer "x" eingibt.
# Im Anschluss soll das Dictionary ausgegeben werden.
# 
#         Lebensmittel: Brot
#         Preis: 3.2
#         Lebensmittel: Milch
#         Preis: 2.1
#         Lebensmittel: x
#         Dictionary: {"Milch": "2.1", "Brot": "3.2"}

artikel = ""
einkaufsliste = {}
while True:
    artikel = input("Lebensmittel: ")
    
    if artikel == "x":
        break
    cost = input("Preis: ")

    else:
        preisliste[artikel] = cost
        
print("Dictionary: ", einkaufsliste)