# a) Geben Sie die Preisliste aus dem obigen Beispiel aus.
#           Brot kostet 3.2 CHF.
#           Milch kostet 2.1 CHF.
#           ...
#           Ketchup kostet 2.1 CHF.

preisliste = {"Brot":3.20, "Milch":2.10, "Orangen":3.75, "Tomaten":2.20}
preisliste["Milch"] = 2.05
del preisliste["Brot"]
preisliste["Tee"] = 4.20
preisliste["Peanuts"] = 3.90
preisliste["Ketchup"] = 2.10

for item, cost in preisliste.items():
    print(item, "kostet", cost, "Franken")

# e) Geben Sie nur Lebensmittel mit einem Preis < 2.0 CHF aus.
sort = []

for artikel, preis in preisliste.items():
    if preis < 2:
        sort.append([artikel, preis])
for item, cost in sort:
    print(item, "kostet", cost, "CHF")
