from turtle import *

# Relative Länge, "laenge = 100" geht auf die normale Länge von 1a)
laenge = numinput("Seite", "Bitte geben Sie die Länge der Seiten ein")

pensize(1)
hideturtle()
# roter Hintergrund
pencolor("red")
fillcolor("red")
begin_fill()  # Roter Quadrat zeichnen
for count in range(4):   # Jede Seite des Quadrates Zeichnen (alle sind gleich)
    forward(laenge * 4);left(90)

end_fill()

#Von unterer linker Ecke zum Anfang vom Kreuz
penup()
left(90);forward(laenge * 1.625)
right(90);forward(laenge * 0.75)
pendown()

#Kreuz zeichnen 
pencolor("white")
fillcolor("white")
begin_fill()
for count in range(4):   #(4mal gleiche Bewegungsmuster)
    forward(laenge * 0.875);right(90)
    forward(laenge * 0.875);left(90)
    forward(laenge * 0.75);left(90)
end_fill()