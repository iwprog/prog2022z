eingabe = 0
summe = 0
anzahl = 1

while True:
    summe = summe + float(eingabe)
    eingabe = input("Geben Sie den " + str(anzahl) + ". Preis ein: ")    
    anzahl = anzahl + 1
    
    if eingabe == 'x':
        break

if summe >= 100 and summe < 1000:
    summe = summe - (summe * 0.05)
    print("5% Rabatt, Gesamtpreis:", summe)
elif summe >= 1000:
    summe = summe - (summe * 0.10)
    print("10% Rabatt, Gesamtpreis:", summe)
else:
    summe = summe
    print("Kein Rabatt, Gesamtpreis:", summe)

# a) Erstellen Sie ein Programm, bei dem mehrere Preisangaben (durchnummeriert) eingegeben werden
# können.
# Wenn der Benutzer ein „x“ eingibt, wird die Eingabe gestoppt.
# 
# Bsp.:
# Geben Sie den 1. Preis ein: 20
# Geben Sie den 2. Preis ein: 165
# Geben Sie den 3. Preis ein: 120.50
# Geben Sie den 4. Preis ein: x

# Danach erfolgt die Summenberechnung nach folgenden Kriterien:
# Bei einem Gesamtbetrag grösser gleich 100 und kleiner 1000 Franken wird ein Rabatt von 5%,
# ab einem Betrag grösser gleich 1000 Franken ein Rabatt von 10% und unter 100 Franken keinen Rabatt
# gewährt.
# Das Programm soll eine Ausgabe erzeugen, aus der der Gesamtbetrag sowie der gewährte Rabatt
# hervorgehen.
# 
# Beispiele:
# Kein Rabatt, Gesamtpreis = 50 CHF
# 5% Rabattt, Gesamtpreis = 768.50 CHF
# 10% Rabatt, Gesamtpreis = 1922.30 CHF