from turtle import *

pensize(1)
hideturtle()
# roter Hintergrund
pencolor("red")
fillcolor("red")
begin_fill()  # Roter Quadrat zeichnen
for count in range(4):   # Jede Seite des Quadrates Zeichnen (alle sind gleich)
    forward(400);left(90)

end_fill()

#Von unterer linker Ecke zum Anfang vom Kreuz
penup()
left(90);forward(162.5)
right(90);forward(75)
pendown()

#Kreuz zeichnen 
pencolor("white")
fillcolor("white")
begin_fill()
for count in range(4):   #(4mal gleiche Bewegungsmuster)
    forward(87.5);right(90)
    forward(87.5);left(90)
    forward(75);left(90)
end_fill()