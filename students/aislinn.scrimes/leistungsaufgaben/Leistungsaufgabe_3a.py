staedteliste = []
summe = 0

while True:
    stadt = input("Geben Sie die Stadt ein: ")
    if stadt == "x":
        break
    
    einwohnerzahl = input("Geben Sie die zugehörige Einwohnerzahl ein: ")
    staedteliste.append([stadt, einwohnerzahl])
    
print(sorted(staedteliste))

# a) Schreiben Sie ein Programm, bei dem Städte und deren Einwohnerzahlen eingegeben werden können.
# Wenn der Benutzer bei einer Stadt ein „x“ eingibt, wird die Eingabe gestoppt.
# 
# Bsp.:
# Geben Sie die Stadt ein: Zürich
# Geben Sie die zugehörige Einwohnerzahl ein: 370000
# Geben Sie die Stadt ein: Bern
# Geben Sie die zugehörige Einwohnerzahl ein: 130000
# Geben Sie die Stadt ein: x
# 
# Während der Eingabe soll im Programm eine Liste (list) erzeugt werden, wie die in der
# ad hoc Übung 3.2 b). Bsp.: [["Zürich", 370000], ["Bern", 130000]]
# Geben Sie die Liste am Ende des Programmes alphabetisch sortiert aus, d.h. einfache Ausgabe mit
# print(liste). Bsp.: [["Bern", 130000], ["Zürich", 370000] ]

