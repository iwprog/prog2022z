for table in range(1,11):
    print(str(table)+ "er Reihe:")
    
    for multiplier in range(1,11):
        ergebnis = multiplier * table
        print(multiplier, "x", table, "=", ergebnis)
    print("______________")

# b) Schreiben Sie ein Programm, welches das 1x1 nach folgendem Muster ausgibt:
# 
# 1er Reihe:
# 1 x 1 = 1
# 2 x 1 = 2 …
# 10 x 1 = 10
# --------------
# 2er Reihe:
# 1 x 2 = 2
# 2 x 2 = 4
# …
# 10 x 2 = 20
# --------------
# …
# --------------
# 10er Reihe:
# 1 x 10 = 10
# 2 x 10 = 20
# …
# 10 x 10 = 100

# Tipp: Verwenden Sie dazu zwei ineinander geschachtelte for-Schleifen.