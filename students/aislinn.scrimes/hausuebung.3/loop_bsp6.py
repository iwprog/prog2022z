#6. Schreiben Sie eine Funktion welche einen Bilderrahmen mit einer bestimmten Länge und Breite
#    unter Zuhilfenahme von Schleifen zeichnet. Weiters soll die Funktion noch die Rahmen- und
#    Fü̈llfarbe entgegennehmen.
#  >>> zeichne_rahmen(100,25, ’black’, ’white)
from turtle import *

def zeichne_rahmen(laenge, abstand, rahmen, fuellfarbe):
    
    pensize(2)
    pencolor(rahmen)
    fillcolor(fuellfarbe)
    
    #outside
    begin_fill()
    for count in range(4):
        fd(laenge);lt(90)
    end_fill()
    
    #abstand
    penup();lt(90);fd(abstand);rt(90);fd(abstand);pendown()
    
    begin_fill()
    for count in range(4):
        fd(laenge-abstand*2);lt(90)
    end_fill()
    
    #zweiter/dritter/vierter rahmen möglich:
    penup();rt(90);fd(abstand);lt(90);fd(laenge+50);pendown()
    
zeichne_rahmen(100, 25, "black", "white")

zeichne_rahmen(200, 30, "violet", "cyan")
