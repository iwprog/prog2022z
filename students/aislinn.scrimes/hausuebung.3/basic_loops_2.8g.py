#g. Schreiben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der Benutzer -1 eingibt
    #und anschliessend die Summe dieser Zahlen ausgibt.
#      Bitte geben Sie Zahl 1 ein: 2
#      Bitte geben Sie Zahl 2 ein: 3
#      Bitte geben Sie Zahl 3 ein: 7
#      Bitte geben Sie Zahl 4 ein: -1
#      Summe: 12

i = 0
step = 1

while i != "-1":
    zahl = input("Bitte geben Sie Zahl "+str(step)+" ein: ")
    i = i + int(zahl)
    step = step + 1
print("Summe: ", i)

# ! wieso funktioniert es nicht? (lauft, aber endet nicht)
