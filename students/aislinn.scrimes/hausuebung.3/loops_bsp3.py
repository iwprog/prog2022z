#3. Schreiben Sie eine Funktion welche basierend auf dem Alter und der Entfernung den Preis für ein
    #SBB Billet berechnet:
#• jedes Ticket verfü̈gt ü̈ber einen Grundpreis von 2 CHF, der unabhä̈ngig von der Entfernung
    #berechnet wird.
#• zusa ̈tzliche werden 0.25 CHF / km berechnet.
#• Kinder unter 16 Jahren zahlen den halben Preis.
#• Kinder unter 6 Jahren reisen kostenlos.

#>>> get_billet_preis(3, 200):
#     0
#>>> get_billet_preis(8, 200):
#     26
#>>> get_billet_preis(30, 200):
#     52

def get_billet_preis(alter, km):
    if alter < 6:
        print('kostenlos')
    elif alter < 16:
        print((2 + (0.25*km)) / 2)
    else:
        print(2 + (0.25*km))

get_billet_preis(3, 200)
get_billet_preis(8, 200)
get_billet_preis(30, 200)
