#f. Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis deren Summe grösser als
    #10 ist. Im Anschluss soll die Summe ausgegeben werden.
      #Bitte geben Sie Zahl 1 ein: 2
      #Bitte geben Sie Zahl 2 ein: 3
      #Bitte geben Sie Zahl 3 ein: 1
      #Bitte geben Sie Zahl 4 ein: 8
      #Summe: 14
        
step = 1
i = 0

while i < 10:
    zahl = input("Bitte geben Sie Zahl "+str(step)+" ein: ")
    i = i + int(zahl)
    step = step + 1
print("Summe: ", i)