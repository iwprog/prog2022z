#a. Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach Alter den Wert vollj ̈ahrig
    #oder minderj ̈ahrig zuru ̈ckgibt. 
       #age = 22
       #print("Mit", age, "ist man", legal_status(age) + ".")
# Ausgabe: Mit 22 ist man vollja ̈hrig. 
#b. Erweitern Sie die Funktion, sodass diese zwischen geschäftsunfähig (bis inklusive 6 Jahre),
    #unmündig (bis inklusive 14 Jahre), mündig minderjährig und volljährig unterscheidet.
    #Weiters soll für ein negatives Alter "ungeboren" zurückgegeben werden. 
       #age = 5
       #print("Mit", age, "ist man", legal_status(age) + ".")
# Ausgabe: Mit 5 ist man gescha ̈ftsunf ̈ahig.

age = int(input("Geben Sie Ihr Alter an: "))

if age < 0:
    print ("Mit",age,"ist man ungeboren")
        
elif age <= 6:
    print ("Mit",age,"ist man geschäftsunfähig")
        
elif age <= 14:
    print ("Mit",age,"ist man unmündig")
        
elif age <= 17:
    print ("Mit",age,"ist man mündig minderjährig")

else:
    print ("Mit",age,"ist man volljährig")