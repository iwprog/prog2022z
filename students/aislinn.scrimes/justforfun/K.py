from turtle import *

def K():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(25);rt(90);pendown()
    
    begin_fill()
    fd(25);lt(90);fd(62.5);rt(90);circle(-50, 90);fd(12.5);lt(90);fd(25);lt(90);fd(12.5);
    circle(75, 90);rt(90);fd(62.5);lt(90);fd(25);lt(90);fd(150)
    end_fill()
    
    penup();lt(180);fd(62.5);rt(90);fd(25);pendown()
    
    begin_fill()
    circle(75, 90);fd(12.5);lt(90);fd(25);lt(90);fd(12.5);circle(-50, 90);lt(90);fd(25)
    end_fill()
    
    penup();fd(87.5);lt(90);fd(100);pendown()