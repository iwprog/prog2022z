from turtle import *

def J():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();lt(90);fd(75);rt(90);fd(25);rt(90);pendown()
    
    begin_fill()
    circle(50, 180);fd(100);lt(90);fd(75);lt(90);fd(25);lt(90)
    fd(50);rt(90);fd(75);circle(-25, 180);lt(90);fd(25)
    end_fill()
    
    penup();lt(90);fd(75);lt(90);fd(125);pendown()