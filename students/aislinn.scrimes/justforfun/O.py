from turtle import *

def O():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();lt(90);fd(75);rt(90);fd(25);rt(90);pendown()
    
    begin_fill()
    circle(50, 180);fd(50);circle(50, 180);fd(50)
    end_fill()
    
    penup();lt(90);fd(25);rt(90);pendown()
    
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    circle(25, 180);fd(50);circle(25, 180);fd(50)
    end_fill()
    
    penup();fd(75);lt(90);fd(100);pendown()