from turtle import *

def H():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")

    #Rechteck zeichnen (und füllen)
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()

    #bewegen vom Rechteckschluss zu Buchstabenanfang
    penup()
    fd(25);lt(90);fd(25)
    pendown()

    #Buchstsabe zeichnen
    pencolor("cyan")
    fillcolor("cyan")

    begin_fill()
    fd(150);rt(90);fd(25);rt(90)
    fd(62.5);lt(90);fd(50);lt(90);fd(62.5)
    rt(90);fd(25);rt(90)
    fd(150);rt(90);fd(25);rt(90)
    fd(62.5);lt(90);fd(50);lt(90);fd(62.5)
    rt(90);fd(25)
    end_fill()

    #Zeiger bewegt sich zu unten rechts, nach rechts zeigend.
    penup()
    lt(90);fd(25);lt(90);fd(125)
    pendown()