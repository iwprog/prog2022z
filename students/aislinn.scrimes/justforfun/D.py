from turtle import *

def D():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();lt(90);fd(25);rt(90);fd(25);pendown()
    
    begin_fill()
    fd(25);circle(75, 180);fd(25);lt(90);fd(150)
    end_fill()
    
    pencolor("violet")
    fillcolor("violet")
    penup();lt(180);fd(25);rt(90);fd(25);pendown()
    
    begin_fill()
    circle(50, 180);lt(90);fd(100)
    end_fill()
    
    penup();fd(50);lt(90);fd(100);pendown()