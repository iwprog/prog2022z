from turtle import *

def N():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")

    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()

    pencolor("cyan")
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(25);pendown()

    begin_fill()
    fd(150);rt(90);fd(25);rt(63.435);fd(111.8034);lt(153.435);fd(100);rt(90);fd(25);rt(90)
    fd(150);rt(90);fd(25);rt(63.435);fd(111.8034);lt(153.435);fd(100);rt(90);fd(25)
    end_fill()
    
    penup();lt(90);fd(25);lt(90);fd(125);pendown()