from turtle import *

def P():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(25);pendown()
    
    begin_fill()
    fd(150);rt(90);fd(62.5);circle(-37.5, 180);fd(37.5);lt(90);fd(75);rt(90);fd(25)
    end_fill()
    
    pencolor("violet")
    fillcolor("violet")
    penup();rt(90);fd(100);rt(90);fd(25);pendown()
    
    begin_fill()
    fd(37.5);circle(12.5, 180);fd(37.5);lt(90);fd(25)
    end_fill()
    
    penup();fd(125);lt(90);fd(100);pendown()