from turtle import *

def T():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();lt(90);fd(25);rt(90);fd(62.5);pendown()
    
    begin_fill()
    fd(25);lt(90);fd(125);rt(90);fd(37.5);lt(90);fd(25)
    lt(90);fd(100);lt(90);fd(25);lt(90);fd(37.5);rt(90);fd(125)
    end_fill()
    
    penup();fd(25);lt(90);fd(87.5);pendown()
