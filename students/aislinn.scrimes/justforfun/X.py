from turtle import *

def X():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(25);pendown()
    
    begin_fill()
    rt(26.565);fd(83.85255);lt(53.13);fd(83.85255);rt(116.565);fd(25);rt(63.435);fd(55.9017)
    lt(126.87);fd(55.9017);rt(63.435);fd(25);
    rt(116.565);fd(83.85255);lt(53.13);fd(83.85255);rt(116.565);fd(25);rt(63.435);fd(55.9017)
    lt(126.87);fd(55.9017);rt(63.435);fd(25)
    end_fill()
    
    penup();lt(90);fd(25);lt(90);fd(125);pendown()