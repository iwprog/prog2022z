from turtle import *

def Kb():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(25);rt(90);pendown()
    
    begin_fill()
    fd(25);lt(90);fd(56.25);rt(126.87);fd(93.75);lt(126.87);fd(37.5);lt(53.13);fd(93.75)
    rt(53.13);fd(56.25);lt(90);fd(25);lt(90);fd(150)
    end_fill()
    
    penup();lt(180);fd(93.75);rt(90);fd(25);pendown()
    
    begin_fill()
    lt(36.87);fd(93.75);rt(126.87);fd(37.5);rt(53.13);fd(93.75);rt(126.87);fd(37.5)
    end_fill()