from turtle import *

def E():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(25);pendown()
    
    begin_fill()
    fd(150);rt(90);fd(100);rt(90);fd(25);rt(90);fd(75);lt(90);
    fd(37.5);lt(90);fd(50);rt(90);fd(25);rt(90);fd(50);lt(90);
    fd(37.5);lt(90);fd(75);rt(90);fd(25);rt(90);fd(100)
    end_fill()
    
    penup();lt(90);fd(25);lt(90);fd(125);pendown()