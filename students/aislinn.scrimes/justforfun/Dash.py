from turtle import *

def dh():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(100);lt(90);fd(200);lt(90);fd(100);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();lt(90);fd(87.5);rt(90);pendown()
    
    begin_fill()
    fd(100);lt(90);fd(25);lt(90);fd(100);lt(90);fd(25)
    end_fill()
    
    penup();fd(87.5);lt(90);fd(100);pendown()