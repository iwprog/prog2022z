from turtle import *

def B():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(25);rt(90);pendown()
    
    pencolor("cyan")
    begin_fill()
    fd(62.5);circle(43.75, 180);fd(62.5);lt(90);fd(87.5)
    end_fill()
    penup();lt(180);fd(62.5);rt(90);pendown()
    begin_fill()
    fd(62.5);circle(43.75, 180);fd(62.5);lt(90);fd(87.5)
    end_fill()
    
    pencolor("violet")
    fillcolor("violet")
    penup();lt(90);fd(25);pendown()
    begin_fill()
    fd(37.5);circle(-18.75, 180);fd(37.5);rt(90);fd(37.5)
    end_fill()
    penup();fd(62.5);rt(90);pendown()
    begin_fill()
    fd(37.5);circle(-18.75, 180);fd(37.5);rt(90);fd(37.5)
    end_fill()
    
    penup()
    rt(180);fd(150);lt(90);fd(100)
    pendown()