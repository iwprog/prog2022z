from turtle import *

def S():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")

    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();fd(25);lt(90);fd(62.5);lt(180);pendown()
    
    begin_fill()
    circle(37.5, 90);fd(25);circle(37.5, 90);fd(12.5);circle(37.5, 90);fd(25);
    circle(-12.5, 90);fd(12.5);circle(-12.5, 90);fd(25);circle(-12.5, 90);lt(90);fd(25);lt(90)
    circle(37.5, 90);fd(25);circle(37.5, 90);fd(12.5);circle(37.5, 90);fd(25);
    circle(-12.5, 90);fd(12.5);circle(-12.5, 90);fd(25);circle(-12.5, 90);lt(90);fd(25);lt(90)
    end_fill()
    
    penup();fd(62.5);lt(90);fd(125);pendown()