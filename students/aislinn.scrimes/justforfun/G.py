from turtle import *

def G():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    pencolor("cyan")
    fillcolor("cyan")
    penup();lt(90);fd(75);rt(90);fd(25);rt(90);pendown()
    
    begin_fill()
    circle(50, 180);fd(25);lt(90);fd(50);lt(90);fd(25);lt(90);fd(25);rt(90);circle(-25, 180)
    fd(50);circle(-25, 180);lt(90);fd(25);lt(90);circle(50, 180);fd(50)
    end_fill()
    
    penup();fd(75);lt(90);fd(125);pendown()