from turtle import *

def A():
    pensize(1)
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(150);lt(90);fd(200);lt(90);fd(150);lt(90);fd(200);lt(90)
    end_fill()
    
    penup()
    fillcolor("cyan")
    fd(25);lt(90);fd(25);rt(90)
    pendown()
    
    pencolor("cyan")
    fillcolor("cyan")
    begin_fill()
    fd(25);lt(78);fd(51);rt(78);fd(30);rt(78);fd(52);lt(78);fd(25)
    lt(104);fd(154.5);lt(76);fd(26.5);lt(76);fd(154.5)
    end_fill()
    
    penup()
    rt(166);fd(75);rt(90);fd(40)
    pendown()
    
    pencolor("violet")
    fillcolor("violet")
    begin_fill()
    fd(20);lt(105);fd(39);lt(150);fd(39)
    end_fill()
    
    penup()
    lt(15);fd(99);lt(90);fd(85)
    pendown()