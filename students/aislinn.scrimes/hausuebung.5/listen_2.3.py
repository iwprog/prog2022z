# Adventskalender: Schreiben Sie eine Funktion, welche eine Liste mit 24 Einträgen erstellt,
# welche zufällig eine der folgenden Überraschungen enthält:
# Samiklaus, Christbaum, Weihnachtskugel, Stiefel, Schneeball.
# Der Benutzer soll im Anschluss beliebige Türen öffnen können und das Programm soll ausgeben,
# welche Überraschung sich hinter dieser versteckt.

# Welche Kalendertür wollen Sie öffnen (oder x für Exit): 21 Samiklaus
# Welche Kalendertür wollen Sie öffnen (oder x für Exit): 12 Weihnachtskugel
# Welche Kalendertür wollen Sie öffnen (oder x für Exit): x

# Hinweis: eine Zufallsauswahl kann man in Python wie folgt programmieren:
# from random import choice
# wähle ein zufälliges element aus der liste aus. auswahlliste = ["Eins", "Zwei", "Drei"]
# zufallselement = choice(auswahlliste)

from random import choice
 
adventskalender = ["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]
 
while True:
  ueberraschung = choice(adventskalender)
  door = input("Welche Kalendertür wollen Sie öffnen (oder x für Exit): ")
  if door == 'x':
    break
  elif int(door) in range(1,25):
    print(ueberraschung)