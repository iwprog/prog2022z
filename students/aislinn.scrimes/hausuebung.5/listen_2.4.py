# Schreiben Sie eine Funktion, welche eine Liste und einen Suffix entgegennimmt und alle Wörter,
# die auf den Suffix enden ausgibt.
#   l_input = [’Gesundheit’, ’Wanderung’, ’Heiterkeit’, ’Gewandtheit’, ’Lustig’]
#   >>> get_suffix_words(l_input, ’heit’)
#   [’Gesundheit’, ’Gewandtheit’]

l_input = ['Gesundheit', ' Wanderung', 'Heiterkeit', 'Gewandtheit', 'Lustig']

def get_suffix_words(l_input, suffix):
    l_output = []
    for element in l_input:
        if suffix in element:
            l_output.append(element)
    print(l_output)
            
get_suffix_words(l_input, 'heit')