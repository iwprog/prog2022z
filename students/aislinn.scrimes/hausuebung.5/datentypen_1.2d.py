# d. Geben Sie nur jene Jahreszeiten aus, die mit den Buchstaben "er" enden.

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

for inhalt in jahreszeiten:
    if inhalt[-2:] == 'er':
        print(inhalt)
