# c. Geben Sie alle Jahreszeiten und zusätzlich die letzten drei Buchstaben der Jahreszeit aus .
#               Frühling - ng
#               Sommer - er
#               Herbst - st
#               Winter - er


jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

for season in jahreszeiten:
    print(season,"-", season[-2:])