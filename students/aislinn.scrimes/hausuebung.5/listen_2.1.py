# Schreiben Sie eine Funktion, welche eine beliebige Liste entgegennimmt und eine neue Liste,
# welche nur noch Einträge mit einer Länge von ≥4 enthält, zurückgibt.
# l_input = [’der’, ’junge’, ’ist’, ’sehr’, ’freundlich’]
# >>> filter_list(l_input)
# [’junge’, ’sehr’, ’freundlich’]

l_input = ["der", "junge", "ist", "sehr", "freundlich"]
list2 = []

def filter_list(l_input):
    for element in l_input:
        if len(element) >= 4:
            list2.append(element)
    print(list2)

filter_list(l_input)