#a:
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
jahreszeiten.sort()
print(jahreszeiten)

#b:
print(jahreszeiten[0:3])

#c:

for item in jahreszeiten:
    print(item, "-", item[-3:])
    
#d:
for item in jahreszeiten:
    if item.find("er") >= 0:
        print(item)
