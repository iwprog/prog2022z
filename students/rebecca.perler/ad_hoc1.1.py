#a
name="Perler"
vorname=" Rebecca"
strasse=" Hauptstrasse"
Hausnummer=" 111"
PLZ=" 1212"
Wohnort=" Hauptort"

print(name, vorname, strasse, Hausnummer, PLZ, Wohnort, sep=",")

#d
b="Hello"
reversed(b)
print(b [::-1])


#b
print(1+2+3+4+5)
print(1-2-3-4-5)
print(1*2*3*4*5)
print(1/2/3/4/5)

#funktioniert nicht:
#a="fliegen"
#weil das + nicht zusammensetzt
#print("wenn" + [a] + "hinter" + [5*a] +"hinterher")

#weil [] macht 'fliegen' in eckigen Klammern, keine Leerschläge
#print("wenn", [a], "hinter", [5*a], "hinterher")

#funktioniert:
a = "fliegen "
print("wenn", a+ "hinter", 5*a+ "hinterher")

