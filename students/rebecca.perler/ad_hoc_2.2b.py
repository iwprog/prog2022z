from turtle import *
def dreieck(seitenlaenge):
    begin_fill()
    left(120)
    forward(seitenlaenge)
    right(120)
    forward(seitenlaenge)
    right(120)
    forward(seitenlaenge)
    end_fill()

pencolor("red")
pensize(5)
fillcolor("green")
dreieck(100)
fillcolor("yellow")
dreieck(200)
fillcolor("blue")
dreieck(50)


