from turtle import *
penup()
back(400)
pendown()

def dreieck():
    fillcolor ("blue")
    begin_fill()
    for i in range(3): #3 für 3 Seiten des Dreiecks
        forward(100)
        lt(120)
    end_fill()

n = int(numinput("Anzahl","Wieviele Dreiecke sollen gezeichnet werden?"))
for count in range(n):
    dreieck()
    penup()
    forward(120)
    pendown()