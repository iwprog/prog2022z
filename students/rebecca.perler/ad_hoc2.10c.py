from turtle import *
speed(10)
penup()
back(400)
lt(90)
forward(200)
rt(90)
pendown()

def dreieck():
    fillcolor ("blue")
    begin_fill()
    for i in range(3): #3 für 3 Seiten des Dreiecks
        forward(100)
        lt(120)
    end_fill()

n = int(numinput("Anzahl","Wieviele Dreiecke sollen gezeichnet werden?"))
m = int(numinput("Anzahl Reihen", "Wieviele Reihen?"))
for count in range(m):
    for count in range(n):
        dreieck()
        penup()
        forward(120)
        pendown()
    penup()
    rt(90)
    forward(120)
    lt(90)
    back(120*n)
    pendown()
    