#basic_loops 2.1:

def guten_morgen():
    print("Guten Morgen!")
    
guten_morgen()

#basic_loops 2.2:
def guten_morgen(name):
    print("Guten Morgen " + name + "!")
    
guten_morgen("Ana")    


#basic_loops 2.3a: [???]
def guten_morgen(name):
    print("Guten Morgen "+name+"!")
    
name = input("Wie ist dein Name?")
    
guten_morgen(name)

#basic_loop 2.3b:
def flaeche_rechteck(laenge, breite):
    flaeche = laenge * breite
    return flaeche

flaeche = flaeche_rechteck(10, 20)
print("Die Fläche beträgt", flaeche, "m2.")

#basic_loop 2.8 a:
i = 0
while i <= 10:
    print(i)
    i += 1
    
#basic_loop 2.8 c:
i = 10
while i >=0:
    print(i)
    i -= 3

    