#üb2.1
def guten_morgen():
    print("Guten Morgen!")
    
guten_morgen()

#üb2.2

def guten_morgen2(name):
    print("Guten Morgen "+name)
    
guten_morgen2("Katrin")

#üb2.3a
def gruss(name):
    print("Guten Morgen "+name+"!")

gruss("Ana")

#üb2.3b
def flaeche(laenge,breite):
    f = laenge*breite
    print("Die Fläche beträgt",str(f),"m2")
    
flaeche(10,20)

#üb2.8a
f = 0
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)
if f<10:
    f=f+1
print(f)


#üb2.8c
g=10
print(g)
if g<=10:
    g=g-3
print(g)
if g<=10:
    g=g-3
print(g)
if g<=10:
    g=g-3
print(g)
if g<=10:
    g=g-1
print(g)