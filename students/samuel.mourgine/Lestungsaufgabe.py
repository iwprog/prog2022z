#Version mit Programm
from turtle import *

def kreuz(quadrat,breite):
    fillcolor("red")
    begin_fill()
    forward(int(quadrat))
    left(90)
    forward(int(quadrat))
    left(90)
    forward(int(quadrat))
    left(90)
    forward(int(quadrat))
    left(90)
    #penup
    penup()
    forward((int(quadrat)-int(breite))/2)
    left(90)
    forward((int(quadrat)-(int(breite)+(int(breite)/6)+int(breite)+(int(breite)/6)))/2)
    pendown()
    end_fill()
    fillcolor("white")
    begin_fill()
    forward(int(breite)+(int(breite)/6))
    left(90)
    forward(int(breite)+(int(breite)/6))
    right(90)
    forward(int(breite))
    right(90)
    forward(int(breite)+(int(breite)/6))
    left(int(breite))
    forward(int(breite)+(int(breite)/6))
    right(90)
    forward(int(breite))
    right(90)
    forward(int(breite)+(int(breite)/6))
    left(90)
    forward(int(breite)+(int(breite)/6))
    right(90)
    forward(int(breite))
    right(90)
    forward(int(breite)+(int(breite)/6))
    left(90)
    forward(int(breite)+(int(breite)/6))
    right(90)
    forward(int(breite))
    end_fill()
    penup()
    forward(300)
#kreuz
#breit = 90
#länge = 90 + 90/6 = 105

kreuz("400","90")

from turtle import *
showturtle()

#version Ohne Programm
#Quadrat
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
end_fill()
#penup
penup()
forward(155)
left(90)
forward(50)

pendown()

#kreuz
#breit = 90
#länge = 90 + 90/6 = 105

fillcolor("white")
begin_fill()
forward(105)
left(90)
forward(105)
right(90)
forward(90)
right(90)
forward(105)
left(90)
forward(105)
right(90)
forward(90)
right(90)
forward(105)
left(90)
forward(105)
right(90)
forward(90)
right(90)
forward(105)
left(90)
forward(105)
right(90)
forward(90)
end_fill()

hideturtle()

