eingabetext = input("Eingabetext: ")
blacklist = input("Blacklist: ")

for wort in eingabetext.split():
    wort_klein = wort.lower()
    if wort_klein in blacklist:
        eingabetext = eingabetext.replace(wort, "*" * len(wort))
print("Ausgabe:", eingabetext)

# Voraussetzung: alle Wörter in der Blacklist sind kleingeschrieben