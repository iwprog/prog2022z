def replace(text, position, einfügetext):
    if text[position] == " ":
        print(text.replace(text, text[:int(position)] + " " + einfügetext + text[(int(position)):]))
    elif int(position) == (int(len(text)) - 1):
        print(text.replace(text, text[:(int(position) + 1)] + " " + einfügetext + text[(int(position) + 1):]))
    elif int(position) == 0:
        print(text.replace(text, einfügetext + " " + text[(int(position)):]))
    else:
        print(text.replace(text, text[:int(position)] + einfügetext + " " + text[(int(position)):]))
# Mit if und else wird nun geschaut, an welcher Stelle das Wort eingefügt wird,
# damit an der richtigen Stelle ein Abstand eingefügt werden kann.
# Mit dem ersten elif kann der Einfügetext am Ende des Strings hinzugefügt werden.
# Mit dem zweiten elif kann der Einfügetext am Anfang des Strings hinzugefügt werden.

replace("das ist ein test", 4, "hier")
replace("das ist ein test", 3, "hier")
replace("das ist ein test", 15, "hier")
replace("das ist ein test", 0, "hier")

