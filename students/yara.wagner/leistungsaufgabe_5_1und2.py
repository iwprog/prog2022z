from urllib.request import urlopen
from csv import writer

with urlopen("https://www.ietf.org/rfc/rfc2616.txt") as source:
    web_content = source.read().decode("utf8")
    worthaeufigkeit = {}
    buchstabenhaeufigkeit = {}
    for wort in web_content.split():
        if wort in worthaeufigkeit:
            worthaeufigkeit[wort] = worthaeufigkeit[wort] + 1
        else:
            worthaeufigkeit[wort] = 1
    for buchstabe in list(web_content):
        if buchstabe in buchstabenhaeufigkeit:
            buchstabenhaeufigkeit[buchstabe] = buchstabenhaeufigkeit[buchstabe] + 1
        else:
            buchstabenhaeufigkeit[buchstabe] = 1

with open("wort.csv", "w", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    for wort, haeufigkeit in worthaeufigkeit.items():
        csv_writer.writerow([wort, haeufigkeit])

with open("buchstabe.csv", "w", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    for buchstabe, haeufigkeit in buchstabenhaeufigkeit.items():
        csv_writer.writerow([buchstabe, haeufigkeit])