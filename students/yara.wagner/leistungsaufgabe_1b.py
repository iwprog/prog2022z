#Erweitern Sie das Programm um eine Eingabemöglichkeit
#(über Eingabefenster) für die Seitenlänge der Schweizer Flagge!


from turtle import *

#Eingabefenster für die Seitenlänge der Schweizer Flagge
laenge = numinput("Schweizer Flagge","Bitte geben Sie eine Seitenlänge ein.")

#roter Hintergrund
pencolor("red") #Stiftfarbe für Hintergrund
fillcolor("red") #Füllfarbe für Hintergrund
hideturtle() #Stift unsichtbar machen
begin_fill() #Füllen starten
forward(laenge) #Hintergrund zeichnen Beginn
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge) #Hintergrund zeichnen Ende
end_fill() #Füllen beenden

#Stift zum Startpunkt des Kreuzes verschieben
penup() #Stift anheben
right(90) #Stift an richtige Stelle setzen
forward(int(laenge/5*2))
right(90)
forward(int(laenge/5))
pendown() #Stift wieder absetzen

#weisses Kreuz
pencolor("white") #Stiftfarbe für Kreuz
fillcolor("white") #Füllfarbe für Kreuz
begin_fill() #Kreuz Füllen starten
forward(int(laenge/5)) #Kreuz zeichnen Beginn
right(90)
forward(int(laenge/5))
left(90)
forward(int(laenge/5))
left(90)
forward(int(laenge/5))
right(90)
forward(int(laenge/5))
left(90)
forward(int(laenge/5))
left(90)
forward(int(laenge/5))
right(90)
forward(int(laenge/5))
left(90)
forward(int(laenge/5))
left(90)
forward(int(laenge/5))
right(90)
forward(int(laenge/5))
left(90)
forward(int(laenge/5)) #Kreuz zeichnen Ende
end_fill() #Füllen beenden
