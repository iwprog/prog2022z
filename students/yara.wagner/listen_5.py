stopwords = ("der", "die", "das", "in", "auf", "unter", "ein", "eine", "ist", "war", "es", "den")
text = input("Bitte geben Sie einen Text ein. ")

def stopword_filter(text, stoppwortliste):
    text_gefiltert = []
    textliste = text.split()
    for wort in textliste:
        if wort.lower() not in stopwords:
            text_gefiltert.append(wort)
    return text_gefiltert

print(stopword_filter(text, stopwords))

# Schwäche des Filters: Wenn ein Wort am Ende eines Satzes mit Satzzeichen steht,
# wird das Wort zusammen mit dem Satzzeichen als ein Element betrachtet. Dadurch
# wird es nicht als Stoppwort erkannt, auch wenn es eines wäre.