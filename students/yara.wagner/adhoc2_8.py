numbers = [12,23,5,56,18,9]

max_value = None
min_value = None

for number in numbers:
    if(max_value is None or number > max_value):
        max_value = number
    if(min_value is None or number < min_value):
        min_value = number

print("Maximalwert:", max_value)
print("Minimalwert:", min_value)
print("Durchschnitt:", sum(numbers)/len(numbers))