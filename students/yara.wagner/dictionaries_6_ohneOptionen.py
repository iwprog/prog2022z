def drei_berge():
    berge = {}
    for i in range(3):
        berg = input("Berg: ")
        hoehe = float(input("Höhe von " + berg + " (in m): "))
        gebirge = input("zugehöriges Gebirge: ")
        berge[berg] = {"Höhe": str(hoehe) + " m (" + str(hoehe * 3.28) + " ft)", "Gebirge": gebirge}
    print("")
    print(berge)
    for berg, details in sorted(berge.items()):
        print(berg, "ist", details["Höhe"], "hoch und gehört zum", details["Gebirge"], "Gebirge.")
    
drei_berge()