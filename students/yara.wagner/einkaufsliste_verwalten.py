from csv import writer
from json import loads, dumps

def artikel_hinzufügen():
    while True:
        artikel = input("Artikel (Abbruch mit x): ")
        if artikel == "x":
            break
        preis = float(input(f"Preis von {artikel}: "))
        menge = float(input(f"Menge von {artikel}: "))
        einkaufsliste.append({"Artikel": artikel, "Preis": preis, "Menge": menge})

def artikel_löschen():
    loeschen = input("Welchen Artikel wollen Sie aus der Einkaufsliste löschen? ")
    for eintrag in einkaufsliste:
        try:
            for zahl, eintrag in enumerate(einkaufsliste):
                if eintrag["Artikel"] == loeschen:
                    del einkaufsliste[zahl]
        except KeyError:
            print(f"Der Artikel {loeschen} existiert nicht in der Einkaufsliste.")

def artikel_suchen():
    suche = input("Nach welchem Artikel wollen Sie suchen? ")
    for eintrag in einkaufsliste:
        if suche in eintrag["Artikel"]:
            print(eintrag["Artikel"])

def einkaufsliste_csv():
    with open("einkaufsliste.csv", "w", encoding="utf8") as file:
        csv_writer = writer(file, delimiter=";")
        for element in einkaufsliste:
            csv_writer.writerow(element.values())

def gesamtbetrag_berechnen():
    summe = 0
    for element in einkaufsliste:
        preis = element["Preis"] * element["Menge"]
        summe = summe + preis
    print(f"Gesamtbetrag des Einkaufs: {summe} CHF")

try:
    with open("einkaufsliste.json", encoding="utf8") as file:
        json_string = file.read()
        einkaufsliste = loads(json_string)
except FileNotFoundError:
    print("Es wurde keine Einkaufsliste gefunden. ")
    einkaufsliste = []

while True:
    print()
    print("Menü:")
    print("(1) Artikel hinzufügen")
    print("(2) Artikel löschen")
    print("(3) Artikel suchen")
    print("(4) Einkaufsliste leeren")
    print("(5) Einkaufsliste in CSV Format exportieren")
    print("(6) Gesamtbetrag des Einkaufs berechnen")
    print("(0) Exit")
    print()
    aktion = int(input("Was wollen sie tun? "))
    if aktion == 1:
       artikel_hinzufügen()
    elif aktion == 2:
        artikel_löschen()
    elif aktion == 3:
        artikel_suchen()
    elif aktion == 4:
        einkaufsliste = []
    elif aktion == 5:
        einkaufsliste_csv()
    elif aktion == 6:
        gesamtbetrag_berechnen()
    elif aktion == 0:
        break
    else:
        continue

with open("einkaufsliste.json", "w", encoding="utf8") as file:
    file.write(dumps(einkaufsliste))