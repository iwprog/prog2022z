from json import loads, dumps

berge = {}

try:
    with open("berge.json") as file:
        bergliste = file.read()
        berge = loads(bergliste)
except FileNotFoundError:
    print("Leider keine gespeicherte Bergliste vorhanden.")
    berge = {}

while True:
    berg = input("Berg (Abbruch mit 'x'): ")
    if berg == "x":
        break
    hoehe = float(input("Höhe von " + berg + " (in m): "))
    berge[berg] = str(hoehe) + " m (" + str(hoehe * 3.28) + " ft)"
print("")

for berg, hoehe in sorted(berge.items()):
    print(berg, "ist", hoehe, "hoch.")

with open("berge.json", "w") as file:
    bergliste = dumps(berge)
    file.write(bergliste)