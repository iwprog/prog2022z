icao_alphabet = {"A": "Alfa", "B": "Bravo", "C": "Charlie", "D": "Delta", "E": "Echo", "F": "Foxtrot", "G": "Golf", "H": "Hotel", "I": "India", "J": "Juliett", "K": "Kilo", "L": "Lima", "M": "Mike", "N": "November", "O": "Oscar", "P": "Papa", "Q": "Quebex", "R": "Romeo", "S": "Sierra", "T": "Tango", "U": "Uniform", "V": "Victor", "W": "Whiskey", "X": "X-ray", "Y": "Yankee", "Z": "Zulu", }
wort = input("Bitte geben Sie ein Wort ein. ")

def icao(inputwort):
    liste = []
    inputwort = inputwort.replace("ä", "ae")
    inputwort = inputwort.replace("ö", "oe")
    inputwort = inputwort.replace("ü", "ue")
    for buchstabe in inputwort:
        buchstabe = buchstabe.upper()
        liste.append(icao_alphabet[buchstabe])
    for element in liste:
        print(element, end = "-")

icao(wort)

# Am Ende hat es einen Bindestrich zu viel, konnte ich leider bisher noch nicht lösen