# 3.1a
person = {"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"}

# 3.1b
telefonbuch = []
telefonbuch.append(person)

# 3.1c
telefonbuch.append({"Vorname": "Tim", "Nachname": "Kurz", "Phone": "732"})
telefonbuch.append({"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"})

# 3.1d
while True:
    vorname = input("Vorname (beenden mit 'x'): ")
    if vorname == "x":
        break
    nachname = input("Nachname: ")
    phone = input("Telefonnummer: ")
    telefonbuch.append({"Vorname": vorname, "Nachname": nachname, "Phone": phone})
print("Datenstruktur:", telefonbuch)