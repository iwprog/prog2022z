raum1 = ["Kaugummi"]
raum2 = ["Ich"]
raum3 = ["Motorrad"]
raum4 = ["Benzin"]
inventar = []

print("Raum 1:", raum1)
print("Raum 2:", raum2)
print("Raum 3:", raum3)
print("Raum 4:", raum4)
print("Inventar:", inventar)
print("")
print("Du befindest dich aktuell in Raum 2. Gehe in andere Räume und führe Aktionen durch, um aus dieser Welt zu flüchten.")
print("")

while True:
    raum = input("In welchem Raum bist du? (raum1, raum2, raum3, raum4) ")
    bewegung = input("Was willst du tun? (links, rechts, nimm, benutze) ")
    if raum == "raum2" and bewegung == "links" and raum1 == ["Kaugummi"]:
        raum2.remove("Ich")
        raum1.append("Ich")
        print("")
        print("Auf dem Boden liegt ein Kaugummi, nimm ihn dir, vielleicht kann er dir später noch nützen.")
        print("")
    elif raum == "raum1" and bewegung == "nimm":
        raum1.remove("Kaugummi")
        inventar.append("Kaugummi")
        print("")
        print("Inventar:", inventar)
        print("")
    elif raum == "raum1" and bewegung == "rechts":
        raum1.remove("Ich")
        raum2.append("Ich")
        print("")
        print("Hier bist du gestartet, der Raum ist leer.")
        print("")
    elif raum == "raum3" and bewegung == "links":
        raum3.remove("Ich")
        raum2.append("Ich")
        print("")
        print("Hier bist du gestartet, der Raum ist leer.")
        print("")
    elif raum == "raum2" and bewegung == "rechts":
        raum2.remove("Ich")
        raum3.append("Ich")
        print("")
        print("Du findest ein altes Motorrad, leider hat der vordere Pneu ein Loch und der Tank ist leer.")
        print("Repariere das Motorrad und nimm es, um zu flüchten!")
        print("")
    elif raum == "raum3" and bewegung == "rechts" and raum4 == ["Benzin"]:
        raum3.remove("Ich")
        raum4.append("Ich")
        print("")
        print("Hier findest du einen vollen Benzinkanister. Nimm ihn mit!")
        print("")
    elif raum == "raum4" and bewegung == "nimm":
        raum4.remove("Benzin")
        inventar.append("Benzin")
        print("")
        print("Inventar:", inventar)
        print("")
    elif raum == "raum4" and bewegung == "links":
        raum4.remove("Ich")
        raum3.append("Ich")
        print("")
        print("Du findest ein altes Motorrad, leider hat der vordere Pneu ein Loch und der Tank ist leer.")
        print("Repariere das Motorrad und nimm es, um zu flüchten!")
        print("")
    elif raum == "raum3" and bewegung == "benutze":
        if inventar == []:
            print("")
            print("Du besitzt keine Gegenstände gesammelt, die du benutzen könntest.")
            print("")
        elif inventar == ["Kaugummi"]:
            inventar.remove("Kaugummi")
            print("")
            print("Super, du hast das Loch im Pneu geflickt!")
            print("Tanke das Motorrad und nimm es, um zu flüchten!")
            print("")
        elif inventar == ["Benzin"]:
            inventar.remove("Benzin")
            print("")
            print("Das Motorrad ist nun vollgetankt!")
            print("Repariere den Pneu und nimm das Motorrad, um zu flüchten!")
            print("")
        elif inventar == ["Benzin", "Kaugummi"] or inventar == ["Kaugummi", "Benzin"]:
            inventar.remove("Kaugummi")
            inventar.remove("Benzin")
            print("")
            print("Super, du hast das Loch im Pneu geflickt und das Motorrad getankt! Nimm es und flüchte damit!")
            print("")
    elif raum == "raum3" and inventar == [] and bewegung == "nimm":
        print("")
        print("Du hast es geschafft! Nun aber schnell weg hier!")
        print("")
        raum3.remove("Motorrad")
        raum3.remove("Ich")
        break
print("Raum 1:", raum1)
print("Raum 2:", raum2)
print("Raum 3:", raum3)
print("Raum 4:", raum4)
print("Inventar:", inventar)

# aktuell gibt es noch Probleme im Code:
# - nicht alle Fälle sind abgedeckt
# - wenn man ein zweites Mal in Raum 1 oder Raum 4 geht (und dieser leer ist), erhält man einen Fehler, wenn man den Raum wechselt.
# - es wird kein Text angezeigt, wenn man die Bewegungen nimm oder benutze ausführt, sie aber eigentlich nicht möglich sind.