# Variante 1: -1 wird bei der Summe nicht miteinberechnet

summe = 0
zahl = input("Bitte geben Sie eine Zahl ein: ")

while zahl != "-1":
    summe = summe + int(zahl)
    zahl = input("Bitte geben Sie eine Zahl ein: ")
print(summe)


#Variante 2: -1 wird bei der Summe miteinberechnet
# summe = 0
# zahl = 0
# 
# while zahl != "-1":
#     zahl = input("Bitte geben Sie eine Zahl ein: ")
#     summe = summe + int(zahl)
# print(summe)