staedte = {"Zürich": 370000, "Genf": 190000, "Basel": 170000, "Bern": 130000}

sortierliste = []
for stadt, einwohnerzahl in staedte.items():
    sortierliste.append([einwohnerzahl, stadt])

for einwohner, stadt in sorted(sortierliste):
    print(stadt, "hat", einwohner, "Einwohner")