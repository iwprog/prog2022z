icao_alphabet = {"A": "Alfa", "B": "Bravo", "C": "Charlie", "D": "Delta", "E": "Echo", "F": "Foxtrot", "G": "Golf", "H": "Hotel", "I": "India", "J": "Juliett", "K": "Kilo", "L": "Lima", "M": "Mike", "N": "November", "O": "Oscar", "P": "Papa", "Q": "Quebex", "R": "Romeo", "S": "Sierra", "T": "Tango", "U": "Uniform", "V": "Victor", "W": "Whiskey", "X": "X-ray", "Y": "Yankee", "Z": "Zulu", }
wort = input("Bitte geben Sie ein Wort ein. ")

def icao(inputwort):
    liste = []
    abbruch = "false"
    for buchstabe in wort:
        buchstabe = buchstabe.upper()
        if buchstabe not in icao_alphabet.keys():
            abbruch = "true"
            abbruch_buchstabe = buchstabe
        else:
            liste.append(icao_alphabet[buchstabe])
    if abbruch == "true":
        print("Kann " + wort + " nicht buchstabieren, da " + str(abbruch_buchstabe) + " nicht definiert wurde.")
    else:
        for element in liste:
            print(element, end = "-")

icao(wort)