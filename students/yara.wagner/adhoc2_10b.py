from turtle import *

def dreieck():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    backward(50)
    left(60)
    forward(50)
    right(120)
    forward(50)
    end_fill()
    penup()
    left(60)
    forward(70)
    pendown()

i=0
anzahl = input("Wie viele Dreiecke sollen gezeichnet werden? ")

while i < int(anzahl):
    dreieck()
    i = i + 1
    