#1.1a Erstellen Sie eine Liste jahreszeiten mit den vier Jahreszeiten.

jahreszeiten = ["Winter", "Frühling", "Sommer", "Herbst"]


#1.1b Löschen Sie die Jahreszeit "Frühling" aus der Liste.

jahreszeiten.remove("Frühling")

#1.1c Fügen Sie die Jahreszeit "Langas" der Liste hinzu.

jahreszeiten.append("Langas")