from random import choice

auswahlliste = ["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]


while True:
    tuerchen = input("Welche Kalendertür wollen Sie öffnen (oder x für Exit): ")
    zufallselement = choice(auswahlliste)
    if tuerchen == "x":
        break
    elif int(tuerchen) in range(1,25):
        print(zufallselement)
    else:
        print("ungültiger Wert")