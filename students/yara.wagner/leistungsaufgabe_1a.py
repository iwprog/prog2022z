# Erstellen Sie die Schweizer Flagge mit der turtle-Grafik und exakt folgendem Aussehen
#(400x400 Pixel), es darf kein Pfeil/Turtle sichtbar sein!


from turtle import *

#roter Hintergrund
pencolor("red") #Stiftfarbe
fillcolor("red") #Füllfarbe
hideturtle() #Stift unsichtbar machen
begin_fill() #Füllen starten
forward(400) #Quadrat zeichnen Beginn
right(90)
forward(400)
right(90)
forward(400)
right(90)
forward(400) #Quadrat zeichnen Ende
end_fill() #Füllen beenden

#Stift zum Startpunkt des Kreuzes verschieben
penup() #Stift anheben
right(90) #Stift an richtige Stelle setzen
forward(160)
right(90)
forward(80)
pendown() #Stift wieder absetzen

#weisses Kreuz
pencolor("white") #Stiftfarbe
fillcolor("white") #Füllfarbe
begin_fill() #Füllen starten
forward(80) #Kreuz zeichnen Beginn
right(90)
forward(80)
left(90)
forward(80)
left(90)
forward(80)
right(90)
forward(80)
left(90)
forward(80)
left(90)
forward(80)
right(90)
forward(80)
left(90)
forward(80)
left(90)
forward(80)
right(90)
forward(80)
left(90)
forward(80) #Kreuz zeichnen Ende
end_fill() #Füllen beenden
