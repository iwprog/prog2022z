from turtle import *

def rahmen(hoehe, breite, rahmenfarbe, fuellfarbe):
    pencolor(rahmenfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    for i in range(1,3):
        forward(breite)
        left(90)
        forward(hoehe)
        left(90)
    end_fill()
    penup()
    forward(breite/4)
    left(90)
    forward(hoehe/4)
    fillcolor("white")
    begin_fill()
    right(90)
    pendown()
    for i in range(1,3):
        forward(breite/2)
        left(90)
        forward(hoehe/2)
        left(90)
    end_fill()

# Wo soll man hier Schleifen einbauen?