def billetpreis(alter, km):
    if alter < 6:
        preis = 0
    elif alter < 16:
        preis = (2 + int(km)*0.25)/2
    else:
        preis = 2 + int(km)*0.25
    print("Der Preis für Ihr Billet beträgt", preis, "CHF")