liste = ["Das", "ist", "ein", "Test", "1", "2", "3", "123"]
liste2 = []

#Variante 1:
# def entfernen():
#     for element in liste:
#         if len(element) < 3:
#             liste.remove(element)
#     print(liste)
# 
# print(liste)
# entfernen()

#Problem: Wenn ein Element aus der Liste entfernt wird,
#rutschen die anderen Elemente einen Index nach vorne,
#wodurch das nächste Element nicht geprüft wird.

#Variante 2:
def entfernen2():
    for element in liste:
        if len(element) >= 3:
            liste2.append(element)
    print(liste2)

print(liste)
entfernen2()
