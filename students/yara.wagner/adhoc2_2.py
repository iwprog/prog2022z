from turtle import *

def dreieck(laenge, color, bgcolor):
    pensize(3)
    pencolor(color)
    fillcolor(bgcolor)
    begin_fill()
    forward(laenge)
    left(120)
    forward(laenge)
    left(120)
    forward(laenge)
    end_fill()

right(90)
dreieck(numinput("Blaues Dreieck", "Bitten geben Sie eine Seitenlänge an."), "red", "cyan")
dreieck(numinput("Gelbes Dreieck", "Bitten geben Sie eine Seitenlänge an."), "red", "yellow")
dreieck(numinput("Grünes Dreieck", "Bitten geben Sie eine Seitenlänge an."), "red", "lightgreen")