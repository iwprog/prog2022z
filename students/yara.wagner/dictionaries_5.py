def drei_berge():
    berge = {}
    for i in range(3):
        berg = input("Berg: ")
        hoehe = float(input("Höhe von " + berg + " (in m): "))
        berge[berg] = str(hoehe) + " m (" + str(hoehe * 3.28) + " ft)"
    print("")
    for berg, hoehe in sorted(berge.items()):
        print(berg, "ist", hoehe, "hoch.")

drei_berge()