woerterbuch = {"Haus": "house", "Zahnarzt": "dentist", "Katze": "cat", "Essen": "food", "Schokolade": "chocolate", "Hallo": "hello", "Zwei": "two", "Auto": "car", "Welt": "world", "Baum": "tree"}
counter = 0

for wort, word in woerterbuch.items():
    loesung = input("Was heisst " + wort + ": ")
    if loesung == word:
        print("RICHTIG!")
        counter = counter + 1
    else:
        print("FALSCH!")

print("Sie haben", counter, "von 10 Vokabeln richtig beantwortet!")