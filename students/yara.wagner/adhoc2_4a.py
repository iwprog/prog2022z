from turtle import *
from math import pi

radius = numinput("Radius", "Bitte geben Sie den Radius ein.")
hoehe = numinput("Höhe", "Bitte geben Sie die Höhe ein.")
volumen = radius**2*pi*hoehe

print("Das Volumen beträgt:", volumen)