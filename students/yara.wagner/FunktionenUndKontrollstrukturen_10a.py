def option1():
    alter = input("Bitte geben Sie Ihr Alter ein: ")
    def herzfrequenz(alter):
        herzfrequenz = 220 - int(alter)
        return herzfrequenz
    print("")
    print("Ihre berechnete Herzfrequenz beträgt:",herzfrequenz(alter))
    print("")

def option2():
    def billetpreis(alter, km):
        if alter < 6:
            preis = 0
        elif alter < 16:
            preis = (2 + int(km)*0.25)/2
        else:
            preis = 2 + int(km)*0.25
        return preis
    alter = int(input("Geben Sie Ihr Alter an. "))
    km = int(input("Geben Sie die Distanz in km an. "))
    print("")
    print("Der Preis für Ihr Billet beträgt", billetpreis(alter, km), "CHF")
    print("")

while True:
    print("========================================")
    print("Programmübersicht:")
    print("1 ... Herzfrequenz berechnen")
    print("2 ... Billetpreis berechnen")
    print("")
    print("0 ... Programm beenden")
    print("========================================")
    wahl = input("Gewählte Option: ")
    print("")
    if wahl == "1":
        print("Herzfrequenz berechnen")
        print("----------------------")
        option1()
    elif wahl == "2":
        print("Billetpreis berechnen")
        print("----------------------")
        option2()
    elif wahl == "0":
        break
    else:
        print("Ungültige Option!")
        print("")
