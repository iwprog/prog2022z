preisliste = {'Milch': 2.05, 'Orangen': 3.75, 'Tomaten': 2.2, 'Tee': 4.2, 'Peanuts': 3.9, 'Ketchup': 2.1}
neue_liste = {}
enthaelt_en = "falsch"

for item in preisliste:
    for position, buchstabe in enumerate(item.lower()):
        if buchstabe == "e":
            position_e = int(position)
            try:
                if item[position_e] == "e" and item[position_e+1] == "n":
                    enthaelt_en = "wahr"
                else:
                    enthaelt_en = "falsch"
            except IndexError as error:
                continue
    if enthaelt_en == "falsch":
        neue_liste[item] = preisliste[item]
print(neue_liste)

# Mit try except wird wird der Fall abgefangen, wenn ein "e" an letzter Stelle
# des Strings steht, und es dadurch keinen nächsten Buchstaben gibt.