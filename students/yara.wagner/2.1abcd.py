# 2.1a
preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

# 2.1b
preisliste["Milch"] = 2.05

# 2.1c
del preisliste["Brot"]

# 2.1d
preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1