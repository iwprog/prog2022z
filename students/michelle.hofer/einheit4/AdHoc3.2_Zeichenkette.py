#Michelle Hofer
#AdHoc 2.3
#a) Schreiben Sie ein Programm, das eine Zeichenkette einliest und eine Zeichenkette ausgibt, die nur aus Sternchen (*) besteht und
    #genauso lang ist wie die eingegebene Zeichenkette.
text = input('Text eingeben: ')
print(len(text) * '*')

#b) Schreiben Sie ein Programm, das eine Zeichenkette und ein Zeichen (hintereinander in zwei getrennten
    #Eingabefeldern) einliest. Ausgegeben werden sollen alle Positionen, an denen das Zeichen,
    #unabhängig von Gross- und Kleinschreibung, in der Zeichenkette vorkommt.
    #Tipp: Benutzen Sie ein FOR-Schleife zusammen mit der Funktion enumerate.
    #Eingabe: Wenn Fliegen hinter Fliegen fliegen
    #Zeichen: f
    #Ausgabe: 6 21 29

text = input('Satz eingeben: ').lower()
zeichen = input(' Zeichen eingeben: ').lower()

for position, element in enumerate(text, start = 1):
    if element == zeichen:
        print(position)

#c)
   

