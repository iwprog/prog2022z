#Michelle Hofer
#Funktionen und Kontrollstrukturen 11, Spass mit Schleifen
#a)
print('\na)')
for i in range(0,9,1):
    print(i * '# ')
#b)
print('\nb)')
for i in range(8,0,-1):
    print(i * '# ')
#c)
print('\nc)')
for i in range(9):
    print(i * '  ' + '# ' * (8-i))
#c) Variante
print('\nc) Variante')
for i in range(8,0,-1):
    print((8-i) * '  ' + '# ' * i)
#d)
print('\nd)')
for i in range(9):
    print((8-i) * '  ' + '# ' * i)
#e)
print('\ne)')    
hashtag1 = '# '
print(7*hashtag1)
for i in range(5):
    print('#           #')
print(7*hashtag1)

#g)
print('\ng)')
hashtag2 = '# '
abstand = '  '
for i in range(7,0,-1):
    if (i == 7 or i == 1):
        print (7 * hashtag2)
    else:
        print(abstand * (i-1) + hashtag2)     

#g) Variante Sabina
'''n=0
m=12
print(7*'# ')
for i in range(5):
    print('         #'[n:m])
    n=n+2
print(7*'# ')

#g) Variante Weichselbraun:
print(7 * '# ')
for i in range(5,0,-1):
    print(i * '  ' + '#')
print(7*'# ')'''

#j)
print('\nj)')
for i in range(11,0,-2):
    print((11-i) * ' ' + '# ' * i)
    
