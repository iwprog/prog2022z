#Michelle Hofer
#Was Soll ich zeichnen? Kreis oder Dreieck?
#wieviele sollen es sein? Wieviele Reihen

from turtle import *
speed(5)
penup()
back(700)
pendown()
def dreieck():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    for i in range(3):
        forward(100)
        left(120)
    end_fill()
    
def quadrat():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    for i in range(4):
        forward(200)
        left(90)
    end_fill()

#2.10 b und c
eingabe = textinput('Eingabe','Was soll ich zeichnen? dreieck oder quadrat')
if eingabe == 'dreieck':
    eingabe2 = numinput('Eingabe','Wieviele soll ich zeichnen')
    eingabe3 = numinput('Eingabe', 'Wieviele Reihen?')
    for n in range(int(eingabe3)):
        for k in range(int(eingabe2)): 
            dreieck()
            penup()
            forward(150)
            pendown()
        penup()
        rt(90)
        fd(150)
        lt(90)
        back(150*eingabe2)
        pendown()

elif eingabe == 'quadrat':
    eingabe2 = numinput('Eingabe','Wieviele soll ich zeichnen')
    for k in range(int(eingabe2)): 
        quadrat()
        penup()
        forward(350)
        pendown()


hideturtle()
exitonclick()
