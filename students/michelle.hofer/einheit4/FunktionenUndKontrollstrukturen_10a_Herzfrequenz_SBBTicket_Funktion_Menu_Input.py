#Michelle Hofer
#Funktionen und Kontrollstrukturen 10a
#Programmübersicht:
    #1 ... Herzfrequenz berechnen
    #2 ... Preis für Fahrkarte berechnen
    #0 ... Programm beenden

#Funktion Herzfrequenz
def berechne_herzfrequenz(alter):
    herzfrequenz = 220 - alter
    return(herzfrequenz)

#Funktion Billetpreis
def get_billet_preis(alter,strecke):
    resultat = 2 + 0.25 * strecke
    if alter <6:
        return(0)
    elif alter <16:
        return(int(resultat/2))
    else:
        return(int(resultat))

#Menü Schlaufe
while True:
    print()
    print('==================================================')
    print('Programmübersicht:')
    print('  1 ... Herzfrequenz berechnen')
    print('  2 ... Preis für Fahrkarte berechnen')
    print()
    print('  0 ... Programm beenden')
    print('==================================================')
    print()
    eingabe = int(input('Gewählte Option: '))
    if eingabe == 0:
        break
    elif eingabe == 1:
        print()
        print('Maximale Herzfrequenz berechnen')
        print('-------------------------------')
        alter = int(input('Bitte geben Sie Ihr Alter ein: '))
        print('Ihre maximale Herzfrequenz beträgt:', berechne_herzfrequenz(alter), 'bpm')
    elif eingabe == 2:
        print()
        print('Preis für Fahrkarte berechnen')
        print('-----------------------------')
        alter = int(input('Bitte geben Sie Ihr Alter ein: '))
        strecke = int(input('Wie viele km wollen Sie reisen? '))
        print()
        print('Die Fahrkarte kostet:', get_billet_preis(alter,strecke), 'CHF')
    else:
        print()
        print('Ungültige Option!')
