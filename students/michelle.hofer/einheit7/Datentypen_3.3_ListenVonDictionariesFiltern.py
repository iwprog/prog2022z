#3.3 Listen von Dictionaries ltern
#a. Entfernen Sie alle Datensätze aus dem Telefonbuch, bei denen eine '1' in der Telefonnummer vorkommt.
datensatz = {'Vorname' : 'Ana', 'Nachname' : 'Skupch', 'Phone' : '123'}
Telefonbuch = [datensatz]
Telefonbuch.append({"Vorname": "Tim", "Nachname": "Kurz", "Phone": '732'})
Telefonbuch.append({"Vorname": "Julia", "Nachname": "Lang", "Phone": '912'})

Telefonbuch2 = []
for index, element in enumerate(Telefonbuch):
    if '1' not in Telefonbuch[index]['Phone']:
        Telefonbuch2.append(Telefonbuch[index])
        
print(Telefonbuch2)