#3 Kombination von Listen und Dictionaries
#3.1 Listen von Dictionaries erstellen
#a. Erstellen Sie ein Dictionary für den folgenden Datensatz: Vorname ->Ana, Nachname -> Skupch, Phone -> 123.
print('a)')
datensatz = {'Vorname' : 'Ana', 'Nachname' : 'Skupch', 'Phone' : 123}
print(datensatz)

#b. Fügen Sie den Datensatz der Liste Telefonbuch hinzu.
print('b)')
Telefonbuch = [datensatz]
print(Telefonbuch)

#c. Fügen Sie die folgenden beiden Datensätze der Liste hinzu: Vorname ->Tim, Nachname -> Kurz,
#Phone -> 732; Vorname -> Julia, Nachname -> Lang, Phone -> 912
print('c)')
Telefonbuch.append({"Vorname": "Tim", "Nachname": "Kurz", "Phone": 732})
Telefonbuch.append({"Vorname": "Julia", "Nachname": "Lang", "Phone": 912})

print(Telefonbuch)
#d. Schreiben Sie ein Programm, welches Personendaten abfragt und diese so lange in Ihr Telefonbuch einfügt,
#bis der Benutzer 'x' eingibt. Im Anschluss soll die erstellte Datenstruktur ausgegeben werden.
# Vorname: Tim
# Nachname: Kurz
# Phone: 712
# Vorname: Jasna
# Nachname: Tusek
# Phone: 181
# Vorname: x
# Datenstruktur: [{"Vorname": "Tim", "Nachname": "Kurz", "Phone": "712"},
# {"Vorname": "Jasna", "Nachname": "Tusek", "Phone": "181"}]
print('d)')

while True:
    vorname = input('Vorname oder abbrechen mit x: ')
    if vorname == 'x':
        break
    nachname = input('Nachname: ')
    phone = input('Phone: ')
    Telefonbuch.append({'Vorname' : vorname, 'Nachname' : nachname, 'Phone' : phone})
print('Datenstruktur:', Telefonbuch)