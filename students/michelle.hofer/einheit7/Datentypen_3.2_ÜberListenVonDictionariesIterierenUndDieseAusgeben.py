# 3.2 Über Listen von Dictionaries iterieren und diese ausgeben
# Bitte verwenden Sie das Telefonbuch aus dem obigen Beispiel für die folgenden Übungen:
# a. Schreiben Sie eine Funktion datensatz_ausgeben, welche den ersten Datensatz aus obigen Beispiel wie folgt ausgibt:
# Vorname : Ana 
# Nachname: Skupch
# Phone : 123
print('a)')
datensatz = {'Vorname' : 'Ana', 'Nachname' : 'Skupch', 'Phone' : '123'}
Telefonbuch = [datensatz]
Telefonbuch.append({"Vorname": "Tim", "Nachname": "Kurz", "Phone": '732'})
Telefonbuch.append({"Vorname": "Julia", "Nachname": "Lang", "Phone": '912'})

def datensatz_ausgeben(liste):
    print("Vorname:", liste[0]["Vorname"])
    print("Nachname:", liste[0]["Nachname"])
    print("Phone:", liste[0]["Phone"])

datensatz_ausgeben(Telefonbuch)

# b. Iterieren Sie über das Telefonbuch und geben Sie alle Vornamen aus.
print('b)')

for index, vorname in enumerate(Telefonbuch):
    print(Telefonbuch[index]['Vorname'])

# c. Iterieren Sie über das Telefonbuch und geben Sie alle Datensäzte mittels der Funktion datensatz_ausgeben aus.
print('c)')

def datensatz_ausgeben2(liste):
    for index, element in enumerate(liste):
        print("Vorname:", liste[index]["Vorname"])
        print("Nachname:", liste[index]["Nachname"])
        print("Phone:", liste[index]["Phone"])

datensatz_ausgeben2(Telefonbuch)

# d. Geben Sie alle Datensätze aus, bei denen eine '1' in der Telefonnummer vorkommt.
print('d)')
for index, vorname in enumerate(Telefonbuch):
    if '1' in Telefonbuch[index]['Phone']:
        print("Vorname:", Telefonbuch[index]["Vorname"],", Nachname:", Telefonbuch[index]["Nachname"],", Phone:",
              Telefonbuch[index]["Phone"] )
    
# e. Geben Sie alle Datensätze aus, bei denen der Vorname der Person drei Zeichen lang ist.
# f. Geben Sie alle Datensäzte aus, bei denen entweder der Vorname oder der Nachname die Zeichenfolge 'an' enthält.
# g. Geben Sie alle Datensäzte aus, bei denen entweder der Vorname oder der Nachname die Zeichenfolge 'an' enthält.
# Die Gross-/Kleinschreibung soll jedoch dieses mal ignoriert werden.