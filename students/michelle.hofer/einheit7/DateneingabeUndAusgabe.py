# d) öffnen sie die folgende url (https://www.gutenberg.org/cache/epub/6079/pg6079.txt)
# in ihrem web browser und speichern sie die zugehörige textdatei in ihrem python verzeichnis.
# schreiben sie nun ein python programm, das
    # 1) diese auf dem bildschirm ausgibt und 
    # 2) ermittelt wie oft die worte "Himmel", "Freiheit", "Spiel" und "Tanz" in der datei vorkommen.
    # (hinweis: es ist wichtig, dass sich ihr python programm und die textdatei im selben verzeichnis befinden,
    # da ansonsten python die datei nicht öffnen kann).
    # 3) erweitern Sie Ihr Python Programm, sodass es die Datei direkt aus dem Web bezieht,
    # sodass das manuelle Speichern der Datei entfallen kann.

#1)
with open('gutenberg.txt', 'r', encoding = 'utf8') as f:
    file_content = f.read()
print(file_content)

#2)
anz_himmel = file_content.count('Himmel')
anz_freiheit = file_content.count('Freiheit')
anz_spiel = file_content.count('Spiel')
anz_tanz = file_content.count('Tanz')

print('Himmel:', anz_himmel, 'Freiheit:', anz_freiheit, 'Spiel:', anz_spiel, 'Tanz:', anz_tanz)

#3)
from urllib.request import urlopen

with urlopen("https://www.gutenberg.org/cache/epub/6079/pg6079.txt") as file:
    content = file.read().decode("utf8")

print(content)
#obige variablen (anz_himmel etc.) einfügen und abändern auf  = content.count
