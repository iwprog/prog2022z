# Leistungsaufgabe 3

# a) Schreiben Sie ein Programm, bei dem Städte und deren Einwohnerzahlen eingegeben werden können.
# Wenn der Benutzer bei einer Stadt ein „x“ eingibt, wird die Eingabe gestoppt.

# Bsp.:
# Geben Sie die Stadt ein: Zürich
# Geben Sie die zugehörige Einwohnerzahl ein: 370000
# Geben Sie die Stadt ein: Bern
# Geben Sie die zugehörige Einwohnerzahl ein: 130000
# Geben Sie die Stadt ein: x

# Während der Eingabe soll im Programm eine Liste (list) erzeugt werden, wie die in der ad hoc Übung 3.2 b).
# Bsp.: [["Zürich", 370000], ["Bern", 130000]]
# Geben Sie die Liste am Ende des Programmes alphabetisch sortiert aus, d.h. einfache Ausgabe mit print(liste).
# Bsp.: [["Bern", 130000], ["Zürich", 370000] ]
print('a) Städteliste')

def erzeuge_liste():
    staedte = []
    while True:
        stadt = input('Geben Sie die Stadt ein (Abbruch mit x): ')
        if stadt == 'x':
            break
        einwohner = int(input('Geben Sie die zugehörige Einwohnerzahl ein: '))
        staedte.append([stadt, einwohner])
    return sorted(staedte)

print(erzeuge_liste())

# b) Erstellen Sie mittels einem Dictionary (dict) ein Mini-Deutsch-Englisch Wörterbuch (10 Vokabeln).
# Ein zugehörige Vokabeltrainer soll alle Vokabeln nach folgendem Muster abfragen:
# Was heisst Haus: house
# RICHTIG!
# Was heisst Zahnarzt: dentist
# RICHTIG!
# Was heisst Zahn: dent
# FALSCH!
# …
# Nach Abfrage aller Vokabeln wird noch die Abfragestatistik ausgegeben:
# Sie haben 7 von 10 Vokabeln richtig beantwortet!
print('b) Vokabeltrainer')
vokabeln = {'Haus' : 'house', 'Maus' : 'mouse', 'Laus' : 'louse', 'Graus' : 'horror', 'Applaus' : 'applause',
            'Teig' : 'dough', 'Husten' : 'cough', 'Sumpf' : 'slough', 'Komplettlösung' : 'walkthrough', 'Lachen' : 'laugh'}

def vokabeltrainer():
    count = 0
    for deutsch, englisch in vokabeln.items():
        abfrage =input('Was heisst ' + deutsch + ' (abbruch mit x) : ')
        abfrage = abfrage.lower()
        if abfrage == 'x':
            break
        elif abfrage == englisch:
            count = count + 1
            print('RICHTIG!')
        else:
            print('FALSCH! Es würde', englisch, 'heissen, tztztz!')
    print('Sie haben', count, 'von', len(vokabeln), 'richtig beantwortet!')

vokabeltrainer()   
