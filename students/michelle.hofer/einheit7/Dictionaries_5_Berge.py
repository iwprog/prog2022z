# Michelle Hofer
# 5. Berge: Schreiben Sie ein Programm, welches für drei Berge deren Höhe in m entgegennimmt und diese
# anschliessend alphabetisch sortiert mit der Höhe in m und in Feet (ft; 1m =3.28 ft) ausgibt.
#1. Berg: Mt. Everest
#1. Höhe: 8848
# 2. Berg: ...
# 2. Höhe: ...
#Mt. Everest ist 8848 m (29029 ft) hoch.

berge = {}
for i in range(3):
    name = input('Name Berg? ')
    hoehe = input('Hoehe des Bergs in Meter? ')
    berge[name]= {'Hoehe m':hoehe, 'Hoehe ft': int(float(hoehe)*3.28)} #int verwendent, damit die Ausgabe nicht 13120.0 ft ist
for key, element in sorted(berge.items()):
    print(key, 'ist', berge[key]['Hoehe m'],'m ('+str(berge[key]['Hoehe ft'])+' ft) hoch.')
        


    