#Drei unterschiedlich grosse Dreicke mit manueller Seitenlänge mit Funktion
#Michelle Hofer

from turtle import *
def dreieck():
    laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben")
    begin_fill()
    left(150)
    forward(laenge)
    left(120)
    forward(laenge)
    left(120)
    forward(laenge)
    left(90)
    end_fill()

pencolor("red")
pensize(5)
speed(5)
fillcolor("yellow")
dreieck()

fillcolor("cyan")
dreieck()

fillcolor("lightgreen")
dreieck()