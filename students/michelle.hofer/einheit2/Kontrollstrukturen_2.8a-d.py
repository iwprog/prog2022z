#Michelle Hofer, Schleifen mit while
#a) schreiben Sie eine Schleife, welche von 0 bis 10 zählt.
i = 0
anzahl = 10
while i <= anzahl:
    print(i)
    i = i + 1

#Variante mit for anstatt while
#for i in range(11):
    #print(i)

#b) Schreiben Sie eine Schleife, welche von 10 bis 0 zählt.
i = 10
anzahl = 0
while i >= anzahl:
    print(i)
    i = i - 1
    
#Variante mit for anstatt while
#for i in range(10,-1,-1):
    #print(i)

#c) Schreiben Sie eine Schleife, welche von 10 bis 0 in Dreierschritten zählt.
i = 10
anzahl = 0
while i >= anzahl:
    print(i)
    i = i - 3

#d) Schreiben Sie eine Schleife, welche von 10 bis 0 zählt und statt 0 das Wort "Start" ausgibt.
i = 10
anzahl = 0
while i >= anzahl:
    if i == 0:
        print("Start")
    else:
        print(i)
    i = i - 1
    
#e) Schreiben Sie eine Schleife, welche drei Zahlen engtegennimmt und die Summe aus diesen berechnet.