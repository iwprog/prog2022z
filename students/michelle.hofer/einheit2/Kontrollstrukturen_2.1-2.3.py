#Michelle Hofer, Kontrollstrukturen 2.1-2.3
#2.1 Funktionen ohne Rückgabewert und Parameter
def guten_morgen1():
    print("Guten Morgen!")
    
guten_morgen1()

#2.2 Funktionen ohne Rückgabewert mit Parameter
def guten_morgen2(name):
    print("Guten Morgen " + name + "!")
    
guten_morgen2("Michelle")

#2.3 Funktionen mit Rückgabewert und Parameter
#a)
def guten_morgen3(name):
    return "Guten Morgen " + name + "!"
    
gruss = guten_morgen3("Michelle")
print(gruss)

#b)
def flaeche_rechteck(laenge,breite):
    resultat = laenge * breite
    return resultat
    
flaeche = flaeche_rechteck(10,20)
print("Die Fläche beträgt", flaeche, "m2.")

#c)
def inch_to_cm(inches):
    resultat = inches * 2.54
    return resultat
    
inch = 1
laenge_in_cm = inch_to_cm(inch)
print(inch, "inch entspricht", laenge_in_cm, "cm.")