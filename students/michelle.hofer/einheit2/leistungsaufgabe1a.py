#Aufgabe Schweizer Kreuz
#Michelle Hofer

from turtle import *
 
#finale Variante/Version 3 (noch optimierter)
#--------------------------------------------------------------------------------
#rotes Quadrat mit Schleife für wiederholende Befehle:
speed(5)
pencolor("red")
fillcolor("red")
begin_fill()
for i in range(4): #eingebaute Schleife-> vier mal forward/left für ein Quadrat
    forward(400)
    left(90)
end_fill()

#weisses Kreuz mit doppelter Schleife:
goto(250,150) #passende Startposition finden; braucht kein penup, da stift bereits rot
pencolor("white")
pensize(5) #mit dickerem Stift erkennt man die Entstehung des Kreuzes besser
fillcolor("white")
begin_fill()
for k in range(4): #vier mal die Schleife i und right(180) ausführen um das gesamte Kreuz zu erhalten
    for i in range(3): #drei mal forward/left ausführen für einen Arm des Kreuzes
        forward(100)
        left(90)
    right(180) #sieht beim zeichnen nicht so schön aus, muss aber eingebaut werden, da sonst kein Kreuz entsteht, sondern nur ein weisses Quadrat
end_fill()
hideturtle()

#Trainingsvarianten für mich:

#2. Variante (mit einigen eingebauten Funktionen/Schleifen)
#------------------------------------------------------------------------------------------------
#rotes Quadrat mit Schleife für wiederholende Befehle:
reset() #eingebaut, damit ich mehrere Varianten in einem File erstellen konnte, setzt alles auf Standard zurück
speed(5)
pencolor("red")
fillcolor("red")
begin_fill()
i = 1
anzahl = 4
while i <= anzahl: #eingebaute Schleife
    forward(400)
    left(90)
    i = i + 1
end_fill()

#weisses Kreuz mit Funktion und Schleife (range):
def leftRightRight(): #eingebaute Funktion
    left(90)
    forward(100)
    right(90)
    forward(100)
    right(90)
    forward(100)

goto(150,250)
pencolor("white")
fillcolor("white")
begin_fill()
i = 1
anzahl = 4
while i <= anzahl: #eingebaute Schleife
    leftRightRight() #hier Funktion verwendet
    i = i + 1
end_fill()
hideturtle()

#1. Variante einfach --> Ursprungsvariante, keine Schleifen, keine Funktion
#--------------------------------------------------------------------------------
#rotes Quadrat mit allen Befehls-Wiederholungen:
reset() #eingebaut, damit ich mehrere Varianten in einem File erstellen konnte, setzt alles auf Standard zurück
speed(5)
pencolor("red")
pensize(5)
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
end_fill()

#weisses Kreuz mit allen Befehls-Wiederholungen:
goto(250,250)
pensize(5)
pencolor("white")
fillcolor("white")
begin_fill()
left(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
left(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
left(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
left(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()
hideturtle()





