#Aufgabe Schweizer Kreuz inklusive Längeneingabe
#Michelle Hofer

from turtle import *
 
#finale Variante (optimiert)
#--------------------------------------------------------------------------------
#rotes Quadrat mit Schleife für wiederholende Befehle:
laenge = numinput("Eingabe","Bitte bestimmen Sie die Länge der Flagge")
speed(5)
pencolor("red")
fillcolor("red")
begin_fill()
for i in range(4): #eingebaute Schleife-> vier mal forward/left für ein Quadrat, ginge auch mit while/anzahl und i = i+1
    forward(laenge)
    left(90)
end_fill()

#weisses Kreuz mit doppelter Schleife:
goto(laenge * 0.625, laenge * 0.375) #Auswahl passender Startpunkt für weisses Kreuz
pencolor("white")
fillcolor("white")
begin_fill()
for k in range(4): #vier mal die Schleife i ausführen um das gesamte Kreuz zu erhalten
    for i in range(3): #drei mal forward/left ausführen für einen Arm des Kreuzes
        forward(laenge * 0.25)
        left(90)
    right(180) #sieht beim zeichnen nicht so schön aus, muss aber eingebaut werden, da sonst kein Kreuz entsteht, sondern nur ein weisses Quadrat
end_fill()
hideturtle()

