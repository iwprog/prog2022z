#Michelle Hofer
#1. Schreiben Sie eine Funktion, welche eine beliebige Liste entgegennimmt und eine neue Liste,
    #welche nur noch Eintraege mit einer Laenge von >= 4 enthaelt zurueckgibt.

l_input = ['der', 'junge', 'ist', 'sehr', 'freundlich']

def filter_list(liste):
    l_output = []
    for inhalt in liste:
        if len(inhalt) >= 4:
            l_output.append(inhalt)
    return l_output
    
print('Ergebnis >= 4:', filter_list(l_input))

#3. Adventskalender: Schreiben Sie eine Funktion, welche eine Liste mit 24 Einträgen erstellt, welche zufällig
    #eine der folgenden Überraschungen enthält: Samiklaus, Christbaum, Weihnachtskugel, Stiefel, Schneeball.
    #Der Benutzer soll im Anschluss beliebige Türen öffnen können und das Programm soll ausgeben, welche
    #Überraschung sich hinter dieser versteckt

from random import choice
ueberraschungen = ['Samiklaus', 'Christbaum', 'Weihnachtskugel', 'Stiefel', 'Schneeball']

def weihnachtskalender(liste):
    while True:
        eingabe = input('Welche Kalendertür wollen Sie öffnen (oder x für Exit): ')
        if eingabe == 'x':
            break
        elif eingabe.isdigit():
            if int(eingabe) in range (1,25):
                print(choice(ueberraschungen))
        else:
            print('ungültige Eingabe')
            
weihnachtskalender(ueberraschungen)

#4. Schreibe eine Funktion, welche eine Liste und einen Suffix entgegennimmt und alle Wörter, die darauf enden ausgibt.

l_input2 = ['Gesundheit', 'Wanderung', 'Heiterkeit','Gewandtheit', 'Lustig']

def get_suffix_words(liste, suffix):
    l_output2 = []
    for inhalt in liste:
        if inhalt.find(suffix[-4:]) > 0:
            l_output2.append(inhalt)
    return l_output2

print('Ergebnis mit Suffix:', get_suffix_words(l_input2, 'heit'))

