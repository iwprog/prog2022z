#Michelle Hofer
#adHoc 3.4
#a) Schreiben Sie eine Funktion allIndex(sequenz, element), die als Parameter eine Sequenz 
    #und ein Element bekommt. Zurückgegeben werden soll eine Liste mit allen Index-Positionen,
    #an denen "element" vorkommt.

listeAllIndex = []
def allIndex(sequenz, element):
    for nr, buchstabe in enumerate(sequenz):
        if buchstabe == element:
            listeAllIndex.append(nr)
    print(listeAllIndex)

allIndex('Chur','l')
allIndex(['H','a','l','l','o',0,0,7],'l')
allIndex('Chur','h')

#b) Gegeben sei die folgende mehrdimensionale Liste:
    #[["Zürich", 370000], ["Genf", 190000], ["Basel", 170000], ["Bern", 130000]]
    #Schreiben Sie ein Programm, dass die Liste wie folgt aufbereitet ausgibt: 
    #Zürich hat 370000 Einwohner
    #Genf hat 190000 Einwohner
    #...

staedte = [["Zürich", 370000], ["Genf", 190000], ["Basel", 170000], ["Bern", 130000]]

for ort, anzahl in staedte:
    print(ort, 'hat', anzahl, 'Einwohner')

#Variante Ausgabe nummerierte Liste
for nr, (ort, anzahl) in enumerate(staedte, start = 1):
    print(str(nr) +'.', ort, 'hat', anzahl, 'Einwohner')
