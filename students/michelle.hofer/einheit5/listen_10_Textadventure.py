#Michelle Hofer
#Der Durchlauf zum Sieg funktioniert, Reaktion auf fehlerhafte Eingaben und bessere Validierungen müssten noch programmiert werden.

spiel = [['Raum 1', 'Du befindest dich im Keller mit einem alten Kaugummi an der Wand', 'Kaugummi'],
         ['Raum 2', 'Du befindest dich wieder im Raum, in dem du aufgewacht bist.','Start'],
         ['Raum 3', 'Hier ist die Garage. Sie enthält ein altes Motorrad mit Loch im Reifen', 'Motorrad'],
         ['Raum 4', 'Willkommen in der Abstellkammer. Hier gibt es einen Kanister Benzin.','Benzin']]

#mit x und y auf Positionen der Liste zugreiffen --> spiel[x][y]:
x = 1
y = 0
z = 0
inventar = []

def links():
    global x #Position x von oben soll verändert werden, nicht ein lokales x
    if x == 0: #Wenn man sich im Raum 1 befindet, darf es nicht mehr weiter links gehen -> spiel[0]
        print('Du kannst hier nicht weiter nach links gehen')
    else:
        x = x - 1 #Raum in Richtung links wechseln 
    
def rechts():
    global x
    if x == 3: #Wenn man sich im Raum 4 befindet -> spiel[3] darf es nicht mehr weiter rechts gehen
        print('Du kannst hier nicht weiter nach rechts gehen')
    else:
        x = x + 1 #Raum in Richtung rechts wechseln

def wähleRichtung(): #input Frage nach der Richtung, welche in einer Schlaufe immer wieder gestellt wird (vgl. while True)
    richtung = input('Wohin möchtest du gehen? Wähle links oder rechts, aber ein bisschen flott! ')
    if richtung == 'links':
        links()        
    elif richtung == 'rechts':
        rechts()
    print(spiel[x][1]) #Beschreibung der Räume gemäss der oben definierten Liste drucken

def nimm():
    global x
    if x == 0: #wenn man sich Im Raum 1 (Keller) befindet, soll nur der Kaugummi genommen werden können            
        for inhalt in inventar:
            if inhalt.find(spiel[0][2]) >= 0:
                print('Dieses Item besitzt du bereits! Schon Vergessen? ')
                wähleRichtung()        
        item = input('Was möchtest du nehmen? ')
        if item == spiel[0][2]:  
            print(item, 'wurde zum Inventar hinzugefügt, herzliche Gratulation!')
            inventar.append(item)
    elif x == 3: #wenn man sich Im Raum 4 (Abstellkammer) befindet, soll nur das Benzin genommen werden können
        for inhalt in inventar:            
            if inhalt.find(spiel[3][2]) >= 0:
                print('Dieses Item besitzt du bereits! Schon Vergessen? ')
                wähleRichtung()                
        item = input('Was möchtest du nehmen? ')
        if item == spiel[3][2]:  
            print(item, 'wurde zum Inventar hinzugefügt, herzliche Gratulation!')
            inventar.append(item)
    else:
        print('Du kannst hier nichts nehmen!!')

def benutze():
    global x
    global z #braucht es um ein Spielende zu definieren
    if x == 2: #Nur wenn man sich Im Raum 3 (Garage) befindet, soll man etzwas benutzen können        
        item1 = input('Was möchtest du benutzen? ')
        if item1 in inventar:            
            item2 = input('Mit was möchtest du '+ item1 +' benutzen? ')
            if item2 == spiel[2][2] and item1 == spiel[0][2]:
                print('Bravissimo! Du hast das Motorrad repariert! ')
                inventar.remove(item1)
            elif item2 == spiel[2][2] and item1 == spiel[3][2]:
                print('Whoop Whoop! Du hast das Spiel gewonnen und fährst mit vollem Tank davon!!')
                inventar.remove(item1)
                z = 'Ende' #braucht es um die While Schlaufe beenden zu können               
    else:
        print('Du kannst hier nichts benutzen!!')

def wähleTätigkeit(): #input Frage nach der Tätigkeit, welche in einer Schlaufe ab und zu gestellt wird (vgl. while True)
    global x
    if x == 1:
        wähleRichtung()
    tätigkeit = input('Was möchtest du tun? Etwas nehmen: tippe "nimm", etwas benutzen: tippe "benutze" ')
    if tätigkeit == 'nimm':
        nimm()        
    elif tätigkeit == 'benutze':
        benutze()
            

print('Du wachst ziemlich benommen auf einem Sofa auf und weisst nicht wo du bist.') #Start des Spiels

while True:
    wähleRichtung()       
    wähleTätigkeit()
    if z == 'Ende':
        break
    
