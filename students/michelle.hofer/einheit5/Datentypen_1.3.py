#Michelle Hofer
#1.3 Listen Filtern
#a. Schreiben Sie eine Funktion, welche eine beliebige Liste auf drei Elemente kürzt.
    #liste = ["Brot", "Zucker", "Salz", "Mehl", "Kerzen"]
    #print("Ergebnis", liste_kuerzen(liste))
    #Ergebnis: ["Brot", "Zucker", "Salz"]
einkaufsliste = ["Brot", "Zucker", "Salz", "Mehl", "Kerzen"]
einkaufsliste2 = []
einkaufsliste3 = []

#Variante: nur die Anzeige kürzen:
def liste_kuerzen(liste):
    return liste[0:3]

print("Ergebnis 1", liste_kuerzen(einkaufsliste))

#Variante: Elemente wirklich löschen:
#def liste_kuerzen2(liste):
    #del liste[3:]
    #return liste

#print("Ergebnis 2", liste_kuerzen2(einkaufsliste))

#b. Schreiben Sie eine Funktion, welche aus einer Liste alle Elemente entfernt,
    #die nicht mit den Buchstaben 'er' enden.

#def liste_loeschen_ohne_er(liste):
    #for inhalt in liste:
        #if inhalt[-2:] != '-er':
            #liste.remove(inhalt)
    #return liste
#print('Ergebnis ohne -er', liste_loeschen_ohne_er(einkaufsliste))

#-->Funktion funktioniert so nicht.. Problem: Wenn ein Element aus der Liste entfernt wird,
    #rutschen die anderen Elemente einen Index nach vorne, wodurch das nächste Element nicht geprüft wird.

#Variante mit neuer Liste:
def liste_loeschen_ohne_er(liste,liste2):
    for inhalt in liste:
        if inhalt[-2:] != 'er':
            liste2.append(inhalt)
    return liste2

print('Ergebnis ohne -er', liste_loeschen_ohne_er(einkaufsliste,einkaufsliste2))
        
#c. Schreiben Sie eine Funktion, welche aus einer Liste alle Texte entfernt,
    #welche weniger als drei Buchstaben haben.
musik = ['Do', 'Re', 'Mi', 'Fa', 'Sola']
musik2 =[]

def liste_erstellen_groesserDrei(liste,liste2):
    for inhalte in liste:
        if len(inhalte) >= 3:
            liste2.append(inhalte)
    return liste2

print('Ergebnis grösser drei', liste_erstellen_groesserDrei(musik,musik2))

#d. Schreiben Sie eine Funktion, welche aus einer Liste alle Texte entfernt, in
    #welchen nicht mindestens zwei 'e' vorkommen.

def liste_erstellen_groesserDrei(liste,liste3):
    for inhalte in liste:
        if inhalte.count('e') >= 2:
            liste3.append(inhalte)
    return liste3

print('Ergebnis mindestens 2x e', liste_erstellen_groesserDrei(einkaufsliste,einkaufsliste3))

#e. Sortieren Sie die Liste der Jahreszeiten alphabetisch.
jahreszeiten = ['Frühling', 'Sommer', 'Herbst', 'Winter']
jahreszeiten.sort()
print(jahreszeiten)