#1.2 Über Listen iterieren
#a. Geben Sie die Jahreszeiten aus der Liste jahreszeiten alphabetisch sortiert aus
jahreszeiten = ['Frühling', 'Sommer', 'Herbst', 'Winter']
print(sorted(jahreszeiten))

#b. Geben Sie nur die ersten drei Elemente der Liste aus.
print(jahreszeiten[0:3])

#c. Geben Sie alle Jahreszeiten und zusätzlich die letzten drei Buchstaben der Jahreszeit aus .
    #Frühling - ng
    #Sommer - er
    #Herbst - st
    #Winter - er

#komplizierte 1. Variante (funktioniert jedoch auch :))
for inhalt in range(len(jahreszeiten)):
    print(jahreszeiten[inhalt], '-', jahreszeiten[inhalt][-2:])

#Variante nach Vereinfachung:
for inhalt in jahreszeiten:
    print(inhalt, '-', inhalt[-2:])
    
#d. Geben Sie nur jene Jahreszeiten aus, die mit den Buchstaben 'er' enden.
for inhalt in jahreszeiten:
    if inhalt[-2:] == 'er':
        print(inhalt)

#e. Geben Sie alle Jahreszeiten + die Information, wie oft der Buchstabe 'r' in diesen vorkommt aus.
for inhalt in jahreszeiten:
    print(inhalt, '-' ,inhalt.count('r'))

#Variante mit Erklärung was ausgegebn wird
for inhalt in jahreszeiten:
    print(inhalt, 'mit' ,inhalt.count('r'),'mal Buchstabe r im Wort')
