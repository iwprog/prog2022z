#Michelle Hofer
#Welche Zeichenfolge soll gezeichnet werden?
# o = Kreis, d = Dreieck, # = Quadrat

from turtle import *
speed(5)
penup()
back(800)
pendown()
def dreieck():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    for i in range(3):
        forward(200)
        left(120)
    forward(200)
    end_fill()
    penup()
    forward(100)
    pendown()
    
def quadrat():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    for i in range(4):
        forward(200)
        left(90)
    forward(200)
    end_fill()
    penup()
    forward(100)
    pendown()
    
def kreis():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    circle(50)
    end_fill()
    penup()
    forward(150)
    pendown()

eingabe = textinput('Eingabe','Welches Muster? d=dreieck #=quadrat o=kreis')
for i in eingabe:
    if i == 'd':
        dreieck()  
    elif i == '#':
        quadrat() 
    elif i == 'o':
        kreis()
      
hideturtle()
exitonclick()
