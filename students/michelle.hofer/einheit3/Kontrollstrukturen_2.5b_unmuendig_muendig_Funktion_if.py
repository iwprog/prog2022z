#Michelle Hofer
#kontrollstrukturen 2.5
#b) Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach Alter zwischen
    #geschäftsunfähig(<=6), unmündig (<=14), mündig minderjährig (>14<18), volljährig(>=18) unterscheidet
    #negatives Alter = ungeboren
age = int(input('Bitte Alter eingeben: '))
def legal_status(age):
    if age <0:
        return "ungeboren"
    elif age <=6:
        return "geschäftsunfähig"
    elif age <=14:
        return "unmündig"
    elif age <18:
        return "mündig minderjährig"
    elif age >=18:
        return "volljährig"
    
print("Mit", age, "ist man", legal_status(age) + ".")


