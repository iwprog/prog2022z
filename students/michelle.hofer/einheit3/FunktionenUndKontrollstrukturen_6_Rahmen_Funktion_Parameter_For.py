#Michelle Hofer
#Funktionen und Kontrollstrukturen
#6. Funktion, welche Bilderrahmen unter Zuhilfenahme von Schleifen zeichne (+ Rahmenfarbe und Füllfarbe)
from turtle import *
speed(5)
pensize(3)
def zeichne_rahmen(laenge,rahmenbreite, rahmenfarbe, fuellfarbe):
    pencolor(rahmenfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    for i in range(4):
        forward(laenge)
        left(90)
    penup()
    goto(rahmenbreite,rahmenbreite)
    pendown()
    for i in range(4):
        forward(laenge-2*rahmenbreite)
        left(90)
    end_fill()
    

zeichne_rahmen(300,100,'black','white')
exitonclick()
