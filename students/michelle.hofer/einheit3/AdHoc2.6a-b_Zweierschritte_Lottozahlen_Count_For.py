#Michelle Hofer
# a)Schreiben Sie ein Programm, das in Zweierschritten von 100 bis 80 zählt:  
count=1
for anzahl in range(100, 79,-2):
    print(str(count) + ". Wert: ", str(anzahl))
    count = count + 1


# b) Schreiben sie ein Programm, das 6 Zufallszahlen zwischen 1 und 42 ausgibt.
count = 1
for i in range(0,6):
    zufallszahl = randint(1, 42)
    print(str(count) + ". Zufahlszahl:", str(zufallszahl))
    count = count + 1


# b)Variante mit separaten variablen kleinster_wert/höchster_wert)
from random import randint
kleinster_wert = 1
höchster_wert = 42
count = 1
for i in range(6):
    zufallszahl = randint(kleinster_wert, höchster_wert)
    print(str(count) + ". Zufahlszahl:", str(zufallszahl))
    count = count + 1





