#Michelle Hofer
#Was Soll ich zeichnen? Kreis oder Dreieck?
#wieviele sollen es sein?

from turtle import *
speed(5)
penup()
back(700)
pendown()
def dreieck():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    forward(200)
    left(120)
    forward(200)
    left(120)
    forward(200)
    end_fill()
    
def quadrat():
    pencolor("blue")
    fillcolor("lightblue")
    begin_fill()
    for i in range(4):
        forward(200)
        left(90)
    end_fill()

eingabe = textinput('Eingabe','Was soll ich zeichnen? dreieck oder quadrat')
if eingabe == 'dreieck':
    eingabe2 = numinput('Eingabe','Wieviele soll ich zeichnen')
    for k in range(int(eingabe2)): 
        dreieck()
        penup()
        left(120)
        forward(350)
        pendown()
elif eingabe == 'quadrat':
    eingabe2 = numinput('Eingabe','Wieviele soll ich zeichnen')
    for k in range(int(eingabe2)): 
        quadrat()
        penup()
        forward(350)
        pendown()


hideturtle()
exitonclick()
