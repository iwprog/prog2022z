#Michelle Hofer, Funktonen und Kontrollstrukturen 2-3
#2. Funktion, welche Alter entgegennimmt und maximale Herzfrequenz berechnet

def berechne_herzfrequenz(alter):
    resultat = 220 - alter
    print(resultat)
    
berechne_herzfrequenz(25)

#3. Funktion, welche basierend auf Alter und Entfernung SBB Ticket berechnet:
    #jedes Ticket verfügt über einen Grundpreis von 2 CHF, der unabhängig von der Entfernung berechnet wird.
    #zusatzliche werden 0.25 CHF / km berechnet.
    #Kinder unter 16 Jahren zahlen den halben Preis.
    #Kinder unter 6 Jahren reisen kostenlos.

def get_billet_preis(alter,strecke):
    resultat = 2 + 0.25 * strecke
    if alter <6:
        print(0)
    elif alter <16:
        print(int(resultat/2))
    else:
        print(int(resultat))
    
get_billet_preis(3,200)
get_billet_preis(8,200)
get_billet_preis(30,200)
