#Michelle Hofer
#übung 2.8
#Algorithmus, der aus einer Zahlenreihe (z.B.12,23,5,56,77,18,9) das Minimum/Maximum und Durchschnitt ausgibt
zahlen = [12,23,5,56,77,18,9]
mini = None
maxi = None
durchschnitt = sum(zahlen) / len(zahlen)

for i in zahlen:
    if(mini is None or i < mini):
        mini = i
        
    if(maxi is None or i > maxi):
        maxi = i 
         
print ('Kleinste Zahl: ' + str(mini))
print ('Grösste Zahl: ' + str(maxi))
print ('Durchschnitt: ' + str(durchschnitt))

#Variante mit builtIn Funktionen Python:
liste = [12,23,5,56,77,18,9]
print ('Kleinste Zahl: ' + str(min(liste)))
print ('Grösste Zahl: ' + str(max(liste)))
print ('Durchschnitt: ' + str(sum(zahlen)/len(zahlen)))



