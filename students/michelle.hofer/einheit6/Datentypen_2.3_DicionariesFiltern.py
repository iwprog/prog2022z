#Michelle Hofer
#2.3 Dictionaries filtern
#a. Entfernen Sie alle Lebensmittel aus dem Dictionary, welche weniger als 2.5 CHF kosten.
print('a)')
preisliste = {'Milch':2.1, 'Brot':3.2, 'Orangen':3.75, 'Tomaten':2.2, 'Papier':1.95}
preisliste['Tee'] = 4.2
preisliste['Peanuts'] = 3.9
preisliste['Ketchup'] = 2.1

preisliste2 = {}
for lebensmittel, preis in preisliste.items():
    if preis > 2.5:
        preisliste2[lebensmittel] = preis

print('Dictionary < CHF 2.50', preisliste2)


#b. Entfernen Sie alle Lebensmittel aus dem Dictionary, welche nicht in der folgenden Liste
    #vorkommen: ["Brot", "Butter", Milch"].
print('b)')

preisliste3 = {}
for lebensmittel, preis in preisliste.items():
    if lebensmittel in ["Brot", "Butter", "Milch"]:
        preisliste3[lebensmittel] = preis

print('Dictionary vorgegebene Liste', preisliste3)

#c. Entfernen Sie alle Lebensmittel aus dem Dictionary, welche die Zeichenkette 'en'im Namen enthalten.
print('c)')

preisliste4 = {}
for lebensmittel, preis in preisliste.items():
    if lebensmittel.find('en') < 0:
        preisliste4[lebensmittel] = preis

print('Dictionary ohne "en"', preisliste4)