#Michelle Hofer
#2. Wortstatistik: Schreiben Sie eine Funktion, welche einen Text entgegennimmt und alle Wörter mit
    #deren Häufigkeit ausgibt. Die Gross-/Kleinschreibung soll dabei nicht berücksichtigt werden.

text = 'Wie häufig kommt fliegen vor in: Wenn Fliegen hinter Fliegen fliegen'

def wortstatistik(eingabe):
    lookup = {}
    for wort in eingabe.lower().split():
        if wort in lookup:
            lookup[wort] = lookup[wort] + 1
        else:
            lookup[wort] = 1
    return lookup


print(wortstatistik(text))  