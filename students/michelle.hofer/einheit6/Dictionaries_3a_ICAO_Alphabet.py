#Michelle Hofer
#Übungsbeispiele zu Dictionaries
#3. ICAO Alphabet
#Die International Civil Aviation Organization (ICAO) Alphabet weist Buchstaben Wörtern zu, damit diese fehlerfrei
#buchstabiert werden können. Schreiben Sie ein Programm, das ein beliebiges Wort buchstabiert.
#a) Erweiterung Fehlermeldung bei Umlauten

alphabet = {'A' : 'Alfa','B' : 'Bravo','C' : 'Charlie','D' : 'Delta','E' : 'Echo','F' : 'Foxtrot','G' : 'Golf',
            'H' : 'Hotel','I' : 'India','J' : 'Juliett','K' : 'Kilo','L' : 'Lima','M' : 'Mike​','N' : 'November',
            'O' : 'Oscar','P' : 'Papa','Q' : 'Quebec','R' : 'Romeo','S' : 'Sierra','T' : 'Tango','U' : 'Uniform',
            'V' : 'Victor','W' : 'Whiskey','X' : 'X:ray','Y' : 'Yankee','Z' : 'Zulu'}
buchstabierwort = input('Welches Wort soll ich buchstabieren? ')

ausgabeliste = []
for buchstabe in buchstabierwort:
    buchstabe = buchstabe.upper()
    if buchstabe not in alphabet:
        print('Kann', buchstabierwort, 'nicht buchstabieren, da', buchstabe, 'nicht definiert wurde.')
        break 
    else:
        ausgabeliste.append(alphabet[buchstabe])

if len(ausgabeliste) == len(buchstabierwort):
    for wort in ausgabeliste:
        print(wort, end = '-') #leider bringe ich das letzte Trennzeichen nicht weg ->nachfragen
    
    
    

        
                            