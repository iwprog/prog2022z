#Michelle Hofer
#5. Remove Stopwords: Schreiben Sie eine Funktion, welche einen Text entgegennimmt und aus diesem alle Stoppwörter,
#die in einer Liste definiert sind, entfernt und eine kleingeschriebene Liste aller Wörter
#(ohne Stoppwörter) zurückgibt.

l_input = 'Heinz war heute in den Bergen. Es war eine lange Wanderung'
stopwords = ['der', 'die', 'das', 'in', 'auf', 'unter','ein', 'eine', 'ist', 'war', 'es', 'den']

def stopword_filter(eingabe, liste):
    l_output=[]
    l_inputklein = l_input.lower().split()
    for wort in l_inputklein:
        if wort not in stopwords:
            l_output.append(wort)
    return l_output

print(stopword_filter(l_input, stopwords))

