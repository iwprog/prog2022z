#Michelle Hofer
#2.2 Über Dictionaries iterieren
#a. Geben Sie die Preiliste aus dem obigen Beispiel aus.
print('a)')

preisliste = {'Milch':2.1, 'Brot':3.2, 'Orangen':3.75, 'Tomaten':2.2, 'Papier':1.95}
preisliste['Tee'] = 4.2
preisliste['Peanuts'] = 3.9
preisliste['Ketchup'] = 2.1

for lebensmittel, preis in preisliste.items():
    print(lebensmittel, 'kostet', preis, 'CHF')

#b. Geben Sie die Preisliste alphabetisch sortiert aus.
print('b)')

for lebensmittel, preis in sorted(preisliste.items()):
    print(lebensmittel, 'kostet', preis, 'CHF')
    

#c. Geben Sie die Preisliste nach dem Preis sortiert aus:
print('c)')
sortierliste = []

for lebensmittel, preis in preisliste.items():
    sortierliste.append([preis, lebensmittel])

print(sortierliste)

for preis, lebensmittel in sorted(sortierliste):
    print(preis, 'CHF kostet', lebensmittel)

#d. Geben Sie die Preisliste absteigend nach dem Preis sortiert aus:
print('d)')
sortierliste.sort()
sortierliste.reverse()
print(sortierliste)

for preis, lebensmittel in sortierliste:
    print(preis, 'CHF kostet', lebensmittel)

#e. Geben Sie nur Lebensmittel mit einem Preis < 2.0 CHF aus.
print('e)')   
for lebensmittel, preis in preisliste.items():
    if preis < 2.0:
        print('Ergebnis < CHF 2.0:',lebensmittel, 'kostet', preis, 'CHF')

#f. Geben Sie alle Lebensmittel aus, in den die Zeichenkette 'en' vorkommt.
print('f)')
for lebensmittel, preis in preisliste.items():
    if lebensmittel.find('en') >= 0:
        print('Ergebnis mit "en":',lebensmittel, 'kostet', preis, 'CHF')