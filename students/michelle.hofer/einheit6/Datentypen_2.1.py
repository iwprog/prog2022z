#Michelle Hofer
#a. Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit
    #den zugehörigen Preisen speichert: Brot → 3.2, Milch → 2.1, Orangen → 3.75, Tomaten → 2.2.

preisliste = {'Milch':2.1, 'Brot':3.2, 'Orangen':3.75, 'Tomaten':2.2}


#b. Ändern Sie den Preis für Milch auf 2.05.
preisliste['Milch'] = 2.05
print(preisliste)

#c. Entfernen Sie den Eintrag für Brot.
del preisliste['Brot']
print(preisliste)

#d. Fügen Sie neue Einträge für Tee → 4.2, Peanuts → 3.9 und Ketchup → 2.1 hinzu.
preisliste['Tee'] = 4.2
preisliste['Peanuts'] = 3.9
preisliste['Ketchup'] = 2.1

#Variante zum hinzufügen über update
preisliste.update({'Suppe' : 2.2, 'Haribo' : 3.4, 'Saft' : 2.35})

print(preisliste)

#e. Schreiben Sie ein Programm, welches Sie nach Lebensmittel und den zugehörigen Preisen
    #fragt und diese so lange einem Dictionary hinzufügt, bis der Benutzer 'x' eingibt.
    #Im Anschluss soll das Dictionary ausgegeben werden.
    #Lebensmittel: Brot
    #Preis: 3.2
    #Lebensmittel: Milch
    #Preis: 2.1
    #Lebensmittel: x
    #Dictionary: {"Milch": "2.1", "Brot": "3.2"}

Dictionary = {}
def erzeuge_lebensmittel():
    while True:
        Lebensmittel = input('Lebensmittel: ')
        if Lebensmittel == 'x':
            break
        Preis = input('Preis: ')
        if Preis == 'x': #Problem Abbruch beim Preis, Lebensmittel wird auch nicht hinzugefügt -> fragen
            break
        Dictionary.update({Lebensmittel : Preis})
    return Dictionary

print('Dictionary:',erzeuge_lebensmittel())

#f. Modizieren Sie ihr Programm, sodass der Preis als Zahl (und nicht als String) gespeichert wird.
#Lebensmittel: Brot
#Preis: 3.2
#Lebensmittel: Milch
#Preis: 2.1
#Lebensmittel: x
#Dictionary: {"Milch": 2.1, "Brot": 3.2}

Dictionary2 = {}
def erzeuge_lebensmittel2():
    while True:
        Lebensmittel = input('Lebensmittel: ')
        if Lebensmittel == 'x':
            break
        Preis = float(input('Preis: '))
        if Preis == 'x':
            break
        Dictionary2.update({Lebensmittel : Preis})
    return Dictionary2

print('Dictionary2:',erzeuge_lebensmittel2())