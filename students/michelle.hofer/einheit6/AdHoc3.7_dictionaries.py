#Erstellen Sie ein Dictionary mit einigen Schweizer Städten und deren Einwohnerzahlen
#(wie im Bsp. auf den Folien zuvor)

#a. Schreiben Sie ein Programm, dass eine Ausgabe nach folgendem Aufbau erzeugt:
    #Zürich hat 370000 Einwohner
    #Genf hat 190000 Einwohner
    #usw.

staedte = {"Zürich": 370000, "Genf": 190000, "Basel": 170000, "Bern": 130000}

#Ausgabe Variante
for stadt in staedte:
    print(stadt,'hat', staedte[stadt], 'Einwohner')
    

for stadt, einwohnerzahl in staedte.items():
    print(stadt,'hat', einwohnerzahl, 'Einwohner')
    
#b. Ändern Sie das Programm so, dass die Liste sortiert nach keys ausgegeben wird. Heisser Tipp:
    #list hat ja eine Methode sort()

#Ausgabe Variante
for inhalt in sorted(staedte):
    print(inhalt,'hat', staedte[inhalt], 'Einwohner')

for stadt, einwohnerzahl in sorted(staedte.items()):
    print(stadt,'hat', einwohnerzahl, 'Einwohner')

#staedte = list(staedte.keys)
#staedte.sort()

#for key, value in staedte.items():
      

#c. Überlegen Sie, wie man die Ausgabe sortiert nach Einwohnerzahlen programmieren könnte.
sortierliste = []

for stadt, einwohnerzahl in staedte.items():
    sortierliste.append([einwohnerzahl, stadt])

for einwohnerzahl, stadt in sortierliste:
	print(einwohnerzahl, 'Einwohner hat', stadt)
