#Grundlegende Kontrollstrukturen 2.3.b
# # Schreiben Sie eine Funktion, welche die Länge und Breite eines Rechtecks
# entgegennimmt und die Flaeche des Rechtecks an das Programm zurueckgibt.

# flaeche = flaeche_rechteck(10, 20)
# print("Die Fläche beträgt", flaeche, "m2.")
# Ausgabe: Die Fläche beträgt 200 m2.

def flaeche_rechteck (breite, laenge):
    return (breite * laenge)


flaeche = flaeche_rechteck(10, 20)
print("Die Fläche beträgt", flaeche, "m2.")