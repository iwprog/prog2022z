# Übungen Dictionaries 2
# Sabina Aebersold
# 24.3.22

# Wortstatistik: Schreiben Sie eine Funktion, welche einen Text entgegennimmt und alle W¨orter mit deren H¨aufigkeit ausgibt. Die Gross-
# /Kleinschreibung soll dabei nicht ber¨ucksichtigt werden.
# l_input = "Der Tag begann sehr gut! Der Morgen war sch¨on."
# >>> word_stat(l_input)
# {’der’: 2, ’tag’: 1, ....}

häufigkeit={}
def word_stat(l_input):
    for wort in l_input.split():
        if wort in häufigkeit:
            häufigkeit[wort]=häufigkeit[wort]+1
        else:
            häufigkeit[wort]=1
    return häufigkeit
    

l_input = ("Der Tag begann sehr gut! Der Morgen war schön. Der Abend war auch schön.")
print(word_stat(l_input))