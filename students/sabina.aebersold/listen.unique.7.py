# Übungen Listen 7
# Sabina Aebersold
# 24.3.22
# 
# . Schreiben Sie eine Funktion unique, welche doppelte Eintr¨age aus der
# Liste entfernt.
# l_input = [’Gesundheit’, ’Wanderung’, ’Gesundheit’,
# ’Gewandtheit’, ’Wanderung’]
# >>> unique(l_input)
# [’Gesundheit’, ’Wanderung’, ’Gewandtheit’]

def unique(l_input):
    l_output=[]
    for element in l_input:
        if element not in l_output:
            l_output.append(element)         
    return l_output
    

l_input =["Gesundheit", "Wanderung", "Gesundheit", "Gewandtheit", "Wanderung"]
print(unique(l_input))