# Funktionen und Kontrollstrukturen 10a
# Sabina Aebersold
# 7.3.22
# 
# a Schreiben Sie ein Programm, welches ein Men¨u ausgibt und die Berechnungen aus den Beispielen 3, 4 und 4a ¨uber dieses zug¨anglich macht.
# Die Men¨uausgabe soll so lange wiederholt werden, bis der Benutzer das
# Programm mit 0 beendet.
#

def get_billet_preis(alter, entfernung):
    grundpreis = 2 + (0.25 * entfernung)
    if alter >= 16:
        return(grundpreis)
    elif alter >=6:
        return(grundpreis/2)
    else:
        return(0)
    

def berechne_herzfrequenz(alter):
    return(220-alter)


eingabe=(1)
while eingabe ==True:
    print("==================================")
    print("Programmübersicht:")
    print(" 1 ... Preis für eine Fahrkarte berechnen")
    print(" 2 ... Herzfrequenz berechnen")
    print(" 0 ... Programm beenden")
    print("==================================")
    eingabe=int(input("Gewählte Option: "))

    if eingabe == 1:
        print("Preis für eine Fahrkarte berechnen")
        print("----------------------------------")
        alter = int(input("Bitte geben Sie Ihr Alter ein: "))
        entfernung = int(input("Wie viele km wollen Sie reisen? "))
        print("Die Fahrkarte kostet:", get_billet_preis(alter, entfernung), "CHF")
    
    elif eingabe == 2:
        print("Herzfrequenz berechnen")
        print("----------------------------------")
        alter = int(input("Wie alt sind Sie?"))
        print("Ihre maximale Herzfrequenz beträgt:", berechne_herzfrequenz(alter))
    
    elif eingabe != 0:
        print("Ungültige Option!")
        
    
    