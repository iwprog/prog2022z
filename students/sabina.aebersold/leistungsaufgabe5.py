# Leistungsaufgabe 5
# Sabina Aebersold
# 6.4.22


#  jedes Wort und die Anzahl der Vorkommnisse des Wortes berechnet.
# (b) berechnet wie oft jeder Buchstaben vorkommt.

from urllib.request import urlopen
from inscriptis import get_text

wörter={}
buchstaben={}

def wort_count(wörter):
    for wort in text.split():
        if wort in wörter:
            wörter[wort]=wörter[wort]+1
        else:
            wörter[wort]=1

def buchstaben_count(buchstaben):
    for buchstabe in text.lower():
        if buchstabe in buchstaben:
            buchstaben[buchstabe]=buchstaben[buchstabe]+1
        else:
            buchstaben[buchstabe]=1

with urlopen("https://www.ietf.org/rfc/rfc2616.txt") as file:
    text=file.read().decode("utf8")
    for wort in text.split():
        if wort in wörter:
            wörter[wort]=wörter[wort]+1
        else:
            wörter[wort]=1
    for buchstabe in text.lower():
        if buchstabe in buchstaben:
            buchstaben[buchstabe]=buchstaben[buchstabe]+1
        else:
            buchstaben[buchstabe]=1

from csv import writer

with open("wörter.csv", "w", encoding="utf8") as f:
    csv_writer=writer(f, delimiter=";")
    for wort, anzahl in sorted(wörter.items()):
        csv_writer.writerow([wort, anzahl])
        
with open("buchstaben.csv", "w", encoding="utf8") as f:
    csv_writer=writer(f, delimiter=";")
    for buchstabe, anzahl in sorted(buchstaben.items()):
        csv_writer.writerow([buchstabe, anzahl])
