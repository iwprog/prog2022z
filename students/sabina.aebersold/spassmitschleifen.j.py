# Funktionen und Kontrollstrukturen
# Sabina Aebersold
# 7.3.22

i = 11
j = 0
for count in range(6):
    print(j * "  ", "# " * i)
    i=i-2
    j=j+1