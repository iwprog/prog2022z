#Grundlagen Kontrollstrukturen 2.8.b
#Sabina Aebersold
#27.2.22

# Schreiben Sie eine Schleife, welche von 0 bis 10 in Dreierschritten zählt


i = 0
while i <= 10:
    print (i)
    i = i + 3
