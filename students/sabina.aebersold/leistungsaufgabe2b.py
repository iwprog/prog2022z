# Leistungsaufgabe 2b.
# Sabina Aebersold
# 9.3.22

for i in range(1, 11):
    print (i,"er Reihe:")
    for j in range(1, 11):
        print (j,"x",i, "=", i*j)
    print("-------------------")
    