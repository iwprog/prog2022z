#Leistungsaufgabe 1
#Sabina Aebersold
#26.2.22

#a) Erstellen Sie die Schweizer Flagge mit der turtle-Grafik
#und exakt folgendem Aussehen (400x400 Pixel), es darf kein Pfeil/Turtle sichtbar sein!

from turtle import*

hideturtle()
speed(6)
#rote Fläche zeichnen
pencolor("red")
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
end_fill()

#Quadrate, um Kreuz zu zeichnen
def kreuz():
    pencolor("white")
    fillcolor("white")
    begin_fill()
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    end_fill()

penup()
left(90)
forward(250)
left(90)
forward(50)
kreuz()

left(90)
forward(100)
kreuz()
kreuz()
right(180)
forward(100)
kreuz()
right(180)
forward(100)
kreuz()

pendown()