# ad hoc 2.8
# Sabina Aebersold
# 4.3.22
# 
# Entwerfen Sie einen Algorithmus mit einem
# Struktogramm, der aus einer Zahlenreihe (z.B. 12,
# 23, 5, 56, 77, 18, 9) das Minimum und das
# Maximum herausfindet und den Durchschnitt
# berechnet.
# b. Setzen Sie den Algorithmus aus a) in python um.

zahlenreihe = (12, 23, 5, 56, 77, 18, 9)
count = 0
summe = 0

for zahl in zahlenreihe:
    summe = summe+zahl
    count = count+1
    
print("Durchschnitt: ", int(summe/count))
  
    
minimum = min(zahlenreihe)
maximum = max(zahlenreihe)

print("Minimum:", minimum)
print("Maximum:", maximum)
