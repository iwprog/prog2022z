#Grundlagen Kontrollstrukturen 2.5.b
# Sabina Aebersold
#1.3.22

# Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach
# Alter den Wert vollj¨ahrig oder minderj¨ahrig zur¨uckgibt.
# Erweitern Sie die Funktion, sodass diese zwischen gesch¨aftsunf¨ahig (bis
# inklusive 6 Jahre), unm¨undig (bis inklusive 14 Jahre), m¨undig minderj¨ahrig
# und vollj¨ahrig unterscheidet. Weiters soll f¨ur ein negatives Alter ungeboren
# zur¨uckgegeben werden.

alter = int(input("Alter: "))

def legal_status(alter):
    if alter <0:
        return ("ungeboren")
    elif alter <= 6:
        return ("unmündig")
    elif alter <=14:
        return ("mündig minderjährig")
    elif alter <=18:
        return ("mündig volljähring")


print("Mit", alter, "ist man", legal_status(alter) + ".")
