# Übungen Datentypen 2.3
# Sabina Aebersold
# 23.3.22

#Alle Lebensmittel entfernen, die 'en' enthalten

preisliste={"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2,
            "Tee": 4.2, "Peanuts": 3.9, "Ketchup": 2.1}

preisliste_modifiziert={}
for lebensmittel, preis in preisliste.items():
    if "en" not in lebensmittel:
        preisliste_modifiziert[lebensmittel]=preis

print(preisliste_modifiziert)

#preisliste={"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2,
#           "Tee": 4.2, "Peanuts": 3.9, "Ketchup": 2.1}

# for lebensmittel, preis in preisliste.items():
#     if "en" in lebensmittel:
#         del preisliste [lebensmittel]
# print(preisliste)

#Funktioniert nicht