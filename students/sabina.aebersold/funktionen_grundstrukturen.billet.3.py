# übungsbeispiele zu funktionen und kontrollstrukturen 3.
# Sabina Aebersold
# 3.3.22
# 
# 3. Schreiben Sie eine Funktion welche basierend auf dem Alter und der
# Entfernung den Preis f¨ur ein SBB Billet berechnet:
# • jedes Ticket verf¨ugt ¨uber einen Grundpreis von 2 CHF, der unabh¨angig von der Entfernung berechnet wird.
# • zus¨atzliche werden 0.25 CHF / km berechnet.
# • Kinder unter 16 Jahren zahlen den halben Preis.
# • Kinder unter 6 Jahren reisen kostenlos.
# >>> get_billet_preis(3, 200)
# 0
# >>> get_billet_preis(8, 200):
# 26
# >>> get_billet_preis(30, 200):
# 52

alter = int(input("Alter: "))
entfernung = int(input("Entfernung: "))

def billet_preis(alter, entfernung):
    grundpreis = 2 + (0.25 * entfernung)
    if alter >= 16:
        return(grundpreis)
    elif alter >=6:
        return(grundpreis/2)
    else:
        return(0)
    
print(int((billet_preis(alter, entfernung))))
        