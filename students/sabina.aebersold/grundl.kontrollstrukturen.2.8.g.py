#Grundlagen Kontrollstrukturen 2.8.g
#Sabina Aebersold
#1.3.22
# 
#  Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der
# Benutzer -1 eingibt und anschliessend die Summe dieser Zahlen ausgibt.
# Bitte geben Sie Zahl 1 ein: 2
# Bitte geben Sie Zahl 2 ein: 3
# Bitte geben Sie Zahl 3 ein: 7
# Bitte geben Sie Zahl 4 ein: -1
# Summe: 12


count=1
summe=0
eingabe = input("Bitte geben Sie Zahl " +str(count)+ " ein: ")

while eingabe != "-1":
    summe = summe + int(eingabe)
    count=count+1
    eingabe = input("Bitte geben Sie Zahl " +str(count)+ " ein: ")

print("Summe: ", summe)