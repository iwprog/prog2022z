#Grundlagen Kontrollstrukturen 2.8.f
#Sabina Aebersold
#1.3.22
# 
# Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis
# deren Summe gr¨osser als 10 ist. Im Anschluss soll die Summe ausgegeben
# werden.
# Bitte geben Sie Zahl 1 ein: 2
# Bitte geben Sie Zahl 2 ein: 3
# Bitte geben Sie Zahl 3 ein: 1
# Bitte geben Sie Zahl 4 ein: 8
# Summe: 14

count = 1
summe = 0

while summe <= 10:
    eingabe = int(input("Bitte geben Sie Zahl " +str(count)+ " ein: "))
    summe= summe + eingabe
    count = count + 1

print("Summe: " + str(summe))

    



