#Grundlagen Kontrollstrukturen 2.7.c
# Sabina Aebersold
#1.3.22
# 
# Schreiben Sie eine Schleife, welche alle Buchstaben des Wortes Dampfschiff
# nummeriert ausgibt.

count = 1
for i in ("Dampfschiff"):
    print(count, "...", i)
    count = count + 1 
    