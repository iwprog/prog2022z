# Leistungsaufgabe 5
# Sabina Aebersold
# 6.4.22

from urllib.request import urlopen
from inscriptis import get_text

wörter={}
buchstaben={}

ressource=input("Ressourcenbezeichnung: ")

def wort_count(wörter):
    for wort in text.split():
        if wort in wörter:
            wörter[wort]=wörter[wort]+1
        else:
            wörter[wort]=1

def buchstaben_count(buchstaben):
    for buchstabe in text.lower():
        if buchstabe in buchstaben:
            buchstaben[buchstabe]=buchstaben[buchstabe]+1
        else:
            buchstaben[buchstabe]=1

if "https://" in ressource:
    with urlopen(ressource) as file:
        text=file.read().decode("utf8")
        wort_count(wörter)
        buchstaben_count(buchstaben)

else:
    with open(ressource, encoding="utf8") as file:
        text=file.read()
        wort_count(wörter)
        buchstaben_count(buchstaben)

from csv import writer

with open("wörter.csv", "w", encoding="utf8") as f:
    csv_writer=writer(f, delimiter=";")
    for wort, anzahl in sorted(wörter.items()):
        csv_writer.writerow([wort, anzahl])
        
with open("buchstaben.csv", "w", encoding="utf8") as f:
    csv_writer=writer(f, delimiter=";")
    for buchstabe, anzahl in sorted(buchstaben.items()):
        csv_writer.writerow([buchstabe, anzahl])

