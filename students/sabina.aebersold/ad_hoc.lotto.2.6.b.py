# #Ad hoc 2.6.b - Lotto
# #Sabina Aebersold
# #1.3.22
# 
# Schreiben sie ein Programm, das 6 Zufallszahlen zwischen
# 1 und 42 ausgibt.
# Die Ausgabe soll wie folgt aussehen:
# 1. Zufallszahl: ...
# ...
# 6. Zufallszahl: ...
# Hinweis: Zufallszahlen werden in Python wie folgt
# generiert:
from random import randint

for i in range(1, 7):
    zufallszahl = randint(1, 42)
    print(str(i)+ ". Zufallszahl: "+str(zufallszahl))
    





