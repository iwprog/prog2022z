#Leistungsaufgabe 1
#Sabina Aebersold
#26.2.22

#a) Erstellen Sie die Schweizer Flagge mit der turtle-Grafik
#und exakt folgendem Aussehen (400x400 Pixel), es darf kein Pfeil/Turtle sichtbar sein!
#b) Erweitern Sie das Programm um eine Eingabemöglichkeit (über Eingabefenster) für die
#Seitenlänge der Schweizer Flagge!

from turtle import*

laenge = numinput ("Schweizer Flagge", "Bitte geben Sie die gewünschte Seitenlänge an:")

hideturtle()
speed(6)
#rote Fläche zeichnen
pencolor("red")
fillcolor("red")
begin_fill()
forward(laenge*4)
left(90)
forward(laenge*4)
left(90)
forward(laenge*4)
left(90)
forward(laenge*4)
end_fill()

#Quadrate, um Kreuz zu zeichnen
def kreuz():
    pencolor("white")
    fillcolor("white")
    begin_fill()
    forward(laenge)
    left(90)
    forward(laenge)
    left(90)
    forward(laenge)
    left(90)
    forward(laenge)
    end_fill()

penup()
left(90)
forward(int(laenge*2.5))
left(90)
forward(laenge/2)
kreuz()

left(90)
forward(laenge)
kreuz()
kreuz()
right(180)
forward(laenge)
kreuz()
right(180)
forward(laenge)
kreuz()

pendown()
