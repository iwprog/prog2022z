#  Ad hoc 3.2.d
# # Sabina Aebersold
# # 9.3.22
# 
# # . Schreiben Sie eine Funktion "replace(text, position, einfügetext)",
# welches im String "text" an der Position "position" den String
# "einfügetext" einfügt und das Ergebnis als String zurückgibt.
# Zeigen Sie an einem Bsp. dass Ihre Fkt. funktioniert.

text=("Morgen fahre ich in den Urlaub")
position=16
einfuegetext=(" nach Frankreich ")

def replace(text, position, einfuegetext):
    return(text[0:position] + einfuegetext + text[position+1::])

print("Alter String: ", text)
print("Neuer String: ", replace(text, position, einfuegetext))

    