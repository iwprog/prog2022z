# Übungen Datentypen 3.2 a
# Sabina Aebersold
# 30.3.22

def datensatz_ausgeben(telefonbuch):
    for person in telefonbuch:
        print("Vorname:", person["Vorname"])
        print("Nachname:", person["Nachname"])
        print("Phone:", person["Phone"])
        print()    
    
telefonbuch=[{"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"},
             {"Vorname": "Tim", "Nachname": "Kurz", "Phone": "732"},
             {"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"}]

datensatz_ausgeben(telefonbuch)