# Übungen Dictionaries 7
# Sabina Aebersold
# 31.3.22

#Dictionary aus Eingabetext machen mit zugehörigen Sternen
text={}
eingabetext=input("Eingabetext: ")
for wort in eingabetext.split():
    wort=wort.lower()
    text[wort]=len(wort)*"*"

#Blacklist als Liste Formatieren
blacklist=[]
blacklist_woerter=input("Blacklist: ")
for wort in blacklist_woerter.split(", "):
    wort=wort.lower()
    blacklist.append(wort)

#Ausgabeliste mit geschwärzten Teilen erstellen, durch Abgleich von Blacklist und Eingabe
ausgabe=[]
for wort, sterne in text.items():
    if wort in blacklist:
        ausgabe.append(text[wort])
    else:
        ausgabe.append(wort)
        
print("Ausgabe: ", " ".join(ausgabe))    

#Gross-/Kleinschreibung? Im Moment wird alles Kleingeschrieben ausgegeben