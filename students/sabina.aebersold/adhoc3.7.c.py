#Ad hoc 3.7.c
#Sabina Aebersold
#23.3.22

#Sortieren nach Einwohnern
einwohner={"Zürich": 370000, "Genf": 190000, "St. Gallen": 80000}

sortierliste=[]
for stadt, einwohnerzahl in einwohner.items():
    sortierliste.append([einwohnerzahl, stadt])
for einwohnerzahl, stadt in sorted(sortierliste):
    print(stadt, "hat", einwohnerzahl, "Einwohner")
