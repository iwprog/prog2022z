# übungsbeispiele zu funktionen und kontrollstrukturen 2.
# Sabina Aebersold
# 3.3.22

# Schreiben Sie eine Funktion, welche das Alter einer Person entgegennimmt und daraus die theoretische
# maximale Herzfrequenz berechnet.
# Die Formel f¨ur die Berechnung der Herzfrequenz lautet:
# max Herzfrequenz = 220 − Alter in Jahren (1)
# >>>berechne_herzfrequenz(25)
# 195

alter = int(input("Wie alt sind Sie? "))

def berechne_herzfrequenz(alter):
    max_herzfrequenz = 220 - alter
    return(max_herzfrequenz)

print(berechne_herzfrequenz(alter))