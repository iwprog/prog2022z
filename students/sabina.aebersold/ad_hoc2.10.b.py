# Sabina Aebersold
# 7.3.22
# #a. Nutzen Sie verschachtelte Schleifen, um vier Dreiecke
# #hintereinander zu zeichnen:
# Erweitern Sie Ihr Programm, sodass der Benutzer die
# Anzahl der gezeichneten Dreiecke frei wählen kann

from turtle import*

anzahl_dreiecke = numinput("Dreieck", "Wie viele Dreiecke möchten Sie zeichnen?")

for count in range (int(anzahl_dreiecke)):
    for count in range(3):
        forward(100)
        left(120)
    penup()
    forward(120)
    pendown()