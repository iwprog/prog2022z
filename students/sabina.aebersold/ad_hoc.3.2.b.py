# Ad hoc 3.2.b
# Sabina Aebersold
# 9.3.22
# 
# Schreiben Sie ein Programm, das eine Zeichenkette und ein
# Zeichen (hintereinander in zwei getrennten Eingabefeldern)
# einliest. Ausgegeben werden sollen alle Positionen, an denen das
# Zeichen, unabhängig von Gross- und Kleinschreibung, in der
# Zeichenkette vorkommt.
# Tipp: Benutzen Sie ein FOR-Schleife zusammen mit der Funktion
# enumerate.
# Eingabe: Wenn Fliegen hinter Fliegen fliegen
# Zeichen: f
# Ausgabe: 6 21 29

eingabe=input("Eingabe: ")
zeichen=input("Zeichen: ")


for position, element in enumerate(eingabe):
    if element.lower()==zeichen.lower():
        print("Ausgabe: ", position)


