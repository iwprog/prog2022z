# Übungen Dictionaries 5
# Sabina Aebersold
# 31.3.22

berge={}
for count in range(1, 4):
    berg=input(str(count)+ ". Berg: ")
    hoehe=input(str(count)+ ". Höhe: ")
    berge[berg]=(hoehe)


for berg, hoehe in sorted(berge.items()):
    hoehe_ft=int(hoehe)*3.28
    print(berg, "ist", hoehe, "m ("+ str(hoehe_ft)+ " ft) hoch.")