# Übungen Dictionaries 5, Erweiterung json
# Sabina Aebersold
# 05.04.22
from json import loads, dumps

berge={}

try:
    with open("berge.json") as f:
        berge_str = f.read()
        berge=loads(berge_str)
except FileNotFoundError:
    print("Leider keine gespeicherterte Bergliste vorhanden.")
    berge = {}
    
for count in range(1, 4):
    berg=input(str(count)+ ". Berg: ")
    hoehe=input(str(count)+ ". Höhe: ")
    berge[berg]=(hoehe)

for berg, hoehe in sorted(berge.items()):
    hoehe_ft=int(hoehe)*3.28
    print(berg, "ist", hoehe, "m ("+ str(hoehe_ft)+ " ft) hoch.")
    
with open("berge.json", "w") as f:
    berge_str=dumps(berge)
    f.write(berge_str)
    
