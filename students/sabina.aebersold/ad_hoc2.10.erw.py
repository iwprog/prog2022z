# ad-hoc übung 2.10 erweitert: basierend auf einem muster sollen quadrate, dreiecke und kreise gezeichnet werden.
# dabei steht ein "d" für ein dreieck, ein "o" für einen kreis und eine "#" für ein quadrat.
# das muster "##ddoo" zeichenet als zum beispiel: quadrat - quadrat - dreieck - dreieck - kreis - kreis
# Sabina Aebersold
# 4.3.22

from turtle import*

form = input("Bitte Zeichenabfolge mit d, o und # eingeben: ")

def dreieck():
    for count in range(3):
        left(120)
        forward(100)
        
    penup()
    forward(100)
    pendown()

def rechteck():
    for count in range(2):
        forward(100)
        left(90)
        forward(50)
        left(90)
    penup()
    forward(120)
    pendown()

def kreis():
    circle(50)
    penup()
    forward(100)
    pendown()
    
for eingabe in form:
    if eingabe == "d":
        dreieck()
    elif eingabe == "#":
        rechteck()
    elif eingabe == "o":
        kreis()


