# Übungsblatt Listen 5.
# Sabina Aebersold
# 23.3.22
# 
# Remove Stopwords: Schreiben Sie eine Funktion, welche einen Text
# entgegennimmt und aus diesem alle Stoppw¨orter, die in einer Liste
# definiert sind, entfernt und eine kleingeschriebene Liste aller W¨orter
# (ohne Stoppw¨orter) zur¨uckgibt.
# l_input = ’Heinz war heute in den Bergen. Es war eine \
# lange Wanderung’
# stopwords = (’der’, ’die’, ’das’, ’in’, ’auf’, ’unter’,
# ’ein’, ’eine’, ’ist’, ’war’, ’es’, ’den’)
# >>> stopword_filter(l_input, stopwords)
# [’heinz’, ’heute’, ’bergen’, ’lange’, ’wanderung’]


def stopword_filter(l_input, stopwords):
    l_output=[]
    liste=l_input.split(" ")
    for element in liste:
        if element in stopwords:
            pass
        else:
            l_output.append(element)
    return(l_output)

l_input=("Heinz war heute in den Bergen. Es war eine lange Wanderung")
stopwords=("der", "die", "das", "in", "auf", "unter", "ein", "eine", "ist",
           "war", "es", "den")
print(stopword_filter(l_input, stopwords))

