# Ad hoc 2.10.c
# Sabina Aebersold
# 7.3.22
# 
# Erstellen Sie eine Programmversion, in welcher der
# Benutzer angeben kann (i) wie viele Dreiecke pro Reihe
# und (ii) in wie vielen Reihen Dreiecke gezeichnet werden
# sollen

from turtle import*

dreiecke_pro_reihe = numinput("Dreieck", "Wie viele Dreiecke möchten Sie in einer Reihe zeichnen?")
dreiecke_pro_spalte = numinput("Dreieck", "Wie viele Dreiecke möchten Sie in einer Spalte zeichnen?")

for count in range (int(dreiecke_pro_spalte)):
    for count in range (int(dreiecke_pro_reihe)):
        for count in range(3):
            forward(100)
            left(120)
        penup()
        forward(120)
        pendown()   
    penup()
    right(180)
    forward(dreiecke_pro_reihe*220-200)
    left(90)
    forward(120)
    right(270)
    pendown()