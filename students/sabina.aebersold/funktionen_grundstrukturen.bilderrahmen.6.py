# übungsbeispiele zu funktionen und kontrollstrukturen 6.
# Sabina Aebersold
# 3.3.22
# 
# Schreiben Sie eine Funktion welche einen Bilderrahmen mit einer bestimmten L¨ange und Breite unter Zuhilfenahme von Schleifen zeichnet.
# Weiters soll die Funktion noch die Rahmen- und F¨ullfarbe entgegennehmen.
# >>> zeichne_rahmen(100,25, ’black’, ’white)

from turtle import *

aussen = numinput("Aussenlänge", "Wie gross soll die Aussenlänge sein?")
abstand = numinput("Abstand", "Wie breit soll der Rahmen sein?")
rahmenfarbe = textinput("Rahmenfarbe", "Welche Farbe soll der Rahmen haben?")
fuellfarbe = textinput("Füllfarbe", "Wie Füllfarbe soll der Rahmen haben?")

def zeichne_rahmen(aussen, abstand, rahmenfarbe, fuellfarbe):
    pencolor(rahmenfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    for count in range(4):
        right(90)
        forward(aussen)
    end_fill()
    fillcolor("white")
    begin_fill()
    penup()
    right(180)
    forward(abstand)
    left(90)
    forward(abstand)
    pendown()
    for count in range(4):
        forward(aussen-abstand*2)
        right(90)
    end_fill()

print(zeichne_rahmen(aussen, abstand, rahmenfarbe, fuellfarbe))

#print(zeichne_rahmen(100, 10, "blue", "red"))