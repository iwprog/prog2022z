#Sabina Aebersold
#2.4.22

# diese auf dem bildschirm ausgibt und 
# ermittelt wie oft die worte "Himmel", "Freiheit", "Spiel" und "Tanz" in
# der datei vorkommen.
# (hinweis: es ist wichtig, dass sich ihr python programm und die
#  textdatei im selben verzeichnis befinden, da ansonsten python
#  die datei nicht öffnen kann).


#Textdatei auf Bildschirm ausgeben
with open ("gutenberg.txt", encoding="utf8") as file:
    text=file.read()
print(text)

# ermittelt wie oft die worte "Himmel", "Freiheit", "Spiel" und "Tanz" in
# der datei vorkommen.
print("Himmel:", text.count("Himmel"))
print("Freiheit:", text.count("Freiheit"))
print("Spiel:", text.count("Spiel"))
print("Tanz:", text.count("Tanz"))

# erweitern Sie Ihr Python Programm, sodass es die Datei direkt aus
# dem Web bezieht, sodass das manuelle Speichern der Datei entfallen kann
from urllib.request import urlopen
from inscriptis import get_text

with urlopen("https://www.gutenberg.org/cache/epub/6079/pg6079.txt") as source:
    text=source.read().decode("utf8")
    print(text)
    