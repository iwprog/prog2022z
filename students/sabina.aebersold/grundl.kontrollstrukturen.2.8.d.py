#Grundlagen Kontrollstrukturen 2.8.d
#Sabina Aebersold
#1.3.22
# 
#  Schreiben Sie eine Schleife, welches von 10 bis 0 z¨ahlt und statt ”0” das
# Wort ”Start” ausgibt.

for i in range(10, -1, -1):
    if i == 0:
        print("Start")
    else:
        print(i)


    