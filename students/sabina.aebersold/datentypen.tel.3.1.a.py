# Übungen Datentypen 3.1 a
# Sabina Aebersold
# 30.3.22

#a
person={"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"}

#b
telefonbuch=[]
telefonbuch.append(person)

#c
telefonbuch.append({"Vorname": "Tim", "Nachname": "Kurz", "Phone": "732"})
telefonbuch.append({"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"})

#d
while True:
    vorname=input("Wie lautet ihr Vorname? Abbruch mit 'x' ")
    if vorname=="x":
        break
    nachname=input("Wie lautet ihr Nachname? ")
    phone=input("Wie lautet ihre Telefonnummer? ")
    telefonbuch.append({"Vorname": vorname, "Nachname": nachname, "Phone": phone})
print("Datenstruktur: ", telefonbuch)