# Leistungsaufgabe 2a.
# Sabina Aebersold
# 9.3.22

preis=input("Bitte geben Sie den 1. Preis ein: ")
count=1
summe=0

while preis != "x":
    summe=summe+int(preis)
    count=count+1
    preis=input("Bitte geben Sie den " +str(count)+ ". Preis ein: ")
    
if summe > 1000:
    print("10% Rabatt, Gesamtpreis = ", summe*0.9, "CHF")
elif summe >=100:
    print("5% Rabatt, Gesamtpreis = ", summe*0.05, "CHF")
else:
    print("Kein Rabatt, Gesamtpreis = ", summe, "CHF")
    
