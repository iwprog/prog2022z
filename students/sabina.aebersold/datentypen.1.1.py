# Übungsblatt Datentypen 1.1
# Sabina Aebersold
# 9.3.22
# #a. Erstellen Sie eine Liste jahreszeiten mit den vier Jahreszeiten.

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
print(jahreszeiten[::])

# b. Löschen Sie die Jahreszeit Frühling aus der Liste.
jahreszeiten.remove("Winter")
print(jahreszeiten[::])

# c. Fügen Sie die Jahreszeit Langas der Liste hinzu.
jahreszeiten.append("Langas")
print(jahreszeiten[::])

# d. Schreiben Sie ein Programm, welches Sie nach Namen fragt und diese so
# lange einer Liste hinzufügt, bis der Benutzer x eingibt. Im Anschluss soll"
# die Liste ausgegeben werden.
# Name: Martin
# Name: Julia
# Name: x
# Liste: ["Martin", "Julia"]

name=input("Name: ")
liste=[]
liste.append(name)
while name !="x":
    name=input("Name: ")
    liste.append(name)
liste.remove("x")     
print("Liste:", liste)



name=input("Name: ")
liste=[]
liste.append(name)
for name in liste:
    if name !="x":
        name=input("Name: ")
        liste.append(name)
    else:
        liste.remove("x")
        print("Liste:", liste)

