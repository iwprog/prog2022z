# Übungen Datentypen 3.3 a
# Sabina Aebersold
# 30.3.22

telefonbuch=[{"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"},
             {"Vorname": "Tim", "Nachname": "Kurz", "Phone": "732"},
             {"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"}]

telefonbuch2=[]    
for person in telefonbuch:
    if "1" not in person["Phone"]:
        telefonbuch2.append(person)
print(telefonbuch2)