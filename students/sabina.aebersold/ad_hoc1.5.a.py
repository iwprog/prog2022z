from turtle import *

laenge = numinput ("Eingabefenster", "Bitte Seitenlänge angeben:")

pensize(5)
pencolor("red")
fillcolor("yellow")
begin_fill()
left(150)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

laenge = laenge *0.5
fillcolor("cyan")
begin_fill()
left(240)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

laenge = laenge *0.5
fillcolor("lime")
begin_fill()
left(240)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()