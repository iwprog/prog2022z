# Leistungsaufgabe 3
# Sabina Aebersold
# 31.3.22


#b
wörterbuch={"Haus": "house", "Zahnarzt": "dentist","Zahn": "dent",
            "Maus": "mouse","Gabel": "fork","Löffel": "spoon",
            "Glück": "luck","Apfel": "apple","Uhr": "clock",
            "Freude": "joy",}
count=0
for deutsch, englisch in wörterbuch.items():
    eingabe=input("Was heisst "+deutsch+": ")
    if eingabe==englisch:
        print("RICHTIG!")
        count=count+1
    else:
        print("FALSCH!")
print("Sie haben", count, "von", len(wörterbuch), "Vokabeln richtig beantwortet!")