#ad hoc Übung - 3.7)
#Autor: Anthony Zoss
#Datum: 25.03.2022

cities = {"Zürich": 370000, "Genf": 190000, "Chur": 30000}

sortierliste = []
for stadt, einwohnerzahl in cities.items():
    sortierliste.append([einwohnerzahl, stadt])
for einwohner, stadt in sorted(sortierliste):
    print(stadt,"har",einwohner,"Einwohner")