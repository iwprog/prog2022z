#Übungsblatt: Dictionaries 3)
#Autor: Anthony Zoss
#Datum: 19.03.2022

#Letztes "-" löschen möglich mit dieser Lösung?

icao = {"Alfa":"a","Bravo":"b","Charlie":"c","Delta":"d","Echo":"e","Foxtrot":"f","Golf":"g","Hotel":"h","India":"i","Juliett":"j","Kilo":"k","Lima":"l","Mike":"m","November":"n","Oscard":"o","Papa":"p","Quebec":"q","Romeo":"r","Sierra":"s","Tango":"t","Uniform":"u","Victor":"v","Whiskey":"w","X-ray":"x","Yankee":"y","Zulu":"z"}

wort_anfang = input("Welches Wort soll ich buchstabieren: ")  
wort = wort_anfang.lower()
wort = list(wort)

print("Ausgabe: ",end="")
for element in wort:
    if element in icao.values():
        for alphabet in icao:
            if element == icao[alphabet]:
                print(alphabet+"-",end="")

#Aufgab a)
print("Ausgabe: ",end="")
for element in wort:
    if element not in icao.values():
        print("Kann",wort_anfang,"nicht beuchstabieren, da '"+element+"' nicht definiert wurde.")
        break
    
    if element in icao.values():
        for alphabet in icao:
            if element == icao[alphabet]:
                print(alphabet+"-",end="")

#Aufgabe b)
def alphabetisierung():
    if element in icao.values():
        for alphabet in icao:
            if element == icao[alphabet]:
                print(alphabet+"-",end="")

print("Ausgabe: ",end="")
for element in wort:
    if element == "ä":
        print("Alfa-Echo-",end="")
        alphabetisierung()
    if element == "ö":
        print("Oscar-Echo-",end="")
        alphabetisierung()
    if element == "ü":
        print("Uniform-Echo-",end="")
        alphabetisierung()

    if element in icao.values():
        alphabetisierung()




