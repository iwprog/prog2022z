#Übungsblatt: Datentypen 2.1)
#Autor: Anthony Zoss
#Datum: 25.03.2022

#Aufgabe a)
preisliste = {"Brot":3.20, "Milch":2.10, "Orangen":3.75, "Tomaten":2.20}

#Aufgabe b)
preisliste["Milch"] = 2.05

#Aufgabe c)
del preisliste["Brot"]

#Aufgabe d)
preisliste["Tee"] = 4.20
preisliste["Peanuts"] = 3.90
preisliste["Ketchup"] = 2.10

#Aufgabe e)
artikel = ""
einkaufsliste = {}
while artikel != "x":
    artikel = input("Lebensmittel: ")
    if artikel in preisliste:
        print("Preis:",preisliste[artikel])
        einkaufsliste[artikel] = str(preisliste[artikel])
    if artikel !="x" and artikel not in preisliste:
        print("Artikel nicht vorhanden")
print("Dictionary:",einkaufsliste)

#Aufgabe f)
artikel = ""
einkaufsliste = {}
while artikel != "x":
    artikel = input("Lebensmittel: ")
    if artikel in preisliste:
        print("Preis:",preisliste[artikel])
        einkaufsliste[artikel] = preisliste[artikel]
    if artikel !="x" and artikel not in preisliste:
        print("Artikel nicht vorhanden")
print("Dictionary:",einkaufsliste)
