#Übungsblatt: Dictionaries 2)
#Autor: Anthony Zoss
#Datum: 19.03.2022

l_input = "Der Tag begann sehr gut! Der Morgen war schön."
satzzeichen = ".,!?"

def word_stat(l_input):
    for x in range (len(satzzeichen)):
        l_input = l_input.replace(satzzeichen[x], "")
    l_input = l_input.lower()

    anzahl = {}
    for wort in l_input.split():
        if wort in anzahl:
            anzahl[wort] = anzahl[wort] + 1
        else:
            anzahl[wort] = 1
    return(anzahl)

print(word_stat(l_input))

