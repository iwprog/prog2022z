#Übungsblatt: Listen 5)
#Autor: Anthony Zoss
#Datum: 25.03.2022

l_input = "Heniz war heute in den Bergen. Es war eine \ lange wanderung"
stopwords = ('der','die','das','in','auf','unter','ein','eine','ist','war','es','den')
satzzeichen = "\.!?," #Satzzeichen sollen entfernt werden

def stop_filter(l_input, stopwords):
    for x in range(len(satzzeichen)):
        l_input = l_input.replace(satzzeichen[x],"") #entferne Satzzeichen
    l_input = l_input.lower() #mach alles klein
    l_input = list(l_input.split()) #mach eine Liste - die Einträge durch Leerzeichen erkennen
    
    for element in stopwords:
        while element in l_input:
            l_input.remove(element) 
    
    return(l_input)

print(stop_filter(l_input, stopwords))