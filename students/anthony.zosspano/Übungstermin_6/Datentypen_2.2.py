#Übungsblatt: Datentypen 2.2)
#Autor: Anthony Zoss
#Datum: 25.03.2022

#Preisliste aus Aufgabe 2.1)
preisliste = {"Brot":3.20, "Milch":2.10, "Orangen":3.75, "Tomaten":2.20}
preisliste["Milch"] = 2.05
del preisliste["Brot"]
preisliste["Tee"] = 4.20
preisliste["Peanuts"] = 3.90
preisliste["Ketchup"] = 2.10

#Aufgabe a)
for artikel in preisliste:
    print(artikel,"kostet",preisliste[artikel],"CHF")

print()

#aufgabe e)
preisliste["Apfel"] = 1.90

preisliste_2 = []
for artikel, preis in preisliste.items():
    if preis < 2:
        preisliste_2.append([artikel, preis])
    for artikel_2, preis_2 in preisliste_2:
        print(artikel_2,"kostet",preis_2,"CHF")









