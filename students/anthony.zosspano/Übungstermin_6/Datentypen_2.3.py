#Übungsblatt: Datentypen 2.3)
#Autor: Anthony Zoss
#Datum: 25.03.2022
#Preisliste aus Aufgabe 2.1)
preisliste = {"Brot":3.20, "Milch":2.10, "Orangen":3.75, "Tomaten":2.20}
preisliste["Milch"] = 2.05
del preisliste["Brot"]
preisliste["Tee"] = 4.20
preisliste["Peanuts"] = 3.90
preisliste["Ketchup"] = 2.10

#Aufgabe c)
preisliste_2 = []
for artikel, preis in preisliste.items():
    if "en" not in artikel:
        preisliste_2.append([artikel, preis])

print(preisliste_2)