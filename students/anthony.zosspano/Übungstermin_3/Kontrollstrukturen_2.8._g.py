#Übungsblatt Kontrollstukturen
#Aufgabe 2.8 g)
#Autor: Anthony Zoss
#Datum: 04.03.2022

lauf = 1
summe = 0
eingabe ="0"
while eingabe != "-1":
    eingabe = input("Bitte geben Sie Zahl "+str(lauf)+" ein: ")
    summe = summe + int(eingabe)
    lauf = lauf+1
print("Summe:",str(summe+1))
