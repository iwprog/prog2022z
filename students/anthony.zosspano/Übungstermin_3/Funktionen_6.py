#Übungsblatt Funktionen und Kontrollstukturen
#Aufgabe 6)
#Autor: Anthony Zoss
#Datum: 04.03.2022
from turtle import *

hideturtle()

def zeichne_rahmen (v1, v2, pen, fill):
    rahmen = v1 - v2 #die Breite des Rahemn bestimmen
    
    pencolor(pen) #Grundeinstellungen bestimmen
    fillcolor(fill)
    
    penup() #Positionier, damit zentriert
    goto(int(-v1/2),int(-v1/2))
    pendown()
    
    begin_fill() #Rahmen
    fd(v1),lt(90),fd(v1),lt(90),fd(v1),lt(90),fd(v1)
    end_fill()
    
    penup() #Positionierung, innerer Viereck zentriert
    home()
    goto(int(-rahmen/2),int(-rahmen/2))
    
    pendown()
    fillcolor("white")
    begin_fill()
    fd(rahmen),lt(90),fd(rahmen),lt(90),fd(rahmen),lt(90),fd(rahmen)
    end_fill()
    

zeichne_rahmen(100,25,"black","cyan")

exitonclick()

