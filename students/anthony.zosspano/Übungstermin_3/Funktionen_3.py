#Übungsblatt Funktionen und Kontrollstukturen
#Aufgabe 3)
#Autor: Anthony Zoss
#Datum: 04.03.2022

def get_billet_preis(alter,km):
    berechnung = round(((km * 0.25) + 2)* 20)/20 #auf 0.05 Rappen runden
    if alter < 6:
        berechnung = 0
        print(berechnung)
    elif alter < 16:
        berechnung = berechnung/2
        print(berechnung)
    elif alter > 18:
        print(berechnung)

get_billet_preis(3,200)
get_billet_preis(8,200)
get_billet_preis(30,200)