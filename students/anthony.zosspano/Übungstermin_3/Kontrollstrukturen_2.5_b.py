#Übungsblatt Kontrollstukturen
#Aufgabe 2.5 b)
#Autor: Anthony Zoss
#Datum: 04.03.2022

alter = int(input("Geben Sie ihr Alter an: "))

if alter < 0:
    print(alter,"ist ein ungültiges Alter")
elif alter <= 6:
    print("Mit",alter,"ist man geschäftsunfähig")
elif alter <= 14:
    print("Mit",alter,"ist man unmündig")
elif alter <=17:
    print("Mit",alter,"ist man mündig minderjährig")
else:
    print("Mit",alter,"ist man volljährig")
    

