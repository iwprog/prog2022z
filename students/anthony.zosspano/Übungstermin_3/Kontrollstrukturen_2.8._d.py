#Übungsblatt Kontrollstukturen
#Aufgabe 2.8 d)
#Autor: Anthony Zoss
#Datum: 04.03.2022

i = 10
while i >= 0:
    if i >= 1:
        print(i)
        i = i-1
    else:
        print("Start")
        i = i-1

