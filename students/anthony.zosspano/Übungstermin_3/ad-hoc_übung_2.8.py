#ad hoc Übung - 2.8)
#Autor: Anthony Zoss
#Datum: 27.02.2022

def zahlenreihe(z1, z2, z3, z4, z5, z6, z7):
    maximum = (max(z1, z2, z3, z4, z5, z6, z7))
    print("Max: " + str(maximum))
    minimum = (min(z1, z2, z3, z4, z5, z6, z7))
    print("Min: " + str(minimum))
    summe = (z1 + z2 + z3 + z4 + z5 + z6 + z7)
    durchschnitt = round((summe/7), 2)
    return "Durchschnitt: " + str(durchschnitt)

print(zahlenreihe(12, 23, 5, 56, 77, 18, 9))