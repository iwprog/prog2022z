#Übungsblatt Kontrollstukturen
#Aufgabe 2.8 f)
#Autor: Anthony Zoss
#Datum: 04.03.2022


lauf = 1
i = 0
while i <= 10:
    zahl = input("Bitte geben Sie Zahl "+str(lauf)+" ein: ")
    i = i + int(zahl)
    lauf = lauf+1
print("Summe:",i)