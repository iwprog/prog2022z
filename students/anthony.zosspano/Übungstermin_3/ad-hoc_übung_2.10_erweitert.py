#ad hoc Übung - 2.10 erweitert)
#Autor: Anthony Zoss
#Datum: 05.05.2022
from turtle import *

def v(): #'v' anstatt '#' verwendet
    begin_fill()
    fd(100),lt(90),fd(100),lt(90),fd(100),lt(90),fd(100),lt(90)
    end_fill()

def d():
    begin_fill()
    fd(100),lt(120),fd(100),lt(120),fd(100),lt(120)
    end_fill()

def o():
    begin_fill()
    circle(50,360)
    end_fill()
    

eingabe = "0"
while eingabe != "q":
    eingabe = input("Was soll gezeichnet werden (v, d, o - um zu beenden, drücken Sie die Taste Q)? ")
    if eingabe == "v":
        v()
        penup()
        fd(110)
        pendown()
    elif eingabe == "d":
        d()
        penup()
        fd(110)
        pendown()
    elif eingabe == "o":
        penup()
        fd(55)
        pendown()
        o()
        penup()
        fd(55)
        pendown()

exitonclick()