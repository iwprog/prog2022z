#ad-hoc Übung 2.2)
#Autor: Anthony Zoss
#Datum: 27.02.2022
from turtle import *
speed(10)

def dreieck():
    begin_fill()
    fd(laenge),lt(120),
    fd(laenge),lt(120),
    fd(laenge)
    end_fill()

pensize(5)
pencolor("red")
lt(270)

laenge = 100
fillcolor("cyan")
dreieck()

laenge = 200
fillcolor("yellow")
dreieck()

laenge = 50
fillcolor("lime")
dreieck()

exitonclick()

