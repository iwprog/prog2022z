#ad-hoc Übung 2.3)
#Autor: Anthony Zoss
#Datum: 27.02.2022
from turtle import *

def jump(winkel, strecke):
    penup()
    lt(winkel),
    fd(strecke),
    rt(winkel)
    pendown()

#Beispiel
jump(60, 100)
fd (100),lt(120),fd(100),lt(120),fd(100)
jump(60, 200)

exitonclick()