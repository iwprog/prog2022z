#ad-hoc Übung 2.8)
#Autor: Anthony Zoss
#Datum: 27.02.2022

def zahlenreihe(z1, z2, z3, z4, z5, z6, z7):
    minimum = min(z1, z2, z3, z4, z5, z6, z7)
    print("Minimum: " + str(minimum))
    maximum = max(z1, z2, z3, z4, z5, z6, z7)
    print("Maximum: " + str(maximum))
    summe = (z1+z2+z3+z4+z5+z6+z7)
    schnitt = round((summe/7), 2)
    return "Durchschnitt: " + str(schnitt)

print(zahlenreihe(12, 23, 5, 56, 77, 18, 9))