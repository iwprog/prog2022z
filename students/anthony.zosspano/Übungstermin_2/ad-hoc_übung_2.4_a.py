#ad-hoc Übung 2.4a)
#Autor: Anthony Zoss
#Datum: 27.02.2022

from math import pi

def volumen(r, h):
    vol = round(((pi *(r*r))*h), 4)
    return "Das Volumen beträgt: " + str(vol)

print(volumen(2,2))

