#ad-hoc Übung 2.2)
#Autor: Anthony Zoss
#Datum: 27.02.2022
from turtle import *
speed(10)

def dreieck():
    begin_fill()
    fd(laenge),lt(120),
    fd(laenge),lt(120),
    fd(laenge)
    end_fill()

pensize(5)
pencolor("red")
lt(270)

laenge = numinput("Dreieck CYAN", "Geben Sie die Seitenlänge für das cyan Dreieck an: ")
fillcolor("cyan")
dreieck()

laenge = numinput("Dreieck YELLOW", "Geben Sie die Seitenlänge für das gelbe Dreieck an: ")
fillcolor("yellow")
dreieck()

laenge = numinput("Dreieck LIME", "Geben Sie die Seitenlänge für das lime Dreieck an: ")
fillcolor("lime")
dreieck()

exitonclick()

