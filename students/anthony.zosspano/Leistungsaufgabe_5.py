#Leistungsaufgabe 5)
#Autor: Anthony Zoss
#Datum: 12.04.2022

from urllib.request import urlopen
from inscriptis import get_text
from csv import writer

#Aufgabe 3) - Erweiterung Programm
ressource = input("Geben Sie die Ressourcenbezeichnung an: ")
try:
    if "https://" in ressource:
        with urlopen(ressource) as source:
            web_content = source.read().decode('utf8')
    else:
        with open(ressource, encoding='utf8') as f:
            file_content = f.read()
except FileNotFoundError:
    print(ressource, "existiert nicht")
    
#Für Aufgabe 1 und 2
#with urlopen ('https://www.ietf.org/rfc/rfc2616.txt') as source:
#     web_content = source.read().decode('utf8')

#Entweder 1a oder 1b unkommentierten - gleichzeitig funktioniert es nicht

# #Aufgabe 1a)
# wörter = {}
# for wort in web_content.split():
#     if wort in wörter:
#         wörter[wort] = wörter[wort] + 1
#     else: wörter[wort] = 1
# 
# ###Dazugehörende Aufgabe 2)
# with open("wort.csv", "w", encoding="utf8") as f:
#     csv_writer = writer(f, delimiter=";")
#     for wort, anzahl in wörter.items():
#         csv_writer.writerow([wort, anzahl])


# #Aufgabe 1b)
# buchstaben = {}
# for wort in web_content.split():
#     for buchstabe in wort:
#         if buchstabe in buchstaben:
#             buchstaben[buchstabe] = buchstaben[buchstabe] + 1
#         else:
#             buchstaben[buchstabe] = 1
# 
# ###Dazugehörende Aufgabe 2
# with open("buchstabe.csv", "w", encoding="utf8") as f:
#     csv_writer = writer(f, delimiter=";")
#     for buchstabe, anzahl in buchstaben.items():
#         csv_writer.writerow([buchstabe, anzahl])
        


    



