#Leistungsaufgabe 1a)
#Autor: Anthony Zoss
#Datum: 27.02.2022
from turtle import *
speed(10) #Geschwindigkeit verschnellern
hideturtle() #Pfeil/Turtle unsichtbar machen
pencolor("white") #Linien mit Hintergrundsfarbe verschwinden lassen

def viereck(): #Definition Viereck für Flagge und Kreuz
    begin_fill(),
    fd(seitenlaenge),lt(90),
    fd(seitenlaenge),lt(90),
    fd(seitenlaenge),lt(90),
    fd(seitenlaenge),lt(90),
    end_fill()

#Flagge Hintergrund
goto(-200, -200) #Positionierung damit die Flagge zentriert bliebt
seitenlaenge = 400 #400px x 400px
fillcolor("red")
viereck()

#Flagge Kreuz
seitenlaenge = 80 #neue Seitenlänge für Einzelteile Kreuz
fillcolor("white")

penup(), goto(-40,-40), pendown() #Neupositionierung damit weiterhin zentriert (penup & -down, damit keine unerwünschte Linien erscheinen)
viereck() #5 Vierecke um einen Krez zu machen
goto(-40,-120)
viereck()
goto(-40,40)
viereck()
goto(-120,-40)
viereck()
goto(40,-40)
viereck()

exitonclick()
