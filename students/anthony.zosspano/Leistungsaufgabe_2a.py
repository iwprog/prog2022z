#Leistungsaufgabe 2a)
#Autor: Anthony Zoss
#Datum: 12.03.2022

preis = " "
summe = 0
lauf = 1
while preis != "x":
    preis = input("Geben Sie den "+str(lauf)+". Preis ein: ")
    while preis.isdigit():
        summe = summe + int(preis)
        preis = input("Geben Sie den "+str(lauf)+". Preis ein: ")
        lauf = lauf + 1
  
    if summe >= 100 and summe <1000:
        rabatt = summe *0.05
        total = summe - rabatt
        print("5% Rabatt, Gesamtpreis: "+str(total))
            
    elif summe >= 1000:
        rabatt = summe * 0.10
        total = summe - rabatt
        print("10% Rabatt, Gesamtpreis: "+str(total))    
        
    else:
        print("kein Rabatt, Gesamtpreis: "+str(summe))