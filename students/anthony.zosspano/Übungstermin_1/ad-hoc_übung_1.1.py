#Aufgabe a)
#Variante 1
Name = "Zoss"
Vorname = "Anthony"
Strasse = "Bettlersmatteweg"
Hausnr = "19"
PLZ = "3778"
Wohnort = "Schönried"
print(Name, Vorname, Strasse, Hausnr, PLZ, Wohnort)
#Variante 2
print("Zoss Anthony Bettlersmatteweg 19 3778 Schönried")
#Variante 3
print ("Name, Vorname, Strasse, Hausnr., PLZ, Wohnort")

#Aufgabe b)
print(1+2+3+4+5)
print(1-2-3-4-5)
print(1*2*3*4*5)
print(1/2/3/4/5)

#Aufgabe c)
#Variante 1
a= "wenn"
b= "fliegen " #mit Leerzeichen
c= "hinter"
d= "hinterher"
print (a, b + c, b*5 + d)

#Variante 2
e= "wenn"
f= "fliegen" #ohne Leerzeichen
g= "hinter"
h= "hinterher"
print (e,f,g,(f+" ")*5+h, sep=(" "))

#Aufgabe d)
txt = "Hello"[::-1]
print(txt)

