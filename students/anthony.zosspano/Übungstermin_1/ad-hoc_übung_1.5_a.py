from turtle import *
left(45)
pensize(5)
pencolor("red")
speed(10)

#Dreieck 1
laenge = numinput("Dreieck", "Bitte geben Sie die Länge des ersten Dreiecks an")

fillcolor("lime")
begin_fill()
fd(laenge),lt(120),fd(laenge),lt(120),fd(laenge),lt(120)
end_fill()

#Dreieck 2
laenge = laenge * 1.30
farbe = textinput("Dreieck", "In welcher Farbe soll ich das zweite Dreieck zeichnen?")

fillcolor(farbe)
begin_fill()
lt(60),bk(laenge),lt(120),bk(laenge),lt(120),bk(laenge),lt(120)
end_fill()

#Dreieck 3
laenge = laenge * 1.50
farbe = textinput("Dreieck", "In welcher Farbe soll ich das dritte Dreieck zeichnen?")

fillcolor(farbe)
begin_fill()
lt(240),bk(laenge),lt(120),bk(laenge),lt(120),bk(laenge),lt(120)
end_fill()

exitonclick()