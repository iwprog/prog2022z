#Aufgabe b)
from turtle import *
#Grundeinstellungen
pensize(5)
pencolor("red")
speed(10)
left(45) #Positionierung für Viereck 1

i=0 #Definition i für Schleifen
begin_fill() #Viereck 1
while i <4: #vier Wiederholungen
    i+=1
    fillcolor("cyan")
    fd(100),lt(90)
else:
    lt(90),fd(100) #Rückkehr an Punkt für Start Viereck 2
    end_fill()
    
left(110) #richtige Richtung für Viereck 2
begin_fill() #Viereck 2
while i <8: #vier Wiederholungen
    i+=1
    fillcolor("yellow")
    fd(100),lt(90)
else:
    end_fill()
    
left(280) #Richtige Richtung für Viereck 3
begin_fill() #Viereck 3
while i <12: #vier Wiederholungen
    i+=1
    fillcolor("magenta")
    fd(100),lt(90)
else:
    end_fill()
    
left(285) #Richtige Richtung für Viereck 4
begin_fill() #Viereck 4
while i <16: #vier Wiederholungen
    i+=1
    fillcolor("blue")
    fd(100),lt(90)
else:
    end_fill()
    
left(285) #Richtige Richtung für Viereck 5   
begin_fill() #Viereck 5
while i <20: #vier Wiederholungen
    i+=1
    fillcolor("lime")
    fd(100),lt(90)
else:
    end_fill()

exitonclick()