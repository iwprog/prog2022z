#Übungsblatt Funktionen und Kontrollstrukturen
#Aufgabe 10a)
#Autor: Anthony Zoss
#Datum: 12.03.2022

# Preis für die Fahrkarte berechnen
def billet_preis(alter, km):
    grundpreis = km * 0.25 + 2
    #Differenzierung nach Alter
    if alter < 6:
        grundpreis = 0
    elif alter < 16:
        grundpreis = int(grundpreis / 2)
    #Wiedergabe
    return "Die Fahrkarte kostet: "+str(grundpreis)+" CHF"

# Preis für den Eintritt ins Schwimmbar berechnen
def eintritt_preis (karte, rabatt, alter):
    preis = 0
    #Differenzierung nach Art der Eintrittskarte
    if karte == "Kurzzeitkarte":
        preis = 5
    elif karte == "Nachmittagskarte":
        preis = 6
    elif karte == "Tageskarte":
        preis = 10
    #Differenzierung nach Alter
    if alter < 6:
        preis = 0
    elif alter <=12:
        preis = preis / 2
    elif alter > 12:
        preis = preis
    #Falls Rabatt vorhanden ist
    if rabatt == True:
        preis = preis * 0.9
    #Wiedergabe
    return "Eintrittspreis: "+str(preis)+" CHF"

# Mahnungsgebühr für die Bibliothek berechnen
def gebuehr_mahnung(mahnung, medium, post):
    gebuehr = 0
    #Differenzierung nach Anzahl Manungen
    if mahnung == 1:
        gebuehr = 2
    elif mahnung == 2:
        gebuehr = 4
    elif mahnung == 3:
        gebuehr = 6
    #Berechnung für die Anzahl Medien    
    gebuehr = gebuehr * medium    
    #Falls Mahnung per Post versendet wurde
    if post == True:
        gebuehr = gebuehr + 2
    #Wiedergabe    
    return "Mahnungsgebühr: "+str(gebuehr)+" CHF"


#Menü
eingabe = " "
while eingabe != "0":
    for i in range (25):
        print("=", end=" ")
    print()
    print("Programmübersicht:")
    print("   1 ... Preis für eine Fahrkarte berechnen")
    print("   2 ... Eintritt für das Schwimmbad berechnen")
    print("   3 ... Mangebühr für die Bibliothek berechnen")
    print()
    print("   0 ... Programm beenden")
    for i in range (25):
        print("=", end=" ")
    print()    
    
    eingabe = input("Gewählte Option: ")
    
    print()
    if eingabe == "1":
        print("Preis für Fahrkarte berechnen")
        for a in range (25):
            print("-", end=" ")
        print()
        alter = int(input("Bitte geben Sie Ihr Alter ein: "))
        km = int(input("Wie viele km wollen Sie reisen? "))
        print()
    
        print(billet_preis(alter, km))
        break
    
    elif eingabe == "2":
        print("Eintritt für das Schwimmbar berechnen")
        for a in range (25):
            print("-", end=" ")
        print()
        karte = (input("Welche Eintrittskarte hätten Sie gerne: "))
        rabatt = (input("Haben Sie eine Ermässigungskarte? "))
        alter = int(input("Bitte geben Sie Ihr Alter ein: "))
        print()
     
        print(eintritt_preis(karte, rabatt, alter))
        break
        
    elif eingabe == "3":
        print("Mahngebür für die Bibliothek berechnen")
        for a in range (25):
            print("-", end=" ")
        print()
        mahnung = int(input("Um welche Mahnung handelt es sich? "))
        medium = int(input("Wie viele Medien haben sie noch offen? "))
        post = input("Wurde die Mahnung per Post versandt? ")
        print()

        print(gebuehr_mahnung(mahnung, medium, post))       
        break

    elif eingabe == "0":
        print("Programm beendet")
    
    else:
        print("Ungültige Option!")
        break



