#ad hoc Übung - 2.10)
#Autor: Anthony Zoss
#Datum: 13.03.2022

from turtle import *

def dreieck():
    begin_fill()
    for i in range(3):
        fd(100)
        lt(120)
    end_fill()

fillcolor("lightblue")

reihe = int(input("Anzhal der Reihen: "))
anzahl = int(input("Anzahl der gezeichneten Dreiecke: "))

abstand = 120
lauf = 1

for r in range(reihe):
    for a in range(anzahl):
        dreieck()
        penup()
        fd(120)
        pendown()
    penup()
    home()
    rt(90), fd(abstand * lauf), lt(90)
    pendown()
    lauf = lauf + 1
    
exitonclick()
