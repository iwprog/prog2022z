#Leistungsaufgabe 3b)
#Autor: Anthony Zoss
#Datum: 03.04.2022

liste = {'be':'sein',
           'beat':'schlagen',
           'become':'werden',
           'begin':'beginnen',
           'blow':'blasen',
           'break':'brechen',
           'bring':'bringen',
           'build':'bauen',
           'buy':'kaufen',
           'catch':'fangen'}
i = 0
for wort in liste:
    frage = input("Was heisst "+wort+" : ")
    if frage == liste[wort]:
        print("RICHTIG!")
        i = i + 1
    else:
        print("FALSCH")
print("Sie haben",i, "von 10 Vokabeln richtig beantwortet")
    
    
#     print(wort) Englisch
#     print(liste[wort]) Deutsch
    