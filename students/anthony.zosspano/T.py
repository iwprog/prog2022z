#Wir schreiben Python - T
#Autor: Anthony Zoss
#Datum: 27.02.2022
from turtle import *
speed(10)
hideturtle()
#fd(300),lt(90),fd(400),lt(90),fd(300),lt(90),fd(400),lt(90) => Rahmen ausgeblendet

#Positionierung Start T
penup()
lt(90), fd(250), rt(90), fd(20)
pendown()

#Grundeinstellungen
pensize(0)
pencolor("black")

# T - Teil 1
fillcolor("red")
begin_fill()
fd(80), rt(90)
fd(200), lt(90)
fd(40), lt(90)
fd(200), rt(45)
fd(80), rt(45)
fd(90), lt(90)
fd(40), lt(90)
fd(110), lt(45)
fd(80), rt(45)
fd(100), lt(90)
fd(40)
end_fill()

#Positionierung T - Teil 2
penup()
lt(90), fd(80), rt(90), fd(200), lt(90), fd(50), lt(90)
pendown()

#T - Teil 2
fillcolor("black")
begin_fill()
fd(190), rt(45)
fd(80), rt(45)
fd(80), rt(90)
fd(40), rt(90)
fd(97.5), lt(90)
fd(207.5), rt(90)
fd(40)
end_fill()

#Positionierung T - Teil 3
penup()
fd(50), rt(90), fd(200), lt(90), fd(80), rt(90), fd(55), rt(90)
pendown()

# T- Teil 3
begin_fill()
fd(90), lt(45)
fd(55), lt(135)
fd(130), lt(90)
fd(40)
end_fill()

#Startposition H
penup()
fd(302.5), lt(90), fd(282.5)
pendown()