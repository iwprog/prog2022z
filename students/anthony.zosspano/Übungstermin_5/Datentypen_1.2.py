#Übungsblatt: Datentypen 1.2)
#Autor: Anthony Zoss
#Datum: 19.03.2022

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

#Aufgabe 1.2a)
print(sorted(jahreszeiten))

#Aufgabe 1.2b)
print(jahreszeiten[0:3])

#Aufgabe 1.2c)
for element in jahreszeiten:
    print(element,"-", element[-2::1])

#Aufgabe 1.2d)
for zeichenkette in jahreszeiten:
    if "er" in zeichenkette:
        print(zeichenkette)
        
#Aufgabe 1.2e)
for buchstabe in jahreszeiten:
    print(buchstabe,"-",buchstabe.count("r"))