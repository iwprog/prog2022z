#Übungsblatt: Listen 3)
#Autor: Anthony Zoss
#Datum: 19.03.2022

from random import choice
ueberraschungen = ['Samiklaus', 'Christbaum', 'Weihnachtskugel', 'Stiefel', 'Schneeball']

eingabe = " "
while eingabe != "x":
    zufallselement = choice(ueberraschungen)
    eingabe = input("Welche Kalendertür wollen Sie öffnen (oder x für Exit): ")
    if eingabe == "x":
        break  
    print(zufallselement)