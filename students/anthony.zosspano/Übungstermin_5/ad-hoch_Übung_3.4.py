#ad hoc Übung - 3.4)
#Autor: Anthony Zoss
#Datum: 13.03.2022

#Aufgabe a)
def allindex(sequenz, element):
    liste = list(sequenz)
    for position, index in enumerate(liste):
        if element in index:
            print(position, end=" ")
    
allindex("Hallo007", "l")


#Aufgabe b)
liste = [["Zürich", 370000], ["Genf", 190000], ["Basel", 170000], ["Bern", 130000]]

for stadt, einwohner in liste:
    print(stadt,"hat",einwohner,"Einwohner")