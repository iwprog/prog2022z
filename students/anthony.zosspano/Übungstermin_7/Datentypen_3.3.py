#Übungsblatt: Datentypen 3.1)
#Autor: Anthony Zoss
#Datum: 03.04.22

#Dictionary aus Aufgabe 3.1
telefonbuch = [
                {'Vorname':'Ana', 'Nachname':'Skupch', 'Phone':'123'},
                {'Vorname':'Tim', 'Nachname':'Kurz', 'Phone':'732'},
                {'Vorname':'Julia', 'Nachname':'Lang', 'Phone':'912'},
                ]

#Aufgabe a)
for tel in telefonbuch:
    if "1" in tel["Phone"]:
        telefonbuch.remove(tel)