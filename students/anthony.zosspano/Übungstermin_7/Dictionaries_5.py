#Übungsblatt: Dictionaries 5)
#Autor: Anthony Zoss
#Datum: 03.04.22

i = 1
berge = {}
while i != 4:
    print(str(i)+". ", end="")
    berg = input("Berg: ")
    print(str(i)+". ", end="")
    höhe = input("Höhe: ")
    
    berge[berg] = höhe
    i = i + 1
    
berge_sortiert = sorted(berge.items(), key = lambda kv: kv[1])

for name_berg, höhe_berg in berge_sortiert:
    höhe_in_ft = round(int(höhe_berg) * 3.28, 2)
    print(name_berg, "ist", höhe_berg, "m ("+str(höhe_in_ft)+") hoch.")
