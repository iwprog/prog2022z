#Übungsblatt: Datentypen 3.2)
#Autor: Anthony Zoss
#Datum: 03.04.22

#Dictionary aus Aufgabe 3.1
telefonbuch = [
                {'Vorname':'Ana', 'Nachname':'Skupch', 'Phone':123},
                {'Vorname':'Tim', 'Nachname':'Kurz', 'Phone':732},
                {'Vorname':'Julia', 'Nachname':'Lang', 'Phone':912},
                ]

#Aufgabe a)
def datensatz_ausgeben():
    for datensatz in telefonbuch:
        print("Vorname :", datensatz["Vorname"])
        print("Nachname:", datensatz["Nachname"])
        print("Phone   :", datensatz["Phone"])
        print()

datensatz_ausgeben()

