#Übungsblatt: Datentypen 3.1)
#Autor: Anthony Zoss
#Datum: 03.04.22

#Aufgabe a)
personendaten = {'Vorname':'Ana', 'Nachname':'Skupch', 'Phone':123}

#Aufgabe b)
telefonbuch = [personendaten]

#Aufgabe c)
telefonbuch.append({'Vorname':'Tim', 'Nachname':'Kurz', 'Phone':732})
telefonbuch.append({'Vorname':'Julia', 'Nachname':'Lang', 'Phone':912})

#Aufgabe d)
telefonbuch_2 = []
while True:
    vorname = input("Vorname: ")
    if vorname == "x":
        break
    nachname = input("Nachname: ")
    phone = str(input("Phone: "))
    print()
    telefonbuch_2.append({'Vorname':vorname, 'Nachname':nachname, 'Phone':phone})    
print("Datenstruktur:",telefonbuch_2)
