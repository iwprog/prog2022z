#Leistungsaufgabe 1b)
#Autor: Anthony Zoss
#Datum: 27.02.2022
from turtle import *
speed(10) #Geschwindigkeit verschnellern
hideturtle() #Pfeil/Turtle unsichtbar machen
pencolor("white") #Linien mit Hintergrundsfarbe verschwinden lassen

#Angabe Seitenlänge über Eingabefenster
seitenlaenge = numinput("Schweizer Flagge", "Geben Sie die Seitenlänge der Schweizer Flagge an: ")

def viereck(): #Definition Viereck für Flagge und Kreuz
    begin_fill(),
    fd(seitenlaenge),lt(90),
    fd(seitenlaenge),lt(90),
    fd(seitenlaenge),lt(90),
    fd(seitenlaenge),lt(90),
    end_fill()

#Flagge Hintergrund
posF = int(seitenlaenge/2) #Berechnung Position Flagge Hintergrund
goto(-posF, -posF) #Positionierung damit die Flagge zentriert bliebt (posF = Position Flagge)
fillcolor("red")
viereck()

#Flagge Kreuz
seitenlaenge = seitenlaenge*0.2 #neue Seitenlänge für Einzelteile Kreuz gem. Eingabe
posK1 = int(seitenlaenge/2) #Positionierung Kreuz (posK = Position Kreuz)
posK2 = int(seitenlaenge*1.5)
fillcolor("white")

penup(), goto(-posK1,-posK1), pendown() #Neupositionierung damit weiterhin zentriert (penup & -down, damit keine unerwünschte Linien erscheinen)
viereck() #5 Vierecke um einen Krez zu machen
goto(-posK1,-posK2)
viereck()
goto(-posK1,posK1)
viereck()
goto(-posK2,-posK1)
viereck()
goto(posK1,-posK1)
viereck()

exitonclick()
