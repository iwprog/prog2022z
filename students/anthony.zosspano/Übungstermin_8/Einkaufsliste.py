from csv import writer
from json import loads, dump

def hauptmenü ():
    print("===============================================")
    print("Einkaufsliste")
    print("===============================================")
    print()
    print("(1) ... Artikel hinzufügen")
    print("(2) ... Artikel löschen")
    print("(3) ... Artikel suchen") #unvollständig
    print("(4) ... Einkaufsliste leeren")
    print("(5) ... Einkaufsliste im CSV Format exportieren") #unvollständig
    print("(6) ... Gesamtbetrag einkauf berechnen") #unvollständig
    print()
    print("(0) ... Exit")
    print()
artikeln = [["Apfel", "2.50"], ["Birne", "2.95"], ["Himbeere", "3.95"], ["Erdbeere", "5.95"]]
            
def artikelliste():
    print("Verfügbare Artikeln:")
    print()
    for artikel, preis in artikeln:
        print("CHF", preis, "...", artikel)
    print()
    print("Zurück zum Hauptmenü (x)")
    print()


try:
    with open("einkaufsliste.json", encoding="utf8") as f:
        json_string = f.read()
        einkaufsliste = loads(json_string)
except FileNotFoundError:
    einkaufsliste = {}


hauptmenü()
while True:
    option = input("Option: ")
    print()
    if option == "0":
        break
    elif option == "1":
        artikelliste()
        
        hinzufügen = ""
        while hinzufügen != "x":
            hinzufügen = input("Welchen Artikel wollen sie in den Warenkorb legen? ")
            #Äpfel
            if hinzufügen == "Apfel":               
                if "Apfel" in einkaufsliste:
                    menge_2_apfel = input("Menge: ")
                    menge_2_apfel = int(menge_2_apfel) + int(menge_apfel)
                    preis_apfel = preis_apfel * menge_2_apfel
                    einkaufsliste["Apfel"] = [int(preis_apfel), menge_2_apfel]
                    print(menge_2_apfel, "Äpfel im Warenkorb")
                else:
                    menge_apfel = input("Menge: ")
                    preis_apfel = int(menge_apfel) * 2.50
                    einkaufsliste["Apfel"] = [int(preis_apfel), menge_apfel]
                    if menge_apfel == "1":
                        print(menge_apfel, "Apfel im Warenkorb")
                    else:
                        print(menge_apfel, "Äpfel im Warenkorb")
            #Birne
            if hinzufügen == "Birne":               
                if "Birne" in einkaufsliste:
                    menge_2_birne = input("Menge: ")
                    menge_2_birne = int(menge_2_birne) + int(menge_birne)
                    preis_birne = preis_birne * menge_2_birne
                    einkaufsliste["Birne"] = [preis_birne, menge_2_birne]
                    print(menge_2_birne, "Birnen im Warenkorb")
                else:
                    menge_birne = input("Menge: ")
                    preis_birne = int(menge_birne) * 3.95
                    einkaufsliste["Birne"] = [preis_birne, menge_birne]
                    if menge_birne == "1":
                        print(menge_birne, "Birne im Warenkorb")
                    else:
                        print(menge_birne, "Birnen im Warenkorb")
            #Himbeere
            if hinzufügen == "Himbeere":               
                if "Himbeere" in einkaufsliste:
                    menge_2_himbeere = input("Menge: ")
                    menge_2_himbeere = int(menge_2_himbeere) + int(menge_himbeere)
                    preis_himbeere = preis_himbeere * menge_2_himbeere
                    einkaufsliste["Himbeere"] = [preis_himbeere, menge_2_himbeere]
                    print(menge_2_himbeere, "Himbeeren im Warenkorb")
                else:
                    menge_himbeere = input("Menge: ")
                    preis_himbeere = int(menge_himbeere) * 2.95
                    einkaufsliste["Himbeere"] = [preis_himbeere, menge_himbeere]
                    if menge_himbeere == "1":
                        print(menge_himbeere, "Himbeere im Warenkorb")
                    else:
                        print(menge_himbeere, "Himbeeren im Warenkorb")                    
            #Erdbeere
            if hinzufügen == "Erdbeere":               
                if "Erdbeere" in einkaufsliste:
                    menge_2_erdbeere = input("Menge: ")
                    menge_2_erdbeere = int(menge_2_erdbeere) + int(menge_erdbeere)
                    preis_erdbeere = preis_erdbeere * menge_2_erdbeere
                    einkaufsliste["Erdbeere"] = [preis_erdbeere, menge_2_erdbeere]
                    print(menge_2_erdbeere, "Erdbeeren im Warenkorb")
                else:
                    menge_erdbeere = input("Menge: ")
                    preis_erdbeere = int(menge_erdbeere) * 5.95
                    einkaufsliste["Erdbeere"] = [preis_erdbeere, menge_erdbeere]
                    if menge_erdbeere == "1":
                        print(menge_erdbeere, "Erdbeere im Warenkorb")
                    else:
                        print(menge_erdbeere, "Erdbeeren im Warenkorb")                 
            #Zurück ins Hauptmenü
            if hinzufügen == "x":
                print()
                hauptmenü()
    
    elif option == "2":
        löschen = input("Welchen Artikel wollen Sie endgültig aus der Einkaufsliste löschen? ")
        if löschen == "Apfel":
            del einkaufsliste["Apfel"]
        if löschen == "Birne":
            del einkaufsliste["Birne"]
        if löschen == "Himbeere":
            del einkaufsliste["Himbeere"]
        if löschen == "Erdbeere":
            del einkaufsliste["Erdbeere"]
    
#     elif option == "3":
#         suche = input("Nach was wollen Sie suchen? ")

            
    elif option == "4":
        bestätigung = input("Wollen Sie Ihre Einkaufsliste leeren? (ja/nein) ")
        if bestätigung == "ja":
            einkaufsliste = {}
            print("Einkaufsliste gelöscht")
        if bestätigung == "nein":
            print("Zurück ins Hauptmenü")
    
    
#     elif option == "5":
#         with open("einkaufsliste.csv", "w", encoding="utf8") as f:
#             csv_writer = writer(f, delimiter=";")
#             for artiekl, preis, menge in einkaufsliste.values():
#                 
            
            
#     elif option == "6":
#         einkaufsliste_2 = einkaufsliste.values()
#         einkaufsliste_2 = list(einkaufsliste_2)
#         for preis, menge in einkaufsliste_2:
#             summe = sum(preis)
#             print(preis)


with open("einkaufsliste.json", "w", encoding="utf8") as f:
    json_string = dumps(einkaufsliste)
    f.write(json_string)
