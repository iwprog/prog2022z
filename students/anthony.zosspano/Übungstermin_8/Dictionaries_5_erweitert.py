#Übungsblatt: Dictionaries 5)
#Autor: Anthony Zoss
#Datum: 03.04.22
from json import loads, dumps

try:
    with open("berge.json", encoding="utf8") as f:
        json_string = f.read()
        berge = loads(json_string)
except FileNotFoundError:
    berge = {}

print(berge)

i = 1
while True:
    print(str(i)+". ", end="")
    berg = input("Berg: ")
    if berg == "x":
        break   
    if berg in berge:
        print("Berg wurde bereits eigegeben")
    if berg not in berge:
        print(str(i)+". ", end="")
        höhe = input("Höhe: ")
        berge[berg] = höhe
        i = i + 1

for berg, höhe in sorted(berge.items()):
    höhe_in_ft = round(int(höhe) * 3.28, 2)    
    print(berg, "ist", höhe, "m ("+str(höhe_in_ft)+") hoch.")
    
with open("berge.json", "w", encoding="utf8") as f:
    json_string = dumps(berge)
    f.write(json_string)
