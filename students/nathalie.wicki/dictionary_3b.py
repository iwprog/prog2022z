icao_alphabet = {"A": "Alfa", "B": "Bravo", "C": "Charlie", "D": "Delta", "E": "Echo", "F": "Foxtrot", "G": "Golf", "H": "Hotel", "I": "India", "J": "Juliett", "K": "Kilo", "L": "Lima", "M": "Mike", "N": "November", "O": "Oscar", "P": "Papa", "Q": "Quebex", "R": "Romeo", "S": "Sierra", "T": "Tango", "U": "Uniform", "V": "Victor", "W": "Whiskey", "X": "X-ray", "Y": "Yankee", "Z": "Zulu", }
wort = input("Bitte geben Sie ein Wort ein. ")

def icao(inputwort):
    liste = []
    for buchstabe in wort:
        buchstabe = buchstabe.upper()
        if buchstabe == 'Ü' or buchstabe == 'Ä' or buchstabe == 'Ö':
            buchstabe_richtig = buchstabe.replace("Ä", "AE")
            buchstabe_richtig = buchstabe.replace("Ö", "OE")
            buchstabe_richtig = buchstabe.replace("Ü", "UE")
            liste.append(icao_alphabet[buchstabe_richtig])
        else:
            liste.append(icao_alphabet[buchstabe])
    
    for element in liste:
            print(element, end = "-")
    

#funktioniert nicht