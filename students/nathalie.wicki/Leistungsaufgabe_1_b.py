from turtle import *
speed(10)
hideturtle()

laenge = numinput("Abfragedialog", "Bitte geben Sie die Seitenlänge an", int())

#quadrat
pencolor("red")
fillcolor("red")
begin_fill()
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
end_fill()

#ausrichten
penup()
right(45)
forward(laenge/1.75)



#kreuz

pendown()
pencolor("white")
fillcolor("white")
begin_fill()
left(135)
forward(laenge/6)
right(90)
forward(laenge/6)

right(90)
forward(laenge/6)

left(90)
forward(laenge/6)

right(90)
forward(laenge/6)

right(90)
forward(laenge/6)

left(90)
forward(laenge/6)

right(90)
forward(laenge/6)

right(90)
forward(laenge/6)

left(90)
forward(laenge/6)

right(90)
forward(laenge/6)

right(90)
forward(laenge/6)

end_fill()