#a
#Erstellen Sie ein Dictionary für den folgenden Datensatz:
eintrag = {'Vorname': 'Ana', 'Nachname': 'Skupch', 'Phone': 123}

#b
telefonbuch = []

telefonbuch.append(eintrag)
print(telefonbuch)

#c

telefonbuch.append({'Vorname': 'Tim', 'Nachname': 'Kurz', 'Phone': 732})
telefonbuch.append({'Vorname': 'Julia', 'Nachname': 'Lang', 'Phone': 912})
print(telefonbuch)

#d
#Schreiben Sie ein Programm, welches Personendaten abfragt und diese so
#lange in Ihr Telefonbuch einfügt, bis der Benutzer x eingibt. Im Anschluss
#soll die erstellte Datenstruktur ausgegeben werden.

telefonbuch2 = []
eingabe = ''

while True:
    vorname = input('Geben Sie einen Vornamen ein (x zum Abbrechen). ')
    if vorname == 'x':
        print ('Datenstruktur:', telefonbuch2)
        break
    nachname = input('Geben Sie einen Nachnamen ein. ')
    telefonnummer = input('Geben Sie eine Telefonnummer ein ')
    telefonbuch2.append({'Vorname': vorname, 'Nachname': nachname, 'Phone': telefonnummer})
    

    