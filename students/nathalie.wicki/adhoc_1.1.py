#1.1a

print ("Name", "Vorname", "Strasse", "Hausnr.", "PLZ", "Wohnort")

#1.1b
print (1+2+3+4+5)
print (1-2-3-4-5)
print (1*2*3*4*5)
print (1/2/3/4/5)

#1.1c

f = "fliegen "
print("wenn", f+ "hinter", 5*f+ "hinterher")

print ("wenn", "fliegen", "hinter", "fliegen " * 5 + "hinterher")

#1.1d

print ("Hello" [4])
print ("Hello" [3])
print ("Hello" [2])
print ("Hello" [1])
print ("Hello" [0])

#Alternative (Wollen wir nun mehr als nur ein Zeichen auslesen, können wir das über das "bis"-Zeichen. In Python wird dazu der Doppelpunkt verwendet.)
print ("Hello" [::-1])



