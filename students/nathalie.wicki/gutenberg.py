#öffnen sie die folgende url (https://www.gutenberg.org/cache/epub/6079/pg6079.txt)
#in ihrem web browser und speichern sie die zugehörige textdatei in ihrem python verzeichnis. schreiben sie nun ein python programm, das 
   # - diese auf dem bildschirm ausgibt und 
    #- ermittelt wie oft die worte "Himmel", "Freiheit", "Spiel" und "Tanz" in der datei vorkommen.
   # (hinweis: es ist wichtig, dass sich ihr python programm und die textdatei im selben verzeichnis befinden, da ansonsten python die datei nicht öffnen kann).
 #   - erweitern Sie Ihr Python Programm, sodass es die Datei direkt aus dem Web bezieht, sodass das manuelle Speichern der Datei entfallen kann.
 
liste = []
with open('gutenberg.txt', 'r', encoding='utf8') as textdatei:
    textdateiausgabe = textdatei.read()
    
liste.append(textdateiausgabe.split())
print(liste)
himmel_zahl = liste.count('Himmel')
freiheit_zahl = liste.count('Freiheit')
spiel_zahl = liste.count('Spiel')
tanz_zahl = liste.count('Tanz')
print('Himmel:', himmel_zahl, 'Freiheit:', freiheit_zahl, 'Spiel:', spiel_zahl, 'Tanz:', tanz_zahl)

#nicht geklappt

 #direkt aus Web
from urllib.request import urlopen
from inscriptis import get_text

with urlopen('https://www.gutenberg.org/cache/epub/6079/pg6079.txt') as source:
    web_content = source.read().decode('utf8')
    text_content = get_text(web_content)
    print(text_content)
    