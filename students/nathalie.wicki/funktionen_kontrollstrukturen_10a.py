def programmübersicht():
    print('=========================================')
    print("Programmübersicht")
    print("1 ... Preis für eine Fahrkarte berechnen")
    print("2 ... Herzfrequenz")
    print("0 ... Programm beenden")
    print('=========================================')



km = 0.25
grundpreis = 2
def get_billet_preis():
    alter = int(input("Geben Sie ihr Alter an!: "))
    entfernung = int(input("Wie weit wollen Sie reisen?"))
    if alter <6:
        return "0"
    elif alter >5 and alter <16:
        preis = grundpreis + (entfernung * km)
        preis = preis / 2
        return preis
    else:
        preis = grundpreis + (entfernung * km)
        return preis
    



def berechne_herzfrequenz():
    alter = int(input("Geben Sie ihr Alter an: "))
    max_Herzfrequenz = 220 - alter
    return max_Herzfrequenz

while True:
    programmübersicht()
    eingabe = input("Gewählte Option: ")
    print("")
    if eingabe == "1":
        print()
        print("Fahrkartenpreis berechnen")
        print("--------------------------------------")
        print("Die Fahrkarte kostet:", get_billet_preis(),"CHF")
        print()
    elif eingabe == "2":
        print()
        print("Maximale Herfrequenz berechenen")
        print("--------------------------------------")
        print("Ihre Maximale Herzfrequenz beträgt:",berechne_herzfrequenz())
        print()
    
    elif eingabe == "0":
        break


