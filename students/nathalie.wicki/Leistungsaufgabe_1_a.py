from turtle import *
speed(10)
hideturtle()
#quadrat
pencolor("red")
fillcolor("red")
begin_fill()
forward(300)
right(90)
forward(300)
right(90)
forward(300)
right(90)
forward(300)
right(90)
end_fill()

#ausrichten
penup()
right(45)
forward(175)

#kreuz
pendown()
pencolor("white")
fillcolor("white")
begin_fill()
left(135)
forward(50)
right(90)
forward(50)

right(90)
forward(50)

left(90)
forward(50)

right(90)
forward(50)

right(90)
forward(50)

left(90)
forward(50)

right(90)
forward(50)

right(90)
forward(50)

left(90)
forward(50)

right(90)
forward(50)

right(90)
forward(50)

end_fill()