#Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis
#deren Summe grosser als 10 ist. Im Anschluss soll die Summe ausgegeben
#werden.
#ohne Nummerierung
summe = 0

while summe < 10:
    zahl = input("Bitte geben Sie eine Zahl ein: ")
    summe = summe + int(zahl)
print(summe)

#mit Nummerierung (nicht geschafft)
summe = 0
eingabe = "0"
counter = 1


while summe <10:
    zahl = int(eingabe)
    summe = summe + zahl
    #for i in range
    eingabe = input("Bitte geben Sie Zahl", counter, "ein")
    
    counter = counter + 1
print(summe)

