#Schreiben Sie ein Programm, welches fur drei Berge deren Hohe
#in m entgegennimmt und diese anschliessend alphabetisch sortiert mit
#der Hohe in m und in Feet (ft; 1m =3.28 ft) ausgibt.

counter = 1
berge = {}
while counter <=3:
    eingabe_berg = input('Geben Sie einen Berg ein ')
    eingabe_hoehe = float(input('Geben Sie die Höhe des Berges ein (in m)'))
    hoehe_feet = eingabe_hoehe * 3.28
    berge[eingabe_berg] = eingabe_hoehe
    counter = counter + 1
if counter >3:
    nr = 1
    for berg, hoehe in sorted(berge.items()):
        print(str(nr) +'.', berg, 'ist', hoehe, '('+ str(hoehe_feet)+')', 'hoch.')
        nr = nr + 1
        
