from turtle import *
speed(10)
left(150)
pencolor("red")
pensize(5)

#eingabefenster
laenge = numinput("Dreieck", "Bitte geben Sie die Länge des Dreiecks an")

#gelbesdreieck
fillcolor("yellow")
begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

#laenge anpassen
laenge = laenge * 0.75

#cyandreieck
fillcolor("cyan")
begin_fill()
right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

# laenge anpassen
laenge = laenge * 0.75

#grünesdreieck
fillcolor("green")
begin_fill()
right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

