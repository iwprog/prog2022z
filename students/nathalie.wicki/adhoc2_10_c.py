from turtle import *
def dreieck_mit_abstand():
    pencolor("blue")
    fillcolor("blue")
    begin_fill()
    forward(50)
    left(120)
    forward(50)
    left(120)
    forward(50)
    left(120)
    end_fill()
    penup()
    forward(100)
    pendown()



i = 0
anzahl_reihen = input("Wie viele Reihen mit Dreiecken sollen gezeichnet werden? ")
anzahl_dreiecke = input("Wie viele Dreiecke sollen pro Reihe gezeichnet werden? ")

for anzahl in range(int(anzahl_reihen)):
    while i < int(anzahl_dreiecke):
        dreieck_mit_abstand()
        i = i + 1
    i = 0
    penup()
    back(int(anzahl_dreiecke) * 70)
    right(90)
    forward(70)
    left(90)
    pendown()
