#Erweitern Sie das vorhergehende Programm, sodass dieses zusatzlich
#zu der Hohe noch das Gebirge entgegennimmt, und dieses auch anschliessend
#ausgibt. Ausserdem sollen die Berge { je nach Wunsch
#des Anwenders { auf- oder absteigend sortiert nach (i) dem Name des
#Berges, (ii) der Hohe oder, (iii) dem Gebirge ausgegeben werden.

counter = 1
berge = {}
while counter <=3:
    eingabe_berg = input('Geben Sie einen Berg ein ')
    eingabe_hoehe = float(input('Geben Sie die Höhe des Berges ein (in m)'))
    eingabe_gebirge = input('Geben Sie das Gebirge ein ')
    hoehe_feet = eingabe_hoehe * 3.28
    berge[eingabe_berg] = eingabe_hoehe
    counter = counter + 1
    
    
if counter >3:
    eingabe_sortieren = input('Sortieren Sie nach (1) Berg, 2(Höhe) oder (3) Gebirge: ')
    if eingabe_sortieren == '1':
        auf_absteigend_berg = input('(i)Sortieren aufsteigend oder (ii) sortieren absteigend')
        if auf_absteigend_berg == 'i':
            print('aufsteigend') #wusste nicht wie umsetzen
        else:
            print('absteigend')
    elif eingabe_sortieren == '2':
        auf_absteigend_hoehe = input('(i)Sortieren aufsteigend oder (ii) sortieren absteigend')
        if auf_absteigend_hoehe == 'i':
            print('aufsteigend')
        else:
            print('absteigend')
                
    elif eingabe_sortieren == '3':
        auf_absteigend_gebirge = input('(i)Sortieren aufsteigend oder (ii) sortieren absteigend')
        if auf_absteigend_gebirge == 'i':
            print('aufsteigend')
        else:
            print('absteigend')
    
    
    nr = 1
    for berg, hoehe in sorted(berge.items()):
        print(str(nr)+'.', 'Berg:', berg)
        print(str(nr)+'.', 'Höhe:', eingabe_hoehe)
        print(str(nr)+'.', 'Gebirge:', eingabe_gebirge)
        nr = nr + 1