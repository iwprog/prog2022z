#entwerfen Algorithmus, aus zahlenreihe(12, 23, 5, 56, 77, 18, 9)
#minimun und das maximum herausfindet und durchschnitt berechnet

numbers = [12, 23, 5, 56, 77, 18, 9]
min_value = None
max_value = None

for number in numbers:
    if min_value is None or number < min_value:
        min_value = number
    if max_value is None or number > max_value:
        max_value = number
    summe = sum(numbers)
print("Minimalwert: ", min_value)
print("Maximalwert: ", max_value)
print("Durchschnitt: ", summe/len(numbers))
