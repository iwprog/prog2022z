liste = ['Hi', 'Heute', "ist", 'ein', 'schöner', 'Tag', 'xo']

def liste_kuerzen():
    for wort in liste:
        if len(wort) < 3:
            liste.remove(wort)
    print(liste)
liste_kuerzen()

#variante 
liste = ['Hi', 'Heute', "ist", 'ein', 'schöner', 'Tag', 'xo']
liste_1 = []

def liste_kuerzen1():
    for wort in liste:
        if len(wort) >= 3:
            liste_1.append(wort)
    print(liste_1)
liste_kuerzen1()