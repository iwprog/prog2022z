counter = 1
eingabe_preis = input("Geben Sie den " + str(counter) + ". Preis ein: ")
summe = 0
while eingabe_preis != 'x':
    summe = int(summe) + int(eingabe_preis)
    counter = counter + 1
    eingabe_preis = input("Geben Sie den " + str(counter) + ". Preis ein: ")
    
if summe <100:
    print("Kein Rabatt, Gesamtpreis =" + str(summe), "CHF")
elif summe >=100 and summe <1000:
    summe = int(summe) * 0.95
    print("5% Rabatt, Gesamtpreis =" + str(summe), "CHF")
elif summe >1000:
    summe = int(summe) * 0.90
    print("10% Rabatt, Gesamtpreis =" + str(summe), "CHF")
