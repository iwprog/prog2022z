from turtle import *
def dreieck_mit_abstand():
    pencolor("blue")
    fillcolor("blue")
    begin_fill()
    forward(50)
    left(120)
    forward(50)
    left(120)
    forward(50)
    left(120)
    end_fill()
    penup()
    forward(100)
    pendown()

i=0
anzahl = input("Wie viele Dreiecke sollen gezeichnet werden? ")

while i < int(anzahl):
    dreieck_mit_abstand()
    i = i + 1
