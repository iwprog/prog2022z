from turtle import *


def zeichne_rahmen(laenge, dicke, stiftfarbe, fuellfarbe):
    pencolor(stiftfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    for seiten in (1, 3):
        forward(laenge)
        left(90)
        forward(laenge)
        left(90)
    end_fill
    penup()
    forward(dicke*1.5)
    left(90)
    forward(dicke*1.5)
    right(90)
    pendown()
    for seiten in (1,3):
        forward(dicke)
        left(90)
        forward(dicke)
        left(90)

zeichne_rahmen(100, 25, "black", "white")
