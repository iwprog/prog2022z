#Adventskalender: Schreiben Sie eine Funktion, welche eine Liste mit 24
#Eintragen erstellt, welche zufallig eine der folgenden Uberraschungen
#enthalt: Samiklaus, Christbaum, Weihnachtskugel, Stiefel, Schneeball.
#Der Benutzer soll im Anschluss beliebige Turen onen konnen und das
#Programm soll ausgeben, welche Uberraschung sich hinter dieser versteckt.
from random import choice

liste = list(range(1, 25))
überraschung = ['Samichlaus', 'Christbaum', 'Weihnachtskugel', 'Stiefel', 'Schneeball']

def adventskalender():
    while True:
        wahl = input('Welche Kalendertür wollen Sie öffnen (oder x für exit)? ')
        zufallselement = choice(überraschung)
        
        if int(wahl) in range(1, 25):
            print(zufallselement)
            
        elif wahl == 'x':
            break

        else:
            print('Eingabe ungültig')
    
adventskalender()