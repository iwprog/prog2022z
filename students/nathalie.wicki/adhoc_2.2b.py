#drei dreiecke mit unterschiedlichen Längen, mittels Eingabe eingeben
from turtle import *
#ausrichtung
right(90)
#grundeinstellungen
pencolor("red")
pensize(5)
speed(10)

#seitenlänge definieren
seitenlaenge = input("Geben Sie die Seitenlänge an")
seitenlaenge = int(seitenlaenge)
#funktion Dreieck / blaues Dreieck
def dreieck(seitenlaenge):
    begin_fill()
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    end_fill()
fillcolor("cyan") 
dreieck(seitenlaenge)

#gelbes Dreieck
seitenlaenge = input("Geben Sie die Seitenlänge an")
seitenlaenge = int(seitenlaenge)
fillcolor("yellow")
dreieck(seitenlaenge)

#grünes Dreieck
seitenlaenge = input("Geben Sie die Seitenlänge an")
seitenlaenge = int(seitenlaenge)
fillcolor("lime")
dreieck(seitenlaenge)