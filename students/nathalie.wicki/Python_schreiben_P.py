from turtle import *
speed(10)
penup()
fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

#ausrichten

forward(100)

#gerader Strich
pendown()
pensize(5)
pencolor("cyan")
left(90)
forward(200)

#Rundung
pensize(5)
pencolor("lime")
right(90)
forward(50)
circle(100, 180)
forward(50)

#strich nach unten
pensize(5)
pencolor("cyan")
left(90)
forward(200)

penup()
forward(200)
left(90)
forward(200)
