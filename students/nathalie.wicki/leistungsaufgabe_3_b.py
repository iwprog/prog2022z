dictionary = {'Haus': 'house', 'Maus': 'mouse', 'Baum': 'tree', 'Blume': 'flower', 'Schnee': 'snow',
              'Zahn': 'dent', 'Zahnarzt': 'dentist','Wasser': 'water', 'Bett': 'bed', 'Küche': 'kitchen'}

counter = 0

for deutsches_wort, englisches_wort in dictionary.items():
    loesung = input("Was heisst " + deutsches_wort + ": ")
    if loesung == englisches_wort:
        print("RICHTIG!")
        counter = counter + 1
    else:
        print("FALSCH!")

print("Sie haben", counter, "von 10 Vokabeln richtig beantwortet!")
