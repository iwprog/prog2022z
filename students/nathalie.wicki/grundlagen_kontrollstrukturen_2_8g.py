#Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der
#Benutzer -1 eingibt und anschliessend die Summe dieser Zahlen ausgibt.
summe = 0
zahl = input("Geben Sie eine Zahl ein: ")

while zahl != "-1":
    summe = summe + int(zahl)
    zahl = input("Geben Sie eine Zahl ein: ")

print(summe)