from urllib.request import urlopen
from inscriptis import get_text
from csv import writer

with urlopen('https://www.ietf.org/rfc/rfc2616.txt') as source:
    web_content = source.read().decode('utf8')
    text_content = get_text(web_content)

wordcount = {}
lettercount = {}
for word in text_content.split():
    if word in wordcount:
        wordcount[word] = wordcount[word] + 1
    else:
        wordcount[word] = 1
    
for letter in text_content:
    if letter in lettercount:
        lettercount[letter] = lettercount[letter] + 1
    else:
        lettercount[letter] = 1
        
with open("word.csv", "w", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    for word, count in wordcount.items():
        csv_writer.writerow([word, count])

with open("letter.csv", "w", encoding="utf8") as file:
    csv_writer = writer(file, delimiter=";")
    for letter, count in lettercount.items():
        csv_writer.writerow([letter, count])
