#b) Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach
#Alter den Wert volljäahrig oder minderjährig zuruckgibt.
#Erweitern Sie die Funktion, sodass diese zwischen geschaftsunfahig (bis
#inklusive 6 Jahre), unmundig (bis inklusive 14 Jahre), mundig minderjahrig
#und volljahrig unterscheidet. Weiters soll fur ein negatives Alter ungeboren
#zuruckgegeben werden.

age = int(input("Wie alt sind Sie?"))
  
def legal_status():
    if age >= 22:
        return("volljährig")
    elif age >=0 and age <= 6:
        return("geschäftsunfähig")
    elif age > 6 and age <= 14:
        return("unmündig")
    elif age > 14 and age <= 21:
        return("mündig minderjährig")
    elif age <= -1:
        return("ungeboren")
    

print("Mit", age, "ist man", legal_status() + ".")

