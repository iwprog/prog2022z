#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: ügk-2.5.b
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
age = int(input('Bitte Alter eingeben.'))

def legal_status(age):
    if age < 7:
        return 'geschäftsunfähig'
    elif age < 15:
        return 'unmündig'
    elif age < 18:
        return 'mündig minderjährig'  
    else:
        return 'volljährig'

print('Mit', age, 'ist man', legal_status(age) + ".")
