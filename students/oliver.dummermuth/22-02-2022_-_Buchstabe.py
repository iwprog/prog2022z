#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: Buchstabe > T
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *
speed = 10
pencolor('black')
fillcolor('lime')
begin_fill()
fd(300)
lt(90)
fd(400)
lt(90)
fd(300)
lt(90)
fd(400)
lt(90)
end_fill()


pencolor('red')
fillcolor('orange')
begin_fill()
penup()
fd(120)
pendown()
fd(60)
lt(90)
fd(340)
rt(90)
fd(120)
lt(90)
fd(60)
lt(90)
fd(300)
lt(90)
fd(60)
lt(90)
fd(120)
rt(90)
fd(340)
end_fill()

penup()
left(90)
fd(180)


