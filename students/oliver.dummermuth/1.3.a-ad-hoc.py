#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.3.a
#Aufgabe: siehe 2_Python_Introduction.pdf, Folie 20


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

pensize(5)
pencolor('red')

fillcolor('cyan')
begin_fill()
right(90)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

fillcolor('yellow')
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

fillcolor('green')
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()
