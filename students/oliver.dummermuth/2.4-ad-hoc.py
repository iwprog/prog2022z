#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.4
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 12


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import numinput, textinput

name = textinput('Eingabefenster', 'Bitte Namen eingeben:')
note1 = numinput('Eingabefenster', 'Bitte Note 1 eingeben')
note2 = numinput('Eingabefenster', 'Bitte Note 2 eingeben')
note3 = numinput('Eingabefenster', 'Bitte Note 3 eingeben')
note4 = numinput('Eingabefenster', 'Bitte Note 4 eingeben')
note5 = numinput('Eingabefenster', 'Bitte Note 5 eingeben')
note6 = numinput('Eingabefenster', 'Bitte Note 6 eingeben')

durchschnitt = 0

def notendurchschnitt(name, note1, note2, note3, note4, note5, note6):
    durchschnitt = round(float((note1 + note1 + note3 + note4 + note5 + note6) / 6), 2)
    return name + ', der Notendurchschnitt beträgt ' + str(durchschnitt) + "."

     

print(notendurchschnitt(name, note1, note2, note3, note4, note5, note6))
