#Datum: 05.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.3, a
#Aufgabe: siehe basic-loops.pdf, Seite 2


#AB DER NÄCHSTEN ZEILE FOLGT CODE
name = input('Bitte Namen eingeben.')

def guten_morgen(name):    
    return 'Guten Morgen ' + name + '!'

gruss = guten_morgen(name) 
print(gruss)
