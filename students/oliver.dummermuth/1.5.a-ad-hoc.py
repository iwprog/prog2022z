#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.5.a
#Aufgabe: siehe 2_Python_Introduction.pdf, Folie 28


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

laenge = numinput('Eingabefenster', 'Bitte Seitenlänge angeben:')

pensize(5)
pencolor('red')

begin_fill()
fillcolor('cyan')
right(90)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

begin_fill()
fillcolor('yellow')
forward(laenge*2)
left(120)
forward(laenge*2)
left(120)
forward(laenge*2)
end_fill()

begin_fill()
fillcolor('lime')
forward(laenge/2)
left(120)
forward(laenge/2)
left(120)
forward(laenge/2)
end_fill()



