#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.2.c
#Aufgabe: siehe 2_Python_Introduction.pdf, Folie 18


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

pensize(5)

pencolor('blue')
left(45)
forward(100)

pencolor('red')
right(90)
forward(100)

pencolor('cyan')
left(90)
forward(100)

pencolor('black')
right(90)
forward(100)





