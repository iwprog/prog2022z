#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: ügk-6
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *


laenge = int(input('Länge:'))
breite = int(input('Breite:'))
rahmenfarbe = input('Rahmenfarbe:')
fuellfarbe = input('Füllfarbe:')

def zeichne_rahmen(laenge, breite, rahmenfarbe, fuellfarbe):
    pencolor(rahmenfarbe)
    fillcolor(fuellfarbe)
    pensize(breite)
    begin_fill()
    
    for i in range(4):
        fd(laenge)
        rt(90)
           

zeichne_rahmen(laenge, breite, rahmenfarbe, fuellfarbe)
 

 
