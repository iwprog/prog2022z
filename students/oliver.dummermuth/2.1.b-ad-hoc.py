#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.1, b
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 3


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

def viereck(farbe1, farbe2, laenge):
    pencolor(farbe1)
    begin_fill()
    fillcolor(farbe2)
    forward(laenge)
    left(90)
    forward(laenge)
    left(90)
    forward(laenge)
    left(90)
    forward(laenge)
    end_fill()
    left(15)
pensize(5)

right(45)
viereck('red', 'cyan', 100)
viereck('red', 'yellow', 100)
viereck('red', 'magenta', 100)
viereck('red', 'blue', 100)
viereck('red', 'lime', 100)
