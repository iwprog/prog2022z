#Datum: 21.03.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 3.7
#Erstellen Sie ein Dictionary mit einigen Schweizer Städten und deren
#Einwohnerzahlen (wie im Bsp. auf den Folien zuvor)
#a. Schreiben Sie ein Programm, dass eine Ausgabe nach
#folgendem Aufbau erzeugt:
#Zürich hat 370000 Einwohner
#Genf hat 190000 Einwohner
#usw.
#b. Ändern Sie das Programm so, dass die Liste sortiert nach keys
#ausgegeben wird. Heisser Tipp: list hat ja eine Methode sort()
#c. Überlegen Sie, wie man die Ausgabe sortiert nach Einwohnerzahlen
#programmieren könnte.


#AB DER NÄCHSTEN ZEILE FOLGT CODE
staedte = {"Zürich": 370000, "Genf": 190000, "Winterthur": 115000, "Solothurn": 16000}

#1:
for stadt in staedte:
    print(stadt, "hat", staedte[stadt], "Einwohner")

#2:
#for stadt, einwohnerzahl in staedte.items():
    #print(stadt, "hat", einwohnerzahl, "Einwohner")