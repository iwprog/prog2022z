#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.5
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 23


#AB DER NÄCHSTEN ZEILE FOLGT CODE
i = 1
i = int(input('Bitte Zahl zwischen 1 und 12 eingeben.'))

if i <= 12:
    if i == 1:
        print('Januar')
    elif i == 2:
        print('Februar')
    elif i == 3:
        print('März')
    elif i == 4:
        print('April')
    elif i == 5:
        print('Mai')
    elif i == 6:
        print('Juni')
    elif i == 7:
        print('Juli')
    elif i == 8:
        print('August')
    elif i == 9:
        print('September')
    elif i == 10:
        print('Oktober')
    elif i == 11:
        print('November')
    elif i == 12:
        print('Dezember')    
    
else:
    print('Bitte Zahl zwischen 1 und 12 eingeben.')

