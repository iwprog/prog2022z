#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: ügk-11.g
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
print('# ' * 7)
for i in range(5, 0, -1):
    print(i * '  ' + '#')
print('# ' * 7)
