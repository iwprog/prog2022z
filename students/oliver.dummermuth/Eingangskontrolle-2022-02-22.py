#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: Eingangskontrolle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import numinput, textinput


#Variante 1:
alter = numinput('Eingabefenster', 'Bitte Alter eingeben:')

if alter < 18:
    print('Eingang verweigert.')
else:
    print('Eingang erlaubt.')

#Variante 2:
jahrgang = numinput('Eingabefenster', 'Bitte Jahrgang eingeben:')
alter = 2022 - jahrgang

if alter < 18:
    print('Eingang verweigert.')
else:
    print('Eingang erlaubt.')


     

