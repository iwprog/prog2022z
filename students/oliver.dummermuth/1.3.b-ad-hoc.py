#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.3.b
#Aufgabe: siehe 2_Python_Introduction.pdf, Folie 20


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

pensize(5)
pencolor('red')

fillcolor('cyan')
right(45)

begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

fillcolor('yellow')
left(15)

begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

fillcolor('magenta')
left(15)

begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

fillcolor('blue')
left(15)

begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()
 
fillcolor('green')
left(15)

begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()
