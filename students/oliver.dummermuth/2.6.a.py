#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.6 a
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 33


#AB DER NÄCHSTEN ZEILE FOLGT CODE
i = 1

for zahl in range(100, 79, -2):    
    print(str(i) + '. Wert:' + str(zahl))
    
    i = i + 1
    
