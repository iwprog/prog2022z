#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.8
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 40


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

laenge = numinput('Eingabefenster', 'Bitte Seitenlänge eingeben:')
anzahl = numinput('Eingabefenster', 'Bitte Anzahl Dreiecke pro Reihe eingeben:')
reihen = numinput('Eingabefenster', 'Bitte Anzahl Reihen eingeben:')

pensize(1)
pencolor('black')


def dreieck():
    begin_fill()
    fillcolor('lime')
    left(180)
    forward(laenge)
    right(120)
    forward(laenge)
    right(120)
    forward(laenge)
    end_fill()

    left(60)

    penup()
    forward(laenge + laenge/2)
    pendown()

def zeilenumbruch():
    penup()
    left(180)
    forward(anzahl*(laenge + 1) + anzahl*(laenge - 1)/2)
    left(90)
    forward(laenge + laenge/2)
    left(90)
    pendown()

for count in range(int(reihen)):
    for count2 in range(int(anzahl)):
        dreieck()

    zeilenumbruch()


