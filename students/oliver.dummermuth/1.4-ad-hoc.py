#Datum: 14.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 1.4
#Aufgabe: siehe 2_Python_Introduction.pdf, Folie 22


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

#Kreis
circle(100)

#Dreieck :)
circle(100, 360, 3)

#Viereck
circle(100, 360, 4)
