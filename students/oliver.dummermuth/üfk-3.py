#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: ügk-3
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
grundpreis = 2
kilometerpreis = 0.25
alter = int(input('Bitte Alter eingeben.'))
strecke = int(input('Bitte Entfernung eingeben.'))
preis = 0

def billet_preis(alter, strecke):
    preis = grundpreis + kilometerpreis * strecke

    if alter < 6:
        preis = 0
    elif alter < 16:
        preis = preis/2
    return int(preis)
    

print(billet_preis(alter, strecke))
 
