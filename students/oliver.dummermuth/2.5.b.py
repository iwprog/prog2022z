#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.5, b
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 27


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

def dreieck():
    begin_fill()
    forward(50)
    right(120)
    forward(50)
    right(120)
    forward(50)
    right(120)
    end_fill()
    penup()
    forward(100)
    pendown()
    
i = 1

anzahl = int(input('Bitte Anzahl eingeben.'))


while i <= anzahl:
    dreieck()
    i = i + 1
