#Datum: 27.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.4, a
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 13


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from math import pi
from turtle import numinput

radius = numinput('Radius', 'Bitte Radius eingeben.')
hoehe = numinput('Höhe', 'Bitte Höhe eingeben.')

def volumen(radius, hoehe):
    volumen = pi * radius * radius * hoehe
    volumen = int(volumen)
    return volumen
     
print('Das Volumen beträgt', str(volumen(radius, hoehe)) + ' Kubikzentimeter.')
