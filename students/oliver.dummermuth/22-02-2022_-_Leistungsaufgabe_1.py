#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: Leistungsaufgabe 1
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

laenge = int(input('Länge in Pixel'))
speed = 10
pencolor('red')
fillcolor('red')

begin_fill()
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
end_fill()

penup()
forward(laenge/5*2)
left(90)
forward(laenge/5)
pendown()

pencolor('white')
fillcolor('white')

begin_fill()
right(90)
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)
forward(laenge/5)
left(90)
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)
forward(laenge/5)
left(90)
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)
forward(laenge/5)
left(90)
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)
forward(laenge/5)
end_fill()

hideturtle()
