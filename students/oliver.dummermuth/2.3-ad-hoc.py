#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.3
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 9


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

def jump(strecke, winkel):
    begin_fill()
    penup()
    left(winkel)
    forward(strecke)
    pendown()
    right(winkel)
    end_fill()

strecke = int(input('Strecke'))
winkel = int(input('Winkel'))

jump(strecke, winkel)
