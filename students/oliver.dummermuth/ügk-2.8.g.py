#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: ügk-2.8.g
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
i = 1
zahl = 0
summe = 0

while zahl != -1:   
    text = 'Bitte geben Sie Zahl ' + str(i) + ' ein:'
    zahl = int(input(text))

    if zahl != -1:   
        summe = summe + zahl
        i = i + 1

print('Summe:', summe)
 
