#Datum: 28.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: ügk-2.8.f
#Aufgabe: siehe Moodle


#AB DER NÄCHSTEN ZEILE FOLGT CODE
i = 1
summe = 0

while summe < 11:   
    text = 'Bitte geben Sie Zahl ' + str(i) + ' ein:'
    zahl = int(input(text))
       
    summe = summe + zahl
    i = i + 1

print('Summe:', summe)
 
