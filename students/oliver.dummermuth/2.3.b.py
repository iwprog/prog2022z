#Datum: 05.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.3, b
#Aufgabe: siehe basic-loops.pdf, Seite 2


#AB DER NÄCHSTEN ZEILE FOLGT CODE
laenge = input('Bitte Länge des Rechtecks eingeben.')
breite = input('Bitte Breite des Rechtecks eingeben.')

def flaeche_rechteck(laenge, breite):
    flaeche = int(laenge)*int(breite)
    return flaeche

flaeche = flaeche_rechteck(laenge, breite)
print('Die Fläche beträgt', flaeche, 'm2.')
