#Datum: 07.03.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 3.2.a
#Aufgabe: Schreiben Sie ein Programm, das eine Zeichenkette einliest und
#eine Zeichenkette ausgibt, die nur aus Sternchen (*) besteht und genauso
#lang ist wie die eingegebene Zeichenkette.



#AB DER NÄCHSTEN ZEILE FOLGT CODE
text = input("Bitte geben Sie eine Zeichenkette ein.")

print("*" * len(text))