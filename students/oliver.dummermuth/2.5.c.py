#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.5, c
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 28


#AB DER NÄCHSTEN ZEILE FOLGT CODE
i = 0



def monat():
        i = int(input('Bitte Zahl zwischen 1 und 12 eingeben.'))
        
        if i <= 12:
            if i == 1:
                print('Januar')
                monat()
            elif i == 2:
                print('Februar')
                monat()
            elif i == 3:
                print('März')
                monat()
            elif i == 4:
                print('April')
                monat()
            elif i == 5:
                print('Mai')
                monat() 
            elif i == 6:
                print('Juni')
                monat() 
            elif i == 7:
                print('Juli')
                monat() 
            elif i == 8:
                print('August')
                monat() 
            elif i == 9:
                print('September')
                monat() 
            elif i == 10:
                print('Oktober')
                monat() 
            elif i == 11:
                print('November')
                monat() 
            elif i == 12:
                print('Dezember')
                monat()
       
        
        elif i == 99:
                print('Ende')
        else:
                monat()

if i == 0:
        monat()
