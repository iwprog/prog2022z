#Datum: 22.02.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 2.2
#Aufgabe: siehe 3_Python_Functions_and_Control_Structures.pdf, Folie 5


#AB DER NÄCHSTEN ZEILE FOLGT CODE
from turtle import *

def dreieck(farbe1, farbe2, laenge):
    pencolor(farbe1)
    begin_fill()
    fillcolor(farbe2)
    forward(laenge)
    left(120)
    forward(laenge)
    left(120)
    forward(laenge)
    end_fill()
pensize(5)

right(90)
dreieck('red', 'cyan', 100)
dreieck('red', 'yellow', 200)
dreieck('red', 'lime', 50)
