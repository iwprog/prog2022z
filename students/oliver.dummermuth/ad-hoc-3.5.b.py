#Datum: 14.03.2022
#Autor: Oliver Dummermuth

#Ad-hoc-Übung: 3.5
#Aufgabe: a. Schreiben Sie ein Programm «Einkaufsliste», welches «Äpfel»,
#«Birnen», «Butter» und «Brot» in einer Liste speichert, diese sortiert
#und anschliessend ausgibt.
#b. Erweitern Sie Ihr Programm, sodass dieses mit einer leeren Liste
#startet und den Benutzer in einer Schleife nach Artikeln für die
#Einkaufsliste fragt, bevor diese sortiert ausgegeben werden.
#c. Erweitern Sie Ihr Programm, sodass dieses neben den Artikeln auch
#deren Preis speichert. Nach Ausgabe der Liste soll auch der zu
#Gesamtpreis ausgegeben werden.
#Hinweis: Sie müssen zur Lösung dieser Aufgabe eine verschachtelte Liste
#analog zum ad hoc Beispiel 3.3 (b) verwenden.


#AB DER NÄCHSTEN ZEILE FOLGT CODE
einkaufsliste = []

while True:
    eingabe = input("Bitte geben Sie die gewünschten Artikel ein einzeln ein. Abbruch mit 'q'.")
    
    if eingabe == "q":
        break
    
    einkaufsliste.append(eingabe)
    
    for i, item in enumerate(sorted(einkaufsliste)):
        print(i, item)      