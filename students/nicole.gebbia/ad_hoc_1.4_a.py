from turtle import*

laenge = numinput ("Dreieck", "Bitte geben Sie die Länge des Dreiecks an")

rt(90)
pencolor ("red")
pensize (5)
fillcolor ("cyan")
begin_fill()

forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()

laenge = laenge *2
fillcolor("yellow")
begin_fill()

right(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

home()

laenge = laenge /4
fillcolor("lime")
begin_fill()
left(90)
forward(laenge)
right(120)
forward(laenge)
right(120)
forward(laenge)
right(120)
forward(laenge)
end_fill()
