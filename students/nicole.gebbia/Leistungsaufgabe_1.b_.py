from turtle import *
#Leistungsaufgabe 1.b)
# verhältnis anhand von 400 Beispiel (165geschätzt:400(ganzes) ausgerechnet.
seitenlaenge = numinput ("Eingabemöglichkeit", "Bitte geben Sie die Seitenangaben an")
def schweizer_flagge(seitenlaenge):
    begin_fill()
    fillcolor("red")
    pencolor("red")
    fd (seitenlaenge)
    rt (90)
    fd (seitenlaenge)
    rt (90)
    fd (seitenlaenge)
    rt (90)
    fd (seitenlaenge)
    rt (90)
    fd (seitenlaenge*0.4) #40%
    rt (90)
    fd (seitenlaenge*0.175) #17.5%
    lt (90)
    end_fill()


def schweizer_kreuz(seitenlaenge):
    begin_fill()
    pencolor("white")
    fillcolor("white")

    i=0
    while i <4:
        fd (seitenlaenge*0.2)
        rt (90)
        fd (seitenlaenge*0.225)
        lt (90)
        fd (seitenlaenge*0.225)
        rt (90)
        i=i+1
    
    hideturtle()
    end_fill()
    
schweizer_flagge(seitenlaenge)
schweizer_kreuz(seitenlaenge)

