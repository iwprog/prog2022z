from turtle import *
#Leistungsaufgabe 1.a)
def schweizer_flagge(seitenlaenge):
    begin_fill()
    fillcolor("red")
    pencolor("red")
    fd (seitenlaenge)
    rt (90)
    fd (seitenlaenge)
    rt (90)
    fd (seitenlaenge)
    rt (90)
    fd (seitenlaenge)
    rt (90)
    fd (162.5)
    rt (90)
    fd (65)
    lt (90)
    end_fill()


def schweizer_kreuz():
    begin_fill()
    pencolor("white")
    fillcolor("white")

    i=0
    while i <4:
        fd (75)
        rt (90)
        fd (100)
        lt (90)
        fd (100)
        rt (90)
        i=i+1
    
    hideturtle()
    end_fill()
    
schweizer_flagge(400)
schweizer_kreuz()


