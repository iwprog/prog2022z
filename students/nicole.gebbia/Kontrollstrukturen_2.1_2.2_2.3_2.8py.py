# 2.1
def guten_morgen():
    print("Guten Morgen!")
guten_morgen()

# 2.2
def guten_morgen():
    name = input("Wie heisst du? ")
    print("Guten Morgen", name +"!")

guten_morgen()
# 2.3 a)
def guten_morgen(name):
    gruss = "Guten Morgen " + name+ "!"
    return gruss
    
print (guten_morgen("Nicole"))

#2.3 b)
def flaeche_rechteck(laenge, breite):
    return laenge*breite
print("Die Fläche beträgt", flaeche_rechteck(10,20), "m2.")

#2.8 a)

i=0
anzahl=10
while i <= anzahl:
    print(str(i)+ ". Durchlauf")
    i = i + 1

#2.8 c)
i= 10
while i >=0:
    print(str(i)+ ". Durchlauf")
    i = i-3

