# 1. Schreiben sie ein Programm, dass eine URL entgegenimmt (zum Beispiel
# https://www.ietf.org/rfc/rfc2616.txt) und
# (a) jedes Wort und die Anzahl der Vorkommnisse des Wortes berechnet.
# (b) berechnet wie oft jeder Buchstaben vorkommt.
#noch nicht ganz optimal, weil anfangsbuchstabe kann auch gross sein etc....

from urllib.request import urlopen
from csv import writer
from csv import reader

try:
    with open("wort.csv", encoding="utf8") as f:
        csv_reader = reader(f, delimiter=";")
except FileNotFoundError:
    print("Datei nicht gefunden.")
    
try:
    with open("buchstabe.csv", encoding="utf8") as file:
        c_reader = reader(file, delimiter=";")
except FileNotFoundError:
    print("Datei nicht gefunden.")

buchstabenliste=[]
woerterliste=[]

eingabe_url=input("Geben Sie eine URL ein: ")
eingabe_wort=input("Geben Sie ein Wort ein; Anzahl des in der angegebener URL wird gefundene Wort, wird angegeben.")
eingabe_buchstabe=input("Gib einen Buchstaben ein: ")
with urlopen(eingabe_url) as f:
    content= f.read().decode("utf8")
    with open ("buchstabe.csv", "r", encoding="utf8") as f_1:
        csv_reader = reader(f_1, delimiter=";")
        for row in csv_reader:
            print(row)
            buchstabenliste.append(row)            
    with open("buchstabe.csv","w", encoding="utf8") as file:
        csv_writer= writer(file, delimiter=";")
        buchstabenliste.append([eingabe_buchstabe,(content.count(eingabe_buchstabe.upper())+content.count(eingabe_buchstabe.lower()))])
        for buchstaben in buchstabenliste:
            if buchstaben == []:
                continue
            #problem es gibt immer eine leere zeile, auch noch beim angeben, dass ignorieren soll. 
            csv_writer.writerow(buchstaben)
    with open ("wort.csv", "r", encoding="utf8") as f_2:
        csv_reader_2 = reader(f_2, delimiter=";")
        for row2 in csv_reader_2:
            woerterliste.append(row2)
    with open("wort.csv", "w", encoding="utf8") as f:
        csv_writer_2= writer(f, delimiter=";")
        woerterliste.append([eingabe_wort, (content.count(eingabe_wort.upper())+content.count(eingabe_wort.lower()))])
        for woerter in woerterliste:
            if woerter == []:
                continue
            csv_writer_2.writerow(woerter)
            
    
print(eingabe_wort, ":", content.count(eingabe_wort))
print(eingabe_buchstabe, ":", content.count(eingabe_buchstabe))


# 2. Schreiben sie die Ergebnisse in zwei CSV Files:
# wort.csv
# gehen, 22
# <br>, 23
# <a, 22
# laufen, 12
# ...
# buchstabe.csv
# a, 922
# b, 929
# ...
