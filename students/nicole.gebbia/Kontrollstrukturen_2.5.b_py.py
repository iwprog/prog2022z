#2.5b)

alter= int(input("Dein Alter? "))

if alter < 0:
    print("Mit", alter, "ist man ungeboren.")
elif alter <=6:
    print("Mit", alter, "ist man geschäftsunfähig.")
elif alter <=14:
    print("Mit", alter, "ist man unmündig.")
elif alter >14 and alter <18:
    print("Mit", alter, "ist man mündig minderjährig.")
elif alter >=18:
    print("Mit", alter, "ist man volljährig.")

