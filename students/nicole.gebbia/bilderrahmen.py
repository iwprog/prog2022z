#6.)
from turtle import*

def zeichne_rahmen(laenge_gross, laenge_klein, farbe_aussen, farbe_innen):
    begin_fill()
    fillcolor(farbe_aussen)
    pencolor(farbe_aussen)
    i=0
    while i <4:
        fd(laenge_gross)
        rt(90)
        i=i+1
    end_fill()
    penup()
    fd((laenge_gross-laenge_klein)/2)
    rt(90)
    fd((laenge_gross-laenge_klein)/2)
    pendown()
    begin_fill()
    fillcolor(farbe_innen)
    pencolor(farbe_aussen)
    i=0
    while i <4:
        fd(laenge_klein)
        lt(90)
        i=i+1
    end_fill()
    
    
        
zeichne_rahmen(100, 60, "black", "yellow")
