# dicts beispiel 5  - berge erweitert mit Speicherung in JSON
# Morena Sager

from json import loads, dumps

try:
    with open("berge.json", encoding="utf8") as source:
        bergliste=loads(source.read())
except FileNotFoundError:
    bergliste = {}

for anzahl in range(1,4):
    berg=input("{}. Berg: ".format(anzahl))
    hoehe=input("{}. Höhe (in m): ".format(anzahl))
    bergliste[berg] = hoehe

 
for bergname, berghoehe in sorted(bergliste.items()):
    print(bergname, "ist", berghoehe,"m ("+ str(int(round(int(berghoehe)*3.28,0)))+" ft) hoch.")
    
    
with open("berge.json", "w", encoding="utf8") as savefile:
    savefile.write(dumps(bergliste))

 