# adhoc Übung 2.10c
# Morena Sager

from turtle import *
speed(10)

penup()
back(300)
left(90)
fd(300)
rt(90)
pendown()


def dreieck():
    for seite in range(3):
        fd(50)
        lt(120)
        
anzahl=int(numinput("Anzahl Dreiecke pro Reihe", "Geben sie eine Zahl zwischen 1 und 10 ein"))
reihe=int(numinput("Anzahl Reihen", "Wie viele Reihen sollen gezeichnet werden?"))
for zahl in range(reihe):
    for zahl in range(anzahl):
        dreieck()
        penup()
        fd(70)
        pendown()
    penup()
    rt(90)
    fd(70)
    lt(90)
    back(70*anzahl)
    pendown()
    
        

