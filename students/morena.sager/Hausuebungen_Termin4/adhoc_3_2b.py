# adhoc Übungen 3.2b
# Morena Sager

# Schreiben Sie ein Programm, das eine Zeichenkette und ein
# Zeichen (hintereinander in zwei getrennten Eingabefeldern)
# einliest. Ausgegeben werden sollen alle Positionen, an denen das
# Zeichen, unabhängig von Gross- und Kleinschreibung, in der
# Zeichenkette vorkommt.
# Tipp: Benutzen Sie ein FOR-Schleife zusammen mit der Funktion
# enumerate.
# Eingabe: Wenn Fliegen hinter Fliegen fliegen
# Zeichen: f
# Ausgabe: 6 21 29


text = input("Bitte geben Sie einen beliebigen Text ein: ")
zeichen = input("Nach welchem Zeichen soll gesucht werden? ")
positionen=[]
for position, element in enumerate(text):
    if element == zeichen:
        positionen.append(position)
print("Ausgabe:", positionen)


# Bitte im Unterricht besprechen
