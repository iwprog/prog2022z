# Übungsbeispiel 11.j - Spass mit Schleifen
# Morena Sager

i=11
a=0

for count in range(6):
    print(a*"  "+" #" * i)
    i=i-2
    a=a+1
    