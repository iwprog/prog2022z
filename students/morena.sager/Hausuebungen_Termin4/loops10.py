# Übungsbeispiel 10a
# Morena Sager

# Schreiben Sie ein Programm, welches ein Men¨u ausgibt und die Berechnungen aus den Beispielen 3, 4 und 4a ¨uber dieses zug¨anglich macht.
# Die Men¨uausgabe soll so lange wiederholt werden, bis der Benutzer das
# Programm mit 0 beendet.


eingabe = ""

def programm_uebersicht():
    print("==================================================")
    print("Programmübersicht:")
    print("1 ... maximale Herzfrequenz berechnen")
    print("2 ... Fahrkartenpreis berechnen")
    print()
    print("0 ... Programm beenden")
    print("==================================================")
    
def berechne_hf():
    alter=int(input("Geben Sie Ihr Alter ein: "))
    print()
    if alter >1 and alter < 100:
        return(220-alter)
            
def berechne_preis():
    alter=int(input("Bitte geben Sie Ihr Alter ein: "))
    entfernung=int(input("Wieviele km wollen Sie reisen? "))
    print()
    if alter<6:
        return 0
    elif alter<16:
        return (2+(0.25*entfernung))/2
    else:
        return 2+(0.25*entfernung)


while True:
    programm_uebersicht()
    print()
    eingabe=input("Gewählte Option: ")
    if eingabe == "1":
        print()
        print("Maximale Herfrequenz berechenen")
        print("--------------------------------------")
        print("Ihre Maximale Herzfrequenz beträgt:",berechne_hf())
        print()
    elif eingabe == "2":
        print()
        print("Fahrkartenpreis berechnen")
        print("--------------------------------------")
        print("Die Fahrkarte kostet:", berechne_preis(),"CHF")
        print()
    elif eingabe == "0":
        break
    else:
        print()
        print("Ungültige Option!")
        print()
    
    
        
        
    