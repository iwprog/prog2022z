# Übungsblatt Datentypen - Aufgabe 1.1 a-d
# Morena Sager

# a.
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

# b.
jahreszeiten.remove("Frühling")

# c.
jahreszeiten.append("Langas")



# d.
namen = []
while True:
    eingabe=input("Geben Sie einen Namen ein oder beenden Sie mit 'x' ")
    if eingabe != "x":
        namen.append(eingabe)
    elif eingabe == "x":
        print("Namen:",namen)
        break
    
    