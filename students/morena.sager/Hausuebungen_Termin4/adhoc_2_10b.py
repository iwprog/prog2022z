# adhoc Übung 2.10b
# Morena Sager

from turtle import *
speed(8)
penup()
back(300)
pendown()


def dreieck():
    for seite in range(3):
        fd(50)
        lt(120)
        
anzahl=int(numinput("Anzahl Dreiecke", "Geben sie eine Zahl zwischen 1 und 10 ein"))

if anzahl > 0 and anzahl < 11:
    for zahl in range(anzahl):
        dreieck()
        penup()
        fd(70)
        pendown()
else:
    print("Geben sie eine Zahl zwischen 1 und 10 ein")
    

exitonclick()