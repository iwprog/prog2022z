# basic datatypes 3.1
# Morena Sager

# a
d_telefonbuch = {"Vorname":"Ana", "Nachname":"Skupch", "Phone":"123"}

# b
telefonbuch = [d_telefonbuch]

# c
telefonbuch.append({"Vorname":"Tim", "Nachname":"Kurz", "Phone":"732"})
telefonbuch.append({"Vorname":"Julia", "Nachname":"Lang", "Phone":"912"})
print(telefonbuch)
print()

# d
telefonbuch2=[]
while True:
    vorname=input("Vorname oder 'x' zum beenden ")
    if vorname == "x":
        break
    nachname=input("Nachname: ")
    telnr=input("Telefonnummer: ")
    telefonbuch2.append({"Vorname":vorname, "Nachname":nachname, "Phone":telnr})

print("Telefonbuch:", telefonbuch2)