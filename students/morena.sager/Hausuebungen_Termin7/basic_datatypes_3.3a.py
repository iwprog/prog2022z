# basic datatypes 3.3a
# Morena Sager

telefonbuch =[{'Vorname': 'Ana', 'Nachname': 'Skupch', 'Phone': '123'},
              {'Vorname': 'Tim', 'Nachname': 'Kurz', 'Phone': '732'},
              {'Vorname': 'Julia', 'Nachname': 'Lang', 'Phone': '912'},
              {'Vorname': 'Lara', 'Nachname': 'Meyer', 'Phone': '165'}]

tel_output = []
for eintrag in telefonbuch:
    if "1" in eintrag["Phone"]:
        tel_output.append(eintrag)
        
for datensatz in tel_output:
    telefonbuch.remove(datensatz)
    
print(telefonbuch)