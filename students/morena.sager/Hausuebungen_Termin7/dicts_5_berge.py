# dicts beispiel 5  - berge
# Morena Sager

bergliste = {}

for anzahl in range(1,4):
    berg=input("{}. Berg: ".format(anzahl))
    hoehe=input("{}. Höhe (in m): ".format(anzahl))
    bergliste[berg] = hoehe

 
for bergname, berghoehe in sorted(bergliste.items()):
    print(bergname, "ist", berghoehe,"m ("+ str(int(round(int(berghoehe)*3.28,0)))+" ft) hoch.")

 