# Leistungsaufgabe 3b
# Morena Sager


woerterbuch ={"Drache":"dragon", "Zwerg":"dwarf", "Zauberstab":"wand",
              "Zauberer":"wizard", "Hexe":"witch", "Eule":"owl", "Kröte":"toad",
              "Zug":"train", "Besen":"broom", "Weide":"willow"}
anzahl=0
for eintrag in woerterbuch:
    antwort=input("Was heisst "+eintrag+"? ")
    if antwort == woerterbuch[eintrag]:
        print("Richtig!")
        anzahl = anzahl + 1
    else:
        print("Falsch!")
        
print("Sie haben", anzahl, "von 10 Vokabeln gewusst")