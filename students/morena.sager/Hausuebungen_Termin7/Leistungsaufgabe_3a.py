# Leistungsaufgabe 3a
# Morena Sager


cities= []

while True:
    stadt = input("Stadt oder 'x' zum beenden ")
    if stadt == "x":
        break
    einwohner = input("Wieviele Einwohner hat "+stadt+"? ")
    cities.append([stadt, einwohner])
    
print(sorted(cities))