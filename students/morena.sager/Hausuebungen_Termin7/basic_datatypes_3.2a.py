# basic datatypes 3.2a
# Morena Sager

telefonbuch =[{'Vorname': 'Ana', 'Nachname': 'Skupch', 'Phone': '123'},
              {'Vorname': 'Tim', 'Nachname': 'Kurz', 'Phone': '732'},
              {'Vorname': 'Julia', 'Nachname': 'Lang', 'Phone': '912'}]

def datensatz_ausgeben():
    for count, eintrag in enumerate(telefonbuch):
        if count == 0:
            print("Vorname:", eintrag["Vorname"])
            print("Nachname:", eintrag["Nachname"])
            print("Phone:", eintrag["Phone"])


datensatz_ausgeben()

