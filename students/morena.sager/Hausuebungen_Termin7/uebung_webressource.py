# uebung webressourcen
# Morena Sager

# öffnen sie die folgende url (https://www.gutenberg.org/cache/epub/6079/pg6079.txt) in
# ihrem web browser und speichern sie die zugehörige textdatei in ihrem python verzeichnis.
# schreiben sie nun ein python programm, das 
#     - diese auf dem bildschirm ausgibt und 
#     - ermittelt wie oft die worte "Himmel", "Freiheit", "Spiel" und "Tanz" in der datei vorkommen.
# (hinweis: es ist wichtig, dass sich ihr python programm und die textdatei im selben verzeichnis befinden,
#  da ansonsten python die datei nicht öffnen kann).
# - erweitern Sie Ihr Python Programm, sodass es die Datei direkt aus dem Web bezieht,
# sodass das manuelle Speichern der Datei entfallen kann.

from urllib.request import urlopen

with urlopen("https://www.gutenberg.org/cache/epub/6079/pg6079.txt") as s:
    with open("gutenberg.txt", "w", encoding="utf8") as f:
        content = s.read().decode("utf8")
        f.write(content)
        
with open("gutenberg.txt", encoding="utf8") as text:
    inhalt = text.read()
    print(inhalt)
          
print("Himmel:",inhalt.count("Himmel"))
print("Freiheit:", inhalt.count("Freiheit"))
print("Spiel:", inhalt.count("Spiel"))
print("Tanz:", inhalt.count("Tanz"))