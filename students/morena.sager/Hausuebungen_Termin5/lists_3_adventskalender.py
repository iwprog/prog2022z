# Listen Beispiel 3
# Morena Sager


from random import choice

ueberraschungen=[]

auswahlliste=["Samichlaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]

while True:
  auswahl=input("Welche Tür wollen Sie öffnen? x zum Beenden ")
  if auswahl == "x":
    break
  if auswahl.isdigit():
    if int(auswahl) >= 1 and int(auswahl) <= 24:
      zufallselement=choice(auswahlliste)
      print("Ihre Überraschung:",zufallselement)
      ueberraschungen.append(zufallselement)
    else:
      continue

print(ueberraschungen)