# lists Beispiel 10 Textadventure
# Morena Sager
rauminventory = ["Kaugummi", "Benzin"]
playerinventory = []
toeff =[]
    
        
print("Du befindest dich an einem unbekannten Ort. Du siehst 2 Pfade.")
richtung=input("Welchen Pfad möchtest du nehmen? links oder rechts? ")
if richtung == "links":
    print("Hier ist nichts spezielles")
    if "Kaugummi" in rauminventory:
        print("Doch, da liegt ein Kaugummi am Boden")  
    while True:
        choice=input("Was möchtest du tun? links, rechts, nimm oder benutze ")
        if choice == "nimm":
            if "Kaugummi" in rauminventory:
                playerinventory.append("Kaugummi")
                rauminventory.remove("Kaugummi")
                print("Du hast den Kaugummi deinem Inventar hinzugefügt")
            else:
                print("Da ist nichts, das du aufheben könntest")
        elif choice == "benutze":
            print("Du kannst hier nichts kombinieren")
        elif choice == "links":
            print("Hier ist nichts spezielles")
            if "Benzin" in rauminventory:
                print("Doch, da ist ein Kanister Benzin")
            while True:
                choice=input("Was möchtest du tun? links, rechts, nimm oder benutze ")
                if choice == "nimm":
                    if "Benzin" in rauminventory:
                        playerinventory.append("Benzin")
                        rauminventory.remove("Benzin")
                        print("Du hast das Benzin deinem Inventar hinzugefügt")
                    else:
                        print("Da ist nichts, das du aufheben könntest")
                elif choice == "benutze":
                    print("Du kannst hier nichts kombinieren")
                elif choice == "links":
                    print("Du befindest dich auf einem Waldweg. Am Wegrand steht ein altes Motorrad mit einem Loch im Reifen.")
                    while True:
                        if len(toeff) == 2:
                            print("Du hast das Motorrad repariert")
                            loesung=input("Möchtest du mit dem Motorrad davonfahren? ja oder nein ")
                            if loesung == "ja":
                                print("Gratulation! Du hast gewonnen")
                                break
                        else:    
                            choice=input("Was möchtest du tun? links, rechts, nimm oder benutze ")
                            if choice == "nimm":
                                print("Da ist nichts, das du aufheben könntest")
                            elif choice == "benutze":
                                benutzen=input("Was möchtest du benutzen? ")
                                if benutzen in playerinventory:
                                    if benutzen == "Kaugummi" or benutzen == "Benzin":
                                        kombinieren=input("Womit möchtest du den Gegenstand kombinieren? ")
                                        if kombinieren == "Motorrad":
                                            if benutzen == "Kaugummi":
                                                print("Du hast den Reifen des Motorrades repariert")
                                                playerinventory.remove(benutzen)
                                                toeff.append(benutzen)
                                            elif benutzen == "Benzin":
                                                print("Du hast den Tank des Motorrades aufgefüllt")
                                                playerinventory.remove(benutzen)
                                                toeff.append(benutzen)
                                            else:
                                                print("Damit kannst du den Gegenstand nicht benutzen")  
                                else:
                                    print("Du besitzt diesen Gegenstand nicht")
                
                     
        
            
if richtung == "rechts":
    print("Du befindest dich auf einem Waldweg. Am Wegrand steht ein altes Motorrad mit einem Loch im Reifen.")
