# basic-datatypes 1.2
# Morena Sager

# a
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
print(sorted(jahreszeiten))

# b
print(jahreszeiten[:-1])

# c
for jahreszeit in jahreszeiten:
    print(jahreszeit, "-", jahreszeit[-2:])

# d
for jahreszeit in jahreszeiten:
    if jahreszeit[-2:] == "er":
        print(jahreszeit)
        
# e
for jahreszeit in jahreszeiten:
    print(jahreszeit,"-",jahreszeit.count("r"))