# b) einkaufsliste
#       - erstellen sie ein programm, das eine einkaufsliste verwaltet, welche über die folgenden funktionen verfügt:
#          (1) artikel hinzufügen
#          (2) artikel löschen
#          (3) artikel suchen
#          (4) einkaufsliste leeren
#          (5) einkaufsliste im csv format exportieren
#          (6) gesamtbetrag einkauf berechnen
#          (0) exit
#     - zu jedem artikel sollen preis und menge gespeichert werden.
#     - die funktion "artikel suchen" soll alle artikel ausgeben, die den suchbegriff enthalten.
#          (der suchbegriff "en" würde zum beispiel 'birnen', 'kirschen' und 'erdbeeren' auswählen).
#     - die einkaufsliste soll mit dem programmende automatisch in der datei "einkaufsliste.json" gespeichert und beim öffnen des programms wiederhergestellt werden.
#     - es soll möglich sein, die einkaufsliste im csv format zu exportieren (z.b. als einkaufsliste.csv)
from csv import writer
from json import loads, dumps

try:
    with open("einkaufsliste_gespeichert.json", encoding="utf") as source:
        items_json = source.read()
        values = loads(items_json)
        einkaufsliste=[values]
except FileNotFoundError:
    einkaufsliste = []
    


def menue():
    print("(1) Artikel hinzufügen")
    print("(2) Artikel löschen")
    print("(3) Artikel suchen")
    print("(4) Einkaufsliste leeren")
    print("(5) Einkaufsliste im csv format exportieren")
    print("(6) Gesamtbetrag Einkauf berechnen")
    print("(0) Exit")
    print()
    
def hinzu(liste):
    while True:
        item=input("Was möchten Sie einkaufen? 'x' zum beenden ")
        if item == "x":
            break
        else:
            preis=float(input("Preis: "))
            menge=int(input("Stückzahl: "))
            liste.append([item, preis, menge])
    
def item_loeschen(liste):
    while True:
        print("Ihre Einkaufsliste:",liste)
        print()
        loeschen=input("Welches Item löschen? 'x' zum beenden ")
        print()
        if loeschen == "x":
            break
        else:
            for eintrag in liste:
                if loeschen in eintrag:
                    liste.remove(eintrag)
                    
def suchen(liste):
    suche=input("Wonach möchten sie suchen? ")
    print()
    for eintrag in liste:
        for element in eintrag:
            if suche.lower() in str(element).lower():
                print(eintrag)
            
def csv_export(liste):
    with open("einkaufsliste.csv", "w", encoding="utf8") as file:
        csv_writer = writer(file, delimiter=";")
        csv_writer.writerow(["Artikel", "Preis", "Stueckzahl"])
        for eintrag in liste:
            csv_writer.writerow(eintrag)
    print("Einkaufsliste wurde exportiert")
    print()

def gesamtbetrag(liste):
    summe=0
    for eintrag in liste:
        summe= summe + eintrag[1] * eintrag[2]
    print("Gesamtbetrag =", summe, "CHF")
    print()

while True:
    menue()
    auswahl=input("Was möchten Sie tun? ")
    print()
    if auswahl == "0":
        break
    elif auswahl == "1":
        hinzu(einkaufsliste)
        print()
        print("Ihre Einkaufsliste:", einkaufsliste)
        print()
    elif auswahl == "2":
        item_loeschen(einkaufsliste)
        print()
        print("Ihre Einkaufsliste:", einkaufsliste)
        print()
    elif auswahl == "3":
        suchen(einkaufsliste)
        print()
    elif auswahl == "4":
        leeren=input("Möchten Sie wirklich die gesamte Einkaufsliste löschen? ja/nein ")
        if leeren == "nein":
            continue
        elif leeren == "ja":
            einkaufsliste=[]
    elif auswahl == "5":
        csv_export(einkaufsliste)
    elif auswahl == "6":
        gesamtbetrag(einkaufsliste)
        
with open("einkaufsliste_gespeichert.json", "w", encoding="utf") as savefile:
    json_string = dumps(einkaufsliste)
    savefile.write(json_string)