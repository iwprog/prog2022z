# Leistungsaufgabe 5 - Aufgabe 3

from urllib.request import urlopen
from csv import writer
from inscriptis import get_text

ressource = input("Welche Ressource möchten sie öffnen? ")

try:
    if "http" in ressource:
        with urlopen(ressource) as s:
            web_content = s.read().decode("utf8")
            text_content = get_text(web_content)
    else:
        with open(ressource, encoding="utf8") as source:
            text_content = source.read()
            
except FileNotFoundError:
    print("Diese Datei existiert nicht")
    
        
text=text_content.split()
wortliste={}
buchstabenliste={}


for wort in text:
    if wort in wortliste:
        wortliste[wort] = wortliste[wort] + 1
    else:
        wortliste[wort] = 1
        
for wort in text:
    for buchstabe in wort:
        if buchstabe.lower() in buchstabenliste:
            buchstabenliste[buchstabe.lower()] = buchstabenliste[buchstabe.lower()] + 1
        else:
            buchstabenliste[buchstabe.lower()] = 1
        

with open("wort2.csv", "w", encoding="utf8") as woerter:
    csv_writer = writer(woerter, delimiter=";")
    for wort, wert in wortliste.items():
        csv_writer.writerow([wort,wert])
        
with open("buchstabe2.csv", "w", encoding="utf8") as buchstabe:
    csv_writer = writer(buchstabe, delimiter=";")
    for wort, wert in sorted(buchstabenliste.items()):
        csv_writer.writerow([wort,wert])
        
        

    
