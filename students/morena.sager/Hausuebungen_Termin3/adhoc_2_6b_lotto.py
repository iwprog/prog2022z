# adhoc Übung 2.6b - Lotto
# Morena Sager

# Schreiben sie ein Programm, das 6 Zufallszahlen zwischen
# 1 und 42 ausgibt.
# Die Ausgabe soll wie folgt aussehen:

# 1. Zufallszahl: ...
# ...
# 6. Zufallszahl: ...

from random import randint

i=1

for zahl in range(6):
    zufallszahl=randint(1,42)
    print(str(i)+". Zufallszahl:", zufallszahl)
    i=i+1