#Übungsblatt Grundlagen Kontrollstrukturen
#Morena Sager

#Übung 2.5b

# Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach
# Alter den Wert volljährig oder minderjährig zurückgibt.
# Erweitern Sie die Funktion, sodass diese zwischen geschäftsunfähig (bis
# inklusive 6 Jahre), unmündig (bis inklusive 14 Jahre), mündig minderjährig
# und volljährig unterscheidet. Weiters soll für ein negatives Alter ungeboren
# zurückgegeben werden.

age=input("Geben Sie ein Alter ein ")

def legal_status(age):
    if int(age)<0:
        return("ungeboren.")
    elif int(age)>=0 and int(age)<=6:
        return"geschäftsunfähig."
    elif int(age)>6 and int(age)<=14:
        return("unmündig.")
    elif int(age)>14 and int(age)<=17:
        return("mündig minderjährig.")
    elif int(age)>=18:
        return("volljährig.")
    
    
print("Mit", age, "ist man", legal_status(age))
# ich wollte noch einbauen, dass wenn man keine zahl eingibt man solange weiter
# nach einer Zahl gefragt wird, bis man auch wirklich eine Zahl eingibt, habs aber auf Anhieb
# nicht geschafft und wollte damit nicht zu viel Zeit "verschwenden"
# eventuell könnte man das im Unterricht anschauen?
