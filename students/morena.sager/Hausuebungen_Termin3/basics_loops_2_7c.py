#Übungsblatt Grundlagen Kontrollstrukturen
#Morena Sager

#2.7c
# Schreiben Sie eine Schleife, welche alle Buchstaben des Wortes Dampfschiff
# nummeriert ausgibt.
# 1 ... D
# ...
# 11 ... f

i=1
for zeichen in "Dampfschiff":
    print(str(i)+". "+zeichen)
    i=i+1