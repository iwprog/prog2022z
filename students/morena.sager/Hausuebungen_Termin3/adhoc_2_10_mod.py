#adhoc Übung 2.10
#erweitert:
#Menü "Was soll ich Zeichnen? --> kreis, dreieck oder quadrat

#advanced: Zeichenmuster
# #do# -> Viereck, Dreieck, Kreis, Viereck
# #### -> 4*Viereck
# oodd# -> Kreis, Kreis, Dreieck, dreieck, Viereck

from turtle import *
speed(8)
penup()
back(400)
pendown()

def dreieck(laenge):
    for seite in range(3):
        fd(laenge)
        lt(120)
        
def quadrat(laenge):
    for seite in range(4):
        fd(laenge)
        lt(90)
        
def kreis(durchmesser):
    circle(durchmesser)
#die Funktion ist eigentlich unnötig, ich lass sie jetzt trotzdem mal drin
    

form=textinput("Was soll ich zeichnen?", "d, o, oder #?")

for muster in form:
    if muster=="d":
        dreieck(100)
        penup()
        fd(120)
        pendown()
    elif muster=="o":
        penup()
        fd(50)
        pendown()
        kreis(50)
        penup()
        fd(75)
        pendown()
    elif muster=="#":
        ersatz=quadrat(100)
        penup()
        fd(120)
        pendown()
    else:
        print("kann ich nicht")
