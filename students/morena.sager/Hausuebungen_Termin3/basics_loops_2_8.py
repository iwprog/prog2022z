#Übungsblatt Grundlagen Kontrollstrukturen
#Morena Sager

# 2.8d
# Schreiben Sie eine Schleife, welches von 10 bis 0 z¨ahlt und statt "0" das
# Wort "Start" ausgibt.

i=0

while i==0:
    print("Start")
    i=i+1
    while i>0 and i<=10:
        print(i)
        i=i+1
        
        
# 2.8f
# Schreiben Sie eine Schleife, welche so lange Zahlen entgegennimmt,
# bis deren Summe grösser als 10 ist. Im Anschluss soll die Summe
# ausgegeben werden.

i=0
summe=0
while summe <= 10:
    i=int(input("Geben Sie eine Zahl ein "))
    summe=summe+i
    
print("Summe:",summe)


# 2.8g
# Schreiben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der
# Benutzer -1 eingibt und anschliessend die Summe dieser Zahlen ausgibt.


a=0
summe=0
eingabe=0

while eingabe !="-1":
    a=int(eingabe)
    summe=summe+a
    eingabe=input("Geben sie eine Zahl ein ")
    
print("Summe:",summe)

    
    
    
    
    
    
    
    