# Übungsbeispiele
# Morena Sager

# Beispiel 6
# Schreiben Sie eine Funktion welche einen Bilderrahmen mit einer bestimmten
# Länge und Breite unter Zuhilfenahme von Schleifen zeichnet.
# Weiters soll die Funktion noch die Rahmen- und Füllfarbe entgegennehmen.

from turtle import *

def zeichne_rahmen(aussen, abstand, rahmen, fuellung):
    pencolor(rahmen)
    fillcolor(fuellung)
    begin_fill()
    for seite in range(4):
        fd(aussen)
        lt(90)
    end_fill()
    penup()
    forward(abstand)
    lt(90)
    fd(abstand)
    pendown()
    fillcolor("white")
    begin_fill()
    for seite in range(4):
        fd(aussen-abstand*2)
        rt(90)
    end_fill()
        
zeichne_rahmen(300,30,"red","blue")