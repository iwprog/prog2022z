# Übungsbeispiele
# Morena Sager


# Beispiel 2
# Schreiben Sie eine Funktion, welche das Alter einer Person entgegennimmt und daraus die theoretische maximale Herzfrequenz berechnet.
# Die Formel f¨ur die Berechnung der Herzfrequenz lautet:
# max Herzfrequenz = 220 − Alter in Jahren

def berechne_hf(alter):
    if alter >1 and alter < 100:
        print(220-int(alter))
            
berechne_hf(25)


# Beispiel 3
# Schreiben Sie eine Funktion welche basierend auf dem Alter und der
# Entfernung den Preis für ein SBB Billet berechnet:
# • jedes Ticket verfügt über einen Grundpreis von 2 CHF, der unabhängig von der Entfernung berechnet wird.
# • zusätzlich werden 0.25 CHF / km berechnet.
# • Kinder unter 16 Jahren zahlen den halben Preis.
# • Kinder unter 6 Jahren reisen kostenlos.


def berechne_preis(alter, entfernung):
    if alter<6:
        return 0
    elif alter<16:
        return (2+(0.25*entfernung))/2
    else:
        return 2+(0.25*entfernung)

print("Ticketpreis:",berechne_preis(16,200),"CHF")
