#Übungsbeispiele, Spass mit Schleifen, 11.e
#Morena Sager

def figur_e():
    for count in range(7):
        if count ==0 or count==6:
            print("# # # # # # #")
        else:
            print("#           #")



def figur_g():
    for count in range(7):
        if count ==0 or count==6:
            print("# # # # # # #")
        elif count==1:
            print("          #")
        elif count==2:
            print("        #")
        elif count==3:
            print("      #")
        elif count==4:
            print("    #")
        elif count==5:
            print("  #")         
        elif count==6:
            print("# # # # # # #")
    
#es gäbe hier sicher eine elegantere Lösung, aber ich komm nicht drauf 
    
        
figur_e()
print("")
figur_g()