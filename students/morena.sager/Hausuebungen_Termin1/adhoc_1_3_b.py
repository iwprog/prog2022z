# adhoc übung 1.3.b
from turtle import *
speed(8)


pensize(5)
pencolor("red")
left(45)
fillcolor("cyan")
begin_fill()
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

fillcolor("yellow")
begin_fill()
left(200)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

fillcolor("violet")
begin_fill()
left(200)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

fillcolor("blue")
begin_fill()
left(200)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

fillcolor("lime")
begin_fill()
left(200)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

left(120)
#detail, dann schaut die Turtle am Schluss
#noch in die gleiche Richtung wie auf dem Bild