# dicts beispiel 5  - berge
# Morena Sager

bergliste = [["B1",1000,"G1"],["B3", 500, "G2"],["B2", 1500, "G1"]]

def menue():
    print("Berge sortieren")
    print("---------------")
    print("1 - nach Name")
    print("2 - nach Höhe")
    print("3 - nach Gebirge")
    
def sort_hoehe(wert):
    return wert[1], wert[0]

def sort_gebirge(wert):
    return wert[2], wert[0]

while True:
    berg=input("Berg: (x zum beenden) ")
    if berg == "x":
        break
    else:
        hoehe=int(input("Höhe (in m): "))
        gebirge=input("Gebirge: ")
        bergliste.append([berg, hoehe, gebirge])
    
        
menue()
frage=input("Was möchten sie tun? ")
sortierung=input("Möchten sie (1) aufsteigend oder (2) absteigend sortieren? ")
if frage == "1":
    if sortierung == "1":
        for bergname, berghoehe, berggebirge in sorted(bergliste):
            print(f"{bergname} ist {berghoehe} m hoch und gehört zum {berggebirge}")
    elif sortierung == "2":
        for bergname, berghoehe, berggebirge in sorted(bergliste, reverse=True):
            print(f"{bergname} ist {berghoehe} m hoch und gehört zum {berggebirge}")
elif frage == "2":
    if sortierung == "1":
        for bergname, berghoehe, berggebirge in sorted(bergliste, key=sort_hoehe):
            print(f"{bergname} ist {berghoehe} m hoch und gehört zum {berggebirge}")
    elif sortierung == "2":
        for bergname, berghoehe, berggebirge in sorted(bergliste, key=sort_hoehe, reverse=True):
            print(f"{bergname} ist {berghoehe} m hoch und gehört zum {berggebirge}")
elif frage == "3":
    if sortierung == "1":
        for bergname, berghoehe, berggebirge in sorted(bergliste, key=sort_gebirge):
            print(f"{bergname} ist {berghoehe} m hoch und gehört zum {berggebirge}")
    elif sortierung == "2":
        for bergname, berghoehe, berggebirge in sorted(bergliste, key=sort_gebirge, reverse=True):
            print(f"{bergname} ist {berghoehe} m hoch und gehört zum {berggebirge}")

 
