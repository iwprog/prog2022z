#Übungen Grundlagen Kontrollstrukturen
#Morena Sager

#Übung 2.1
def guten_morgen():
    print("Guten Morgen!")
    
guten_morgen()

#Übung 2.2

def guten_morgen2(name):
    print("Guten Morgen "+name+"!")
    
guten_morgen2("Morena")

#Übung 2.3a
def gruss():
    name=input("Wie heisst du? ")
    print("Guten Morgen "+name+"!")
gruss()

#Übung 2.3b
def rechtecksflaeche(hoehe, breite):
    return hoehe*breite

ergebnis=rechtecksflaeche(140,90)
print("Die Fläche beträgt "+str(ergebnis),"m2.")
    


#Übung 2.8a
a=0
while a<=10:
    print(a)
    a=a+1

# #Übung 2.8c
b=10
while b>0:
    print(b)
    b=b-3
    if b<0:
        print(0)


