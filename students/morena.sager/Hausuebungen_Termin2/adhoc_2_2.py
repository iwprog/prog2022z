#adhoc Übung 2.2
#Morena Sager

from turtle import *

def dreieck():
    laenge=numinput("Seitenlänge Dreieck", "Bitte Seitenlänge angeben")
    begin_fill()
    fd(laenge)
    lt(120)
    fd(laenge)
    lt(120)
    fd(laenge)
    end_fill()
    
pencolor("red")
pensize(5)
speed(8)
fillcolor("cyan")
rt(90)
dreieck()

fillcolor("yellow")
dreieck()

fillcolor("lime")
dreieck()

#Alternative
# def dreieck(laenge):
#     begin_fill()
#     fd(laenge)
#     lt(120)
#     fd(laenge)
#     lt(120)
#     fd(laenge)
#     end_fill()
#     
# pencolor("red")
# pensize(5)
# speed(8)
# fillcolor("cyan")
# rt(90)
# dreieck(100)
# 
# fillcolor("yellow")
# dreieck(200)
# 
# fillcolor("lime")
# dreieck(50)