# Leistungsaufgabe 1b
# Morena Sager

#ich habe die Kommentare aus Teilaufgabe a entfernt, und nur noch die Kommentare für die Änderung zu Teilaufgabe b inkludiert
from turtle import *

speed(8)
pencolor("red")
fillcolor("red")

# da man die Seitenlänge per input abfragen soll, muss dieser hier eingesetzt werden und die Variable laenge wird definiert
# anstatt forward 400 wird die Variable eingegeben
laenge = numinput("Seitenlänge", "Wie gross soll die Seitenlänge der Flagge sein?")
begin_fill()
forward(int(laenge))
left(90)
forward(int(laenge))
left(90)
forward(int(laenge))
left(90)
forward(int(laenge))

end_fill()

#auch hie muss die fd-Bewegung mit der richtigen Länge angegeben werden.
#ich habe für die Flagge und das Kreuz das quadrat in 25 kleinere quadrate geteilt. somit kann ich immer mit der ursprünglichen Länge arbeiten
#muss aber entsprechend die Länge, ausgehend von der ursprünglichen Länge berechnen
# plus immer den Datentyp zu int ändern

left(90)
forward(int(laenge/5*2))
left(90)
forward(int(laenge/5))

fillcolor("white")
begin_fill()

def kreuz_viertel():
    forward(int(laenge/5))
    left(90)
    forward(int(laenge/5))
    right(90)
    forward(int(laenge/5))
    right(90)

kreuz_viertel()
kreuz_viertel()
kreuz_viertel()
kreuz_viertel()

end_fill()

hideturtle()

