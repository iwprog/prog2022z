#Wir schreiben Python; Buchstabe P
#Morena Sager

from turtle import *

speed(8)
fd(300);lt(90);fd(400);lt(90)
fd(300);lt(90);fd(400);lt(90)

def buchstabe_p():
    penup()
    fd(20)
    left(90)
    fd(20)
    pendown()

    pensize(5)
    pencolor("blue")
    fillcolor("blue")
    begin_fill()

    fd(360)
    rt(90)
    fd(100)
    circle(-100,180)
    lt(90)
    fd(160)
    rt(90)
    fd(100)
    end_fill()

    penup()
    pencolor("white")
    lt(180)
    fd(100)
    lt(90)
    fd(220)
    pendown()

    fillcolor("white")
    begin_fill()
    fd(100)
    rt(90)
    circle(-50,180)
    end_fill()

    pencolor("black")
    penup()
    lt(90)
    fd(240)
    lt(90)
    fd(180)
    pendown()
    
buchstabe_p()