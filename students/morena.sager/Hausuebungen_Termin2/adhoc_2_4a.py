#Adhoc Übung 2.4a
#Morena Sager
# Volumen = PI*r^2*h

from math import pi

def volumen_zylinder(r,h):
    return pi*float(r**2)*float(h)

volumen=volumen_zylinder(4,2)
print("Das Volumen beträgt: "+ str(volumen))
