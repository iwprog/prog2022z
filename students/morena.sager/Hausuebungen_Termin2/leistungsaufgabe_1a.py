# Leistungsaufgabe 1a
# Morena Sager

#import der turtle tools
from turtle import *

#ich erhöhe erst den speed, damit alles schneller geht beim Zeichnen
speed(8)

#dann das setzen der roten Farbe des pens und der Füllung
pencolor("red")
fillcolor("red")

#beginnen der Füllung und das Zeichnen des Quadrats
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
#beenden der Füllung
end_fill()

#um das Weisse Kreuz zeichnen zu können, muss die Turle an eine geeignete Stelle für den Start bewegt werden
#da pencolor immer noch rot ist kann die turtle fahren, ohne gehoben zu werden
left(90)
forward(160)
left(90)
forward(80)

#Füllfarbe muss auf weiss geändert werden, und Füllung begonnen werden

fillcolor("white")
begin_fill()

#das Kreuz kann jetzt gezeichnet werden

#ich erstelle eine Funktion, die jeweils einen Viertel des Kreuzes zeichnet
def kreuz_viertel():
    forward(80)
    left(90)
    forward(80)
    right(90)
    forward(80)
    right(90)

#viermal die Funktion ausfüllen
# könnte alternativ wohl auch mit einer Schleife gelöst werden, spart in diesem Fall aber nicht unbedingt viel Zeit/code
kreuz_viertel()
kreuz_viertel()
kreuz_viertel()
kreuz_viertel()

#kreuz fertig, Ende der Füllung noch einfügen

end_fill()

#Turtle unsichtbar machen
hideturtle()
