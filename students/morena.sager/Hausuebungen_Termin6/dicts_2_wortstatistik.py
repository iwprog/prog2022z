# dicts 2 Wortstatistik
# Morena Sager

l_input = "Der Tag begann sehr gut! Der Morgen war schön."

def word_stat(liste):
    l_output = {}
    for wort in liste.split():
        if wort.lower() in l_output:
            l_output[wort.lower()] = l_output[wort.lower()] + 1
        else:
            l_output[wort.lower()] = 1
    return l_output

print(word_stat(l_input))


