# basic datatypes 2.3c
# Morena Sager

preisliste = {"Brot":3.2, "Milch":2.1,
           "Orangen":3.75,"Tomaten":2.2,
           "Tee":4.2, "Peanuts":3.9,
              "Ketchup":2.1}
loeschen =[]
for item in preisliste:
    if "en" in item:
        loeschen.append(item)

for item in loeschen:
    del preisliste[item]
        
print(preisliste)

#variante 2
print()
einkauf = dict(preisliste)

for item in preisliste:
    if "en" in item:
        del einkauf[item]
        
print(einkauf)