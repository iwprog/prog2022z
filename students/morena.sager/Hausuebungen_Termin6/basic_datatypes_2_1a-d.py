# basics datatypes 2.1
# Morena Sager

# a.
# Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit
# den zugehörigen Preisen speichert: Brot --> 3.2, Milch --> 2.1,
# Orangen --> 3.75, Tomaten --> 2.2.

einkauf = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
print(einkauf)

# b. Ändern Sie den Preis für Milch auf 2.05

einkauf["Milch"] = 2.05
print(einkauf)

# c. Entfernen sie den eintrag für Brot

del einkauf["Brot"]
print(einkauf)

# d. Fügen Sie neue Einträge für Tee --> 4.2, Peanuts --> 3.9
# und Ketchup --> 2.1 hinzu.

einkauf["Tee"] = 4.2
einkauf["Peanuts"] = 3.9
einkauf["Ketchup"] = 2.1
print(einkauf)