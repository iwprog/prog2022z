# basic datatypes 2.2
# Morena Sager

preisliste = {"Brot":3.2, "Milch":2.1,
           "Orangen":3.75,"Tomaten":2.2,
           "Tee":4.2, "Peanuts":3.9, "Ketchup":2.1}

# a

for item, price in preisliste.items():
    print(item, "kostet", price, "CHF.")
    
print()  
# e

for item, price in preisliste.items():
    if price < 3.0:
        print(item, "kostet", price, "CHF.")
# habe 3.- genommen, weil auf der Liste nichts steht, das weniger als 2.- kostet
        
