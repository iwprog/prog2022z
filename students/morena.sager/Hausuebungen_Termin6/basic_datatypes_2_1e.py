# basic datatypes 2.1e
# Morena Sager


# Schreiben Sie ein Programm, welches Sie nach Lebensmittel und den zugehörigen Preisen fragt und diese so lange einem Dictionary hinzufügt,
# bis der Benutzer x eingibt. Im Anschluss soll das Dictionary ausgegeben
# werden.

einkauf={}

while True:
    item=input("Lebensmittel oder 'x' zum beenden ")
    if item == "x":
        break
    else:
        price=input("Preis ")
        einkauf[item] = float(price)
        
        
print("Dictionary:", einkauf)