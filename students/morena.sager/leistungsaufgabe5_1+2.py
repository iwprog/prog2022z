# Leistungsaufgabe 5 - Aufgabe 1 und 2
from urllib.request import urlopen
from csv import writer

with urlopen("https://www.ietf.org/rfc/rfc2616.txt") as s:
    web_content = s.read().decode("utf8")
    
text=web_content.split()
wortliste={}
buchstabenliste={}


for wort in text:
    if wort in wortliste:
        wortliste[wort] = wortliste[wort] + 1
    else:
        wortliste[wort] = 1
        
for wort in text:
    for buchstabe in wort:
        if buchstabe.lower() in buchstabenliste:
            buchstabenliste[buchstabe.lower()] = buchstabenliste[buchstabe.lower()] + 1
        else:
            buchstabenliste[buchstabe.lower()] = 1
        

with open("wort.csv", "w", encoding="utf8") as woerter:
    csv_writer = writer(woerter, delimiter=";")
    for wort, wert in wortliste.items():
        csv_writer.writerow([wort,wert])
        
with open("buchstabe.csv", "w", encoding="utf8") as buchstabe:
    csv_writer = writer(buchstabe, delimiter=";")
    for wort, wert in sorted(buchstabenliste.items()):
        csv_writer.writerow([wort,wert])
        
        

    