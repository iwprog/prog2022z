#Leistungsaufgabe 2a

i=1
summe=0 
def berechne_preis():
    if summe < 100:
        return round(summe,2)
    elif summe >= 100 and summe < 1000:
        return round(summe*0.95,2)
    elif summe >= 1000:
        return round(summe*0.9,2)
    
while True:
    eingabe = input("Geben Sie den {}. Preis ein: ".format(i))
    i=i+1
    if eingabe == "x":
        break
    elif eingabe != "x":
        summe=summe+float(eingabe)
    
if summe < 100:
    print("Kein Rabatt, Gesamtpreis =", berechne_preis(),"CHF")
elif summe >= 100 and summe < 1000:
    print("5% Rabatt, Gesamtpreis =", berechne_preis(), "CHF")
elif summe >= 1000:
    print("10% Rabatt, Gesamtpreis =", berechne_preis(), "CHF")