#Leistungsaufgabe 2.b

def zahlenreihe(zahl):
    print(zahl,"er Reihe:")
    for mal in range(1,11):
        summe=zahl*mal
        print(zahl,"x","=",summe)
    print("------------------")
    
for zahl in range(1,11):
    zahlenreihe(zahl)