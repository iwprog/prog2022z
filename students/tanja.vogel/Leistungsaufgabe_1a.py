#Leistungsaufgabe 1
# weisses Kreuz auf rotem Grund, Schweizer Fahne, keine Grössen vorgabe
from turtle import *

#zuerst mit Rot zeichnen; Hintergrund der Flagge
pencolor("red")
fillcolor("red")
#nach Bestimmung der Parameter, füllen des "Hintergrunds"
begin_fill()
forward(200)
left(90)
forward(200)
left(90)
forward(200)
left(90)
forward(200)
end_fill()
#Hintergrund vorhanden, nun weiter zum weissen Kreuz
#zuerst Turtle richtig platzieren
left(90)
forward(80)
left(90)
forward(40)
#nun Farbe wechseln von rot auf weiss, aber nur die Füllung
fillcolor("white")
begin_fill()
#Kreuz unter vier Teilen zeichnen
def kreuz_teil():
    forward(40)
    left(90)
    forward(40)
    right(90)
    forward(40)
    right(90)
#Diese Funktion muss nun viermal ausgeführt werden.
kreuz_teil()
kreuz_teil()
kreuz_teil()
kreuz_teil()
end_fill()
#danach noch den Turtle verstecken
hideturtle()