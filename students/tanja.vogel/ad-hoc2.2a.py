#ad-hoc 2.2
 
from turtle import *

def dreieck():
    laenge = numinput("Seitenlänge Dreieck", "Bitte geben Sie die Seitenlänge an:")
    begin_fill()
    forward(laenge)
    left(120)
    forward(laenge)
    left(120)
    forward(laenge)
    end_fill()
    
pencolor("red")
pensize(5)
speed(8)
fillcolor("cyan")
right(90)
dreieck()

fillcolor("yellow")
dreieck()

fillcolor("lime")
dreieck()