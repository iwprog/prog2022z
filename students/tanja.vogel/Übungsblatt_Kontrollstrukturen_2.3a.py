#Schreiben Sie eine Funktion, welche einen Namen entgegennimmt und anschliessend
#den Text Guten Morgen Name! zurückgibt
def guten_morgen(name):
    print("Guten Morgen", name + "!")
    
def gruss(name):
    gruss = guten_morgen(name)
    return gruss

name = input("Wie heissen Sie?")

gruss(name)