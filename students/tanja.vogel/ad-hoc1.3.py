# Ad-hoc 1.3 a

from turtle import *

right(90)
pensize(5)
pencolor("red")
fillcolor("cyan")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

fillcolor("yellow")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

fillcolor("lightgreen")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()