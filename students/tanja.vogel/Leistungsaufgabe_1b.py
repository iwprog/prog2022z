#Leistungsaufgabe 1
# weisses Kreuz auf rotem Grund, Schweizer Fahne, keine Grössen vorgabe
#Erweiterung um Eingabemöglichkeit für Seitenlänge
from turtle import *

pencolor("red")
fillcolor("red")

#Seitenlängen von 1a, irrelevant
#Variabeln anstatt fixe Zahlen
laenge = numinput("Seitenlänge", "Bitte gewünschte Seitenlänge der Flagge eingeben!")
begin_fill()
forward(int(laenge))
left(90)
forward(int(laenge))
left(90)
forward(int(laenge))
left(90)
forward(int(laenge))
end_fill()


left(90)
forward(80)
left(90)
forward(40)

fillcolor("white")
begin_fill()

def kreuz_teil():
    forward(40)
    left(90)
    forward(40)
    right(90)
    forward(40)
    right(90)

kreuz_teil()
kreuz_teil()
kreuz_teil()
kreuz_teil()
end_fill()

hideturtle()