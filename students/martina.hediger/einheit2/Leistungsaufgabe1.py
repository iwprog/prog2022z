#Leistungsaufgabe 1a und b, 27.2. Martina Hediger

from turtle import *
hideturtle()
speed(8)
#Definition der Ecke. Am Schluss sollen sich vier rote Ecken zusammensetzen

laenge_flagge = int(numinput("Eingabefenster", "Wie lang soll eine Seite der Flagge sein?"))

def ecke():
    pencolor("red")
    fillcolor("red")
    begin_fill()
    forward(laenge_flagge*0.4) #entspricht bei 400 der 160
    left(90)
    forward(laenge_flagge*0.4)
    left(90)
    forward(laenge_flagge*0.2)
    right(90)
    forward(laenge_flagge*0.2)
    left(90)
    forward(laenge_flagge*0.2)
    left(90)
    forward(laenge_flagge*0.6)
    end_fill()
    
def rotation(): #damit die nächste Ecke, korrekt anfängt
    left(180)
    forward(laenge_flagge)
    left(180)

#Befehele zur Aufürung des Schweizer Kreuzes
ecke()
rotation()
ecke()
rotation()
ecke()
rotation()
ecke()
