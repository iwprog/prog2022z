#ad hoc Übung 2.4a Martina Hediger 27.2.22

from math import pi

radius = float(input("Wie gross ist der Radius in (cm)?"))
hoehe = float(input("Wie hoch ist der Zylinder (cm)?"))

def zylinder(radius, hoehe):
    kreisflaeche = 2 * radius * pi
    volumen = kreisflaeche * hoehe
    return "Das Volumen beträgt:" + str(volumen) + " cm"

print(zylinder(radius, hoehe))
