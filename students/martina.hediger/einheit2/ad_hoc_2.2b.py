#ad hoc Übung 2.2b Martina Hediger

from turtle import *

laenge = numinput("Dreiecklänge", "Seitenlänge des Dreiecks")

def dreieck(laenge, color, bgcolor): #name der Variablen kann ich selber setzen. Sinnvolle und sprechende Namen nehmen. Niemals Variablennamen verwenden, die einem Befehl entsprechen
    pencolor(color)
    fillcolor(bgcolor)
    begin_fill()
    forward(laenge)
    left(-120)
    forward(laenge)
    left(-120)
    forward(laenge)
    end_fill()

pensize(5)
left(210)
speed(8) #damit schneller gezeichnet wird
dreieck(laenge * 3, "red", "yellow")
dreieck(laenge * 2, "red", "cyan")
dreieck(laenge, "red", "lightgreen")