#Konvertierung zwischen Datentypen, 27.2.22, Martina Hediger, Aufgabe 2.3 a

laenge = int(input("Länge des Rechtecks?"))
breite = int(input("Breite des Rechtecks?")) #hier muss ich float() oder int() nehmen, damit der Input als Zahl hinterlegt wird, Alternative zu numinput

def flaeche_rechteck(laenge, breite):
    flaeche = laenge * breite
    return "Die Fläche beträgt " + str(flaeche) + " m2" #hier brauche ich str(), weil ich Text und Zahlen nicht zusammenzählen kann

print(flaeche_rechteck(laenge, breite))
