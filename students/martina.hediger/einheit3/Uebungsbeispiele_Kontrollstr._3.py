# übungsbeispiele zu funktionen und kontrollstrukturen, aufgabe 3 1.3.22, Martina Hediger
# Schreiben Sie eine Funktion welche basierend auf dem Alter und der
# Entfernung den Preis fur ein SBB Billet berechnet:
# jedes Ticket verfugt uber einen Grundpreis von 2 CHF, der unabh
# ängig von der Entfernung berechnet wird.
# zusätzliche werden 0.25 CHF / km berechnet.
# Kinder unter 16 Jahren zahlen den halben Preis.
# Kinder unter 6 Jahren reisen kostenlos.

from math import *

alter = int(input("Wie alt sind Sie?"))
entfernung_km = int(input("Wie lange ist die Fahrtstrecke (km)"))

def get_billet_preis(alter, entfernung_km):
    grundpreis_chf = 2.00
    preis_fuer_entfernung = 0.25 * entfernung_km
    betrag = grundpreis_chf + preis_fuer_entfernung
    if 6 <= alter < 16:
        halberpreis = betrag / 2
        print("Preis:", halberpreis)
    elif alter < 6:
        print("Preis: 0")
    else:
        alter >= 16
        print("Preis:", betrag)
        
get_billet_preis(alter, entfernung_km)