#ad hoc 2.8, Martina Hediger, 6.3.22

from random import randint

liste = randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100), randint(1, 100)
print("Nummern in der Zahlenreihe: ", liste)

max_wert = max(liste)  # max() -> um die grösste Zahl zu ermitteln
print("Maximum: ", str(max_wert))

min_wert = min(liste)
print("Minimum: ", str(min_wert))

#print(len(liste)) gibt die Anzahl Zahlen in der liste aus.

durchschnitt = sum(liste)/len(liste) #sum() berechnet die summe
print("Durchschnitt: ", str(durchschnitt))
