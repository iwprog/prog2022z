# übungsbeispiele zu funktionen und kontrollstrukturen, aufgabe 11e, 6.3.22, Martina Hediger


count = 0
while count < 1:
    print("# # # # # # #")
    count = count + 1
while count < 6:
    print("#           #")
    count = count + 1
while count == 6:
    print("# # # # # # #")
    count = count + 1

