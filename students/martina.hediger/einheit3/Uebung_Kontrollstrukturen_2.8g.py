# #Übung Grundlagen Kontrollstrukturen 2.8g, Martina Hediger, 1.3.22
# g. Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der
# Benutzer -1 eingibt und anschliessend die Summe dieser Zahlen ausgibt.
# Bitte geben Sie Zahl 1 ein: 2
# Bitte geben Sie Zahl 2 ein: 3
# Bitte geben Sie Zahl 3 ein: 7
# Bitte geben Sie Zahl 4 ein: -1
# Summe: 12

eingabe = input("Bitte geben Sie Zahl 1 ein: \n") #\n verursacht einen Zeilenumbruch, damit es gleich aussieht, wie bei den anderen abfragen
count = 2
summe = 0
while eingabe != "-1":
    for nummerieren in "ein: ",:
        print("Bitte geben Sie Zahl", str(count), str(nummerieren)) # damit die Nummer im Fragetext nummeriert ist
        eingabe = input("") #die Nummerierung in input hat nicht geklappt. Gibt es da eine Möglichkeit?
        count = count + 1 #zählt, bis -1 eingegeben wird. 
    zahl = int(eingabe)
    summe = summe + zahl
     
print("Summe:", str(summe))