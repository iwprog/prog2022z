#ad hoc 2.6a, Martina Hediger, 6.3.22

# Schreiben sie ein Programm, das 6 Zufallszahlen zwischen 1 und 42 ausgibt
# Zufallszahlen werden in Python wie folgt generiert: 
# from random import randint
# zufahlszahl = randint(kleinster_wert, höchster_wert)

from random import randint

# zufallszahl = randint(1, 42) #wenn ich die Variable hier habe, kommt in der While schlaufe, immer dieselbe zahl

count = 1
while 1 <= count <=6:
    zufallszahl = randint(1, 42)
    print(str(count) + ". Zufallszahl: " + str(zufallszahl)) # texte zusammenfügen mit + oder komma
    count = count + 1
