#übungsbeispiele zu funktionen und kontrollstrukturen, aufgabe 2, 1.3.22, Martina Hediger
# Schreiben Sie eine Funktion, welche das Alter einer Person entgegennimmt
# und daraus die theoretische maximale Herzfrequenz berechnet.
# Die Formel fur die Berechnung der Herzfrequenz lautet:

from math import *

# def berechne_herzfrequenz():
#     alter = int(input("Wie alt sind Sie?"))
#     ergebnis = 220 - alter
#     print(ergebnis)
#     
# berechne_herzfrequenz()

#gemäss Aufgabenstellung müsste der Code wohl so aussehen: 

alter = int(input("Wie alt sind Sie?"))
def berechne_herzfrequenz(alter):
    ergebnis = 220 - alter
    print(ergebnis)
    
berechne_herzfrequenz(alter)
    