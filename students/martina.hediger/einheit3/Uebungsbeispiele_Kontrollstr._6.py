# übungsbeispiele zu funktionen und kontrollstrukturen, aufgabe 6, 1.3.22, Martina Hediger
# Schreiben Sie eine Funktion welche einen Bilderrahmen mit einer bestimmten
# Länge und Breite unter Zuhilfenahme von Schleifen zeichnet.
# Weiters soll die Funktion noch die Rahmen- und Fullfarbe entgegennehmen.

from turtle import *

laenge = numinput("Fenster", "Wie lange soll der Rahmen sein?")
breite = numinput("Fenster", "Wie breit soll der Rahmen sein?")
rahmenfarbe = textinput("Fenster", "Welche Farbe soll der Rahmen haben?")
fuellfarbe = textinput("Fenster", "Welche Füllfarbe soll der Rahmen haben?")

def zeichne_rahmen(laenge, breite, rahmenfarbe, fuellfarbe):
    pensize(3)
    pencolor(rahmenfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    count = 0
    while count < 4:
        forward(laenge)
        left(90)
        count = count + 1
    end_fill()

    forward(breite)
    left(90)
    penup()
    forward(breite)
    right(90)
    pendown()
    
    pencolor(rahmenfarbe)
    fillcolor("white")
    begin_fill()
    count = 0
    while count < 4:
        forward(laenge - breite - breite)
        left(90)
        count = count + 1
    end_fill()
    
speed(8)
zeichne_rahmen(laenge, breite, rahmenfarbe, fuellfarbe)    
