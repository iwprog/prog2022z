#Übung Grundlagen Kontrollstrukturen 2.8f, Martina Hediger, 1.3.22
# Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis
# deren Summe grösser als 10 ist. Im Anschluss soll die Summe ausgegebenwerden.

count = 1
summe = 0
while summe < 11:
    for nummerieren in "ein: ",:
        print("Bitte geben Sie Zahl", str(count), str(nummerieren)) # damit die Nummer im Fragetext nummeriert ist
        eingabe = input("") #die Nummerierung in input hat nicht geklappt. Gibt es da eine Möglichkeit?
        count = count + 1 #zählt, bis die Summe grösser als 10 ist
    zahl = int(eingabe)
    summe = summe + zahl
     
print("Summe:", str(summe))