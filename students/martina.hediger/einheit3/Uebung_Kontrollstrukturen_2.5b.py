#Übungsblatt Grundlagen Kontrollstrukturen

# 2.5 Verzweigungen
# a. Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach
# Alter den Wert volljahrig oder minderjahrig zuruckgibt.
# age = 22
# print("Mit", age, "ist man", legal_status(age) + ".")
# # Ausgabe: Mit 22 ist man volljahrig.
# b. Erweitern Sie die Funktion, sodass diese zwischen geschaftsunfahig (bis
# inklusive 6 Jahre), unmundig (bis inklusive 14 Jahre), mundig minderjahrig
# und volljahrig unterscheidet. Weiters soll fur ein negatives Alter ungeboren
# zuruckgegeben werden.
# age = 5
# print("Mit", age, "ist man", legal_status(age) + ".")
# # Ausgabe: Mit 5 ist man geschaftsunfahig.

eingabe = int(input("Wie alt bist du?"))

def legal_status():
    if 0 <= eingabe <= 6 :
        return "geschäftsunfähig"
    elif 6 < eingabe <= 14:
        return "unmündig"
    elif 15 <= eingabe < 22:
        return "mündig minderjährig"
    elif eingabe >= 22:
        return "volljährig"
    elif eingabe < 0:
        return "ungeboren"
   
print("Mit", eingabe, "ist man", legal_status() +".")