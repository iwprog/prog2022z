#ad hoc 2.10 Martina Hediger 28.2.22
# e) ad-hoc übung 2.10 erweitert: basierend auf einem muster sollen quadrate, dreiecke und kreise gezeichnet werden.
# dabei steht ein "d" für ein dreieck, ein "o" für einen kreis und eine "#" für ein quadrat.
# das muster "##ddoo" zeichenet als zum beispiel: quadrat - quadrat - dreieck - dreieck - kreis - kreis.

from turtle import *
speed(8)
laenge=50
abstand=50
radius=50
winkel=360

def dreieck(laenge, abstand):
    for dseiten in range(3):
        forward(laenge)
        left(120)
    forward(laenge) 
    penup()
    forward(abstand)
    pendown()
    
def quadrat(laenge):
    for qseiten in range(4):
        forward(laenge)
        left(90)
    forward(laenge) 
    penup()
    forward(abstand)
    pendown()
    
def kreis(radius, winkel):
    circle(radius, winkel)
    penup()
    forward(abstand)
    pendown()

muster = textinput("Eingabefenster", "Geben Sie ein Muster mit den Zeichen #, d und o ein")
anzahl= int(len(muster))
print("dein Muster: ", muster)

count = 0
for i in muster:
    if count <= anzahl:
        if "d" in muster:
            dreieck(laenge, abstand)
            count = count + 1
        if "o" in muster:
            kreis(radius, winkel)
            count = count + 1
        if "#" in muster:
            quadrat(laenge)
            count = count + 1
#die Reihenfolge der Eingabe klappt nicht immer
    