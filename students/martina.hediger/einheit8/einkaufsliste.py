#einkaufsliste
#Martina Hediger 10.04.22

from json import loads, dumps
from csv import writer

def fileladen(einkaufsliste): 
    try: 
        with open("einkaufsliste.json") as f:
            einkaufsliste_str= f.read()
            einkaufsliste = loads(einkaufsliste_str)
            return einkaufsliste
    except FileNotFoundError:
        print("Leider keine gespeicherte Einkaufsliste vorhanden")
        einkaufsliste = []
        
def filespeichern(einkaufsliste):
    with open("einkaufsliste.json", "w") as f:
        einkaufsliste_str = dumps(einkaufsliste)
        f.write(einkaufsliste_str)
    
def artikel_hinzufügen(einkaufsliste):
    while True:
        artikel = input("Artikel: (abbrechen mit x)")
        if artikel == "x":
            break
        preis = input("Preis: ")
        menge = input("Menge: ")
        datensatz = [artikel, preis, menge]
        einkaufsliste.append(datensatz)
    return einkaufsliste
        
def artikel_löschen(einkaufsliste):
    while True:
        print(einkaufsliste)
        artikel = input("Welchen Artikel löschen? (abbrechen mit x)")
        if artikel == "x":
            break
        neue_liste = []
        for item in einkaufsliste:
            if artikel not in item:
                neue_liste.append(item)
        return neue_liste              
def artikel_suchen(einkaufsliste):
    while True:
        wort = input("Welchen Artikel suchen? (abbrechen mit x)")
        ausgabe = []
        if wort == "x":
            break
        for artikel, preis, menge in einkaufsliste:
            if wort in artikel: #wieso kann ich lower hier nicht verwenden?
                datensatz = artikel, preis, menge
                ausgabe.append(datensatz)
        return ausgabe
#         if wort not in artikel:
#             return "Artikel nicht in der Einkaufsliste gefunden"

def einkaufsliste_leeren(einkaufsliste):
    frage = input("Einkaufsliste wirklich leeren? (J / N)")
    if frage == "J":
        einkaufsliste = []
        return einkaufsliste
    elif frage == "N":
        einkaufsliste = einkaufsliste
        
def csv_exportieren(einkaufsliste):
    with open("einkaufsliste.csv", "w", encoding="utf8") as f:
        csv_writer = writer(f, delimiter=",")
        csv_writer.writerows(einkaufsliste)
    print("csv erfolgreich exportiert")
def gesamtbetrag_berechnen(einkaufsliste):
    summe = float(0)
    for artikel, preis, menge in einkaufsliste:
        summe = summe + (float(preis)*float(menge))
    return summe
def menu(einkaufsliste):
    while True:
        print("---------------------------------------------")
        print("Optionen: ")
        print("(1) artikel hinzufügen ")
        print("(2) artikel löschen ")
        print("(3) artikel suchen ")
        print("(4) einkaufsliste leeren ")
        print("(5) einkaufsliste im csv format exportieren ")
        print("(6) gesamtbetrag einkauf berechnen ")
        print("(0) exit ")
        option = input("Welche Funktion ausführen?")
        print("---------------------------------------------")
        if option == "1":
            einkaufsliste = fileladen(einkaufsliste)
            artikel_hinzufügen(einkaufsliste)
            filespeichern(einkaufsliste)
        elif option == "2":
            einkaufsliste = fileladen(einkaufsliste)
            einkaufsliste = artikel_löschen(einkaufsliste)
            filespeichern(einkaufsliste)
        elif option == "3":
            einkaufsliste = fileladen(einkaufsliste)
            print(artikel_suchen(einkaufsliste))
        elif option == "4":
            einkaufsliste = fileladen(einkaufsliste)
            einkaufsliste = einkaufsliste_leeren(einkaufsliste)
            filespeichern(einkaufsliste)
        elif option == "5":
            einkaufsliste = fileladen(einkaufsliste)
            csv_exportieren(einkaufsliste)
        elif option == "6":
            einkaufsliste = fileladen(einkaufsliste)
            print("Gesamtbetrag Einkauf: ", gesamtbetrag_berechnen(einkaufsliste))
        elif option == "0" or "x":
            break
einkaufsliste = []
menu(einkaufsliste)
