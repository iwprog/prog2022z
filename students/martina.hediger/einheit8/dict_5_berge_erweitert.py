# dicts.pdf beispiel 5 (berge): erweitern sie das programm aus der letzten hausübung,
#sodass es sich die bereits eingegebenen berge merkt. 
# #zu programmende sollen die berge in der datei "berge.json" gespeichert werden.    
# #wenn "berge.json" existiert, sollen die berge aus der datei eingelesen werden.
# #

from json import loads, dumps

try: 
    with open("berge.json") as f:
        berge_str= f.read()
        berge = loads(berge_str)
except FileNotFoundError:
    print("Leider keine gespeicherte berge vorhanden")
    berge = {}
    
### besprochene Lösung von Beispiel 5###
for i in range(3):
    berg = input('Berg: ')
    hoehe = float(input("Höhe von " + berg + " (in m): "))
    berge[berg] = str(hoehe) + " m (" + str(round(hoehe * 3.28)) + 'ft)'
    #                 1000 m        (1640 ft)
    # berge[berg] = hoehe -> in Dict Berge berg = Wert hoehe setzen
print(berge)# macht einen Absatz

for berg, hoehe in sorted(berge.items()):
    print(berg, 'ist', hoehe, 'hoch.')

with open("berge.json", "w") as f:
    berge_str = dumps(berge)
    f.write(berge_str)