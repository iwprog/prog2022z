#Leistungsaufgabe5 Wortstatistiken
#Martina Hediger
from urllib.request import urlopen
from csv import writer

anzahl_wort = {}
anzahl_buchstabe = {}

ressource = input("Geben Sie die URL der Webdatei oder den Dateinamen an: ")

if "https://" in ressource: 
    with urlopen(ressource) as f:
        web_content = f.read().decode("utf8")
        text = web_content.replace("("," ").replace(")"," ").replace("."," ").replace("!", " ").replace(","," ").replace("?"," ").replace("'\n\n\n\n\n\n"," ")
        for wort in text.split(" "):
            if wort in anzahl_wort:
                anzahl_wort[wort] = anzahl_wort[wort] + 1
            else: 
                anzahl_wort[wort] = 1
            for buchstabe in wort:
                if buchstabe in anzahl_buchstabe:
                    anzahl_buchstabe[buchstabe] = anzahl_buchstabe[buchstabe] + 1
                else:
                    anzahl_buchstabe[buchstabe] = 1
else:
    try:
        with open(ressource, encoding='utf8') as f:
            content = f.read()
            text = content.replace("("," ").replace(")"," ").replace("."," ").replace("!", " ").replace(","," ").replace("?"," ").replace("'\n\n\n\n\n\n"," ")
            for wort in text.split(" "):
                if wort in anzahl_wort:
                    anzahl_wort[wort] = anzahl_wort[wort] + 1
                else: 
                    anzahl_wort[wort] = 1
                for buchstabe in wort:
                    if buchstabe in anzahl_buchstabe:
                        anzahl_buchstabe[buchstabe] = anzahl_buchstabe[buchstabe] + 1
                    else:
                        anzahl_buchstabe[buchstabe] = 1
    except FileNotFoundError:
        print("Datei " + ressource + "nicht vorhanden")

with open("wort.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for element in anzahl_wort.items():
        csv_writer.writerow(element)

with open("buchstabe.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for element in anzahl_buchstabe.items():
        csv_writer.writerow(element)
