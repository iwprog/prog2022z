#Martina Hediger, 14.2.22, Übung 1.2c - noch nicht fertig

from turtle import *

pencolor("blue")
pensize(5)
left(120)
forward(100) #Zahl sagt, wie viele Punkte vorwärtsgehen
left(120) #Zahl sagt um wie viele Grad drehen
forward(100)
left(120)
forward(100)

pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)

pencolor("light green")
forward(100)
left(120)
forward(100)
left(120)
forward(100)