#übungsblatt grundlagen datentypen, 1.1. a-d, Martina Hediger, 13.3.22

#a. Erstellen Sie eine Liste jahreszeiten mit den vier Jahreszeiten

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
print("Aufg. a: ", jahreszeiten)

# bLöschen Sie die Jahreszeit Frühling aus der Liste.
del jahreszeiten[0]
print("Aufg. b: ", jahreszeiten)

#c Fügen Sie die Jahreszeit Langas der Liste hinzu.
#geht nicht, weil an der Stelle 0 neu der Sommer steht: 
# jahreszeiten[0] = "Langas"
# print(jahreszeiten)

jahreszeiten.append("Langas")
print("Aufg. c: ", jahreszeiten)


#d Schreiben Sie ein Programm, welches Sie nach Namen fragt und diese so
# lange einer Liste hinzufügt, bis der Benutzer x eingibt. Im Anschluss soll
# die Liste ausgegeben werden.
# Name: Martin
# Name: Julia
# Name: x
# Liste: ["Martin", "Julia"]

name = input("Name: ")
liste = []

while name !="x": 
    liste.append(name)
    name = input("Geben Sie einen Namen ein: ")
print("Liste: ", liste)















