#ad hoc 2.10b Martina Hediger 8.3.22

from turtle import *
speed(8)
laenge=100
abstand=20

def dreieck(laenge, abstand):
    for dseiten in range(3):
        forward(laenge)
        left(120)
    forward(laenge) 
    penup()
    forward(abstand)
    pendown()
    
anzfig = int(numinput("Fenster", "Wie oft soll das Dreieck gezeichnet werden?"))

for zeichnung in range(anzfig):
    dreieck(laenge, abstand)