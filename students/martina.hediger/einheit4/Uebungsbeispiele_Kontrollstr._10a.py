#übungsblatt zu funktionen und kontrollstrukturen, Aufg. 10a, Martina Hediger, 8.3.22

def get_billet_preis():
    alter = int(input("Bitte geben Sie Ihr Alter ein: "))
    entfernung_km = int(input("Wie viele km wollen Sie reisen? "))
    grundpreis_chf = 2.00
    preis_fuer_entfernung = 0.25 * entfernung_km
    betrag = grundpreis_chf + preis_fuer_entfernung
    if 6 <= alter < 16:
        halberpreis = betrag / 2
        return halberpreis 
    elif alter < 6:
        return 0
    else:
        alter >= 16
        return betrag

def berechnet_eintrittspreis(karte, ermaessigung, alter):
    if alter <= 6:
        return 0
    if 6 < alter <= 12:
        return karte / 2
    if ermaessigung == True:
        return karte * 0.9
    else:
        return karte

def berechne_mahngebühr(mahnung, medien, versandart):
    if versandart == True:
        versandart = 2.00
    if mahnung == 1:
        return medien * 2.00 + versandart
    if mahnung == 2:
        return medien * 4.00 + versandart
    if mahnung == 3:
        return medien * 6.00 + versandart

eingabe = None


while eingabe !=0: 
    print("==============================")
    print("Programmübersicht:")
    print("1 ... Preis für eine Fahrkarte berechnen")
    print("2 ... Eintritt für das Schwimmbad berechnen")
    print("3 ... Mahngebühr fürr die Bibliothek berechnen")
    print("0 ... Programm beenden")
    print("")
    print("==============================")
    print("")
    
    eingabe = input("Geben Sie die Zahl ein: ")
    print("Gewählte Option: ", eingabe)
           
    for zähler in eingabe:
        if "1" == zähler:
            print("Preis für die Fahrkarte berechnen")
            print("---------------------------------")
            preis = str(get_billet_preis())
            print("Die Fahrkarte kostet: ", preis , "CHF")
        if "2" == zähler:
            print("Eintritt für das Schwimmbad berechnen")
            print("-------------------------------------")
            alter = int(input("Wie alt sind Sie?"))
            ermaessigung = input("Haben Sie eine Ermässigungskarte? ")
            if ermaessigung == "ja":
                ermaessigungskarte = True
            if ermaessigung == "nein":
                ermaessigungskarte = False

            karte = input("Möchten Sie eine kurzzeitkarte, nachmittagskarte oder tageskarte? ")
            if karte == "kurzzeitkarte":
                karte = int(5)
            if karte == "nachmittagskarte":
                karte = int(6)
            if karte == "tageskarte":
                karte = int(10)

            berechnet_eintrittspreis(karte, ermaessigung, alter)
            print("Eintrittspreis Schwimmbad: ", berechnet_eintrittspreis(karte, ermaessigungskarte, alter))
        if "3" == zähler:
            print("Mahngebühr für die Bibliothek berechnen")
            print("---------------------------------------")
            mahnung = int(input("Welcher Mahnlauf ist es? 1, 2 oder 3? "))
            medien = int(input("Wie viele Medien werden gemahnt? "))
            versandart = input("Wird die Mahnung per Post versandt? ")
            if versandart == "ja":
                versandart = True
            if versandart == "nein":
                versandart = False
            mahngebühr = berechne_mahngebühr(mahnung, medien, versandart)
            print("Mahngebühr für die Bibliothek: ", mahngebühr)
        if "0" == zähler:
            break
        if zähler > "3":
            print("Ungültige Option")

#Warum wird bei Eingabe einer 10, die Option 1 Ausgegeben?