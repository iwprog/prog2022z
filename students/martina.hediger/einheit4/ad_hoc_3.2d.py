#ad hoc Übung 3.2d, Martina Hediger, 8.3.22

text="Ananas ist eine exotische Frucht"
position=5 #es sind Positionen von 0 bis 32 möglich
einfügetext="saft"

def replace(text, position, einfügetext):
    print("text: ", text)
    print("position: ", position)
    print("einfügetext: ", einfügetext)
    print("Ergebnis: ", text[:position] + einfügetext + text[position+1:])

replace(text, position, einfügetext)

