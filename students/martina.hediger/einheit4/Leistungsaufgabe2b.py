#Leistungsaufgabe 2a Martina Hediger, 13.3.22
#Schreiben Sie ein Programm, welches das 1x1 nach folgendem Muster ausgibt:

for reihe in range(1,11):
    print(str(reihe) + "er Reihe:")
    for zähler in range (1,11):
        multiplikation = zähler * reihe
        print(zähler, "x", reihe, "=", str(multiplikation))
    print("-" * 10)