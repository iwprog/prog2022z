#ad hoc 2.10c Martina Hediger 8.3.22
#Erstellen Sie eine Programmversion, in welcher der Benutzer angeben kann (i) wie viele Dreiecke pro Reihe und ii in wie vielen Reihen Dreiecke gezeichnet werden sollen

from turtle import *
speed(8)
laenge=40
abstand=10

def dreieck(laenge, abstand):
    for dseiten in range(3):
        forward(laenge)
        left(120)
    forward(laenge) 
    penup()
    forward(abstand)
    pendown()
    
anzfig = int(numinput("Fenster", "Wie oft soll das Dreieck in der Reihe gezeichnet werden?"))
anzreihe = int(numinput("Fenster", "Wie viele Reihen mit Dreiecken sollen gezeichnet werden?"))

#anzahl durchläufe:
for repetition in range(anzreihe):
    #wie oft soll das dreieck gezeichnet werden: 
    for zeichnung in range(anzfig):
        dreieck(laenge, abstand)
    #das eröffnet eine neue Reihe:
    penup()
    hideturtle()
    back((laenge+abstand)*anzfig)
    right(90)
    forward(laenge)
    left(90)
    showturtle()
    pendown()
