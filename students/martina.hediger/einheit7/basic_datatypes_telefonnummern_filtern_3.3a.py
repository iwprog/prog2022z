# basic-datatypes.pdf, 3.2a datensatz ausgeben
# Martina Hediger, 2.4.22
# 3.3 Listen von Dictionaries filtern
# a. Entfernen Sie alle Datensätze aus dem Telefonbuch, bei denen eine 1 in # der Telefonnummer vorkommt.

telefonbuch = []
data1 = {"Vorname": "Ana", "Nachname": "Skupch", "Phone": 123}
data2 = {"Vorname": "Tim", "Nachname": "Kurz", "Phone": 732}
data3 = {"Vorname": "Julia", "Nachname": "Lang", "Phone": 912}
telefonbuch.append(data1)
telefonbuch.append(data2)
telefonbuch.append(data3)

for data in telefonbuch: # gibt die datensätze einzeln aus.
    for key, value in data.items(): #packt die datensätze (dict) aus. erzeugt Tupel.
        if "1" in str(value):
            telefonbuch.remove(data)
print(telefonbuch)