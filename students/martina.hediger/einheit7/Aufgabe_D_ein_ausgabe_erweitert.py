# d) öffnen sie die folgende url (https://www.gutenberg.org/cache/epub/6079/pg6079.txt) in ihrem web browser und speichern sie die
# zugehörige textdatei in ihrem python verzeichnis. schreiben sie nun ein python programm, das 
#- diese auf dem bildschirm ausgibt und ermittelt wie oft die worte "Himmel", "Freiheit", "Spiel" und "Tanz" in der datei vorkommen.
# (hinweis: es ist wichtig, dass sich ihr python programm und die textdatei im selben verzeichnis befinden, da ansonsten python die datei nicht öffnen kann).
#- erweitern Sie Ihr Python Programm, sodass es die Datei direkt aus dem Web bezieht, sodass das manuelle Speichern der Datei entfallen kann.

from urllib.request import urlopen
from inscriptis import get_text

with urlopen('https://www.gutenberg.org/cache/epub/6079/pg6079.txt') as source:
    # Achtung: es ist notwendig die Daten mittels decode in Strings zu konvertieren
    web_content = source.read().decode('utf8')
    file_content = get_text(web_content)
    print(file_content)

file_content = file_content.split()

suchworte = {"himmel": 0, "freiheit": 0, "spiel": 0, "tanz": 0, "poesie": 0}

for wort in file_content:
    if wort.lower() in suchworte.keys():
        suchworte[wort.lower()] = suchworte[wort.lower()] + 1
print("Häufigkeit Vorkommen Suchworte: ", suchworte)




