#dicts.pdf Aufgabe 7. Text schwärzen
#Martina Hediger, 2.4.22

eingabetext = "David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen"
eingabetext = eingabetext.split()

blacklist = ["david", "mayer", "htw", "chur"]
ausgabe = []

for wort in eingabetext:
    if wort.lower() in blacklist:
        ausgabe.append(wort.replace(wort, "*" * len(wort))) #wort in Blacklist -> durch sterne ersetzen und zur ausgabe hinzufügen
    else:
        ausgabe.append(wort) #wort unverändert zur ausgabeliste hinzufügen. 
print(ausgabe)


