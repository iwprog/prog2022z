#Leistungsaufgabe 3
#Martina Hediger, 3.4.22

# a)
liste = []
datensatz = []

while True:
    eingabe_stadt = input("Geben Sie die Stadt ein: ")
    if eingabe_stadt != "x":
        eingabe_zahl = input("Geben Sie die zugehörige Einwohnerzahl ein: ")
        datensatz = [eingabe_stadt, eingabe_zahl]
        liste.append(datensatz)
    else:
        break
print(sorted(liste)) #alphabetisch sortierte ausgabe

# b) Mini-Deutsch-Englisch Wörterbuch (10 Vokabeln).

wörterbuch = {"Haus": "house", "Zahnarzt": "dentist", "Stift": "pen", "Tisch": "table", "Tür": "door", "Pflanze": "plant", "Blume": "flower",
              "Baum": "tree", "Wald": "forest", "See": "lake"}
statistik = 0
for abfragewort in wörterbuch.keys():
    frage = input("Was heisst "+ abfragewort + ":")
    if frage == wörterbuch[abfragewort]:
        print("RICHTIG!")
        statistik = statistik + 1
    elif frage == "x":
        break
    else:
        print("FALSCH!")
        statistik = statistik
print("Sie haben ", statistik, "von", len(wörterbuch), "Vokabeln richtig beantwortet!") 
            
        