# basic-datatypes.pdf, 3.1 a, b, c, d (telefonbuch datenstruktur und dateneingabe)  
# Martina Hediger, 2.4.22

# Erstellen Sie ein Dictionary für den folgenden Datensatz: Vorname ->Ana, Nachname -> Skupch, Phone -> 123.
data1 = {"Vorname": "Ana", "Nachname": "Skupch", "Phone": 123}
print("Datensatz von Aufgabe a: ", data1)
# b. Fügen Sie den Datensatz der Liste Telefonbuch hinzu.
telefonbuch = []
telefonbuch.append(data1)
print("Liste von Aufgabe b: ", telefonbuch)
# c. Fügen Sie die folgenden beiden Datensätze der Liste hinzu: Vorname !# Tim, Nachname ! Kurz,
#Phone ! 732; Vorname ! Julia, Nachname # ! Lang, Phone ! 912
data2 = {"Vorname": "Tim", "Nachname": "Kurz", "Phone": 732}
data3 = {"Vorname": "Julia", "Nachname": "Lang", "Phone": 912}
telefonbuch.append(data2)
telefonbuch.append(data3)
print("Liste von Aufgabe c: ", telefonbuch)
# d. Schreiben Sie ein Programm, welches Personendaten abfragt und diese so lange in Ihr Telefonbuch
#einfügt, bis der Benutzer x eingibt. Im Anschluss soll die erstellte Datenstruktur ausgegeben werden.
data = {}
eingabe_v = {}
eingabe_n = {}
eingabe_p = {}
while True:
    eingabe = input("Vorname: ")
    if eingabe != "x":
        eingabe_v["Vorname"] = eingabe
        eingabe_n["Nachname"] = input("Nachname: ")
        eingabe_p["Phone"] = input("Phone: ")
        data.update(eingabe_v)
        data.update(eingabe_n)
        data.update(eingabe_p)
#         print("Datensatz: ", data)
        telefonbuch.append(data)
    if eingabe == "x":
        break
print("Aufgabe d: ", telefonbuch)
    