#dicts.pdf Aufgabe 5 Berge
#Martina Hediger, 2.4.22

# 5. Berge: Schreiben Sie ein Programm, welches fur drei Berge deren Hohe
# in m entgegennimmt und diese anschliessend alphabetisch sortiert mit
# der Hohe in m und in Feet (ft; 1m =3.28 ft) ausgibt.
# 1. Berg: Mt. Everest
# 1. Hohe: 8848
# 2. Berg: ...
# 2. Hohe: ...
# ....
# Mt. Everest ist 8848 m (29029 ft) hoch.

feet=3.28
verzeichnis = {}
for i in range(2):
    berg = input("Berg: ")
    höhe = float(input("Höhe: "))
    verzeichnis[berg] = höhe
count = 1
for key, value in sorted(verzeichnis.items()):
    print(str(count) +". Berg: ", key)
    print(str(count) +". Höhe: ", value)
    count = count + 1
    print(key, "ist", int(value), "m", "(" + str(höhe*feet), "ft) hoch.")
