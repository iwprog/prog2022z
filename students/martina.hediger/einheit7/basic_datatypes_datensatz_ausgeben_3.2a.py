# basic-datatypes.pdf, 3.2a datensatz ausgeben
# Martina Hediger, 2.4.22

# Bitte verwenden Sie das Telefonbuch aus dem obigen Beispiel für die folgenden # Übungen:
# a. Schreiben Sie eine Funktion datensatz_ausgeben, welche den ersten Datensatz
# aus obigen Beispiel wie folgt ausgibt: 
# Vorname : Ana
# Nachname: Skupch
# Phone : 123

telefonbuch = []
data1 = {"Vorname": "Ana", "Nachname": "Skupch", "Phone": 123}
data2 = {"Vorname": "Tim", "Nachname": "Kurz", "Phone": 732}
data3 = {"Vorname": "Julia", "Nachname": "Lang", "Phone": 912}
telefonbuch.append(data1)
telefonbuch.append(data2)
telefonbuch.append(data3)

def datensatz_ausgeben(telefonbuch):
    for data in telefonbuch: # gibt die datensätze einzeln aus.
        for key, value in data.items(): #packt die datensätze (dict) aus. erzeugt Tupel. 
            print(key, ": ", value)
        
datensatz_ausgeben(telefonbuch)