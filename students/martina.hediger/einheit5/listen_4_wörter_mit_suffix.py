# Schreiben Sie eine Funktion, welche eine Liste und einen Suffix entgegennimmt
# und alle Worter, die auf den Sux enden ausgibt.
# l_input = ['Gesundheit', 'Wanderung', 'Heiterkeit',
# 'Gewandtheit', 'Lustig']
# >>> get_suffix_words(l_input, 'heit')
# ['Gesundheit', 'Gewandtheit']

l_input = ['Gesundheit', 'Wanderung', 'Heiterkeit','Gewandtheit', 'Lustig']
suffix = "heit"
liste = []


def get_suffix_words(l_input, suffix):
    for i in range(len(l_input)):
        position = len(l_input[i]) - len(suffix)
        vergleich = l_input[i][position:]
        if suffix == vergleich:
            liste.append(l_input[i])
    return liste

print(get_suffix_words(l_input, suffix))
