#listen.pdf Aufgabe 1 Martina Hediger 19.3.22

def filter_list(l_input):
    count = 0        
    while count < len(l_input):
        laenge = len(l_input[count])
        if laenge < 4:
            l_input.pop(count)
            count = count
        if laenge >= 4:
            count = count + 1
    return l_input

l_input = ['der', 'junge', 'ist', 'sehr', 'freundlich']
print(filter_list(l_input))