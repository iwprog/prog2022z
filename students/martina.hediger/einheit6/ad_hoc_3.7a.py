#ad hoc Übung 3.7a Dictionary, Martina hediger, 21.3.22

print("Aufgabe a")
städte = {"Zürich": 370000, "Genf": 19000, "Basel": 170000, "Bern": 130000} #hier definiere ich einen Dictionary

for key in städte:
    print(key, "hat", städte[key], "Einwohner") #der key Bsp. Zürich wird iteriert. mit dict_einwohnerzahlen[key] rufe ich den Wert des jeweiligen Keys auf.
print("")
print("Aufgabe b")
for key in sorted(städte):#sorted -> Sachen sortiert ausgeben. städte.sort() geht nicht, weil dictionary keine festgelegte reihenfolge hat. 
    print(key, "hat", städte[key], "Einwohner")
print("")
print("Aufgabe c: sortieren nach Einwohnern")

sortierliste = []
for stadt, einwohnerzahl in städte.items():
    sortierliste.append([einwohnerzahl, stadt]) #Plätze von key und value tauschen und Päärchen je in einer Liste in der Liste speichern
sortierliste.sort() #die einwohnerzahlen sind neu key und können sortiert werden
for zahl, ort in sortierliste:
    print(zahl, "hat", ort)

