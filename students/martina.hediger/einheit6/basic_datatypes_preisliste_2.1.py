#basic-datatypes.pdf ; Preisliste 2.1
#Martina Hediger, 27.3.22

#Aufgabe a
preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}
print(preisliste)

#Aufgabe b -> Wert ändern

preisliste["Milch"] = 2.05
print(preisliste)

#Aufgabe c -> Eintrag löschen
del preisliste["Brot"]
print(preisliste)

#Aufgabe d -> neue Einträge hinzufügen
preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
print(preisliste)

#Aufgabe e
while True:
    eingabe_key = input("Geben Sie das Lebensmittel an : (mit x beenden)")
    if eingabe_key == "x":
        break
    else: 
        eingabe_value = input("Geben Sie den Preis des Lebensmittels an :")
        preisliste[eingabe_key] = eingabe_value
print(preisliste)
