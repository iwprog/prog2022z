#basic-datatypes.pdf ; Dictionaries ltern 2.3c
#Martina Hediger, 27.3.22


preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 1.2, "Tee":4.2, "Peanuts":3.9, "Ketchup":2.1}
liste_löschen = [] #globale Liste, damit alle zutreffenden Werte dazugefügt werden und für zweite for schlaufe verwendet werden kann

#herausfinden, welche Werte en am Schluss haben
for key, value in preisliste.items(): #.items() -> erzeugt Tupel
    if key[-2:]=="en":
        liste_löschen.append(key)

#Löschung der Einträge, die en am Schluss haben
for key in liste_löschen:
    del preisliste[key] # del in zweiter for-Schleife, weil bei erster das Problem, mit der kleiner werdenden Dict und dadurch fehlermeldung bei iteration
print(preisliste)
