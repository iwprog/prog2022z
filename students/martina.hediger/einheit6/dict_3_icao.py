#dict.pdf Aufgabe 3
#Martina Hediger 19.3.22

# Welches Wort soll ich buchstabieren: Synthese
# Ausgabe: Sierra-Yankee-November-Tango-Hotel-Echo-Sierra-Echo

icao = {
"A":"Alfa",
"B":"Bravo",
"C":"Charlie",
"D":"Delta",
"E":"Echo",
"F":"Foxtrot",
"G":"Golf",
"H":"Hotel",
"I":"India",
"J":"Juliett",
"K":"Kilo",
"L":"Lima",
"M":"Mike",
"N":"November",
"O":"Oscar",
"P":"Papa",
"Q":"Quebec",
"R":"Romeo",
"S":"Sierra",
"T":"Tango",
"U":"Uniform",
"V":"Victor",
"W":"Whiskey",
"X":"X-ray",
"Y":"Yankee",
"Z":"Zulu"
}

eingabe = input("Welches Wort soll ich buchstabieren :")
l_eingabe = eingabe.upper()
def buchstabieren(l_eingabe):
    l_ausgabe = []
    for i in l_eingabe:
        if i in icao.keys():
            l_ausgabe.append(icao[i])
    return l_ausgabe
print(buchstabieren(l_eingabe))