#listen.pdf Aufgabe 7
#Martina Hediger 19.3.22

# Schreiben Sie eine Funktion unique, welche doppelte Eintrage aus derListe entfernt.
# l_input = ['Gesundheit', 'Wanderung', 'Gesundheit', 'Gewandtheit', 'Wanderung']
# >>> unique(l_input)
# ['Gesundheit', 'Wanderung', 'Gewandtheit']

l_input = ['Gesundheit', 'Wanderung', 'Gesundheit', 'Gewandtheit', 'Wanderung']
print("Ausgangsliste", l_input)
def unique(l_input):
    l_unique = []
    for wort in l_input:
        if wort not in l_unique:
            l_unique.append(wort)
    return l_unique
print("Liste ohne Dopplung: ", unique(l_input))
