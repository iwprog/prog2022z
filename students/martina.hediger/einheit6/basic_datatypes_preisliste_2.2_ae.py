#basic-datatypes.pdf ; Preisliste 2.2 a,e 
#Martina Hediger, 27.3.22

preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 1.2}
preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1

for key in preisliste:
    if preisliste[key] < 2.0:
        print("Artikel günstiger als 2.0:")
        print(key, "kostet", preisliste[key], "CHF")

