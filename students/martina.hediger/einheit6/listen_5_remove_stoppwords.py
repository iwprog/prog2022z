#Übungsbeispiele Listen, Aufg. 5 - Remove Stoppwords
#Martina Hediger 27.03.22

l_input = 'Heinz war heute in den Bergen. Es war eine lange Wanderung'
l_input = l_input.lower() #wörter alle kleingeschrieben
stopwords = ['der', 'die', 'das', 'in', 'auf', 'unter', 'ein', 'eine', 'ist', 'war', 'es', 'den']


def stopword_filter(l_input, stopwords):
    l_input = l_input.split() #text in eine Liste umwandeln
    #Eingangsliste mit Stopwortliste vergleichen. Stoppwörter in separater Liste speichern
    liste_löschen = []
    for i in l_input:
        if i in stopwords:
            liste_löschen.append(i)
    #gespeicherte Stoppwörter aus der Eingangsliste löschen: 
    for b in liste_löschen:
        l_input.remove(b)
    return l_input


print("Liste aller Wörter ohne Stoppwörter: ", stopword_filter(l_input, stopwords))

        



