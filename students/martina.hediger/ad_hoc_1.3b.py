#Martina Hediger, 15.2.22, Übung 1.3b

from turtle import *

pencolor("red")
pensize(5)

#cyan Quadrat
fillcolor("cyan")
begin_fill()
left(45)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
end_fill()

right(160)
#gelbes Quadrat
fillcolor("yellow")
begin_fill()
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
end_fill()

right(160)
#magenta Quadrat
fillcolor("magenta")
begin_fill()
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
end_fill()


right(160)
#blaues Quadrat
fillcolor("blue")
begin_fill()
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
end_fill()

right(80)
#hellgrün Quadrat
fillcolor("light green")
begin_fill()
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
end_fill()