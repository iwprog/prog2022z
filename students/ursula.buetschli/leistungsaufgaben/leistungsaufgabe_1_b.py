from turtle import *

# rotes Quadrat (400X400px)
hideturtle()
pensize(1)
pencolor("red")
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
end_fill()


laenge = numinput("Eingabefenster", "Geben Sie eine Seitenlänge für das weisse Kreuz ein")

# Position zum Zeichnen des weissen Kreuzes


penup()
left(90)
forward(200+0.5*laenge)
left(90)
forward(200+0.5*laenge)
pendown()


# weisses Kreuz
def kreuzbalken(laenge):
    forward(laenge)
    left(90)
    forward(laenge)
    left(90)
    forward(laenge)
    right(90)
    
fillcolor("white")
begin_fill()
kreuzbalken(laenge)
kreuzbalken(laenge)
kreuzbalken(laenge)
kreuzbalken(laenge)
end_fill()
