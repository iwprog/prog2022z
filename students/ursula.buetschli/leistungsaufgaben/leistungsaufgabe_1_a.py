# Prog Leistungsaufgabe 1
# Erstellen Sie die Schweizer Flagge mit der turtle-Grafik
# und exakt folgendem Aussehen (400x400 Pixel),
# es darf kein Pfeil/Turtle sichtbar sein
# Autor: Ursula Bütschli
# Datum: 19.02.2022


# Import aller Befehle der turtle-Bibliothek
from turtle import *

# rotes Quadrat (400X400px)
hideturtle()
pensize(1)
pencolor("red")
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
end_fill()

# Position zum Zeichnen des weissen Kreuzes
penup()

left(90)
forward(250)
left(90)
forward(150)
right(90)
pendown()

# weisses Kreuz
def kreuzbalken(laenge):
    forward(laenge)
    left(90)
    forward(laenge)
    left(90)
    forward(laenge)
    right(90)
    
fillcolor("white")
begin_fill()
kreuzbalken(100)
kreuzbalken(100)
kreuzbalken(100)
kreuzbalken(100)
end_fill()
