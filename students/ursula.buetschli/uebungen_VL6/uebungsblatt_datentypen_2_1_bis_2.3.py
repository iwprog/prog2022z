# 2.1 Dictionnaries
# a. Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit
# den zugehörigen Preisen speichert: Brot → 3.2, Milch → 2.1, Orangen
# → 3.75, Tomaten → 2.2.

preisliste ={"Brot":3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}
print(preisliste)


# b. Ändern Sie den Preis für Milch auf 2.05.

preisliste["Milch"] = 2.05
print(preisliste)
# 
# c. Entfernen Sie den Eintrag für Brot.

del preisliste["Brot"]
print(preisliste)

# d. Fügen Sie neue Einträge für Tee → 4.2, Peanuts → 3.9 und Ketchup
# → 2.1 hinzu.
# Ist gleich wie beim ändern
# wie hängt man die einzelnen Einträge aneinander?
preisliste["Tee"] = 4.2
print(preisliste)
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
print(preisliste)

# 2.2 Über Dictionaries iterieren
# a. Geben Sie die Preiliste aus dem obigen Beispiel aus.
# Brot kostet 3.2 CHF.
# Milch kostet 2.1 CHF.
# ...
# Ketchup kostet 2.1 CHF.

preisliste["Brot"] = 3.2
for artikel in sorted(preisliste):
    print(artikel, "kostet", preisliste[artikel], "CHF")
    
# e. Geben Sie nur Lebensmittel mit einem Preis < 2.0 CHF aus

for artikel in preisliste:
    if preisliste[artikel] < 2.0:
        print("Artikel günstiger als 2.0:")
        print(artikel, "kostet", preisliste[artikel], "CHF")
        
        
# # 2.3 Dictionaries filtern
# # c. Entfernen Sie alle Lebensmittel aus dem Dictionary, welche die Zeichenkette en im Namen enthalten.
# liste_loeschen=[]
# for artikel, preis in preisliste.items(): #.items() -> erzeugt Tupel
#     if artikel[-2:]=="en":
#         liste_loeschen.append(artikel)
# 
# 
# for key in liste_loeschen:
#     del preisliste[artikel] # del in zweiter for-Schleife, weil bei erster das Problem, mit der kleiner werdenden Dict und dadurch fehlermeldung bei iteration
# print(preisliste)


# preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 1.2, "Tee":4.2, "Peanuts":3.9, "Ketchup":2.1}
liste_löschen = [] #globale Liste, damit alle zutreffenden Werte dazugefügt werden und für zweite for schlaufe verwendet werden kann

#herausfinden, welche Werte en am Schluss haben
for artikel, preis in preisliste.items(): #.items() -> erzeugt Tupel
    if artikel[-2:]=="en":
        liste_löschen.append(artikel)

#Löschung der Einträge, die en am Schluss haben
for artikel in liste_löschen:
    del preisliste[artikel] # del in zweiter for-Schleife, weil bei erster das Problem, mit der kleiner werdenden Dict und dadurch fehlermeldung bei iteration
print(preisliste)

