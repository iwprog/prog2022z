# 5. Remove Stopwords: Schreiben Sie eine Funktion, welche einen Text
# entgegennimmt und aus diesem alle Stoppw¨orter, die in einer Liste
# definiert sind, entfernt und eine kleingeschriebene Liste aller W¨orter
# (ohne Stoppw¨orter) zur¨uckgibt.

# l_input = ’Heinz war heute in den Bergen. Es war eine \
# lange Wanderung’
# stopwords = (’der’, ’die’, ’das’, ’in’, ’auf’, ’unter’,
# ’ein’, ’eine’, ’ist’, ’war’, ’es’, ’den’)
# >>> stopword_filter(l_input, stopwords)
# [’heinz’, ’heute’, ’bergen’, ’lange’, ’wanderung’]


satz = "Heinz war heute in den Bergen. Es war eine lange Wanderung"
satz = satz.lower()
stoppwords = "der die das in auf unter ein eine ist war es den"
satz = satz.split()   # Text in Liste verwandeln

stoppwords = stoppwords.split()

def stoppword_filter(satz, stoppwords):
    word_loeschen = []
    for i in satz:
        if i in stoppwords:
            word_loeschen.append(i)  # fügt die Stoppwörter in satz2 in eine liste
    
    for word in word_loeschen:
        satz.remove(word)
    return satz
#        
print("Gefilterte Liste:", stoppword_filter(satz, stoppwords))      


l_input = 'Heinz war heute in den Bergen. Es war eine lange Wanderung'
l_input = l_input.lower() #wörter alle kleingeschrieben
stopwords = ['der', 'die', 'das', 'in', 'auf', 'unter', 'ein', 'eine', 'ist', 'war', 'es', 'den']


def stopword_filter(l_input, stopwords):
    l_input = l_input.split() #text in eine Liste umwandeln
    #Eingangsliste mit Stopwortliste vergleichen. Stoppwörter in separater Liste speichern
    liste_löschen = []
    for i in l_input:
        if i in stopwords:
            liste_löschen.append(i)
    #gespeicherte Stoppwörter aus der Eingangsliste löschen: 
    for b in liste_löschen:
        l_input.remove(b)
    return l_input


print("Liste aller Wörter ohne Stoppwörter: ", stopword_filter(l_input, stopwords))

