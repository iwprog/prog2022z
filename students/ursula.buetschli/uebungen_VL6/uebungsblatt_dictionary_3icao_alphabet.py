# 3. Die “International Civil Aviation Organization (ICAO) Alphabet weist
# Buchstaben W¨ortern zu1
# , damit diese fehlerfrei buchstabiert werden
# k¨onnen. Schreiben Sie ein Programm, das ein beliebiges Wort buchstabiert.
# Welches Wort soll ich buchstabieren: Synthese
# Ausgabe: Sierra-Yankee-November-Tango-Hotel-Echo-Sierra-Echo
# (a) Erweitern Sie obiges Programm, sodass es einen Fehler ausgibt
# wenn das Wort einen Buchstaben enth¨alt, der nicht im ICAO Alphabet definiert ist.
# Welches Wort soll ich buchstabieren: ¨Ubung
# Kann ¨Ubung nicht buchstabieren, da ’¨U’ nicht definiert
# wurde.

#dict.pdf Aufgabe 3
#Martina Hediger 19.3.22

# Welches Wort soll ich buchstabieren: Synthese
# Ausgabe: Sierra-Yankee-November-Tango-Hotel-Echo-Sierra-Echo

icao = {
"A":"Alfa",
"B":"Bravo",
"C":"Charlie",
"D":"Delta",
"E":"Echo",
"F":"Foxtrot",
"G":"Golf",
"H":"Hotel",
"I":"India",
"J":"Juliett",
"K":"Kilo",
"L":"Lima",
"M":"Mike",
"N":"November",
"O":"Oscar",
"P":"Papa",
"Q":"Quebec",
"R":"Romeo",
"S":"Sierra",
"T":"Tango",
"U":"Uniform",
"V":"Victor",
"W":"Whiskey",
"X":"X-ray",
"Y":"Yankee",
"Z":"Zulu"
}

l_input = input("Welches Wort soll ich buchstabieren :")
l_input = l_input.upper()

l_output = []
for buchstabe in l_input:
    if buchstabe not in icao.keys():
        print("Kann", l_input, "nicht buchstabieren, da", buchstabe, "nicht fefiniert ist.")
        break
    else:
        l_output.append(icao[buchstabe])
    
   
if len(l_input) == len(l_output):
    for wort in l_output:
        print(wort, end = '-')
    
# # (b) Erweitern Sie Ihr Programm, sodass dieses Umlaute automatisch
# entsprechend umschreibt (sprich ¨o=oe, ...)
# Welches Wort soll ich buchstabieren: ¨Ubung
# Ausgabe: Uniform-Echo-Bravo-Uniform-November-Gold

alphabet = {'A' : 'Alfa','B' : 'Bravo','C' : 'Charlie','D' : 'Delta','E' : 'Echo','F' : 'Foxtrot','G' : 'Golf',
            'H' : 'Hotel','I' : 'India','J' : 'Juliett','K' : 'Kilo','L' : 'Lima','M' : 'Mike​','N' : 'November',
            'O' : 'Oscar','P' : 'Papa','Q' : 'Quebec','R' : 'Romeo','S' : 'Sierra','T' : 'Tango','U' : 'Uniform',
            'V' : 'Victor','W' : 'Whiskey','X' : 'X:ray','Y' : 'Yankee','Z' : 'Zulu',
            'Ö' : 'Oscar-Echo',  'Ä' : 'Alfa Echo', 'Ü':'Uniform-Echo'}