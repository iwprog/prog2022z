# 2. Wortstatistik: Schreiben Sie eine Funktion, welche einen Text entgegennimmt und alle W¨orter mit deren
# H¨aufigkeit ausgibt. Die Gross-
# /Kleinschreibung soll dabei nicht ber¨ucksichtigt werden.
# l_input = "Der Tag begann sehr gut! Der Morgen war sch¨on."
# >>> word_stat(l_input)
# {’der’: 2, ’tag’: 1, ....}

l_input = "Der Tag begann sehr gut! Der Morgen war schön."

def word_stat(l_input):
    anzahl_wort = {}
    for wort in l_input.split():
        if wort in anzahl_wort:
            anzahl_wort[wort] = anzahl_wort[wort] + 1
        else:
            anzahl_wort[wort] =1
            
    return anzahl_wort

print(word_stat(l_input))



