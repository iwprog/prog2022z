# 2.1 Dictionnaries
# a. Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit
# den zugehörigen Preisen speichert: Brot → 3.2, Milch → 2.1, Orangen
# → 3.75, Tomaten → 2.2.

preisliste ={"Brot":3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}
print(preisliste)


# b. Ändern Sie den Preis für Milch auf 2.05.

preisliste["Milch"] = 2.05
print(preisliste)
# 
# c. Entfernen Sie den Eintrag für Brot.

del preisliste["Brot"]
print(preisliste)

# d. Fügen Sie neue Einträge für Tee → 4.2, Peanuts → 3.9 und Ketchup
# → 2.1 hinzu.
# Ist gleich wie beim ändern
# wie hängt man die einzelnen Einträge aneinander?
preisliste["Tee"] = 4.2
print(preisliste)
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
print(preisliste)