# ad hoc Übung 3.7
# a. Schreiben Sie ein Programm, dass eine Ausgabe nach
# folgendem Aufbau erzeugt:
# Zürich hat 370000 Einwohner
# Genf hat 190000 Einwohner

staedte = {"Zürich":370000, "Genf": 190000, "Bern": 1300000, "Basel": 170000}
print("ad hoc 3.7a")
# Variente 1
for stadt in staedte:
    print(stadt, "hat", staedte[stadt], "Einwohner")
    
# Variante 2
for stadt, einwohnerzahl in staedte.items():
    print(stadt, "hat", einwohnerzahl, "Einwohner")
    
print()   

# b. Ändern Sie das Programm so, dass die Liste sortiert nach
# keys ausgegeben wird. Heisser Tipp: list hat ja eine
# Methode sort()

print("ad hoc 3.7b")
# Variante 1
for stadt in sorted(staedte):
    print(stadt, "hat", staedte[stadt], "Einwohner")
    
# Variante 2
for stadt, einwohnerzahl in sorted(staedte.items()):
    print(stadt, "hat", einwohnerzahl, "Einwohner")
    
print()
    
# c. Überlegen Sie, wie man die Ausgabe sortiert nach
# Einwohnerzahlen programmieren könnte.

print("ad hoc 3.7c")



staedte = {"Zürich": 370000, "Genf": 190000, "Basel": 170000, "Bern": 130000, "Basel": 170000}
sortierliste = []
for stadt, einwohner in staedte.items():
    sortierliste.append([einwohner, stadt])
    print(stadt, "hat", einwohner, "Einwohner")
print()

print("Variante2")
import operator
sortierliste = sorted(staedte.items(), key=operator.itemgetter(1))
print(sortierliste)
