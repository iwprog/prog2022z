# ad hoc Übung 1.2.d
# Zeichnen Dreiecks mit roter Strichfarbe
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *
# ändert die Strichdicke
pensize(5)

# ändert die Strichfarbe
pencolor("red")

forward(100)
left(120)
forward(100)
left(120)
forward(100)
