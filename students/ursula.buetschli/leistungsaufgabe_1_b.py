# Prog Leistungsaufgabe 1
# Erstellen Sie die Schweizer Flagge mit der turtle-Grafik
# und exakt folgendem Aussehen (400x400 Pixel),
# es darf kein Pfeil/Turtle sichtbar sein
# Autor: Ursula Bütschli
# Datum: 19.02.2022


# Import aller Befehle der turtle-Bibliothek
from turtle import *

# zeichnen rotes Quadrat (400X400px)
hideturtle()
pensize(1)
pencolor("red")
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
end_fill()

# Anzeige Eingabefenster für die Variabel Länge der Seitenlänge des weissen Kreuzes



laenge = numinput("Eingabefenster", "Geben Sie eine Seitenlänge für das weisse Kreuz ein")

# Position zum Zeichnen des weissen Kreuzes, ausgehend vom Mittelpunkt des roten Quadrat
penup()

left(90)
forward(200)
left(90)
forward(200)
right(90)
forward(1.5*laenge)
left(90)
pendown()

# zeichnen weisses Kreuz mit der Seitenlänge als Variabel (laenge)
fillcolor("white")
begin_fill()
pencolor("white")
forward(0.5*laenge)
left(90)
forward(laenge)
right(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
right(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
right(90)
forward(laenge)
left(90)
forward(laenge)
left(90)
forward(laenge)
right(90)
forward(laenge)
left(90)
forward(0.5*laenge)


