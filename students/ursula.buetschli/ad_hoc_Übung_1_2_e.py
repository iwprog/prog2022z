# ad hoc Übung 1.2.d
# Zeichnen von drei gleichschenkligen Dreiecken,
# die verbunden sind an einem 120 Grad Winkel
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *
# rotes Dreieck

pensize(5)
pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)

# blaues Dreieck

pensize(5)
pencolor("blue")
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

# grünes Dreieck

pensize(5)
pencolor("lime")
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
