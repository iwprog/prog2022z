# ad hoc Übung 1.1.c
#  Schreiben Sie einen Befehl, der den Satz
# wenn fliegen hinter fliegen fliegen fliegen fliegen fliegen hinterher
# Autor: Ursula Bütschli
# Datum: 17.02.2022

print ("wenn fliegen hinter", 5*"fliegen "+"hinterher")
# oder
f="fliegen "
print ("wenn", f+"hinter", 5*f + "hinterher")

