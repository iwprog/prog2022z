# ad hoc Übung 1.3.a
# Zeichnen von drei gleichschenkligen Dreiecken,
# die verbunden sind an einem 120 Grad Winkel
# und jeweils mit den Farben lime, yellow, cyan gefüllt sind
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *
# grün gefülltes Dreieck

left(30)
pensize(5)
fillcolor("lime")
begin_fill()
pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

# gelb gefülltes Dreieck

fillcolor("yellow")
begin_fill()
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

# hellblau gefülltes Dreieck

fillcolor("cyan")
begin_fill()
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()
