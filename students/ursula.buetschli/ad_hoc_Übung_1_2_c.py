# ad hoc Übung 1.2.b
# Zeichnen einer Zickzacklinie
# mit der Strichfarbe blue, red, cyan, black
# und der Strichdicke 5
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *
# ändert die Strichdicke
pensize(5)

# ändert die Strichfarbe
pencolor("blue")
left(45)
forward(100)
pencolor("red")
right(90)
forward(100)
pencolor("cyan")
left(90)
forward(100)
pencolor("black")
right(90)
forward(100)