# ad hoc Übung 1.3.b
# Zeichnen von 5 Quadraten,
# die verbunden sind an einem Winkel
# und jeweils mit den Farben cyan, yellow, magenta, blue, lime gefüllt sind
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *
# cyan gefülltes Quadrat

left(30)
pensize(5)
fillcolor("cyan")
begin_fill()
pencolor("red")

forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()
home()

# gelb gefülltes Quadrat

fillcolor("yellow")
begin_fill()
right(130)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

# magenta gefülltes Quadrat

fillcolor("magenta")
begin_fill()
left(20)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

# blaues gefülltes Quadrat

fillcolor("blue")
begin_fill()
left(20)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

# hellgrün gefülltes Quadrat

fillcolor("lime")
begin_fill()
left(10)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

