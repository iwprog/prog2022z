# ad hoc Übung 1.2.b
# Zeichnen eines Rechteckes
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *

forward(100)
left(90)
forward(50)
left(90)
forward(100)
left(90)
forward(50)