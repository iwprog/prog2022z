# ad hoc Übungen 1.1.b
# Schreiben Sie einen Befehl, der alle Zahlen von 1 bis 5
# addiert, subtrahiert, multipliziert und dividiert
# Autor: Ursula Bütschli
# Datum: 17.02.2022

print (1+2+3+4+5)
print (1-2-3-4-5)
print (1*2*3*4*5)
print (1/2/3/4/5)

# ad hoc 1.1.c
print ("wenn fliegen hinter", 5*"fliegen ","hinerher")
# oder
f="fliegen "
print ("wenn", f+"hinter", 5*f + "hinterher")
