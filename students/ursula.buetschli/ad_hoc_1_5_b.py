# ad hoc Übung 1.5.b
# Eingabefenster mit variabeler Länge zum zeichnen von fünf Quadraten, der Seitenlänge jeweils kleiner sind als das voherige
# und sich um einen Winkel drehen
# Autor: Ursula Bütschli
# Datum: 19.02.2022

from turtle import *

# öffnet das Eingabefesnster und weisst der Variabel laenge den Tastaturwert im Eingabefenster zu
laenge = numinput("Eingabefenster", "Geben Sie eine Seitenlänge ein")

# rafische Angabe zum Stift
pensize(5)
pencolor("red")

# erstes grosses cyan Quadrat
fillcolor("cyan")
begin_fill()
pencolor("red")
left(30)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
end_fill()


# gelb gefülltes Quadrat

fillcolor("yellow")
begin_fill()
left(110)
forward(laenge/1.2)
left(90)
forward(laenge/1.2)
left(90)
forward(laenge/1.2)
left(90)
forward(laenge/1.2)
end_fill()

# magenta gefülltes Quadrat

fillcolor("magenta")
begin_fill()
left(20)
forward(laenge/1.4)
left(90)
forward(laenge/1.4)
left(90)
forward(laenge/1.4)
left(90)
forward(laenge/1.4)
end_fill()

# blaues gefülltes Quadrat

fillcolor("blue")
begin_fill()
left(20)
forward(laenge/1.6)
left(90)
forward(laenge/1.6)
left(90)
forward(laenge/1.6)
left(90)
forward(laenge/1.6)
end_fill()

# hellgrün gefülltes Quadrat

fillcolor("lime")
begin_fill()
left(10)
forward(laenge/1.8)
left(90)
forward(laenge/1.8)
left(90)
forward(laenge/1.8)
left(90)
forward(laenge/1.8)
end_fill()

