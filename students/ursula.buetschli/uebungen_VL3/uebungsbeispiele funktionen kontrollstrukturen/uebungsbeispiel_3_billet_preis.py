#Uebungsbeispiel 2 billet preis
# Funktion, die basierend auf dem Alter und der Entfernungden Preis für ein billet berechnet
# • jedes Ticket verfügt über einen Grundpreis von 2 CHF, der unabhängig von der Entfernung berechnet wird.
# • zusätzlich werden 0.25 CHF / km berechnet.
# • Kinder unter 16 Jahren zahlen den halben Preis.
# • Kinder unter 6 Jahren reisen kostenlos.


# Autor: Ursula Bütschli
# Datum: 06.03.2022

alter=int(input("Geben Sie Ihr Alter ein. "))
entfernung=int(input("Geben Sie die Entfernung ein. "))
summe=2

def preis(alter):
    if alter >0 and alter <6:
        return 0
    elif alter >=6 and alter <16:
        return((entfernung*0.25)+2)/2
    elif alter >=16:
        return((entfernung*0.25)+2)
        
    
    
print("Ihr Ticket kostet",preis(alter), "Franken.")  



