#Uebungsbeispiel 6 bilderrahmen# Schreiben Sie eine Funktion welche einen Bilderrahmen mit einer bestimmten
# Länge und Breite unter Zuhilfenahme von Schleifen zeichnet.
# Weiters soll die Funktion noch die Rahmen- und Füllfarbe entgegennehmen.
# Autor: Ursula Bütschli
# Datum: 06.03.2022


from turtle import *

# Frage, weshalbv funktioniert es nicht mit Tastaturinput?

# rahmenfarbe = input("Geben Sie die Rahmenfarbe an.")
# fuellfarbe1 = input ("Geben Sie die Füllfarbe an.")
# fuellfarbe1 = input ("Geben Sie die Füllfarbe an.")

def zeichne_rahmen(laenge, breite, rahmenfarbe, fuellfarbe1, fuellfarbe2):
        fillcolor(fuellfarbe1)
        pencolor(rahmenfarbe)
        begin_fill()
        for i in range(2):
            forward(laenge)
            left(90)
            forward(breite)
            left(90)
        end_fill()
        penup()
        forward(laenge*0.15)
        left(90)
        forward(breite*0.15)
        right(90)
        pendown()
        
        fillcolor(fuellfarbe2)
        begin_fill()
        for i in range(2):
            forward(laenge*0.7)
            left(90)
            forward(breite*0.7)
            left(90)
        end_fill()
        


zeichne_rahmen(100, 150, "blue", "blue", "white")