#Uebungsbeispiel 11 Spass mit schleifen
# Autor: Ursula Bütschli
# Datum: 06.03.2022

# uebungsbeisiel 11.e
#gibt einen Rahmen aus

print("gibt einen Rahmen aus")            
def figur_e():
    for count in range(7):
        if count ==0 or count==6:
            print("# # # # # # #")
        else:
            print("#           #")
            
figur_e()

# uebungsbeisiel 11.g
#gibt einen ein spiegelverkehrtes "Z" aus         
 
 
print("gibt ein spiegelverkehrte Z aus")
def figur_g():
    for count in range(7):
        if count ==0 or count==6:
            print("# # # # # # #")
        elif count==1:
            print("          #")
        elif count==2:
            print("        #")
        elif count==3:
            print("      #")
        elif count==4:
            print("    #")
        elif count==5:
            print("  #")         
        elif count==6:
            print("# # # # # # #")
            
figur_g()     
