# ad_ hoc_ uebung 2.6b Lotto
# Schreiben sie ein Programm, das 6 Zufallszahlen zwischen
# 1 und 42 ausgibt.

# Autor: Ursula Bütschli
# Datum: 06.03.2022

from random import randint

i=1
for zahl in range(6):
    zufallszahl = randint(1,42)
    print(str(i) + ". Zufallszahl", zufallszahl)
    i=i+1
    
