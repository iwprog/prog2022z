alter = input("Bitte geben Sie Ihr Alter ein.")

def legal_status(alter):
    if int(alter) < 18:
        return("minderjährig")
    elif int(alter) >=18:
        return("volljährig")
        
    
print("Mit", alter, "ist man", legal_status(alter) + ".")


# Ohne Funktion mit if - else

# alter = input("Bitte geben Sie Ihr Alter ein.")
# 
# 
# if int(alter) >= 18:
#     print("Mit", alter, "ist man volljährig.")
# else:
#     print("Mit", alter, "ist man minderjährig.")
# 
