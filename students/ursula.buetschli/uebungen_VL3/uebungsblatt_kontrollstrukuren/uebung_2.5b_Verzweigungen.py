alter = input("Bitte geben Sie Ihr Alter ein.")

def legal_status(alter):
    if int(alter) <0 :
        return("ungeboren")
    elif int(alter) >0 and int(alter) <=6:
        return("geschäftsfähig")
    elif int(alter) >6 and int(alter)<=14:
        return("unmündig")
    elif int(alter) >14 and int(alter)<=17:
        return("mündig minderjährig")
    elif int(alter) >=18:
        return("volljährig")
    
print("Mit", alter, "ist man", legal_status(alter) + ".")


# Ohne Funktion mit if - else

# alter = input("Bitte geben Sie Ihr Alter ein.")
# 
# 
# if int(alter) >= 18:
#     print("Mit", alter, "ist man volljährig.")
# else:
#     print("Mit", alter, "ist man minderjährig.")
# 
