# uebung 2.7a
# Schleife, die alle Buchstaben des Wortes "Dampfschiff" untereinander ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022
        
for zeichen in "Dampfschiff":
    print(zeichen)
    
# uebung 2.7b
# Schleife, die die Farben rot, gelb, grün, blau, orang ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022    
    
for farben in "rot","gelb", "grün", "blau", "orange":
    print(farben)
 
# uebung 2.7c
# Schleife, die Schleife, die alle Buchstaben des Wortes "Dampfschiff" untereinander un dnummeriert ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022    
     
 
i=1
for zeichen in "Dampfschiff":
    print(str(i)+"."+zeichen)
    i=i+1