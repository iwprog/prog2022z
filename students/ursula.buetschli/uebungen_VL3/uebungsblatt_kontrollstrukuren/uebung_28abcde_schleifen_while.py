# uebung 2.8a
# while-Schleife, die die Zahlen von 0 bis 10 ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022
print("Uebung 2.8a, Schleife von 0 bis 10")

i=0
while i<=10:
    print (str(i))
    i=i+1
    
    
# uebung 2.8b
# while-Schleife, die die Zahlen von 10 bis 0 ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022

print("Uebung 2.8b, Schleife von 10 bis 0")

i = 10
while i >= 0:
    print("Zahl", i)
    i = i - 1
    
# uebung 2.8c
# while-Schleife, die die Zahlen von 10 bis 0 in Dreierschritten ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022    

print("Uebung 2.8c, Schleife von 10 bis 0 in Dreierschritten")

i = 10
while i >= 0:
    print("Zahl", i)
    i = i - 3

# uebung 2.8d
# while-Schleife, die die Zahlen von 10 bis 0 ausgibt un danstelle von 0 das Wort "start" ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022

print("Uebung 2.8d, 10 bis 0 zählt und anstelle von 0 das Wort start ausgibt")

i = 10
while i >= 1:
    print("Zahl", i)
    i = i - 1
    while i == 0:
        print("Start")
        i = i - 1
    
# uebung 2.8e
# while-Schleife, die drei Zahlen entgegennimmt und die Summe ausgibt
# Autor: Ursula Bütschli
# Datum: 06.03.2022

print("2.83, while-Schleife, die drei Zahlen entgegennimmt und die Summe ausgibt")

i = 1
a = 0
summe = 0
eingabe=0

while i <=3:     
    eingabe = input("Bitte geben Sie eine Zahl ein.")
    a=int(eingabe)  
    summe=summe+a
    print(summe)
    i = i + 1
   
print("Summe:", summe)

# uebung 2.8f
# while-Schleife, die die so lange Zahlen entgegnnimmt, bis die Summe grösser 10 ist
# Autor: Ursula Bütschli
# Datum: 06.03.2022

print("while-Schleife, die die so lange Zahlen entgegnnimmt, bis die Summe grösser 10 ist")

i = 0
summe = 0
while summe <= 10:
    i=int(input("Geben Sie eine Zahl ein. "))
    summe=summe+i
    
print("Summe:", summe)

# uebung 2.8g
# while-Schleife, die die so lange Zahlen entgegnnimmt, bis eine -1 eingegeben wird
# Autor: Ursula Bütschli
# Datum: 06.03.2022

print("while-Schleife, die die so lange Zahlen entgegnnimmt, bis eine -1 eingegeben wird")

i = 0
summe = 0
eingabe=0

while eingabe !="-1":
    i = int(eingabe)
    summe=summe+i
    eingabe=input("Geben sie eine Zahl ein. ")
                  
print("Summe:", summe)                  