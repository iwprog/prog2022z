# ad hoc Übung 1.1.a
# Stellen Sie mit der print-Anweisung einen Datensatz aus
# folgenden einzelnen Bestandteilen zusammen
# Name, Vorname, Strasse, Hausnr., PLZ, Wohnort
# Autor: Ursula Bütschli
# Datum: 17.02.2022
print("Name")
print("Vorname")
print("Strasse")
print("Hausnr.")
print("PLZ")
print("Wohnort")