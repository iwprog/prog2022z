# Autor: Ursula Bütschli
# Datum: 27.02.2022
# 2.8-a Schleifen mit while
# Schreiben Sie eine Schleife, welche von 0 bis 10 z¨ahlt.

i = 0
while i <=10:
    print(i)
    i = i + 1

# Autor: Ursula Bütschli
# Datum: 27.02.2022
# 2.8-a Schleifen mit while
# Schreiben Sie eine Schleife, welche von 0 bis 10 z¨ahlt.

i = 10
while i >=10:
    print(i)
    i = i - 1