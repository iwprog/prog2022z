# Autor: Ursula Bütschli
# Datum: 27.02.2022
# 22.2 Funktionen ohne R¨uckgabewert mit Parameter
# Schreiben Sie eine Funktion, welche als Parameter einen Namen entgegennimmt und anschliessend den Text Guten Morgen Name! ausgibt.
# >>> guten_morgen("Ana")
# Guten Morgen Ana!

def guten_morgen(name):
    print("Guten Morgen "+ name+"!")
    
guten_morgen("Ursula")