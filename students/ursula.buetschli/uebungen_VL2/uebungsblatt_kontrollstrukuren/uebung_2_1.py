# Autor: Ursula Bütschli
# Datum: 27.02.2022
# 2.1 Funktionen ohne R¨uckgabewert und Parameter
# Schreiben Sie eine Funktion guten morgen, welche den Text Guten Morgen!
# auf dem Bildschirm ausgibt.
# >>> guten_morgen()
# Guten Morgen!

def guten_morgen():
    print("Guten Morgen!")
    
guten_morgen()