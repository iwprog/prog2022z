# Autor: Ursula Bütschli
# Datum: 27.02.2022
# 2.8-c Schleifen mit while
# Schreiben Sie eine Schleife, welche von 10 bis 0 in Dreierschritten z¨ahlt.

i = 10
while i >=0:
    print(i)
    i = i - 3