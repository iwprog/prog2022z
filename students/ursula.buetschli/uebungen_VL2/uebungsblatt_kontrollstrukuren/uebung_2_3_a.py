# Autor: Ursula Bütschli
# Datum: 27.02.2022
# 2.3.a Funktionen mit R¨uckgabewert und Parameter
# Schreiben Sie eine Funktion, welche einen Namen entgegennimmt und
# anschliessend den Text Guten Morgen Name! zur¨uckgibt
# gruss = guten_morgen("Ana")
# print(gruss)
# Ausgabe: Guten Morgen Ana!

def gruss():
    name = input("Wie heisst du? ")
    print("Guten Morgen "+ name+"!")
    
gruss()