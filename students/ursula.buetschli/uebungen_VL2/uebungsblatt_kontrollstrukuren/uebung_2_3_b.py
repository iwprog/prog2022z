# Autor: Ursula Bütschli
# Datum: 27.02.2022
# 2.3.b Funktionen mit R¨uckgabewert und Parameter
# b. Schreiben Sie eine Funktion, welche die L¨ange und Breite eines Rechtecks
# entgegennimmt und die Fl¨ache des Rechtecks an das Programm zur¨uckgibt.
# flaeche = flaeche_rechteck(10, 20)
# print("Die Fl¨ache betr¨agt", flaeche, "m2.")

def flaecherechteck(hoehe, breite):
    return hoehe*breite
    
flaeche=flaecherechteck(10, 20)
print("Die Fläche beträgt "+str(flaeche),"m2")

flaeche=flaecherechteck(40, 20)
print("Die Fläche beträgt "+str(flaeche),"m2")