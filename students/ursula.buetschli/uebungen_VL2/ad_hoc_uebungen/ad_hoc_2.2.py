# ad hoc Übung 1.5.a
# Eingabefesnter mit variabeler Länge zum zeichnen von drei Dreiecke, der Seitenlänge jeweils 2 mal grösser sind als das voherige
# Autor: Ursula Bütschli
# Datum: 19.02.2022

from turtle import *

# öffnet das Eingabefesnster und weisst der Variabel laenge den Tastaturwert im Eingabefenster zu
laenge = numinput("Eingabefenster", "Geben Sie eine Seitenlänge ein")

# erstes kleines grünes Dreieck
left(90)

def dreieck(laenge):
    begin_fill()
    pensize(5)
    pencolor("red")
    forward(laenge)
    right(120)
    forward(laenge)
    right(120)
    forward(laenge)
    right(120)
    end_fill()
    
fillcolor("lime")
dreieck(laenge)

# zweites  mitleres blaues Dreieck
right(120)
fillcolor("cyan")
dreieck(laenge*2)



# drittes grosses gelbes Dreieck
right(120)
fillcolor("yellow")
dreieck(laenge*4)


