# Autor: Ursula Bütschli
# Datum: 27.02.2022
# ad hoc Übung (2.4a)
# Schreiben Sie eine Funktion, die das Volumen eines
# Zylinders berechnet:


from math import pi

def volumenzylinder(radius, hoehe):
    return ((radius*radius)*pi)*hoehe

volumen=volumenzylinder(2,2)
print("Das Volumen beträgt: "+str(volumen),"m3")
