# ad hoc Übung 1.2.a
# Zeichnen eines Quadrates
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *

forward(50)
left(90)
forward(50)
left(90)
forward(50)
left(90)
forward(50)