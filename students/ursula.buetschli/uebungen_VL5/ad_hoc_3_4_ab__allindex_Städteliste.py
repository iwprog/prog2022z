# a. Schreiben Sie eine Funktion allIndex(sequenz, element), die als
# Parameter eine Sequenz und ein Element bekommt. Zurückgegeben
# werden soll eine Liste mit allen Index-Positionen, an denen "element"
# vorkommt.
# Beispiel
# >>> allIndex(['H', 'a', 'l', 'l', 'o', 0, 0, 7], "l")
# [2, 3]
# >>> allIndex("Chur", "l")
# []
sequenz = input("Bitte geben Sie eine Sequenz ein.")
element = input("Bitte geben sie das gesuchte Element ein.")

list(sequenz)

buchstabe = element
liste = []

def allIndex(sequenz, element):
    for position, element in enumerate(sequenz):
        if element == buchstabe:
            liste.append(position)
    return liste

position = allIndex(sequenz, element)


print(list(sequenz), buchstabe, position)

# b. Gegeben sei die folgende mehrdimensionale Liste:
# [["Zürich", 370000], ["Genf", 190000],
# ["Basel", 170000], ["Bern", 130000]]
# Schreiben Sie ein Programm, dass die Liste wie folgt aufbereitet
# ausgibt:
# Zürich hat 370000 Einwohner
# Genf hat 190000 Einwohner

liste = [["Zürich", 370000], ["Genf", 190000], ["Basel", 170000], ["Bern", 130000]]

for ort, einwohner in enumerate(liste):
    print(liste[0][0],  "hat", liste[0][1], "Einwohner")  
    