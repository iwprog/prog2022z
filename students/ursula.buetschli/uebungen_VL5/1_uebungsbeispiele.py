# 1. Schreiben Sie eine Funktion, welche eine beliebige Liste entgegennimmt
# und eine neue Liste, welche nur noch Eintr¨age mit einer L¨ange von ≥4
# enth¨alt zur¨uckgibt.
# l_input = [’der’, ’junge’, ’ist’, ’sehr’, ’freundlich’]
# >>> filter_list(l_input)
# [’junge’, ’sehr’, ’freundlich’]

liste_1 = ["der", "junge", "ist", "sehr", "freundlich"]
liste_2 =[]

def filter_list(liste_1):
    for element in liste_1:
        if len(element) >= 4:
            liste_2.append(element)
    return liste_2

print(filter_list(liste_1))