# a. Geben Sie die Jahreszeiten aus der Liste jahreszeiten alphabetisch sortiert aus.

list = ["Winter", "Frühling", "Sommer", "Herbst"]
list.sort()
print(list)

sorted(list)
list=sorted(list)


# b. Geben Sie nur die ersten drei Elemente der Liste aus

list = ["Winter", "Frühling", "Sommer", "Herbst"]
print(list[:3])

# c. Geben Sie alle Jahreszeiten und zusätzlich die letzten drei Buchstaben der
# Jahreszeit aus .
# Frühling - ng
# Sommer - er
# Herbst - st
# Winter - er
list = ["Winter", "Frühling", "Sommer", "Herbst"]
for element in list:
    print(element, "-", element[-3:])
    
# d. Geben Sie nur jene Jahreszeiten aus, die mit den Buchstaben er enden.

list = ["Winter", "Frühling", "Sommer", "Herbst"]
for element in list:
    if element[-2:] == "er":
        print(element)
        
        