# ad hoc Übung 1.4
# Draw a circle
# Autor: Ursula Bütschli
# Datum: 18.02.2022

from turtle import *
#  ganzer Kreis
# steps = Anzahl Schritte, je weniger steps desto eckiger wird der Kreis

circle(radius=100, extent=None, steps=None)

# halber Kreis zunehmender Mond
penup()
forward(200)
pendown()
circle(radius=100, extent=200, steps=None)

# halber Kreis abnehmender Mond

penup()
left(180)
forward(200)
right(90)
forward(100)
right(90)
pendown()
circle(radius=100, extent=200, steps=None)

# Oktogramm
penup()
right(180)
forward(200)
pendown()
circle(radius=100, extent=None, steps=8)