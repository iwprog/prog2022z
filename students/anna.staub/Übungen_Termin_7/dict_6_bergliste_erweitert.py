# dicts 6
# berge erweitert
# Termin 7
# Anna Staub

# Erweitern Sie dict 5 berge:
# Dritte Eingabe: Gebirge
# Anschliessende Ausgabe nach Wunsch des Anwenders: nach Name, Höhe oder Gebirge, auf- oder absteigend


def sortiert_nach_name():
    return sorted(bergliste)

def sortiert_nach_hoehe():
    sortierliste = []
    for name, gebirge, hoehe in bergliste:
        sortierliste.append([hoehe, name, gebirge])
    sortierliste = sorted(sortierliste)
    print("***",sortierliste,"***")
    ergebnis = []
    for hoehe, name, gebirge in sortierliste:
        ergebnis.append([name, gebirge, hoehe])
    print("***",ergebnis,"***")
    return ergebnis

def sortiert_nach_gebirge():
    sortierliste = []
    for name, gebirge, hoehe in bergliste:
        sortierliste.append([gebirge, hoehe, name])
    sortierliste = sorted(sortierliste)
    ergebnis = []
    for gebirge, hoehe, name in sortierliste:
        ergebnis.append([name, gebirge, hoehe])
    return ergebnis

def menu():
    sortierung = input("Sortiere nach (1) Name, (2) Höhe oder (3) Gebirge?: ")
    if sortierung == "1":
        auf_ab = input("Sortiere (1) aufsteigend oder (2) absteigend nach Name?: ")
        if auf_ab == "1":
            final_list=sortiert_nach_name()
            ausgabe(final_list)
        elif auf_ab == "2":
            final_list=sortiert_nach_name()
            final_list.reverse()
            ausgabe(final_list)
            
        
    elif sortierung == "2":
        auf_ab = input("Sortiere (1) aufsteigend oder (2) absteigend nach Name?: ")
        if auf_ab == "1":
            final_list=sortiert_nach_hoehe()
            ausgabe(final_list)
        elif auf_ab == "2":
            final_list=sortiert_nach_hoehe()
            final_list.reverse()
            ausgabe(final_list)
        
    elif sortierung == "3":
        auf_ab = input("Sortiere (1) aufsteigend oder (2) absteigend nach Name?: ")
        if auf_ab == "1":
            final_list=sortiert_nach_gebirge()
            ausgabe(final_list)
        elif auf_ab == "2":
            final_list=sortiert_nach_gebirge()
            final_list.reverse()
            ausgabe(final_list)

def ausgabe(final_list):
    for name, gebirge, hoehe in final_list:
        print(name, "ist", hoehe, "m hoch und gehört zum", gebirge, "Gebirge.")
    

bergliste = [["Berg 1", "G1", 900],["Berg 2", "G1", 400], ["Berg 3", "G2", 600]] 
menu()