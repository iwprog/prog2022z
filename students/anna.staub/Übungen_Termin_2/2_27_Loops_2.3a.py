# Schreiben Sie eine Funktion, welche als Parameter einen Namen entgegen-nimmt und anschliessend den Text Guten Morgen Name! ausgibt.def guten_morgen():
def guten_morgen(name):
    print('Guten Morgen', name + '!')

def gruss(name):
    gruss = guten_morgen(name)
    return gruss

name = input('Wie ist ihr Name? ')

gruss(name)