from turtle import *

def dreieck(laenge, farbe):
    fillcolor(farbe)
    pencolor('red')
    pensize(5)
    begin_fill()
    fd(laenge)
    lt(120)
    fd(laenge)
    lt(120)
    fd(laenge)
    end_fill()

rt(90)
dreieck(numinput('Blaues Dreieck','Bitte geben Sie die Seitenlänge an'), 'cyan')
dreieck(numinput('Gelbes Dreieck','Bitte geben Sie die Seitenlänge an'), 'yellow')
dreieck(numinput('Grünes Dreieck', 'Bitte geben Sie die Seitenlänge an'), 'lime')