#27.2.2022
#Anna Staub
#AdHoc Übung 2.4a (Foliensatz 3)
from math import pi

def zylindervolumen(radius, height):
    volume = pi*(radius*radius)*height
    return(volume)
#Um selbst Radius und Höhe einzugeben wären noch die folgenden Zeilen nötig:
#
#radius = int(input('Bitte geben Sie den Radius an'))
#height = int(input('Bitte geben Sie die Zylinderhöhe an'))
#dafür wäre es dann:
#print('Das Volumen beträgt', zylindervolumen(radius, height))
print('Das Volumen beträgt', zylindervolumen(2, 2))