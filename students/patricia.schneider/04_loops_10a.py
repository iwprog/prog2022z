# übungsblatt funktionen und kontrollstrukturen
# aufgabe 10a)

def programm1():
    alter = input("Wie alt sind Sie? ")
    alter = int(alter)
    entfernung = input("Wie weit fahren Sie? ")
    entfernung = int(entfernung)
    def ticket_preis(alter, entfernung):
        if alter < 6:
            preis = 0
        elif alter < 16:
            preis = (2 + int(entfernung)*0.25)/2
        else:
            preis = (2 + int(entfernung)*0.25)
        return preis
    print("Ihr Ticketpreis beträgt: ", ticket_preis(alter, entfernung), "CHF")

def programm2():
    alter = input("Wie alt sind Sie? ")
    alter = int(alter)
    def max_herzfrequenz(alter):
        max_herzfrequenz = 220 - alter
        return max_herzfrequenz
    print("Ihre maximale Herzfrequenz beträgt:", max_herzfrequenz(alter))



while True:
    print("")
    print("Programmübersicht:")
    print(" 1 ... Preis für Fahrkarte berechnen")
    print(" 2 ... Maximale Herzfrequenz berechnen")
    print("")
    print(" 0 ... Programm beenden")
    print("")
    eingabe = input("Bitte wählen Sie eine Programmoption: ")
    
    if eingabe == "1":
        print("Gewählte Option: ", eingabe)
        programm1()
    elif eingabe == "2":
        print("Gewählte Option: ", eingabe)
        programm2()
    elif eingabe == "0":
        break
    else:
        print("Ungültige Option!")
          

