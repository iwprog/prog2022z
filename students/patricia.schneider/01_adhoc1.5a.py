#turtle example

#adhoc übung 1.5a)

from turtle import *

laenge = numinput("Dreieck", "Bitte geben Sie die Länge des Dreiecks an")

pencolor("red")
pensize(4)
fillcolor("yellow")
begin_fill()

left(150)
forward(laenge)
left(120)
forward(laenge)
left (120)
forward(laenge)
right(120)

end_fill()

#laenge anpassen 1
laenge = laenge * 0.5

pencolor("red")
pensize(4)
fillcolor("cyan")
begin_fill()

forward(laenge)
left(120)
forward(laenge)
left (120)
forward(laenge)
right(120)

end_fill()

#laenge anpassen 2
laenge = laenge * 0.5

pencolor("red")
pensize(4)
fillcolor("lime")
begin_fill()

forward(laenge)
left(120)
forward(laenge)
left (120)
forward(laenge)

end_fill()