# ad hoc übung 2.10 b)

from turtle import *
hideturtle()
speed(10)

def dreieck(laenge):
    for seite in range(3):
        forward(laenge)
        left(120)
    penup()
    forward(laenge+50)
    pendown()

anzahl = input("Wieviele Dreiecke soll ich zeichnen? ")
anzahl = int(anzahl)

for i in range (anzahl):
    dreieck(100)