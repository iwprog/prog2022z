#turtle example

#übung adhoc 1.2 e)

from turtle import *

pencolor("red")
pensize(5)

forward(100)
left(120)
forward(100)
left(120)
forward(100)

pencolor("blue")
pensize(5)

right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

pencolor("lime")
pensize(5)

right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

