# ad hoc übung 2.8)

def zahlenreihe(zahl1,zahl2,zahl3,zahl4,zahl5,zahl6,zahl7):
    i = 0
    summe = 0
 
    for zahl in zahl1,zahl2,zahl3,zahl4,zahl5,zahl6,zahl7:
        summe = summe + zahl
        i = i + 1
        durchschnitt = summe/i
        durchschnitt = round(durchschnitt,2)
        maxi = (max(zahl1,zahl2,zahl3,zahl4,zahl5,zahl6,zahl7))
        mini = (min(zahl1,zahl2,zahl3,zahl4,zahl5,zahl6,zahl7))
            
    print("Die kleinste Zahl in der Reihe ist:", mini)
    print("Die grösste Zahl in der Reihe ist:", maxi)
    print("Die Summe ist:", summe)
    print("Der Durchschnitt der Zahlenreihe ist:", durchschnitt)

zahlenreihe(12,23,5,56,77,18,9)

