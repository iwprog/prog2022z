# übungsblatt funktionen und kontrollstrukturen
# aufgabe 6)

from turtle import *

def zeichne_rahmen(aussen, innen, rahmenfarbe, fuellfarbe):
    pencolor(rahmenfarbe)
    pensize(5)
    fillcolor(fuellfarbe)
    
    begin_fill()
    for seite in range(4):
        forward(aussen)
        left(90)
    end_fill()
    
    penup()
    
    forward(innen)
    left(90)
    forward(innen)
    
    pendown()
    
    fillcolor("white")
    begin_fill()
    for seite in range(4):
        forward(aussen-innen*2)
        right(90)
    end_fill()

zeichne_rahmen(250,50,"green","hotpink")

    