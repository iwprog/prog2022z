# leistungsaufgabe 2 a)

i = 1
summe = 0
preis = 0

while preis != "x":
    summe = summe + float(preis)
    preis = input("Geben Sie den " + str(i) + ". Preis ein: ")
    i = i + 1

if summe < 100:
    summe = str(summe)
    rabatt = "Kein Rabatt"
    print(rabatt + ", Gesamtpreis = " + summe + " CHF")
    
elif summe >= 1000:
    summe = str(round(summe * 0.9,2))
    rabatt = "10% Rabatt"
    print(rabatt + ", Gesamtpreis = " + summe + " CHF")
    
else:
    summe = str(round(summe * 0.95,2))
    rabatt = "5% Rabatt"
    print(rabatt + ", Gesamtpreis = " + summe + " CHF")
    