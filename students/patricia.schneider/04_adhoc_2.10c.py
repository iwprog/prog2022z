# ad hoc 2.10 c)

from turtle import *
hideturtle()
speed(10)

def dreieck(laenge):
    for seite in range(3):
        forward(laenge)
        left(120)
    penup()
    forward(laenge+50)
    pendown()

zeilen = input("Wieviele Dreiecke soll ich pro Reihe zeichnen? ")
zeilen = int(zeilen)
reihen = input("In wievielen Reihen soll ich Dreiecke zeichnen? ")
reihen = int(reihen)

for i in range (reihen):
    for i in range (zeilen):
        dreieck(100)
    penup()
    right(90)
    forward(150)
    left(90)
    back(150*zeilen)
    pendown()
    
        
