# übungsblatt grundlagen kontrollstrukturen
# aufgabe 2.8 d)

i = 10

while i >= 1:
    print(i)
    i = i - 1
print("Start")


# übungsblatt grundlagen kontrollstrukturen
# aufgabe 2.8 f)

summe = 0

while summe <= 10:
    zahl = input("Bitte geben Sie eine Zahl ein: ")
    summe = summe + int(zahl)
    
print("Summe: ", summe)


# übungsblatt grundlagen kontrollstrukturen
# aufgabe 2.8 g)

summe = 0
zahl = input("Bitte geben Sie eine Zahl ein: ")

while zahl != "-1":
    summe = summe + int(zahl)
    zahl = input("Bitte geben Sie eine Zahl ein: ")
    
print("Summe: ", summe)