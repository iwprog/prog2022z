# listen übungsbeispiel 3) 

import random

# https://www.delftstack.com/de/howto/python/python-randomly-select-from-list/

l_ueberraschung=["Samiklaus", "Christbaum", "Weihnachtskugel",
               "Stiefel", "Schneeball"]


while True:
  tuer=input("Welche Kalendertür wollen Sie öffnen (oder x für Exit): ")
  if tuer == "x":
    break
  if tuer.isdigit():
    if int(tuer) >= 1 and int(tuer) <= 24:
      ueberraschung=random.choice(l_ueberraschung)
      print(ueberraschung)
    else:
      continue

