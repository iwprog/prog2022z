# übungsblatt grundlagen kontrollstrukturen
# aufgabe 2.5 b)

age = input("Wie alt bist du? ")
age = int(age)

def legal_status(age):
        if age < 0:
            return("ungeboren")
        elif age >= 0 and age <= 6:
            return("geschäftsunfähig")
        elif age > 6 and age <= 14:
            return("unmündig")
        elif age > 14 and age <= 17:
            return("mündig minderjährig")
        elif age >= 18:
            return("volljährig")
    
print("Mit", age, "ist man", legal_status(age) + ".")

