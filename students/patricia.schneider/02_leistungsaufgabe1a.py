# leistungsaufgabe 1a)

from turtle import *
hideturtle() #versteckt pfeil resp. turtle

def ch_flagge(laenge):
    
    # quadratische rote grundfläche
    
    begin_fill()
    fillcolor("red")
    pencolor("red")
    forward(laenge)
    right(90)
    forward(laenge)
    right(90)
    forward(laenge)
    right(90)
    forward(laenge)
    end_fill()
    
    penup() # bewegt den pfeil ohne zu zeichnen
    
    # positionierung des pfeiles im roten quadrat
    
    right(180)
    forward(laenge/2.5)
    left(90)
    forward(laenge/5)
    
    pendown() # jetzt zeichnen wir wieder
    
    # weisses kreuz
    
    begin_fill()
    fillcolor("white")
    pencolor("white")
    
    forward(laenge/5)
    left(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    left(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    left(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    left(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    right(90)
    forward(laenge/5)
    end_fill()

    
ch_flagge(200)


