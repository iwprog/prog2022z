# basic datatypes 1.2c) 

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

for element in jahreszeiten:
    print(element, "-", element[-3:])


# 1.2d)

for element in jahreszeiten:
    if element[-2:] == "er":
        print(element)