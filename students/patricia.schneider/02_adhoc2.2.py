#adhoc Übung 2.2)

from turtle import *

def dreieck(seitenlaenge, farbe):
    fillcolor(farbe)
    begin_fill()
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    end_fill()
    
pensize(5)
pencolor("red")

right(90)
dreieck(100,"cyan")
dreieck(200,"yellow")
dreieck(50,"lime")

