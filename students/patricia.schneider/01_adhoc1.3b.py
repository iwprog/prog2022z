#turtle example

# übung adhoc 1.3 b)

from turtle import *

# option: shape("turtle")

pencolor("red")
pensize(5)
fillcolor("cyan")
begin_fill()

left(36)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)

end_fill()

pencolor("red")
pensize(5)
fillcolor("yellow")
begin_fill()

right(160)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)

end_fill()

pencolor("red")
pensize(5)
fillcolor("hotpink")
begin_fill()

right(160)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)

end_fill()

pencolor("red")
pensize(5)
fillcolor("blue")
begin_fill()

right(160)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)

end_fill()

pencolor("red")
pensize(5)
fillcolor("lime")
begin_fill()

right(160)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)

end_fill()