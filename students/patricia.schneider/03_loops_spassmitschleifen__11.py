# übungsblatt funktionen und kontrollstrukturen
# aufgabe 11e)

def schleife_e():
    for zeile in range(7):
        if zeile == 0 or zeile == 6:
            print("# # # # # # #")
        else:
            print("#           #")

schleife_e()



# übungsblatt funktionen und kontrollstrukturen
# aufgabe 11g)


for zeile in "1111111","0000010","0000100","0001000","0010000","0100000","1111111":
    for zeichen in zeile:
        if zeichen == "1":
            print("# ",end="",sep="")
        elif zeichen == "0":
            print("  ",end="",sep="")
    print("")